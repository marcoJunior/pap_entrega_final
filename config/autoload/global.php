<?php

//ini_set("error_reporting", E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_USER_DEPRECATED);

use Zend\Json\Json;

Json::$useBuiltinEncoderDecoder = true;

return array(
    'dbMysql' => [
        'driver' => 'Pdo',
        'dsn' => 'mysql:dbname=csgestor_admin;host=127.0.0.1;port=3306',
        'username' => 'csgestor_max',
        'password' => 'amdsdl7586',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
        ),
    ],
    'service_manager' => array(
        'aliases' => [
            'adapter' => 'dbMysql',
        ],
        'factories' => array(
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'dbMysql' => function($sm) {
                $config = $sm->get('config');
                $config = $config['dbMysql'];
                $dbAdapter = new Zend\Db\Adapter\Adapter($config);
                return $dbAdapter;
            },
        ),
    ),
);
