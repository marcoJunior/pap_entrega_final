<?php

return [
    'AssetManager',
    'Application',
    'Autenticacao',
    'Home',
    'Cadastros',
    'Base',
    //Novos modulos de cadastros
    'Usuarios',
    'Produtos',
    'Cliente',
    'Planos',
    'Recurso',
    'Sistemas',
    'Documentacoes',
    'TempoExperiencia',
    'ConexaoClienteMaxScalla',
];
