<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Logs\Entity\Log as LogEntity;
use Logs\Mapper\Log as LogMapper;
use Logs\Mapper\Hydrator\Log as LogHydrator;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\Mvc\Router\Http\TreeRouteStack;
use Zend\Mvc\Router\PriorityList;
use Zend\Mvc\Router\Http\Segment;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();

        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

//        $service = $sm->get(\Application\Service\Events::class);
        $eventManager->attach(MvcEvent::EVENT_ROUTE, [$this, 'validaRota'], 200);
//        $eventManager->attach(MvcEvent::EVENT_RENDER, [$service, 'validaRota'], 201);
    }

    public function validaRota(MvcEvent $e)
    {
        $sm = $e->getApplication()->getServiceManager();
        $events = $sm->get(\Application\Service\Events::class);

        $acl = $events->getAcl();

        $rota = $this->getRotaAtual($sm);
        $tipoRequisicao = $this->getTipoRequisicao();

        if ($this->validaExisteRota($sm, $rota) && !$acl->isAllowed('tipo-usuario', $rota, $tipoRequisicao)) {

            $headers = $e->getResponse()->getHeaders();
            $headers->addHeaderLine("Content-type: application/octet-stream");

            $json = new \Zend\View\Model\JsonModel(['messagem' => 'Permissao negada!']);

            $e->getResponse()->setStatusCode(403);
            $e->getResponse()->setContent($json->serialize());

            if ($tipoRequisicao === "get") {
                $e->getResponse()->getHeaders()->addHeaderLine('Location', '/dashboard');
            }

            if ($tipoRequisicao !== "get") {
                return $e->getResponse();
            }
        }
    }

    public function validaExisteRota($sm, $rota)
    {
        $rotas = $sm->get('RotasPersonalizadas');
        return $rota !== "/dashboard" && in_array($rota, $rotas->getTodasRotas());
    }

    public function getRotaAtual($sm)
    {
        $router = $sm->get('router');
        $request = $sm->get('request');
        $matchedRoute = $router->match($request);
        return $matchedRoute !== null ? "/{$matchedRoute->getMatchedRouteName()}" : "/";
    }

    public function getTipoRequisicao()
    {
        return strtolower(filter_input(INPUT_SERVER, 'REQUEST_METHOD'));
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Logs' => __DIR__ . '/src/Logs',
                    'APISql' => __DIR__ . '/src/APISql',
                    'APIGrid' => __DIR__ . '/src/APIGridGitLab',
                    'APIFiltro' => __DIR__ . '/src/APIFiltro',
                    'APIHelper' => __DIR__ . '/src/APIHelper',
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'RotasPersonalizadas' => function ($sm) {
                    return new View\Helper\Rotas($sm);
                },
                \Application\Service\Events::class => function ($sm) {
                    return new \Application\Service\Events($sm);
                },
                //APIFiltro
                \APIFiltro\Service\Init::class => function ($sm) {
                    $service = new \APIFiltro\Service\Init();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\Loja::class => function ($sm) {
                    $service = new \APIFiltro\Service\Loja();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\IntervaloData::class => function ($sm) {
                    $service = new \APIFiltro\Service\IntervaloData();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\Data::class => function ($sm) {
                    $service = new \APIFiltro\Service\Data();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\Regioes::class => function ($sm) {
                    $service = new \APIFiltro\Service\Regioes();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\Representante::class => function ($sm) {
                    $service = new \APIFiltro\Service\Representante();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\TipoPessoa::class => function ($sm) {
                    $service = new \APIFiltro\Service\TipoPessoa();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\RamoAtividade::class => function ($sm) {
                    $service = new \APIFiltro\Service\RamoAtividade();
                    return $service->setServiceManager($sm);
                },
                \APIFiltro\Service\Pesquisa::class => function ($sm) {
                    $service = new \APIFiltro\Service\Pesquisa();
                    return $service->setServiceManager($sm);
                },
                //APIHelper
                \APIHelper\Service\APIHelper::class => function ($sm) {
                    $service = new \APIHelper\Service\APIHelper();
                    return $service->setServiceManager($sm);
                },
                //APISql
                \APISql\Service\ConvertObject::class => function ($sm) {
                    $service = new \APISql\Service\ConvertObject();
                    return $service->setServiceManager($sm);
                },
                \APISql\Service\HelperSql::class => function ($sm) {
                    $service = new \APISql\Service\HelperSql();
                    return $service->setServiceManager($sm);
                },
                \APISql\Service\Transaction::class => function ($sm) {
                    $service = new \APISql\Service\Transaction();
                    return $service->setServiceManager($sm);
                },
                //Logs
                \Logs\Service\Log::class => function ($sm) {
                    $service = new \Logs\Service\Log();
                    return $service->setServiceManager($sm);
                },
                \Application\Service\DropZone::class => function ($sm) {
                    $service = new \Application\Service\DropZone();
                    return $service->setServiceManager($sm);
                },
                \Application\Service\Count::class => function ($sm) {
                    $service = new \Application\Service\Count();
                    return $service->setServiceManager($sm);
                },
                \Application\Service\Acl::class => function ($sm) {
                    $service = new \Application\Service\Acl();
                    return $service->setServiceManager($sm);
                },
                ////////////////////////
                'Logs\Mapper\Log' => function ($sm) {
                    $mapper = new LogMapper();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new LogEntity())
                        ->setHydrator(new LogHydrator());
                    return $mapper;
                },
                'Application\Mapper\Count' => function ($sm) {
                    $mapper = new Mapper\Count();
                    $mapper->setEntityPrototype(new Entity\Count())
                        ->setHydrator(new ClassMethods());
                    return $mapper;
                },
            ),
        );
    }

    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'acl' => function () {
                    return new View\Helper\Acl();
                },
                'rotas' => function () {
                    return new View\Helper\Rotas();
                },
                'tipoUsuarios' => function () {
                    return new View\Helper\TipoUsuario();
                },
            ],
        ];
    }

}
