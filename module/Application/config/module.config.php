<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

return [
    'router' => [
        'routes' => [
            '/lojas' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/loja[/:controller]',
                    'constraints' => [
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'action' => 'aplicativos'
                    ]
                ],
            ],
//            'home' => [
//                'type' => 'Zend\Mvc\Router\Http\Literal',
//                'options' => [
//                    'route'    => '/',
//                    'defaults' => [
//                        'controller' => 'Application\Controller\Index',
//                        'action'     => 'index',
//                    ],
//                ],
//            ],
            'logs' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/logs[/:controller[/:action]]',
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ],
                    'defaults' => [
                        'action' => 'layout',
                    ]
                ],
            ],
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/application',
                    'defaults' => [
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                    ],
                ],
            ],
            'rotas' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/rotas',
                    'defaults' => [
                        'controller' => 'Application\Controller\Index',
                        'action' => 'rotas',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '[/:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                    ],
                ],
            ],
            'dropzone' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/dropzone',
                    'defaults' => [
                        'controller' => 'Index',
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ],
        'factories' => [
            'translator' => 'Zend\Mvc\Service\TranslatorServiceFactory',
        ],
    ],
    'translator' => [
        'locale' => 'pt_BR',
        'translation_file_patterns' => [
            [
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            'Application\Controller\Index' => function ($sl) {
                $controller = new Controller\IndexController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'Acessos' => function ($sl) {
                $controller = new \Logs\Controller\LogController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'Upload' => function ($sl) {
                $controller = new \Application\Controller\IndexController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'apps' => 'Application\Controller\IndexController'
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'application' => __DIR__ . '/../view/layout.phtml',
            'top-navigation2' => __DIR__ . '/../view/layout/top-navigation.phtml',
            'sidebar2' => __DIR__ . '/../view/layout/sidebar.phtml',
            'modal' => __DIR__ . '/../view/layout/modal.phtml',
            'footer' => __DIR__ . '/../view/layout/footer.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            /////////EMPRESAS
            'logs/acessos' => __DIR__ . '/../view/layout/logs/acessos/index.phtml',
            'logs/acessos/grid' => __DIR__ . '/../view/layout/logs/acessos/grid.phtml',
            'logs/acessos/modal' => __DIR__ . '/../view/layout/logs/acessos/modal.phtml',
            'logs/acessos/formulario' => __DIR__ . '/../view/layout/logs/acessos/formulario.phtml',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'asset_manager' => [
        'resolver_configs' => [
            'paths' => [
                'APIGridGitLab' => __DIR__ . '/../src',
            ],
        ],
    ],
    // Placeholder for console routes
    'console' => [
        'router' => [
            'routes' => [
            ],
        ],
    ],
];
