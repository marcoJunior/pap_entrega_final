<?php

namespace APIFiltro\Entity;

use APIFiltro\Entity\Opcoes as OpcoesEntity;

abstract class AbstractFiltro
{

    protected $opcoes;

    function getOpcoes()
    {
        return $this->opcoes;
    }

    function setOpcoes($opcoes)
    {
        $opcoesEntity = new OpcoesEntity;
        $opcoesEntity->exchangeArray((array) $opcoes);
        $this->opcoes = $opcoesEntity;
        return $this;
    }

}
