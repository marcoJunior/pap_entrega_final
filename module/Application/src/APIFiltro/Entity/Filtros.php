<?php

namespace APIFiltro\Entity;

use APIFiltro\Entity\Where;
use APIFiltro\Entity\Status;
use APIFiltro\Entity\IntervaloData;

class Filtros extends Where
{

    protected $codigo;
    protected $data;
    protected $pesquisa = '';
    protected $intervaloData;
    protected $loja;
    protected $produto;
    protected $grupoProdutos;
    protected $fabricantes;
    protected $ramoAtividade;
    protected $regioes;
    protected $representantes;
    protected $tipoPessoa;
    protected $status;
    protected $cartao;
    protected $portadores;
    protected $clientes;
    protected $funcionarios;
    protected $formaBusca;
    protected $colunaBusca;
    protected $serie;
    protected $modulo;
    protected $plano;

    static function get()
    {
        return new Filtros();
    }

    function __construct($array = false)
    {
        if ($array && is_array($array)) {
            $this->exchangeArray($array);
        }
    }

    /**
     * @return mixed
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * @param mixed $codigo
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function getProduto()
    {
        return $this->produto;
    }

    function setProduto($produto)
    {
        $this->produto = $produto;
    }

    function getGrupoProdutos()
    {
        return $this->grupoProdutos;
    }

    function setGrupoProdutos($grupoProdutos)
    {
        $this->grupoProdutos = $grupoProdutos;
    }

    function getPesquisa()
    {
        return $this->pesquisa;
    }

    function setPesquisa($pesquisa)
    {
        $this->pesquisa = $pesquisa;
    }

    /**
     * @return \APIFiltro\Entity\Status
     */
    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status; //new Status($status);
        return $this;
    }

    function setIntervaloData($intervaloData)
    {
        $this->intervaloData = new IntervaloData($intervaloData);
        return $this;
    }

    /**
     * @return \APIFiltro\Entity\IntervaloData
     */
    function getIntervaloData()
    {
        return $this->intervaloData;
    }

    /* Metodos Padrão */

    function getFabricantes()
    {
        return $this->fabricantes;
    }

    function setFabricantes($fabricantes)
    {
        $this->fabricantes = $fabricantes;
        return $this;
    }

    function getCartao()
    {
        return $this->cartao;
    }

    function setCartao($cartao)
    {
        $this->cartao = $cartao;
        return $this;
    }

    function getData()
    {
        return $this->data;
    }

    function getLoja()
    {
        return $this->loja;
    }

    function getRamoAtividade()
    {
        return $this->ramoAtividade;
    }

    function getRegioes()
    {
        return $this->regioes;
    }

    function getRepresentantes()
    {
        return $this->representantes;
    }

    function getTipoPessoa()
    {
        return $this->tipoPessoa;
    }

    function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    function setLoja($loja)
    {
        $this->loja = $loja;
        return $this;
    }

    function setRamoAtividade($ramoAtividade)
    {
        $this->ramoAtividade = $ramoAtividade;
        return $this;
    }

    function setRegioes($regioes)
    {
        $this->regioes = $regioes;
        return $this;
    }

    function setRepresentantes($representantes)
    {
        $this->representantes = $representantes;
        return $this;
    }

    function setTipoPessoa($tipoPessoa)
    {
        $this->tipoPessoa = $tipoPessoa;
        return $this;
    }

    function getPortadores()
    {
        return $this->portadores;
    }

    function setPortadores($portadores)
    {
        $this->portadores = $portadores;
        return $this;
    }

    function getClientes()
    {
        return $this->clientes;
    }

    function setClientes($clientes)
    {
        $this->clientes = $clientes;
        return $this;
    }

    function getFuncionarios()
    {
        return $this->funcionarios;
    }

    function setFuncionarios($funcionarios)
    {
        $this->funcionarios = $funcionarios;
    }

    function getFormaBusca()
    {
        return $this->formaBusca;
    }

    function getColunaBusca()
    {
        return $this->colunaBusca;
    }

    function setFormaBusca($formaBusca)
    {
        $this->formaBusca = $formaBusca;
    }

    function setColunaBusca($colunaBusca)
    {
        $this->colunaBusca = $colunaBusca;
    }

    /**
     * @return mixed
     */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
     * @param mixed serie
     */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
     * @return mixed
     */
    public function getModulo()
    {
        return $this->modulo;
    }

    /**
     * @param mixed modulo
     */
    public function setModulo($modulo)
    {
        $this->modulo = $modulo;
    }

    function getPlano()
    {
        return $this->plano;
    }

    function setPlano($plano)
    {
        $this->plano = $plano;
    }

}
