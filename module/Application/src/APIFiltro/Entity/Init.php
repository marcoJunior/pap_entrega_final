<?php

namespace APIFiltro\Entity;

class Init
{

    protected $libs = '';
    protected $html = '';
    protected $init = '';

    function getLibs()
    {
        return $this->libs;
    }

    function getHtml()
    {
        return $this->html;
    }

    function getInit()
    {
        return $this->init;
    }

    function setLibs($libs)
    {
        $this->libs = $libs;
        return $this;
    }

    function setHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    function setInit($init)
    {
        $this->init = $init;
        return $this;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

    public function concatArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {

                $valor = $this->{'get' . ucfirst($atributo)}() . $valor;

                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }

        return $this;
    }

    public function montarHtml()
    {
        //$mobile = file_get_contents(__DIR__ . '/../../../view/apifiltro/mobile.phtml');
        //$this->html = $mobile . '<span class="Filtros pull-right">' . $this->html . '</span>';
    }

    public function setClass()
    {
        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

}
