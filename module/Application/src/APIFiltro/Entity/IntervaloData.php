<?php

namespace APIFiltro\Entity;

class IntervaloData extends \APIHelper\Entity\AbstractEntity
{

    protected $inicial;
    protected $final;

    function __construct($array = null)
    {
        if (is_string($array)) {
            $array = json_decode($array, true);
        }
        
        if (isset($array['inicial'])) {
            $this->inicial = $array['inicial'] ? : '';
            $this->final = $array['final'] ? : '';
        }
    }

    function isCompleto()
    {
        return $this->getInicial() && $this->getFinal();
    }

    function getInicial()
    {
        return $this->inicial;
    }

    function getFinal()
    {
        return $this->final;
    }

    function setInicial($inicial)
    {
        $this->inicial = $inicial;
        return $this;
    }

    function setFinal($final)
    {
        $this->final = $final;
        return $this;
    }

}
