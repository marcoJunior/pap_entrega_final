<?php

namespace APIFiltro\Entity;

class Opcoes extends \APIHelper\Entity\AbstractEntity
{

    protected $title = '';
    protected $multiplo = true;
    protected $status = [];
    protected $valor = '';
    protected $tipo = '';
    protected $intervaloData = '';

    function __construct()
    {
        $this->intervaloData = new IntervaloData();
    }

    static function get()
    {
        return new Opcoes;
    }

    function getStatus()
    {
        return $this->status;
    }

    function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    function getMultiplo()
    {
        return $this->multiplo;
    }

    function setMultiplo($multiplo)
    {
        $this->multiplo = $multiplo;
        return $this;
    }

    function getTitle()
    {
        return $this->title;
    }

    function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    function getValor()
    {
        return $this->valor;
    }

    function setValor($valor)
    {
        $this->valor = $valor;
        return $this;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return IntervaloData
     */
    function getIntervaloData()
    {
        return $this->intervaloData;
    }

    function setIntervaloData($intervaloData)
    {
        $this->intervaloData = new IntervaloData($intervaloData);
        return $this;
    }

}
