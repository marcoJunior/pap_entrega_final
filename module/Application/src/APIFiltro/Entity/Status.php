<?php

namespace APIFiltro\Entity;

class Status
{

    protected $null = true;
    protected $aberto = false;
    protected $compensado = false;
    protected $emaberto = false;
    protected $ematraso = false;
    protected $recebidas = false;
    protected $emitidas = false;
    protected $pagas = false;

    /**
     * Cheques
     */
    protected $adepositar = false;
    protected $depositado = false;
    protected $devolvido = false;

    /**
     * Conta corrente
     */
    protected $debito = false;
    protected $credito = false;

    /**
     * Getter de constantes
     */
    function getAdepositar()
    {
        return CHR(165) . " Dep";
    }

    function getDepositado()
    {
        return 'Depos';
    }

    function getDevolvido()
    {
        return 'Devol';
    }

    function getDebito()
    {
        return '-';
    }

    function getCredito()
    {
        return '+';
    }

    function __construct($string = '')
    {
        if (property_exists($this, strtolower($string))) {
            $this->setNull(0);
            $this->{'set' . ucfirst($string)}(true);
        }
    }

    /**
     * Validacoes
     */
    function isNull()
    {
        return !!$this->null;
    }

    function isAberto()
    {
        return !!$this->aberto;
    }

    function isCompensado()
    {
        return !!$this->compensado;
    }

    function isEmaberto()
    {
        return !!$this->emaberto;
    }

    function isEmatraso()
    {
        return !!$this->ematraso;
    }

    function isRecebidas()
    {
        return !!$this->recebidas;
    }

    function isEmitidas()
    {
        return !!$this->emitidas;
    }

    function isPagas()
    {
        return !!$this->pagas;
    }

    function isAdepositar()
    {
        return !!$this->adepositar;
    }

    function isDevolvido()
    {
        return !!$this->devolvido;
    }

    function isDepositado()
    {
        return !!$this->depositado;
    }

    function isDebito()
    {
        return !!$this->debito;
    }

    function isCredito()
    {
        return !!$this->credito;
    }

    /**
     * Setters
     */
    function setPagas($pagas)
    {
        $this->pagas = $pagas;
        return $this;
    }

    function setNull($null)
    {
        $this->null = (bool) $null;
        return $this;
    }

    function setEmaberto($emaberto)
    {
        $this->emaberto = (bool) $emaberto;
        return $this;
    }

    function setEmatraso($ematraso)
    {
        $this->ematraso = (bool) $ematraso;
        return $this;
    }

    function setRecebidas($recebidas)
    {
        $this->recebidas = (bool) $recebidas;
        return $this;
    }

    function setEmitidas($emitidas)
    {
        $this->emitidas = (bool) $emitidas;
        return $this;
    }

    function setAberto($aberto)
    {
        $this->aberto = (bool) $aberto;
        return $this;
    }

    function setCompensado($compensado)
    {
        $this->compensado = (bool) $compensado;
        return $this;
    }

    function setAdepositar($adepositar)
    {
        $this->adepositar = (bool) $adepositar;
        return $this;
    }

    function setDepositado($depositado)
    {
        $this->depositado = (bool) $depositado;
        return $this;
    }

    function setDevolvido($devolvido)
    {
        $this->devolvido = (bool) $devolvido;
        return $this;
    }

    function setDebito($debito)
    {
        $this->debito = (bool) $debito;
        return $this;
    }

    function setCredito($credito)
    {
        $this->credito = (bool) $credito;
        return $this;
    }

    function toArray()
    {
        return get_object_vars($this);
    }

    function getSelecionado()
    {
        foreach ($this->toArray() as $propriedade => $bool) {
            if ($bool && is_callable([$this, 'get' . ucfirst($propriedade)])) {
                return $this->{'get' . ucfirst($propriedade)}();
            }
        }
    }

}
