<?php

namespace APIFiltro\Entity;

use Zend\Db\Sql\Where as WhereSql;
use APIHelper\Entity\AbstractEntity;

class Where extends AbstractEntity
{

    /**
     * @var WhereSql
     */
    protected $where;

    /**
     * @return WhereSql
     */
    function getWhere()
    {
        return $this->where;
    }

    function setWhere(WhereSql $where)
    {
        $this->where = $where;
        return $this;
    }

    function whereIntervaloData($coluna)
    {
        $intervalo = $this->getIntervaloData();
        if ($intervalo instanceof \APIFiltro\Entity\IntervaloData) {
            $this->getWhere()->between($coluna, $intervalo->getInicial(), $intervalo->getFinal());
        }

        return $this;
    }

    function whereLoja($coluna)
    {
        $loja = $this->getLoja();

        if ($loja) {
            $this->getWhere()->in($coluna, (array) $loja);
        }

        return $this;
    }

    function whereRepresentante($coluna)
    {
        $representante = $this->getRepresentantes();

        if ($representante) {
            $this->getWhere()->in($coluna, (array) $representante);
        }

        return $this;
    }

    function whereStatus($coluna)
    {
        $valor = $this->getStatus()->getSelecionado();
        return $this->getWhere()->in($coluna, (array) $valor);
    }

    function where($filtro, $coluna)
    {
        if (property_exists($this, $filtro)) {
            $valor = $this->{'get' . ucfirst($filtro)}();

            if ($valor) {

                $this->getWhere()->in($coluna, (array) $valor);
            }
        }
        return $this;
    }

}
