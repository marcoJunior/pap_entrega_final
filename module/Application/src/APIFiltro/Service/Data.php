<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class Data extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{
    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $data = $this->getValores();
        $html = $this->criarHtml($data);
        $js = $this->getJavascript();

        return [
            'html' => $html,
            'init' => $js
        ];
    }

    /**
     * Os valores podem set:
     * recebidos por get (0)
     * recebidos pelas opções da controller (1)
     * o dia de hoje subtraido 3 meses (2)
     * @return array com entidades Loja
     */
    public function getValores()
    {
        $data = filter_input(INPUT_GET, 'data');

        if (!$data) {
            $data = $this->getOpcoes()->getValor();
        }

        if (!$data) {
            $data = date("d/m/Y", strtotime("-3 months"));
        }

        return $data;
    }

    /**
     * Monta o html
     * @param string $valores
     * @return array [html][javascript]
     */
    public function criarHtml($valores)
    {
        $html = $this->helper()->getFile(__DIR__ . "/../../../view/apifiltro/data.phtml", [
            'data' => $valores,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . "../../../../../../public/scripts/apifiltro/js/data.js");
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
