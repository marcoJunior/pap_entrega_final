<?php

namespace APIFiltro\Service;

/**
 * FiltroInterface
 *
 * Implement to inject Filtros into Service
 */
interface FiltroInterface
{

    function getOpcoes();

    function setOpcoes($opcoes);

    /**
     * @return string Html do filtro
     */
    public function getFiltro();

    /**
     * Busca valores para criar o filtro
     * @return array com entidades
     */
    public function getValores();

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet);

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript();
}
