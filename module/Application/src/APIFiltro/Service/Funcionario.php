<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class Funcionario extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $retorno = ['html' => '', 'init' => ''];

        $valores = $this->getValores();

        $retorno['html'] = $this->criarHtml($valores);
        $retorno['init'] = $this->getJavascript();

        return $retorno;
    }

    public function getValores()
    {
        $serviceFuncionario = $this->serviceFuncionarios();
        $valores = $serviceFuncionario->getFuncionarios();
        return $valores;
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet)
    {
        $multiplo = $this->getOpcoes()->getMultiplo() ? 'Multiplo' : '';
        $caminho = "/../../../view/apifiltro/funcionario$multiplo.phtml";
        $html = $this->helper()->getFile(__DIR__ . $caminho, [
            'funcionarios' => $resultSet,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * @return \Cadastro\Service\Funcionarios
     */
    public function serviceFuncionarios()
    {
        return $this->getServiceManager()->get('Cadastros\Service\Funcionarios');
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . '/../../../../../public/scripts/apifiltro/js/funcionario.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
