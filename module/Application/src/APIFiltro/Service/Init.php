<?php

/**
 * @author Janley Santos Soares <janley@maxscalla.com.br>
 */

namespace APIFiltro\Service;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Entity\Init as InitEntity;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class Init //implements ServiceManagerAwareInterface
{

    const FILTROS_ADMIN = ['loja', 'funcionario'];

    /**
     * Na controller:
     * chamar este metodo passando por array as chaves dos filtros que deseja
     * utilizar e o retorno do metodo deve ser atribuido
     * ao $this->layout()->setVariable('filtros')
     *
     * No html:
     * basta usar $this->filtros->getHtml() onde quer incluir o html
     * $this->filtros->getLibs() na footer
     * $this->filtros->getInit() no fim da pagina
     *
     * @param type $config
     * @return array key{nomeFiltro} value{html}
     */
    public function loadFiltros($config = null)
    {
        $filtros = new InitEntity;

        $tipoLogin = "admin";

        if ($config) {
            foreach ((array) $config as $configuracao) {
                if (!is_array($configuracao)) {
                    continue;
                }
                $filtro = $this->getFiltro($configuracao, $tipoLogin);
                $filtros->concatArray($filtro);
            }
            $filtros->montarHtml($filtro);
            $filtros->setLibs($this->getLib());
        }

        return $filtros;
    }

    /**
     * Retorna array
     * @param array $configuracoes
     * @param string $tipoLogin
     * @return array [html, init]
     */
    public function getFiltro($configuracoes, $tipoLogin)
    {
        if ($tipoLogin !== 'admin' && $tipoLogin !== 'funcionario' && $tipoLogin !== 'master' && in_array(strtolower(key($configuracoes)),
                self::FILTROS_ADMIN)) {
            return array();
        }

        $service = $this->getService($configuracoes);
        return $service->getFiltro();
    }

    /**
     * @param array $configuracoes
     * @return object Service
     */
    public function getService(array $configuracoes)
    {

        $get = 'APIFiltro\Service\\' . ucfirst(key($configuracoes));

        if (!$this->getServiceManager()->has($get)) {
            throw new \InvalidArgumentException("Nâo foi criada a service $get");
        }

        $service = $this->getServiceManager()->get($get);
        $service->setOpcoes($configuracoes[key($configuracoes)]);
        return $service;
    }

    public function getLib()
    {
        $view = new ViewModel();
        $view->setTemplate('apifiltro/libs');

        try {
            $phpRender = $this->getServiceManager()->get('ViewRenderer');
            $html = $phpRender->render($view);
        } catch (RuntimeException $exc) {
            $html = "";
        }

        return $html;
    }

    /**
     * @return \Login\Service\Login
     */
    public function serviceLogin()
    {
        return $this->getServiceManager()->get(\Login\Service\Login::class);
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
