<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;
use APIFiltro\Entity\Opcoes as OpcoesEntity;

class IntervaloData extends AbstractFiltro implements FiltroInterface //ServiceManagerAwareInterface,
{

    protected $opcoes;

    static function get()
    {
        return new IntervaloData();
    }

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $data = $this->getValores();
        $html = $this->criarHtml($data);
        $js = $this->getJavascript();

        return [
            'html' => $html,
            'init' => $js
        ];
    }

    /**
     * Busca as lojas cadastradas
     * @return array com entidades Loja
     */
    public function getValores()
    {
        $entity = new OpcoesEntity();
        $entity->getIntervaloData()->setFinal(false);
        $entity->getIntervaloData()->setInicial(false);
        $opcoes = $entity->getIntervaloData();

        if ($this->getOpcoes() !== null) {
            $opcoes = $this->getOpcoes()->getIntervaloData();
        }

        return [
            'inicial' => $opcoes->getInicial() ?: date("01/m/Y"),
            'final' => $opcoes->getFinal() ?: date('t/m/Y')
        ];
    }

    /**
     * Monta o html
     * @param string $valores
     * @return array [html][javascript]
     */
    public function criarHtml($valores)
    {
        $tipo = ucfirst($this->getOpcoes()->getTipo());
        $caminho = "/../../../view/apifiltro/intervaloData$tipo.phtml";
        $html = $this->helper()->getFile(__DIR__ . $caminho, [
            'valores' => $valores,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . '/../../../../../public/scripts/apifiltro/js/intervaloData.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
