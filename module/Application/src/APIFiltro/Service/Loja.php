<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class Loja extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $lojas = $this->getValores();
        $html = $this->criarHtml($lojas);
        $js = $this->getJavascript();

        return [
            'html' => $html,
            'init' => $js
        ];
    }

    /**
     * Busca as lojas cadastradas
     * @return array com entidades Loja
     */
    public function getValores()
    {
        $service = $this->serviceLoja();
        return $service->get();
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($lojas)
    {
        $html = $this->helper()->getFile(__DIR__ . '/../../../view/apifiltro/loja.phtml', [
            'lojas' => $lojas,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . '../../../../../../public/scripts/apifiltro/js/lojaSelected.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public $serviceLoja = NULL;

    /**
     * @return \Cadastro\Service\Loja
     */
    public function serviceLoja()
    {
        if ($this->serviceLoja === NULL) {
            $this->serviceLoja = $this->getServiceManager()->get('Cadastro\Service\Loja');
        }

        return $this->serviceLoja;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
