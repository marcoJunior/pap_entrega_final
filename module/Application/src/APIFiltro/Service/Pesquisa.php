<?php

namespace APIFiltro\Service;

use APIFiltro\Entity\AbstractFiltro;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class Pesquisa extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $retorno = ['html' => '', 'init' => ''];

        //$valores = $this->getValores();

        $retorno['html'] = $this->criarHtml(null);
        $retorno['init'] = $this->getJavascript();

        return $retorno;
    }

    public function getValores()
    {
        $servicePesquisa = $this->servicePesquisas();
        $valores = $servicePesquisa->getPesquisas();
        return $valores;
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet)
    {
        $caminho = "/../../../view/apifiltro/pesquisa.phtml";
        $html = $this->helper()->getFile(__DIR__ . $caminho, []);

        return $html;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . '/../../../../../public/scripts/apifiltro/js/pesquisa.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
