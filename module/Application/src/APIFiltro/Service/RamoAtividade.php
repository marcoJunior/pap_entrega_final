<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class RamoAtividade extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $lojas = $this->getValores();

        return [
            'html' => $this->criarHtml($lojas),
            'init' => $this->getJavascript()
        ];
    }

    public function getValores()
    {
        $serviceRamoAtividade = $this->serviceRamoAtividade();
        return $serviceRamoAtividade->selecionar();
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet)
    {
        $html = $this->helper()->getFile(__DIR__ . '/../../../view/apifiltro/ramoAtividade.phtml', [
            'ramosAtividade' => $resultSet,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    protected $serviceRamoAtividade = NULL;

    /**
     * @return \Cadastro\Service\RamoAtividade
     */
    public function serviceRamoAtividade()
    {
        if ($this->serviceRamoAtividade === NULL) {
            $this->serviceRamoAtividade = $this->getServiceManager()->get('Cadastro\Service\RamoAtividade');
        }
        return $this->serviceRamoAtividade;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        $js = '';
//        if (is_file(__DIR__ . '../../../../../../public/scripts/apifiltro/js/ramosAtividade.js')) {
//            $js = file_get_contents(__DIR__ . '../../../../../../public/scripts/apifiltro/js/ramosAtividade.js');
//        }
        return $js;
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
