<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class Regioes extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface
{

    /**
     * @return string Html do filtro
     */
    public function getFiltro()
    {
        $lojas = $this->getValores();
        $html = $this->criarHtml($lojas);
        $js = $this->getJavascript();

        return [
            'html' => $html,
            'init' => $js
        ];
    }

    public function getValores()
    {
        $serviceRegioes = $this->serviceRegioes();
        return $serviceRegioes->selecionar();
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet)
    {
        $html = $this->helper()->getFile(__DIR__ . '/../../../view/apifiltro/multiplasRegioes.phtml', [
            'regioes' => $resultSet,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * @return \Cadastro\Service\Regioes
     */
    public function serviceRegioes()
    {
        return $this->getServiceManager()->get('Cadastro\Service\Regioes');
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript()
    {
        return file_get_contents(__DIR__ . '../../../../../../public/scripts/apifiltro/js/regioes.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper()
    {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
