<?php

namespace APIFiltro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APIFiltro\Service\FiltroInterface;
use APIFiltro\Entity\AbstractFiltro;

class Status extends AbstractFiltro implements ServiceManagerAwareInterface, FiltroInterface {

    /**
     * @return string Html do filtro
     */
    public function getFiltro() {
        return [
            'html' => $this->criarHtml($this->getValores()),
            'init' => $this->getJavascript()
        ];
    }

    public function getValores() {
        return $this->getOpcoes()->getStatus();
    }

    /**
     * Monta o html
     * @return array [html][javascript]
     */
    public function criarHtml($resultSet) {

        $multiplo = $this->getOpcoes()->getMultiplo() ? 'Multiplo' : '';
        $caminho = "/../../../view/apifiltro/status$multiplo.phtml";
        $html = $this->helper()->getFile(__DIR__ . $caminho, [
            'status' => $resultSet,
            'opcoes' => $this->getOpcoes(),
        ]);

        return $html;
    }

    /**
     * Javascript com o método para buscar itens selecionados
     * @return string
     */
    public function getJavascript() {
        return file_get_contents(__DIR__ . '../../../../../../public/scripts/apifiltro/js/status.js');
    }

    /**
     * @return \APIHelper\Service\APIHelper
     */
    public function helper() {
        return $this->getServiceManager()->get('APIHelper\Service\APIHelper');
    }

    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager() {
        return $this->serviceManager;
    }

}
