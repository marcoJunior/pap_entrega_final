<?php

namespace APIHelper\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use APIHelper\Exception\Mensagem;

class IndexController extends AbstractActionController
{

    public function getDefesaAoConsumidorAction()
    {
        $json = new JsonModel;

        try {

            $helper = $this->serviceHelper();
            $return = $helper->getDefesaAoConsumidor();
            $json->setVariable('html', $return);
        } catch (Mensagem $msg) {
            $json->setVariable('msg', $msg->toArray());
        }

        return $json;
    }

    /**
     * @return \APIHelper\Service\ServiceHelper
     */
    public function serviceHelper()
    {
        return $this->getServiceLocator()->get('APIHelper\Service\APIHelper');
    }

}
