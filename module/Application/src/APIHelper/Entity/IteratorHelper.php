<?php

namespace APIHelper\Entity;

use Iterator;
use APIHelper\Entity\AbstractEntity;

/**
 * @author Vinicius Meira <vinicius@maxscalla.com.br>
 */
class IteratorHelper extends AbstractEntity implements Iterator
{

    protected $configuracao = [];
    protected $objectName = '';
    protected $i = 0;

    /**
     * Propriedade que carrega indices para usar nas configurações
     * Ex: Produtos.
     * @var mixed
     */
    protected $data = [];

    public function __construct($objeto = [], $array = [])
    {
        if (!is_array($array) || !is_object($objeto)) {
            return;
        }

        $this->objectName = get_class($objeto);

        foreach ($array as $configuracao) {

            if (!is_array($configuracao)) {
                continue;
            }
            $objeto = new $this->objectName;

            $objeto->exchangeArray($configuracao);

            $this->addData($objeto);
        }
    }

    public function addData($data)
    {
        $this->configuracao[] = $data;
    }

    public function toArray()
    {
        $array = [];

        foreach ($this as $key => $prop) {
            if ($prop instanceof AbstractEntity) {
                $array[$key] = $prop->toArray();
            } else {
                $array[$key] = $prop;
            }
        }

        return $array;
    }

    function getObjectName()
    {
        return $this->objectName;
    }

    function setObjectName($objectName)
    {
        $this->objectName = $objectName;
    }

    function setPosicao($i)
    {
        $this->i = $i;
        return $this;
    }

    function getConfiguracao()
    {
        return $this->configuracao;
    }

    function getData()
    {
        return $this->data;
    }

    function setConfiguracao($configuracao)
    {
        $this->configuracao = $configuracao;
        return $this;
    }

    function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    function rewind()
    {
        return $this->i = 0;
    }

    function current()
    {
        return $this->configuracao[$this->i];
    }

    function key()
    {
        return $this->i;
    }

    function next()
    {
        return $this->i++;
    }

    /**
     * @return int
     */
    function count()
    {
        return count($this->configuracao);
    }

    function valid()
    {
        return isset($this->configuracao[$this->i]) && $this->objectInstanceof($this->configuracao[$this->i]);
    }

    public function objectInstanceof($object)
    {
        return $object instanceof $this->objectName;
    }

    public function unsetKey($key)
    {
        unset($this->configuracao[$key]);
    }

}
