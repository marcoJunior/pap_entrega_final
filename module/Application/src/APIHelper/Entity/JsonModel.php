<?php

namespace APIHelper\Entity;

use Zend\View\Model\JsonModel as ZendJsonModel;
use APIHelper\Exception\Mensagem;

/**
 * @author Vinicius Meira
 */
class JsonModel extends ZendJsonModel
{

    public function setVariable($name, $value)
    {
        if ($value instanceof Mensagem) {
            $value = $value->toArray();
            $name = 'msg';
        }

        return parent::setVariable($name, $value);
    }

    public function setVariables($variables)
    {
        if ($variables instanceof Mensagem) {
            $variables = ['msg' => $variables->toArray()];
        }
        return parent::setVariables($variables, true);
    }

}
