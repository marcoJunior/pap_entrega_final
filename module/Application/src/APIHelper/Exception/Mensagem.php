<?php

namespace APIHelper\Exception;

class Mensagem extends \Exception
{

    const TIPO_AVISO = 'warning';
    const TIPO_SUCESSO = 'success';
    const TIPO_INFORMACAO = 'info';
    const TIPO_ERRO = 'error';

    protected $tipo;

    public function __construct($string, $tipo = self::TIPO_SUCESSO)
    {
        $this->mensagem = $string;
        $this->message = $string;
        $this->tipo = $tipo;
    }

    public function toArray()
    {
        return [
            'mensagem' => $this->mensagem,
            'tipo' => $this->tipo
        ];
    }

}
