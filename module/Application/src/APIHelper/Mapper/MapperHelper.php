<?php

namespace APIHelper\Mapper;

use ZfcBase\Mapper\AbstractDbMapper;
use APIHelper\Mapper\Where;
use Zend\Db\Sql\Expression;

class MapperHelper extends AbstractDbMapper
{

    /**
     * @return \Zend\Db\Adapter\Driver\ResultInterface
     */
    protected function insert($entity, $tableName = null, \Zend\Stdlib\Hydrator\HydratorInterface $hydrator = null)
    {
        return parent::insert(array_filter($this->entityToArray($entity), function($var) {
                            return $var !== NULL && $var !== FALSE && $var !== '';
                        }), $tableName, $hydrator);
    }

    protected function update($entity, $where, $tableName = null, \Zend\Stdlib\Hydrator\HydratorInterface $hydrator = null)
    {
        $array = $this->entityToArray($entity);

        return parent::update(array_filter($array, function($var) {
                            return $var !== NULL && $var !== FALSE && $var !== '';
                        }), $where, $tableName, $hydrator);
    }

    public function getTableName()
    {
        return parent::getTableName();
    }

}
