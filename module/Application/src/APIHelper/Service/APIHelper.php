<?php

namespace APIHelper\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Login\Entity\Conexao as Conexao;
use Zend\Db\Adapter\Adapter;

class APIHelper //implements ServiceManagerAwareInterface
{

    public $serviceManager;

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Retorna um arquivo como string, neste arquivo é possivel usar a chave de
     * <$var> como propriedade de <$this>
     *
     * @param string $filename
     * @param array $var
     * @return string
     */
    public function getFile($filename, $var)
    {
        $file = '';

        foreach ($var as $key => $value) {
            $this->$key = $value;
        }

        if (is_file($filename)) {
            ob_start();
            include $filename;
            $file = ob_get_clean();
        }

        return $file;
    }

    /**
     * Retorna uma mapper conectada caso $conexao seja entidade Conexao
     *
     * @param String $caminho
     * @param Conexao|null $conexao
     * @return Configuracao\Mapper\ConfigEcommerce
     */
    public function getConSm($caminho, $conexao = null)
    {
        $sm = $this->getServiceManager();
        $mapper = $sm->get($caminho);

        if ($conexao instanceof Conexao) {
            $adapter = new Adapter($this->getArrayConfig($conexao));
            $mapper->setDbAdapter($adapter);
        }

        return $mapper;
    }

    public function getArrayConfig(Conexao $conexao)
    {
        $banco = $conexao->getBanco();
        $ip = $conexao->getIp();
        $porta = $conexao->getPorta();

        return array(
            'driver' => 'Pdo',
            'dsn' => "pgsql:dbname=$banco;host=$ip;port=$porta",
            'username' => 'postgres',
            'password' => 'maxr2w2e8f4',
        );
    }

    public static function verificarEmail($endereco)
    {
        return !!preg_match('#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#', $endereco);
    }

    /**
     * @param \Zend\Db\ResultSet\HydratingResultSet $resp
     * @return array
     */
    public function arrayResposta($resp)
    {
        $retorno = [];
        foreach ($resp as $entity) {
            $retorno[] = $entity->toArray();
        }
        return $retorno;
    }

    /**
     * (58472416000192, '##.###.###/####-##')
     * @param string $val valor
     * @param string $mask como será a mascara
     * @return string valor com mascara
     */
    public function mascara($val, $mask)
    {
        $maskared = '';
        $k = 0;
        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k])) {
                    $maskared .= $val[$k++];
                }
            } else {
                if (isset($mask[$i])) {
                    $maskared .= $mask[$i];
                }
            }
        }
        return $maskared;
    }

    static public function getImagensPadraoProduto($string, $size = '')
    {
        $imagens = explode('||', $string);

        //var_dump($imagens);
        $imagem1 = self::verificaImagem($imagens[0], 'produto', $size);

        $imagens2 = isset($imagens[1]) ? $imagens[1] : false;
        $imagem2 = self::verificaImagem($imagens2, $imagem1, $size);

        //var_dump($imagem1, $imagem2);
        return [$imagem1, $imagem2];
    }

    static public function verificaImagem($url, $tipo = null, $size = '')
    {
        $exists = false;

        if (count(explode('apiproduto', $url)) > 1) {
            return $url;
        }

        if ($url) {
            $exists = file_exists(explode('?', __DIR__ . '/../../../../../public/' . $url)[0]);
        }
        if ($exists) {
            return self::replaceExtensao($url, $size);
        } elseif ($tipo === 'produto') {
            return 'images/produto_sem_foto.gif';
        } elseif ($tipo === 'slide') {
            return 'images/slide-sem-imagem.jpg';
        } elseif (is_string($tipo) && !empty($tipo)) {
            return $tipo;
        } else {
            return '';
        }
    }

    static public function replaceExtensao($url, $size)
    {
        $extensaoComParametros = pathinfo($url, PATHINFO_EXTENSION);
        $extensao = explode('?', $extensaoComParametros)[0];

        if (strpos($url, 'x')) {
            return $url;
        }

        if (!empty($size)) {

            $replace = 'x' . $size . "." . $extensao;
            $url = str_replace('.' . strtolower($extensao), $replace, $url);
        }

        return $url;
    }

    static public function getVariavelUrl($url, $chave)
    {
        $retorno = '';
        $explode = explode('?', $url);
        if (isset($explode[1])) {
            $parse = [];
            parse_str($explode[1], $parse);
            if ($parse[$chave]) {
                $retorno = $parse[$chave];
            }
        }
        return $retorno;
    }

    /**
     * Cria diretorio caso não exista
     * @param type $diretorio
     */
    static public function criarDiretorio($diretorio)
    {
        $root = __DIR__ . '/../../../../..';
        if (!is_dir("$root/$diretorio")) {
            mkdir("$root/$diretorio");
        }
    }

    static public function contains($agulha, $palheiro)
    {
        return strpos($agulha, $palheiro) != false;
    }

    /**
     * Retorna dados da conexão
     *
     * @return \Login\Entity\ConexaoUsuario
     */
    static public function getConexao()
    {
        return $GLOBALS['conexao'];
    }

    /**
     * Retorna url
     *
     * @return string
     */
    static public function getUrl($tipo = '')
    {
        $url = trim(APIHelper::getConexao()->getDominio());

        if (empty(APIHelper::getConexao()->getDominio())) {
            $url = trim(APIHelper::getConexao()->getSubdominio());
        }

        if ($tipo === 'convite') {
            $url = $url . '/venda?pagina=showLogin&firstlogin=true';
        }

        if (empty($url)) {
            $url = $_SERVER['HTTP_HOST'] . '/sign-in?loja=' . APIHelper::getConexao()->getApelido();
        }

        return $url;
    }

    public function getDefesaAoConsumidor()
    {
        $url = "http://www.planalto.gov.br/ccivil_03/leis/l8078.htm";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
        $content = curl_exec($ch);
        curl_close($ch);

        if ($content) {
            $content = strip_tags(utf8_encode($content), '<p><b><i><br>');
            $replaces = ['Regulamento<br>', 'Mensagem de veto<br>', 'Vigência<br>', 'Texto compilado'];
            return str_replace($replaces, '', $content);
        }

        $textoException = 'Não foi possível abrir o código de defesa ao consumidor  . <br>';
        $textoException .= '<a href="' . $url . '" target="_blank">Clique aqui e leia</a>';
        return $textoException;
    }

    static public function isDevel()
    {
        $ipSecnet = '189.85.67.147';

        $serverAddr = filter_input(INPUT_SERVER, 'SERVER_ADDR') ?: filter_input(INPUT_SERVER, 'REMOTE_ADDR');

        return $serverAddr !== $ipSecnet || strpos(filter_input(INPUT_SERVER, 'HTTP_HOST'), 'devel');
    }

    /**
     * Retorna se o login corresponde á homologação da MOIP
     *
     * @return boolean
     */
    static public function isMoipHomolocao()
    {
        return self::getConexao()->getEmail() === 'moip@mohmal.com';
    }

}
