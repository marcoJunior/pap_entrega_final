<?php

namespace APISql\Mapper\Hydrator;

use Zend\Stdlib\Hydrator\ClassMethods;

class LazyLoadHydrator extends ClassMethods
{
    protected $mapperManager;

    public function __construct($mapperManager)
    {
        $this->mapperManager = $mapperManager;
        parent::__construct();
    }

    public function extract($object)
    {
        $entity = $this->getEntity();

        if (!$object instanceof $entity) {
            throw new \Exception('$object must be an instance of ' . $entity);
        }

        $this->underscoreSeparatedKeys = false;
        $data = parent::extract($object);
        $data = $this->mapToTable($data);

        return $data;
    }

    public function hydrate(array $data, $object)
    {
        $entity = $this->getEntity();

        if (!$object instanceof $entity) {
            throw new \Exception('$object must be an instance of ' . $entity);
        }

        $data = $this->mapToObject($data);

        if (method_exists($object, 'setMapperManager')) {
            $object->setMapperManager($this->mapperManager);
        }

        $this->underscoreSeparatedKeys = true;
        return parent::hydrate($data, $object);
    }

    protected function mapToTable($data)
    {
        foreach ($this->getTemporary() as $attribute) {
            unset($data[$attribute]);
        }

        foreach ($this->getMap() as $attribute => $column) {
            $data = $this->mapField($attribute, $column, $data);
        }
        return $data;
    }

    protected function mapToObject($data)
    {
        foreach ($this->getMap() as $attribute => $column) {
            $data = $this->mapField($column, $attribute, $data);
        }
        return $data;
    }

    protected function mapField($keyFrom, $keyTo, array $array)
    {
        $array[$keyTo] = $array[$keyFrom];
        unset($array[$keyFrom]);
        return $array;
    }

    /*
     * @return string
     */

    protected function getEntity()
    {
        return new \Exception('Entity not declared');
    }

    /*
     * @return array formato: array('classAttribute' => 'column')
     */

    protected function getMap()
    {
        return new \Exception('Map array not declared');
    }

    /**
     * Lista de atributos que não serão persistidos
     * @return array formato: array('column')
     */
    protected function getTemporary()
    {
        return array();
    }
}
