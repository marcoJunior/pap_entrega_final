<?php

namespace APISql\Mapper;

use Zend\ServiceManager\ServiceManager;

class MapperManager
{

    protected $serviceManager;

    public function __construct(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
    }

    public function get($mapperName)
    {
        return $this->serviceManager->get($mapperName);
    }
}
