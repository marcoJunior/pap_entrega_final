<?php

namespace APISql\Model;

class Helper {

    public $cfiatvcfw;

    public function exchangeArray($data) {
        $columns = array(
            'cfiatvcfw',
        );

        foreach ($columns as $colum) {
            $this->$colum = (!empty($data[$colum])) ? trim($data[$colum]) : '';
        }
    }

}
