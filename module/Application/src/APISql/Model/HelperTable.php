<?php

namespace APISql\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;

class HelperTable implements ServiceManagerAwareInterface
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function setConfigDb()
    {
        $conexao = $GLOBALS['conexao'];
        $cfidbtvld = trim(strtolower(!empty($conexao->getBancoTintometrico()) ? $conexao->getBancoTintometrico() : $conexao->getBanco()));

        if (empty($cfidbtvld)) {
            return false;
        }

        $this->host = trim(strtolower($conexao->getIp()));
        $this->port = trim(strtolower($conexao->getPorta()));
        $this->dbname = $cfidbtvld;
        $this->login = 'postgres';
        $this->password = 'maxr2w2e8f4';

        return true;
    }

    public function getCon()
    {
        $con = pg_connect(
                "host=" . $this->host . "
                port=" . $this->port . "
                dbname=" . strtolower($this->dbname) . "
                user=" . $this->login . "
                password=" . $this->password
                ) or die("Não foi possivel conectar ao servidor PostGreSQL");

        return $con;
    }

    public function execQuery($con, $sql)
    {
        $result = pg_query($con, $sql);

        if (!$result) {
            echo "Problem with query " . $sql . "<br/>";
            echo pg_last_error();
            exit();
        }
        pg_close($con);
        return $result;
    }

    public function validateSchema($schema)
    {
        $validate = "SELECT schema_name FROM information_schema.schemata WHERE schema_name = '$schema'";

        if (!$this->setConfigDb()) {
            return false;
        }

        $con = $this->getCon();
        $result = $this->execQuery($con, $validate);

        return $result;
    }

    public function createConnectionTableGateway($config, $mapper)
    {

        try {
            $adapter = new Adapter($this->setConfigConectionDb($config));

            $adapter->getDriver()->getConnection()->connect();

            $resultSetPrototype = new ResultSet();
            if (!empty($mapper)) {
                $resultSetPrototype->setArrayObjectPrototype(new $mapper);
            }
            $tableGateway = new TableGateway($config['table'], $adapter, null, $resultSetPrototype);

            return $tableGateway;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function setConfigConectionDb($config)
    {

        $password = 'maxr2w2e8f4';
        if (isset($config['senha']) && $config['senha'] != '') {
            $password = $config['senha'];
        }
        $user = 'postgres';
        if (isset($config['usuario']) && $config['usuario'] != '') {
            $user = $config['usuario'];
        }

        return array(
            'driver' => 'Pdo',
            'dsn' => 'pgsql:dbname=' . strtolower($config['banco']) . ';host=' . $config['ip'] . ';port=' . $config['porta'],
            'username' => $user,
            'password' => $password
        );
    }

    public function contaLinhasTabelaBanco($nomeTabela, $mapper, $configPadrao, Where $whereReceb = null)
    {

        $dbAdapter = new Adapter($this->tableGateway->adapter->getDriver());
        $sql = "VACUUM $nomeTabela";
        $dbAdapter->query($sql, Adapter::QUERY_MODE_EXECUTE);

        $selectCont = new Select();
        $selectCont->from($nomeTabela);
        $selectCont->columns(array("count" => new Expression('count(*)')));

        $where = new Where();
        if (isset(explode("&", $_SERVER['HTTP_REFERER'])[1])) {
            $valorUrl = explode("&", $_SERVER['HTTP_REFERER'])[1];
        } else {
            $valorUrl = false;
        }

        if ($valorUrl) {
            $tipoValor = $configPadrao[explode("=", $valorUrl)[0]]['tipo'];
            if ($tipoValor == 'int') {
                $where->equalTo($mapper[explode("=", $valorUrl)[0]], explode("=", $valorUrl)[1]);
            } else {
                $where->like($mapper[explode("=", $valorUrl)[0]], "%" . explode("=", $valorUrl)[1] . "%");
            }
        } else {
            $where = $whereReceb;
        }

        $selectCont->where($where);
        return $dbAdapter->query($selectCont->getSqlString($this->tableGateway->getAdapter()->getPlatform()), Adapter::QUERY_MODE_EXECUTE)->toArray()[0];
    }

}
