<?php

namespace APISql\Service;

use Zend\Db\ResultSet\HydratingResultSet;

class ConvertObject
{

    /**
     * Converte HydratingResultSet para array utilizando os tratamentos dos
     *  getters/setters
     * @param  HydratingResultSet $resultSet
     * @return array
     */
    public static function resultSetToArray(HydratingResultSet $resultSet, array $colunas = null)
    {
        $lista = [];
        foreach ($resultSet as $entity) {
            $row = $entity->toArray();
            if ($colunas) {
                $filter = function ($key) use ($colunas) {
                    return in_array($key, $colunas);
                };
                $row = array_filter($row, $filter, ARRAY_FILTER_USE_KEY);
            }
            $lista[] = $row;
        }
        return $lista;
    }

    public static function convertObject($obj)
    {
        $lista = [];
        foreach ($obj as $chave => $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key) ?: "");
                if (is_object($registros)) {
                    foreach ((array) $registros as $i => $val) {
                        $lista[$chave][$key][$i] = trim($val);
                    }
                } else {
                    $lista[$chave][$key] = trim($registros);
                }
            }
        }
        return $lista;
    }

    public static function convertObjectIncludeKeyCod($obj)
    {
        $lista = null;
        foreach ($obj as $chave => $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key));
                $lista[$value->getCodigo()][$key] = trim($registros);
            }
        }
        return $lista;
    }

    public static function convertEntity($obj)
    {
        $lista = [];
        foreach ((array) $obj as $key => $value) {
            $key = ltrim(str_replace("*", "", $key));
            $lista[$key] = is_string($value) ? trim($value) : $value;
        }
        return $lista;
    }

    public static function convertObjectEmUmaChave($obj)
    {
        $lista = null;
        foreach ($obj as $value) {
            foreach ((array) $value as $key => $registros) {
                $key = ltrim(str_replace("*", "", $key));
                $lista[$key] = trim($registros);
            }
        }
        return $lista;
    }

}
