<?php

namespace APISql\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use InvalidArgumentException;
use Zend\Db\Sql\Where;
use APISql\Entity\Codigo;
use APIHelper\Service\APIHelper;

class HelperSql implements ServiceManagerAwareInterface
{

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function getLimitAndOffset($limit = 1, $offset = 0)
    {
        $offsetResult = 0;
        if ($limit != 1 && $offset != 0) {
            $offsetResult = $limit * $offset;
        }
        return array('limit' => $limit, 'offset' => $offsetResult);
    }

    public function getOrder($config)
    {
        if (!empty($config['order']['column'])) {
            return $config['order']['column'] . ',sr_recno ' . $config['order']["order"];
        } else {
            return 'cficodped DESC';
        }

        return false;
    }

    public function getConfig($config)
    {
        $clauses = array();
        if (isset($config['limit'])) {
            $clauses = $this->getLimitAndOffset($config['limit']['offset'], $config['limit']['page']);
        }

        if (isset($config['like']['value'])) {
            if (!empty($config['like']['value']) || $config['like']['value'] == '0') {
                $clauses['like'] = $config['like'];
                if (!isset($clauses['like']['option'])) {
                    $clauses['like']['option'] = 0;
                }
            }
        }

        if (isset($config['order']['column']) && !empty($config['order']['column'])) {
            $clauses['order'] = array($config['order']['column'] . ' ' . $config['order']['order'], 'sr_recno ' . $config['order']['order']);
        } else {
            if (isset($config['order'])) {
                $clauses['order'] = array($config['order']["default"][0] . ' ' . $config['order']["default"][1], 'sr_recno ' . $config['order']["default"][1]);
            }
        }

        if (isset($config['datini']) && $config['datini'] != "'undefined'" && isset($config['datfim'])) {
            $clauses['datini'] = str_replace("'", '', $config['datini']);
            $clauses['datfim'] = str_replace("'", '', $config['datfim']);
        }

        if (isset($config['lojas'])) {
            $clauses['lojas'] = $config['lojas'];
        }

        if (isset($config['filtros'])) {
            $clauses['filtros'] = $config['filtros'];
        }

        $clauses['representantesVinculados'] = $this->getRepresentantesVinculados();

        return $clauses;
    }

    /**
     * @param array
     * @return string utilizada para criar querys Sql dinâmicamente
     * @copyright (c) 2015, Leandro Machado
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function createWhereIn($data)
    {
        $whereIn = '';
        foreach ($data as $value) {
            if (end($data) !== $value) {
                if (isset($string) && !empty($string)) {
                    $whereIn .= '"' . $value . '","';
                } else {
                    $whereIn .= $value . ',';
                }
            } else {
                if (isset($string) && !empty($string)) {
                    $whereIn .= $value . '"';
                } else {
                    $whereIn .= $value;
                }
            }
        }
        return $whereIn;
    }

    /**
     * @param object
     * @return type array ou int resultados capturados no banco de dados
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    public function converteObjectSqlToArray($object)
    {
        $return = array();
        if (is_object($object)) {
            foreach ($object as $key => $value) {
                $return[$key] = (array) $value;
            }
        } else {
            $return = (int) $object;
        }
        return $return;
    }

    /**
     * @param array $representantes Array dos representantes selecionados
     * @param string $column Nome da coluna para criação do where
     * @param bool $isBi True ou False para saber se é um gráfico ou não
     * @return string Tendo como valor um where in ou where para addPredicates
     * na model
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     * @copyright (c) 2015, Leandro Machado
     */
    public function clausesByRepOrNot($representantes, $column, $isBi = false)
    {
        $conexao = $GLOBALS['conexao'];
        $tipoLogin = $this->serviceLogin()->getTipoLogin();

        if (empty($column)) {
            throw new InvalidArgumentException('Informe o 2° parâmetro na chamada do método que seria uma string com nome da coluna para o where ou in');
        }

        if ($tipoLogin === 'admin' || $tipoLogin === 'master' || $tipoLogin === 'funcionario') {
            if (empty($representantes)) {
                return false;
            }
            $in = $this->createWhereIn($representantes);
            $clause = "$column IN($in)";
        } else {
            $clause = "$column = " . $conexao->getCodigoRepresentante();
        }

        if ($isBi) {
            return $clause . ' AND ';
        }

        return $clause;
    }

    /**
     * @param string Nome do esquema
     * @throws Para quando não tiver parâmetro setado
     * @return bool  True p/ quando obter nome do esquema
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     * @copyright (c) 2015, Leandro Machado
     */
    public function setValidateSchema($schema)
    {
        try {
            if (!isset($schema) || empty($schema)) {
                throw new
                InvalidArgumentException('Não existe o parâmetro "' . $schema . '" relacionado ao esquema!');
            }

            $retorno = false;

            $model = $this->getServiceManager()->
                    get('APISql/Model/HelperTable');

            $vResult = $model->validateSchema($schema);

            if (!is_bool($vResult)) {
                while ($myrow = pg_fetch_assoc($vResult)) {
                    $retorno = $myrow;
                }
            }
            if (is_array($retorno)) {
                $retorno = true;
            }

            return $retorno;
        } catch (InvalidArgumentException $e) {
            $message = array(
                'msg' => $e->getMessage()
            );
            return $message;
        }
    }

    public function createConnectionTableGateway($config, $mapper)
    {
        $model = $this->getServiceManager()->get('APISql/Model/HelperTable');

        if (is_array($config)) {
            return $model->createConnectionTableGateway($config, $mapper);
        }
        return false;
    }

    /**
     * @param array $conexao Array com as configurações do DB
     * @param object $objectModel Instacia de uma model
     * @param array $model Array com a table e a mapper utilizada pela Model
     * @return object  $objectModel Objecto com a nova conexao do TableGateway
     * @author  Janley Santos <janley@maxscalla.com.br>
     * @copyright (c) 2015, Janley Santos
     */
    public function createTableGateway($conexao, $objectModel, $model)
    {
        $conexao['table'] = $model['table'];
        $this->tableGateway = $this->createConnectionTableGateway($conexao, $model['mapper']);
        $objectModel->__construct($this->tableGateway);
        return $objectModel;
    }

    /**
     * Retorna array para conexão com table gateway
     *
     * @param EntidadeConexao $conexao
     * @return Array
     */
    public function entidadeConexaoParaTableGateway(\Login\Entity\Conexao $conexao)
    {
        return [
            'banco' => $conexao->getBanco(),
            'ip' => $conexao->getIp(),
            'porta' => $conexao->getPorta(),
        ];
    }

    /**
     * @return \Login\Service\Login
     */
    public function serviceLogin()
    {
        return $this->getServiceManager()->get('Login\Service\Login');
    }

    public function contaLinhasTabelaBanco($nomeTabela, $where, $configPadrao, Where $whereReceb = null)
    {
        $model = $this->getServiceManager()->get('APISql/Model/HelperTable');
        return $model->contaLinhasTabelaBanco($nomeTabela, $where, $configPadrao, $whereReceb);
    }

    /**
     * @return \Cadastro\Service\VinculoRepresentantes
     */
    public function getServiceVinculoRepresentantes()
    {
        return $this->getServiceManager()->get('Cadastro/Service/VinculoRepresentantes');
    }

    /**
     * Retorna representantes vinculados dos funcionários
     * @return array
     */
    public function getRepresentantesVinculados()
    {
        $representantes = [];

        if (APIHelper::getConexao()->getNivel() !== 'F') {
            return false;
        }

        $reps = $this
                ->getServiceVinculoRepresentantes()
                ->get(new \Cadastro\Entity\Action\VinculoRepresentantes(['funcionario' => APIHelper::getConexao()->getReferencia()])
        );

        if ($reps) {
            foreach ($reps as $valores) {
                $representantes[] = $valores['representante'];
            }
        }

        return $representantes;
    }

}
