<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Application\View\LojaAplicativos;
use Application\Entity\PainelAplicativo;
use Application\View\ApplicationOffLine;
use Zend\Mvc\Controller\AbstractActionController;
use Application\Entity\DropZone as DropZoneEntity;

class IndexController extends AbstractActionController
{

    protected $service;

    function getService()
    {
        return $this->service;
    }

    function setService($service)
    {
        $this->service = $service;
    }

    public function indexAction()
    {
        return new ViewModel();
    }

    public function aplicativosAction()
    {

        $phpRenderer = $this->getService()->get('ViewRenderer');
        $viewClientes = new LojaAplicativos($phpRenderer);
        $viewClientes->setVariable('titulo', 'Loja de aplicativos');
        $viewClientes->setVariable('apps', $this->getAplicativos());

        $app = new ApplicationOffLine($phpRenderer);
        $app->addChild($viewClientes);
        return $app;
    }

    public function getAplicativos()
    {
        return [
            'forca' => new PainelAplicativo(),
            'alfred' => new PainelAplicativo(),
            'separacao' => new PainelAplicativo(),
        ];
    }

    public function rotasAction()
    {
//        var_dump($this->serviceRotas()->getRotas());
//        die();
//
        return $this->serviceRotas();
    }

    public function logoAction()
    {
        $service = $this->serviceDropZone();

        $dropZone = new DropZoneEntity('cliente');
        return new JsonModel((array) $service->enviar($dropZone));
    }

    public function produtosAction()
    {
        $service = $this->serviceDropZone();

        $dropZone = new DropZoneEntity('produtos');
        return new JsonModel((array) $service->enviar($dropZone));
    }

    /**
     * @return \Application\Service\DropZone
     */
    public function serviceDropZOne()
    {
        return $this->getService()->get(\Application\Service\DropZone::class);
    }

    /**
     * @return \Application\Service\Rotas
     */
    public function serviceRotas()
    {
        return $this->getService()->get('RotasPersonalizadas');
    }

}
