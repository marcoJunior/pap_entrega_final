<?php

namespace Application\Entity;

use APIHelper\Entity\AbstractEntity;

/**
 * @author Marco Junior
 */
class DropZone extends AbstractEntity
{

    protected $arqNomeTemp = '';
    protected $tabela = '';
    protected $diretorio = '';

    function __construct($tabela)
    {
        $this->tabela = $tabela;
        $this->diretorio = __DIR__ . "/../../../../../public/imagens/admin/logo/$tabela/";
    }

    function getArqNomeTemp()
    {
        return $this->arqNomeTemp;
    }

    function getTabela()
    {
        return $this->tabela;
    }

    function getDiretorio()
    {
        return $this->diretorio;
    }

    function setArqNomeTemp($arqNomeTemp)
    {
        $this->arqNomeTemp = $arqNomeTemp;
    }

    function setTabela($tabela)
    {
        $this->tabela = $tabela;
    }

    function setDiretorio($diretorio)
    {
        $this->diretorio = $diretorio;
    }

}
