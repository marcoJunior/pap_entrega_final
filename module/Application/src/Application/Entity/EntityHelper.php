<?php

namespace Application\Entity;

use InvalidArgumentException;
use IteratorAggregate;
use Traversable;

class EntityHelper implements IteratorAggregate
{

    function __construct($data = false)
    {
        if ($data) {
            $this->exchangeArray($data);
        }
    }

    public function exchangeArray($data)
    {
        if (
                !is_array($data) &&
                !($data instanceof Traversable)
        ) {
            throw new InvalidArgumentException
            ('O parametro deve ser um array ou instancia de Traversable');
        }
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

    public function getIterator()
    {
        return new \ArrayIterator($this);
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

}
