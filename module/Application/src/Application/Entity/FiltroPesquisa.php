<?php

namespace Application\Entity;

use APIHelper\Entity\AbstractEntity;

/**
 * @author Vinicius Meira do Nascimento
 */
class FiltroPesquisa extends AbstractEntity
{

    /**
     * Valor digitado na pesquisa
     * @var string
     */
    protected $valor = '';

    /**
     * Coluna que será pesquisada
     * @var string
     */
    protected $coluna = '';

    /**
     * Opção do modo que irá pesquisar na coluna
     * @exemple 'iniciando'
     * @exemple 'contendo'
     * @exemple '='
     * @exemple '<'
     * @exemple '>'
     * @exemple '<='
     * @exemple '>='
     * @type {String}
     */
    protected $opcao = '';

    public function getValorParaWhere()
    {
        $replaceVirgula = str_replace(',', '.', $this->getValor());
        if (is_numeric($replaceVirgula)) {
            return $replaceVirgula;
        } else {
            return strtoupper($this->getValor());
        }
    }

    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    public function setColuna($coluna)
    {
        $this->coluna = $coluna;
    }

    public function setOpcao($opcao)
    {
        $this->opcao = $opcao;
    }

    public function getValor()
    {
        return $this->valor;
    }

    public function getColuna()
    {
        return $this->coluna;
    }

    public function getOpcao()
    {
        return $this->opcao;
    }

}
