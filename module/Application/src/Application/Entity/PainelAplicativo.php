<?php

namespace Application\Entity;

use APIHelper\Entity\AbstractEntity;

class PainelAplicativo extends AbstractEntity
{

    protected $logo = "link imagem";
    protected $nome = "Aplicação";
    protected $nomeDescricao = "Para que";
    protected $indicadorUmDescricao = Null;
    protected $indicadorUmValor = Null;
    protected $indicadorDoisDescricao = Null;
    protected $indicadorDoisValor = Null;
    protected $preco = 0.00;
    protected $linkMaisInfo = "toastr.warning('Opção ainda não disponivel!<br>Desculpe o inconveniente!', 'ALERTA', {positionClass: 'toast-top-right'});";
    protected $contratado = false;
    protected $corBox = 'green-sharp';
    protected $styleBox = '';
    protected $botaoUm = '';
    protected $botaoDois = '';
    protected $ativoEmBreve = false;

    static function get()
    {
        return new PainelAplicativo();
    }

    function getLogo()
    {
        return $this->logo;
    }

    function getNome()
    {
        return $this->nome;
    }

    function getNomeDescricao()
    {
        return $this->nomeDescricao;
    }

    function getIndicadorUmDescricao()
    {
        return $this->indicadorUmDescricao;
    }

    function getIndicadorUmValor()
    {
        return $this->indicadorUmValor;
    }

    function getIndicadorDoisDescricao()
    {
        return $this->indicadorDoisDescricao;
    }

    function getIndicadorDoisValor()
    {
        return $this->indicadorDoisValor;
    }

    function getPreco()
    {
        return $this->preco;
    }

    function getLinkMaisInfo()
    {
        return $this->linkMaisInfo;
    }

    function getContratado()
    {
        return $this->contratado;
    }

    function getCorBox()
    {
        return $this->corBox;
    }

    function getBotaoUm()
    {
        return $this->botaoUm;
    }

    function getBotaoDois()
    {
        return $this->botaoDois;
    }

    function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
        return $this;
    }

    function setNomeDescricao($nomeDescricao)
    {
        $this->nomeDescricao = $nomeDescricao;
        return $this;
    }

    function setIndicadorUmDescricao($indicadorUmDescricao)
    {
        $this->indicadorUmDescricao = $indicadorUmDescricao;
        return $this;
    }

    function setIndicadorUmValor($indicadorUmValor)
    {
        $this->indicadorUmValor = $indicadorUmValor;
        return $this;
    }

    function setIndicadorDoisDescricao($indicadorDoisDescricao)
    {
        $this->indicadorDoisDescricao = $indicadorDoisDescricao;
        return $this;
    }

    function setIndicadorDoisValor($indicadorDoisValor)
    {
        $this->indicadorDoisValor = $indicadorDoisValor;
        return $this;
    }

    function setPreco($preco)
    {
        $this->preco = $preco;
        return $this;
    }

    function setLinkMaisInfo($linkMaisInfo)
    {
        $this->linkMaisInfo = $linkMaisInfo;
        return $this;
    }

    function setContratado($contratado)
    {
        $this->contratado = $contratado;
        return $this;
    }

    function setCorBox($corBox)
    {
        $this->corBox = $corBox;
        return $this;
    }

    function setBotaoUm($botaoUm)
    {
        $this->botaoUm = $botaoUm;
        return $this;
    }

    function setBotaoDois($botaoDois)
    {
        $this->botaoDois = $botaoDois;
        return $this;
    }

    function getStyleBox()
    {
        return $this->styleBox;
    }

    function setStyleBox($styleBox)
    {
        $this->styleBox = $styleBox;
        return $this;
    }

    function getAtivoEmBreve()
    {
        return $this->ativoEmBreve;
    }

    function setAtivoEmBreve($ativoEmBreve)
    {
        $this->ativoEmBreve = $ativoEmBreve ? 'block' : 'none';
        return $this;
    }

}
