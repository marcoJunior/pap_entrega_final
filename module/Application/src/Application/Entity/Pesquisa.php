<?php

namespace Application\Entity;

use APIHelper\Entity\AbstractEntity;
use Application\Exception\Exception;
use Application\Mapper\Where;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\Operator;

/**
 * @author Vinicius Meira
 * Esta classe é responsável por aplicar a pesquisa em todas as grids do sistema
 * Não aplicar regras de negócio aqui, apenas lógica sobre a pesquisa
 */
class Pesquisa extends AbstractEntity
{
    const OPCAO_INICIANDO = 'iniciando';
    const OPCAO_CONTENDO = 'contendo';

    /**
     * Valor digitado na pesquisa
     * @var FiltroPesquisa|null
     */
    protected $filtro;

    /**
     * Faz pesquisa por um ID ou vários ID's de produto
     * @var array|string
     */
    protected $ids;

    /**
     * @var int
     */
    protected $quantidade;

    /**
     * @var int
     */
    protected $pagina = 0;

    /**
     * @var string
     */
    protected $ordenacao;

    /**
     * @var boolean
     */
    protected $iniciando = false;

    /**
     * Coluna personalizada, para uso do where na querys
     *
     * @var array
     */
    protected $colunas;

    /**
     * Aplica no select o columns, o order, o limit, o offset e o where do parametro
     * @param  Select $select
     */
    public function aplicarSelect(Select $select)
    {
        if ($this->getColunas()) {
            $select->columns($this->getColunas());
        }

        if ($this->getOrdenacao()) {
            $select->order($this->getOrdenacao());
        }

        if ($this->getQuantidade()) {
            $select->limit($this->getQuantidade())
                ->offset($this->getQuantidade() * $this->getPagina());
        }

        $this->aplicarFiltro($select);
    }

    /**
     * Aplica o filtro
     * @param  Select   $select
     */
    public function aplicarFiltro(Select $select)
    {
        if ($this->getFiltro() && !$this->getFiltro()->getColuna()) {
            $this->aplicarFiltroEmTodasColunas($select);
        } elseif ($this->getFiltro()) {
            $this->aplicarFiltroPorColuna($select);
        }
    }

    /**
     * Metodo que aplica o where na coluna de acordo com o filtro da pesquisa
     * @param  Select $select
     */
    private function aplicarFiltroPorColuna(Select $select)
    {
        $select->where(new Where());
        $filtro = $this->getFiltro();
        $val = $filtro->getValor();
        $col = $filtro->getColuna();

        switch ($filtro->getOpcao()) {
            case self::OPCAO_INICIANDO:
                $select->where->likeAllPossibilities($col, $val, true);
                break;
            case self::OPCAO_CONTENDO:
                $select->where->likeAllPossibilities($col, $val);
                break;
            case Operator::OPERATOR_EQUAL_TO:
                $select->where->equalTo($col, $val);
                break;
            case Operator::OPERATOR_LESS_THAN:
                $select->where->lessThan($col, $val);
                break;
            case Operator::OPERATOR_LESS_THAN_OR_EQUAL_TO:
                $select->where->lessThanOrEqualTo($col, $val);
                break;
            case Operator::OPERATOR_GREATER_THAN:
                $select->where->greaterThan($col, $val);
                break;
            case Operator::OPERATOR_GREATER_THAN_OR_EQUAL_TO:
                $select->where->greaterThanOrEqualTo($col, $val);
                break;
            default:
                throw new Exception("Filtro inválido", 422);
                break;
        }
    }


    /**
     * Metodo que aplica o where em todas as colunas da tabela
     * @param  Select $select
     */
    public function aplicarFiltroEmTodasColunas(Select $select)
    {
        $select->where(new Where());
        $nest = $select->where->nest;
        foreach ($this->getColunas() as $coluna) {
            $col = new Expression("UPPER(CAST($coluna AS CHAR))");
            $val = $this->getFiltro()->getValorParaWhere();

            switch ($this->getFiltro()->getOpcao()) {
                case self::OPCAO_INICIANDO:
                    $nest->or->likeAllPossibilities($col, $val, true);
                    break;
                case self::OPCAO_CONTENDO:
                    $nest->or->likeAllPossibilities($col, $val);
                    break;
                default:
                    throw new Exception("Filtro inválido", 422);
                    break;
            }
        }
        $nest->unest;
    }

    function setIniciando($iniciando)
    {
        $this->iniciando = !!$iniciando;
    }

    function getIniciando()
    {
        return $this->iniciando;
    }

    function getFiltro()
    {
        return $this->filtro;
    }

    function setFiltro(array $filtro)
    {
        $this->filtro = new FiltroPesquisa($filtro);
    }

    function getQuantidade()
    {
        return $this->quantidade;
    }

    function getPagina()
    {
        return $this->pagina;
    }

    function setQuantidade($quantidade)
    {
        $this->quantidade = (int) $quantidade;
    }

    function setPagina($pagina)
    {
        $this->pagina = (int) $pagina;
    }

    function getIds()
    {
        return $this->ids;
    }

    function setIds(array $ids)
    {
        $this->ids = $ids;
    }

    function getColunas()
    {
        return $this->colunas;
    }

    function setColunas(array $colunas)
    {
        $this->colunas = $colunas;
    }

    function getOrdenacao()
    {
        return $this->ordenacao;
    }

    function setOrdenacao($ordenacao)
    {
        $this->ordenacao = $ordenacao;
    }

}
