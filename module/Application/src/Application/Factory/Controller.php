<?php

namespace Application\Factory;

use Zend\Mvc\Controller\AbstractRestfulController;

class Controller extends AbstractRestfulController
{

    protected $service;

    function getService()
    {
        return $this->service;
    }

    function setService($service)
    {
        $this->service = $service;
    }

    /**
     * @param Zend\Mvc\Controller\ControllerManager $controllerManager
     */
    public function __invoke($controllerManager)
    {
//        die('aqui');
        // get underlaying service manager
        /* @var Zend\ServiceManager\ServiceManager $serviceManager */
        $serviceManager = $controllerManager->getServiceLocator();

        $this->setService($serviceManager);

        // get some dependency
//        $dependencyService = $serviceManager->get('Application\Service\MyDependencyService');
        // inject it to the constructor of the controller
        return $this;
    }

}
