<?php

namespace Application\Factory;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\AbstractFactoryInterface;

class Helper implements AbstractFactoryInterface
{

    /**
     * {@inheritDoc}
     */
    public function __invoke(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $service = new $requestedName;
        $service->setServiceManager($serviceLocator);
        if ($this->canCreateServiceWithName($serviceLocator, $name, $requestedName)) {
            return $service;
        }
    }

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName): bool
    {
        if (class_exists($requestedName)) {
            return true;
        }

        return false;
    }

    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $class = $requestedName;
        return new $class;
    }
}
