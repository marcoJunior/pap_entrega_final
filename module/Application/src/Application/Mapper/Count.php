<?php

namespace Application\Mapper;

use Application\Entity\Pesquisa;
use Zend\Db\Sql\Expression;
use ZfcBase\Mapper\AbstractDbMapper;
use Zend\Stdlib\Hydrator\HydratorInterface;

class Count extends AbstractDbMapper
{

    protected $tableName;

    public function setTableName($tableName)
    {
        $this->tableName = $tableName;
    }

    public function getTableName()
    {
        return parent::getTableName();
    }

    /**
     * @param  Pesquisa $pesquisa
     * @return int
     */
    public function selecionarCount(Pesquisa $pesquisa)
    {
        $select = $this->getSelect();
        $pesquisa->aplicarFiltro($select);
        $select->columns(['totalRegistros' => new Expression('1')]);
        $totalRegistros = $this->select($select)->count();
        return $totalRegistros;
    }

}
