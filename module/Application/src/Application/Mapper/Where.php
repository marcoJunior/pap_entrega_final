<?php

namespace Application\Mapper;

use Zend\Db\Sql\Where as WhereZend;

class Where extends WhereZend
{

    /**
     * Faz where
     * @param type    $identifier
     * @param type    $valor
     * @param boolean $iniciando
     * @return \Application\Mapper\Where
     */
    public function likeAllPossibilities($identifier, $valor, $iniciando = false)
    {
        if (!$valor) {
            return $this;
        }
        $nest = $this->nest();
        foreach (array_filter(explode(' ', $valor)) as $i => $string) {
            if ($i === 0 && $iniciando) {
                $nest->like($identifier,  "$string%");
                continue;
            }

            $nest->like($identifier,  "%$string%");
        }
        $nest->unnest();

        return $this;
    }

    public function nest()
    {
        $predicateSet = new Where();
        $predicateSet->setUnnest($this);
        $this->addPredicate($predicateSet, ($this->nextPredicateCombineOperator) ?: $this->defaultCombination);
        $this->nextPredicateCombineOperator = null;
        return $predicateSet;
    }

}
