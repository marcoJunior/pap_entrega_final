<?php

namespace Application\Service;

use Zend\Permissions\Acl\Acl as AclZend;

class Acl extends AclZend
{

    public function init()
    {
        $this->addRole('admin');
        $this->addRole('convidado');
        $this->addResource('');
        $this->addResource('home');
        $this->addResource('autenticacao');

        $this->allow('convidado', 'autenticacao');
        $this->allow('admin', '');
        $this->allow('admin', 'home');
    }

}
