<?php

namespace Application\Service;

use Zend\Permissions\Acl\Acl;

//use Zend\Permissions\Acl\Role\GenericRole as Role;
//use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class AclUsuarios extends Acl
{

    /**
     * @apiVersion 1.0.0
     * @apiName __construct
     * @apiGroup Acess Control List
     *
     * @apiSuccessExample {array} Success Urls do sidebar:
     *     HTTP/1.1 200 OK
     *     {
     *      Inicia as constantes com todas urls disponiveis para acesso,
     * e logo em seguida define quais urls devem ser bloqueadas.
     *     }
     * @apiErrorExample {json} Houve algum erro
     *    HTTP/1.1 500 Internal Server Error
     */
    public function __construct($permissoes, $rotasDoSistema)
    {
        $this->setAcl($permissoes, $rotasDoSistema);
    }

    private function setAcl($permissoes, $rotasDoSistema)
    {
        $this->setNegarOuPermitirAcesso($permissoes, $rotasDoSistema);
    }

    /**
     * @apiVersion 1.0.0
     * @apiName setNegarOuPermitirAcesso
     * @apiGroup Acess Control List
     *
     * @apiSuccessExample {array} Success Urls do sidebar:
     *     HTTP/1.1 200 OK
     *     {
     *     }
     * @apiErrorExample {json} Houve algum erro
     *    HTTP/1.1 500 Internal Server Error
     */
    private function setNegarOuPermitirAcesso($permissoes, $rotasDoSistema)
    {
        $this->addRole('tipo-usuario');

        foreach ($rotasDoSistema as $key => $rota) {
            $this->addResource($rota);
        }

        foreach ($permissoes as $n => $permissao) {
            if ($permissao->getPrivilegios() != null) {
                $this->allow('tipo-usuario', $permissao->getUrl(), $permissao->getPrivilegios());
            }
        }
    }


}
