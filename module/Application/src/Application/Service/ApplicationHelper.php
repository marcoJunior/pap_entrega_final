<?php

namespace Application\Service;

use APIHelper\Service\ServiceHelper;

class ApplicationHelper extends ServiceHelper
{

    /**
     * @return \ControleAcesso\Service\ControleAcesso
     */
    public function serviceApplication()
    {
        return $this->getServiceManager()->get(\Application\Service\Application::class);
    }

    /**
     * @return \ControleAcesso\Service\ControleAcesso
     */
    public function serviceControleAcesso()
    {
        return $this->getServiceManager()->get(\ControleAcesso\Service\ControleAcesso::class);
    }
}
