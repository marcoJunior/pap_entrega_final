<?php

namespace Application\Service;

use APIHelper\Service\ServiceHelper;

class Cookie extends ServiceHelper
{

    /**
     * Criar cookies
     * @param string $name
     * @param string $value
     * @param integer $days
     */
    static public function set($name, $value, $days)
    {
        return setcookie($name, $value, time() + $days * 24 * 60 * 60);
    }

    /**
     * Busca o cookie por nome
     * @param string $name
     * @return string
     */
    static public function get($name = '')
    {
        $cookie = filter_input(INPUT_COOKIE, $name);
        return $cookie =='deleted' ? false : $cookie;
    }

    /**
     * Excluir cookies
     * @param string $name
     */
    static public function delete($name, $days = 365)
    {
        return setcookie($name, "", (time() + $days * 24 * 60 * 60));
    }

}
