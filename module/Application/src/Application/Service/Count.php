<?php

namespace Application\Service;

use Application\Entity\Pesquisa;
use Application\Entity\Count as CountEntity;
use APIHelper\Service\ServiceHelper;

class Count extends ServiceHelper
{

    public function selecionarCount(Pesquisa $pesquisa, $tabela, $referencia)
    {
        $mapper = $this
            ->getServiceManager()
            ->get('Application\Mapper\Count');

        $dbAdapter = $this->getServiceManager()->get($referencia);
        $mapper->setDbAdapter($dbAdapter);
        $mapper->setTableName($tabela);

        $total = $mapper->selecionarCount($pesquisa);

        return $this->getCount($total, $pesquisa);
    }

    public function getCount($total, Pesquisa $pesquisa)
    {
        $count = new CountEntity();
        $count->setTotalRegistros($total);
        $count->setQuantidadePaginas(ceil($count->getTotalRegistros() / $pesquisa->getQuantidade()));
        $count->setPagina($pesquisa->getPagina() + 1);
        return $count;
    }

}
