<?php

namespace Application\Service;

use APIHelper\Service\ServiceHelper;
use Application\Entity\DropZone as DropZoneEntity;

class DropZone extends ServiceHelper
{

    /**
     *  UPLOAD DE ARQUIVO DO TIPO .JPG, .PNG, PARA LOGO DE EMPRESAS.
     */
    static public function enviar(DropZoneEntity $dropZone)
    {
        if (!empty($_FILES)) {
            
            $dropZone->setArqNomeTemp($_FILES['file']['tmp_name']);
            $dropZone->setDiretorio($dropZone->getDiretorio() . $_FILES['file']['name']);

            return move_uploaded_file($dropZone->getArqNomeTemp(),
                    $dropZone->getDiretorio());
        }

        return false;
    }

}
