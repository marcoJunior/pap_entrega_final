<?php

namespace Application\Service;

//acl
use Application\Service\AclUsuarios;
//
use Zend\Mvc\MvcEvent;
use Application\Service\ApplicationHelper;

class Events extends ApplicationHelper
{

    protected $sm = null;

    public function __construct($sm)
    {
        $this->sm = $sm;
        return $this->initAcl();
    }

    public function initAcl()
    {

        $zendAutenticacao = $this->sm->get(\Zend\Authentication\AuthenticationService::class);
        $usuario = $zendAutenticacao->authenticate()->getIdentity();

        if ($usuario === null) {
            return;
        }

//        if ($usuario->getAcl() === null) {
        $servicesPermissoes = $this->sm->get(\Usuarios\Service\Permissoes::class);
        $rotasPersonalizadas = $this->sm->get('RotasPersonalizadas');

        if ($usuario) {
            $permissoes = $servicesPermissoes->selecionarPorTipo((int) $usuario->getUsuario()->getIdTipo());

            $usuario->setAcl(
                new AclUsuarios($permissoes, $rotasPersonalizadas->getTodasRotas())
            );
        }
        return $usuario->getAcl();
//        }
    }

    public function getAcl()
    {
        $zendAutenticacao = $this->sm->get(\Zend\Authentication\AuthenticationService::class);
        $usuario = $zendAutenticacao->authenticate()->getIdentity();

        if ($usuario === null) {
            return;
        }

        return $usuario->getAcl();
    }

}
