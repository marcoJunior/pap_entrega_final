<?php

namespace Application\Service;

use Application\Entity\Pesquisa;
use Application\Exception\Exception;
use APIHelper\Entity\AbstractEntity;
use Zend\Db\ResultSet\HydratingResultSet;

/**
 * Interface que define os metodos de (C)reate (R)ead (U)pdate (D)elete das services
 * CRUD significa (C)reate (R)ead (U)pdate (D)elete
 */
interface ServiceCrud
{
    /**
     * Responsável por buscar entidades do banco com páginação
     * @param  $pesquisa Pesquisa
     * @return HydratingResultSet
     */
    public function selecionar(Pesquisa $pesquisa);

    /**
     * Responsável por buscar entidades do banco com páginação e converter
     *  para array.
     *
     * Usado para retornar para o javascript em json com os nomes das colunas
     *  tratado pelo hydrator.
     *
     * @param  $pesquisa Pesquisa
     * @return array
     */
    public function selecionarArray(Pesquisa $pesquisa);

    /**
     * Responsável por buscar uma unica entidade do banco
     * @param  int  $id
     * @throws Exception
     * @return AbstractEntity
     */
    public function selecionarPorId($id);

    /**
     * Responsável por deletar um item do banco
     * @throws Exception
     * @param  int  $id
     */
    public function deletarPorId($id);

    /**
     * Responsável por editar um item do banco
     * @param  int $id
     * @param  array $data
     * @throws Exception
     * @return AbstractEntity
     */
    public function editarPorId($id, array $data);

    /**
     * Responsável por inserir um item do banco
     * @param  array $data
     * @throws Exception
     * @return AbstractEntity
     */
    public function inserir(array $data);
}
