<?php

namespace Application\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class ApplicationOffLine extends ViewModel
{

    /**
     * @var PhpRenderer
     */
    protected $projetoVersao = '1.0';
    protected $template = 'application';

    /**
     * @var ViewModel
     * $pagina->getVariable('mostrar') boolean
     */
    protected $paginas;

    public function __construct(PhpRenderer $renderer, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);


        $this->setVariable('v', $this->projetoVersao);
        $this->init($renderer);
    }

    /**
     * Seta o html
     */
    public function init($render)
    {
        $this->topNavigation();
        $this->sidebar();
//        $this->footer();

        /**
         * Title
         */
        $render->headTitle('ERP TPWS');
        /**
         * JS
         */
        $this->setJs($render);
        /**
         * CSS
         */
        $this->setCss($render);
        /**
         * Favicon
         */
        $render->headLink(['rel' => 'shortcut icon', 'type' => 'image/x-icon', 'href' => $render->basePath('img/favicon.ico')]);
        /**
         * Meta's
         */
        $render->headMeta()->appendHttpEquiv('Content-Type',
                'text/html; charset=utf-8');
        $render->headMeta()->appendName('robots', 'INDEX,FOLLOW');
        $render->headMeta()->appendName('viewport',
                'width=device-width, initial-scale=1.0');
        $render->headMeta()->appendName('description', 'ERP TPWS');
    }

    public function footer()
    {
        $view = new ViewModel;
        $view->setTemplate('layout-offline/footer');
        $view->setCaptureTo('footer');
        $this->addChild($view);
    }

    public function sidebar()
    {
        $view = new ViewModel;
        $view->setTemplate('layout-offline/sidebar');
        $view->setCaptureTo('sidebar');
        $this->addChild($view);
    }

    public function topNavigation()
    {
        $view = new ViewModel;
        $view->setTemplate('layout-offline/top-navigation');
        $view->setCaptureTo('topNavigation');
        $view->setVariables($this->getVariables());

        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel;
        $view->setTemplate('modal');
        $view->setCaptureTo('modal');
        $view->setVariables($this->getVariables());

        $this->addChild($view);
    }

    public function setJs($render)
    {
        foreach (array_reverse($this->getJs()) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo;
            } else if (is_array($arquivo)){
                $js = $arquivo[0];
            } else {
                $js = $render->basePath($arquivo . "?" . $this->getVariable('v'));
            }
            $render->headScript()->prependFile($js);
        }
    }

    public function setCss($render)
    {
        foreach (array_reverse($this->getCss()) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo;
            } else if (is_array($arquivo)){
                $css = $arquivo[0];
            } else {
                $css = $render->basePath($arquivo . "?" . $this->getVariable('v'));
            }

            $render->headLink()->prependStylesheet($css);
        }
    }

    public function getCss()
    {
        return [
            "https://www.google.com/uds/api/visualization/1.1/8ab533e17577f1138b5bcef1fcc75d5a/ui+pt_BR.css",
            '../font-awesome/css/font-awesome.min.css',
            '../metronic/assets/global/css/components.css',
            '../metronic/assets/global/plugins/select2/select2.css',
            '../metronic/assets/global/plugins/bootstrap/css/bootstrap.min.css',
            '../metronic/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',

            '../metronic/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
            '../metronic/assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
            '../metronic/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',

            '../metronic/assets/global/plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css',
            '../metronic/assets/global/plugins/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css',
            '../metronic/assets/global/plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css',
            '../metronic/assets/global/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css',

            '../metronic/assets/admin/layout4/css/layout.css',
            '../metronic/assets/admin/layout4/css/sidebar.css',
            '../metronic/assets/admin/layout4/css/themes/light.css',
            '../metronic/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',

            '../js/bibliotecas/bootstrap-toastr/toastr.min.css',
            '../metronic/assets/global/plugins/vanillatoasts/vanillatoasts.css',
            '../scripts/dropzone/dropzone.css',
            '../metronic/assets/admin/pages/css/profile.css',

            '../APIGridGitLab/public/css/api_grid.css',
            '../metronic/assets/global/plugins/bootstrap-tabdrop/css/tabdrop.css',
        ];
    }

    public function getJs()
    {
        return [
            '../metronic/assets/global/plugins/jquery/dist/jquery.min.js',
            '../metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js',
            "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['line', 'corechart','bar']}]}",
            "https://www.google.com/uds/api/visualization/1.1/8ab533e17577f1138b5bcef1fcc75d5a/dygraph,webfontloader,format+pt_BR,default+pt_BR,ui+pt_BR,line+pt_BR,bar+pt_BR,corechart+pt_BR.I.js",
            '../js/bibliotecas/validator.min.js',
            '../js/bibliotecas/jquery-mask/jquery-mask.js',
            '../metronic/assets/global/scripts/metronic.js',
            '../metronic/assets/admin/layout4/scripts/layout.js',
            '../metronic/assets/global/plugins/jquery.blockui.min.js',
            '../metronic/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
            '../metronic/assets/global/plugins/moment/moment.js',
            '../metronic/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            '../metronic/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
            '../metronic/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',

            '../metronic/assets/global/plugins/datatables.net/js/jquery.dataTables.min.js',
            '../metronic/assets/global/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js',
            '../metronic/assets/global/plugins/datatables.net-buttons/js/dataTables.buttons.min.js',
            '../metronic/assets/global/plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
            '../metronic/assets/global/plugins/datatables.net-buttons/js/buttons.flash.min.js',
            '../metronic/assets/global/plugins/datatables.net-buttons/js/buttons.html5.min.js',
            '../metronic/assets/global/plugins/datatables.net-buttons/js/buttons.print.min.js',
            '../metronic/assets/global/plugins/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js',
            '../metronic/assets/global/plugins/datatables.net-keytable/js/dataTables.keyTable.min.js',
            '../metronic/assets/global/plugins/datatables.net-responsive/js/dataTables.responsive.min.js',
            '../metronic/assets/global/plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js',
            '../metronic/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js',

            '../APIGridGitLab/public/js/api_grid.js',

            '../js/bibliotecas/bootstrap-toastr/toastr.min.js',
            '../metronic/assets/global/plugins/vanillatoasts/vanillatoasts.js',
            '../scripts/dropzone/dropzone.js',
            '../js/init.js',
            '../scripts/chart-settings.js',
            '../scripts/apifiltro/init.js',
            '../app/manutencao/formulario.js',
        ];
    }

}