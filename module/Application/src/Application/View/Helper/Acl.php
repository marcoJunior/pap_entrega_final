<?php

namespace Application\View\Helper;

use Application\Service\AclUsuarios;
use Zend\View\Helper\AbstractHelper;

class Acl extends AbstractHelper
{

    protected $acl = null;

    function getAcl()
    {
        return $this->acl;
    }

    function setAcl($acl)
    {
        $this->acl = $acl;
    }

    public function __invoke()
    {
        if ($this->getAcl() == null) {
            $sm = $this->getView()->getHelperPluginManager()->getServiceLocator();
            $this->setAcl($sm->get(\Application\Service\Events::class)->getAcl());
        }
        return $this->getAcl();
    }

}
