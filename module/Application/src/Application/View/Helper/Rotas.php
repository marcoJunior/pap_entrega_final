<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class Rotas extends AbstractHelper
{

    protected $rotas = null;
    protected $todasRotas = null;
    protected $gruposRotas = [
        'dashboard' => 'Dashboard',
        'sistemas' => 'Sistemas',
        'recursos' => 'Recursos',
        'clientes' => 'Clientes',
        'usuarios' => 'Usuarios',
        'logs' => 'Logs',
    ];

    public function getTodasRotas()
    {
        return $this->todasRotas;
    }

    public function getRotas()
    {
        return $this->rotas;
    }

    public function setRotas($rotas)
    {
        $this->rotas = $rotas;
    }

    public function addRota($rota, $key = null)
    {
        $this->todasRotas[] = $rota;
        $this->rotas[$key][] = $rota;
    }

    public function __invoke()
    {
        return $this->carregarRotas();
    }

    function __construct($sm = null)
    {
        return array_reverse($this->carregarRotas($sm));
    }

    private function carregarRotas($sm = null)
    {
        if ($this->getRotas() === null) {

            if ($sm !== null) {
                $rotas = $sm->get('Router');
            } else {

                $rotas = $this->getView()
                        ->getHelperPluginManager()
                        ->getServiceLocator()->get('Router');
            }

            //$rota = new \Zend\Mvc\Router\Http\Segment($key);
            foreach ($rotas->getRoutes() as $key => $value) {

                if ($key !== "/") {
                    $chave = $this->getDescricaoRota(explode('/', $key)[0]);
                    $this->addRota($value->assemble(), trim($chave) !== '' ? $chave : null);
                }
            }
        }

        return $this->getRotas();
    }

    private function getDescricaoRota($nomeRota)
    {
        return isset($this->gruposRotas[$nomeRota]) ? $this->gruposRotas[$nomeRota] : 'Outras';
    }
}
