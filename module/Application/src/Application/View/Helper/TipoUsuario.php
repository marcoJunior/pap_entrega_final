<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;

class TipoUsuario extends AbstractHelper
{

    protected $tipoUsuario = null;

    public function getTipoUsuario()
    {
        return $this->tipoUsuario;
    }

    public function __invoke()
    {
        return $this->carregarTiposUsuarios();
    }

    private function carregarTiposUsuarios()
    {
        if ($this->getTipoUsuario() === null) {

            $sm = $this->getView()
                ->getHelperPluginManager()
                ->getServiceLocator();

            $serviceTiposPermissoes = $sm->get(\Usuarios\Service\TiposPermissoes::class);
            $this->tipoUsuario = $serviceTiposPermissoes->selecionarObjeto();
        }

        return $this->getTipoUsuario();
    }

}
