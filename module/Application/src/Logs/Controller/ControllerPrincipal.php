<?php

namespace Logs\Controller;

use APIGrid\Controller\APIGridController;

class ControllerPrincipal extends APIGridController
{

    /**
     * @return \Logs\Service\Log
     */
    public function serviceLogsAcessos()
    {
        return $this->getService()->get(\Logs\Service\Log::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
