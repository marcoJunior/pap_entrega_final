<?php

namespace Logs\Controller;

use Logs\View\Log;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\View\Application;

class LogController extends ControllerPrincipal
{

  public function layoutAction()
  {
      $get = $this->getRequest()->getQuery();

      $phpRenderer = $this->getService()->get('ViewRenderer');
      $viewLogAcessos = new Log($phpRenderer);
      $viewLogAcessos->setVariable('titulo','Logs');
      $viewLogAcessos->grid();
      $viewLogAcessos->formulario();
      $viewLogAcessos->modal();

      $service = $this->serviceFiltro();

      $filtros[] = [
          'intervaloData' => [
              'intervaloData' => $get->toArray(),
              'valor' => $get->toArray()
          ]
      ];

      $viewLogAcessos->setVariable('filtros', $service->loadFiltros($filtros));

      $app = new Application($phpRenderer);
      $app->addChild($viewLogAcessos);
      return $app;

  }

    public function selecionarAction()
    {
        $service = $this->serviceLogsAcessos();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionar($post,$get));
    }

    public function selecionarUnicoAction()
    {
        $service = $this->serviceLogsAcessos();
        $get = $this->getRequest()->getQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->getClientes($post,$get));
    }

    public function alterarAction()
    {
        $service = $this->serviceLogsAcessos();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function adicaoAction()
    {
        $service = $this->serviceLogsAcessos();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceLogsAcessos();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
