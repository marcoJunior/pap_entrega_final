<?php

namespace Logs\Entity;

use Application\Entity\EntityHelper;

class Log extends EntityHelper
{

    protected $data;
    protected $ip;
    protected $apelido;
    protected $login;
    protected $sistema;

    /**
    * @return mixed
    */
    public function getData()
    {
        return $this->data;
    }

    /**
    * @param mixed data
    */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
    * @return mixed
    */
    public function getIp()
    {
        return $this->ip;
    }

    /**
    * @param mixed ip
    */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
    * @return mixed
    */
    public function getApelido()
    {
        return $this->apelido;
    }

    /**
    * @param mixed apelido
    */
    public function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    /**
    * @return mixed
    */
    public function getLogin()
    {
        return $this->login;
    }

    /**
    * @param mixed login
    */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
    * @return mixed
    */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
    * @param mixed sistema
    */
    public function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }
}
