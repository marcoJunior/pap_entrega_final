<?php

namespace Logs\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Log extends Hydrator
{

    protected function getEntity()
    {
        return 'Logs\Entity\Log';
    }

    public function getMap()
    {
        return [
            'data'       => 'cfidtalog',
            'ip'            => 'cfiniplog',
            'login'       => 'cfilgnlog',
            'sistema' => 'cfisislog',
            'apelido'  => 'cfiapelog'

        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Log();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
