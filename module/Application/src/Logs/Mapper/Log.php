<?php

namespace Logs\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Log extends APIGrid
{

    public $tableName = 'mxlogcfi';
    public $mapperName = 'Logs\Mapper\Hydrator\Log';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setJoin(
                new APIGridJoinEntity('mxlgncfi',
                'mxlogcfi.cfilgnlog = mxlgncfi.cficodlgn',
                ['cfilgnlog' => new Expression("mxlgncfi.cfiusulgn")], ''
        ));

        $this->setLimitOffset(true);

        $this->setWhereRetiraIpLocal();

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '',
                    str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    function setWhereRetiraIpLocal()
    {
        $where = new Where();

        $where->notLike('cfiniplog', '192.168.1%');
        $where->notEqualTo('cfiniplog', '127.0.0.1');
        $where->notEqualTo('cfiniplog', '191.255.244.170');
        $this->setWhere($where);
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Intervalo de data
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereIntervalodata(FiltrosEntity $filtros)
    {

        $where = new Where();
        $inicial = new \DateTime(str_replace('/', '-',
                        $filtros->getIntervaloData()->getInicial()));
        $final = new \DateTime(str_replace('/', '-',
                        $filtros->getIntervaloData()->getFinal()));

        $where->between('cfidtalog', $inicial->format('d/m/Y'),
                $final->format('d/m/Y'));
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'cfidtalog' => new Expression(" to_char(cfidtalog, 'DD/MM/YYYY HH:MI') "),
            'cfiniplog',
            'cfilgnlog',
            'cfisislog',
            'cfiapelog'
        ];
    }

    public function adicao($formulario, APIGridEntityAction $postEntity)
    {
        $entityFormaulario = new ModulosValidadeEntity();
        $entityFormaulario->exchangeArray($formulario);
        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        try {

            $select = $this->getSelect()
                    ->columns($this->getColunas());

            $where = new Where();
            $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
            $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

            $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

            if (count($dbVerificaExistencia) > 0) {
                return "Cadastro já existente !";
            }
        } catch (Exception $exc) {
            return false;
        }

        return $this->insert($lista)->getAffectedRows();
    }

    public function alterar($formulario)
    {
        $entityFormaulario = new ModulosValidadeEntity();
        $entityFormaulario->exchangeArray($formulario);

        $where = new Where();
        $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
        $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        return $this->update($lista, $where)->getAffectedRows();
    }

    public function excluir($codigo, $modulo)
    {
        $where = new Where();
        $where->equalTo('cfivldmva', $codigo);
        $where->equalTo('cfimdlmva', $modulo);

        return $this->delete($where)->getAffectedRows();
    }

}
