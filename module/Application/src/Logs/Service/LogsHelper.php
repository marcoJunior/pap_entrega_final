<?php

namespace Logs\Service;

use APIGrid\Service\APIGrid;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class LogsHelper extends APIGrid
{

    /**
     * @return Logs\Mapper\Log
     */
    public function mapperLogsAcessos()
    {
        return $this->getServiceManager()->get('Logs\Mapper\Log');
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

}
