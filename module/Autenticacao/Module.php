<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Autenticacao;

use Zend\Mvc\MvcEvent;
use Zend\Authentication\AuthenticationService;
use Zend\Stdlib\Hydrator\ClassMethods;
use Zend\ServiceManager\AbstractFactoryInterface;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $app = $e->getApplication();
        $sm = $app->getServiceManager();

        $app->getEventManager()->attach('route', [$sm->get(Service\ControleAcesso::class), 'checarIdentidadePermitida']);

//        $app->getEventManager()->attach('route', [$sm->get(Service\LogsAcesso::class), 'gravarLog']);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\ControleAcesso::class => function ($sm) {
                    return new Service\ControleAcesso(
                        $sm->get(\Zend\Authentication\AuthenticationService::class)
                    );
                },
                Adapter\DbCliente::class => function ($sm) {
                    $service = new Adapter\DbCliente();
                    return $service->setServiceManager($sm);
                },
                Service\Autenticacao::class => function ($sm) {
                    $service = new Service\Autenticacao();
                    return $service->setServiceManager($sm);
                },
                Service\Usuarios::class => function ($sm) {
                    $service = new Service\Usuarios();
                    return $service->setServiceManager($sm);
                },
                Service\Conexao::class => function ($sm) {
                    $service = new Service\Conexao();
                    return $service->setServiceManager($sm);
                },
                Service\LogsAcesso::class => function ($sm) {
                    $service = new Service\LogsAcesso();
                    return $service->setServiceManager($sm);
                },
                Service\Cookie::class => function ($sm) {
                    $service = new Service\Cookie();
                    return $service->setServiceManager($sm);
                },
                ///////////////////////////////////
                \Zend\Authentication\AuthenticationService::class => function ($sm) {
                    $serviceAuth = new AuthenticationService();
                    $adapter = $sm->get(Adapter\DbCliente::class);
                    $serviceAuth->setAdapter($adapter);

                    return $serviceAuth;
                },
                'Autenticacao\Mapper\Conexao' => function ($sm) {
                    $mapper = new Mapper\Conexao();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Entity\Conexao())
                        ->setHydrator(new ClassMethods());

                    return $mapper;
                },
                'Autenticacao\Mapper\LogsAcesso' => function ($sm) {
                    $mapper = new Mapper\LogsAcesso();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Entity\LogAcesso())
                        ->setHydrator(new ClassMethods());

                    return $mapper;
                },
                'Autenticacao\Mapper\Usuarios' => function ($sm) {
                    $mapper = new Mapper\Usuarios();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Entity\Usuario())
                        ->setHydrator(new ClassMethods());

                    return $mapper;
                },
            ),
        );
    }

}
