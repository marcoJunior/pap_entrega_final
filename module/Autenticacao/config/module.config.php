<?php

namespace Autenticacao;

return [
    'controllers' => [
        'factories' => [
            \Autenticacao\Controller\IndexController::class => function ($sl) {
                $controller = new \Autenticacao\Controller\IndexController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ],
    'router' => [
        'routes' => [
            'autenticacao' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => \Autenticacao\Controller\IndexController::class,
                        'action' => 'index',
                        'redirectUrl' => '/dashboard'
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'entrar' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'entrar',
                            'defaults' => [
                                'controller' => \Autenticacao\Controller\IndexController::class,
                                'action' => 'entrar',
                            ],
                        ],
                    ],
                    'sair' => [
                        'type' => 'Segment',
                        'options' => [
                            'route' => 'sair',
                            'defaults' => [
                                'controller' => \Autenticacao\Controller\IndexController::class,
                                'action' => 'sair',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
];
