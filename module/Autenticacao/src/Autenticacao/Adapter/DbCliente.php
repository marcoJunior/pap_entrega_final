<?php

namespace Autenticacao\Adapter;

use Autenticacao\Service;
use Autenticacao\Service\AutenticacaoHelper;
use Autenticacao\Entity\Identidade;
use Autenticacao\Exception\RuntimeException;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

class DbCliente extends AutenticacaoHelper implements AdapterInterface
{

    public function authenticate()
    {
        if ($identidade = $this->authenticationService()->getIdentity()) {
            return new Result(Result::SUCCESS, $identidade);
        }

        $token = $this->getToken();

        if (!$token) {
            return new Result(Result::FAILURE, null);
        }

        try {
            $identidade = $this->buscarIdentidade($token);
            return new Result(Result::SUCCESS, $identidade);
        } catch (RuntimeException $e) {
            return new Result(Result::FAILURE, null);
        }
    }

    /**
     * Busca o token seja na service Autenticacao (Requisição do login) ou no
     * Cookie
     * @return string|null
     */
    private function getToken()
    {
        $token = $this->serviceAutenticacao()->getToken();
        if ($token)
            return $token;
        $token = $this->getServiceManager()->get(Service\Cookie::class)->get('token_login');
        if ($token)
            return $token;
    }

    /**
     * Faz query na tabela de conexão com join na de usuarios a partid do token
     * e monta Objeto de Identidade
     * @param  string $token
     * @return Identidade
     */
    private function buscarIdentidade($token)
    {
        $con = $this->serviceConexao()->selecionarIdentidadePorToken($token);
        return new Identidade($con);
    }

}
