<?php

namespace Autenticacao\Controller;

use Zend\View\Model\JsonModel;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{

    protected $service;

    public function getService()
    {
        return $this->service;
    }

    public function setService($service)
    {
        $this->service = $service;
    }

    public function indexAction()
    {

        if (!$this->identity()) {
            $this->layout('autenticacao/index');
        } else {
            $this->redirect()->toUrl($this->params('redirectUrl'));
        }
        return $this;
    }

    public function entrarAction()
    {

        $sl = $this->getService();
        $autenticacao = $sl->get(\Autenticacao\Service\Autenticacao::class);
        $zendAutenticacao = $sl->get(\Zend\Authentication\AuthenticationService::class);

        $autenticacao->executarLogin($this->request->getPost());
        $identity = $zendAutenticacao->authenticate()->getIdentity();

        if ($identity) {
            return new JsonModel(['url' => '/dashboard']);
        }

        $this->response->setStatusCode(401);
        return new JsonModel(['mensagem' => 'Acesso negado']);
    }

    public function sairAction()
    {
        $cookie = $this->getService()->get(\Autenticacao\Service\Cookie::class);
        $cookie->delete('PHPSESSID');
        $cookie->delete('token_login');
        $this->redirect()->toUrl('/');
    }

}
