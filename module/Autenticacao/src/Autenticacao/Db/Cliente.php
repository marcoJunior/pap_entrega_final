<?php

namespace Autenticacao\Db;

use Autenticacao\Entity\Identidade;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class Cliente implements FactoryInterface
{
    /**
     * @param ServiceLocatorInterface $sl
     * @return Adapter
     */
    public function createService(ServiceLocatorInterface $sl)
    {
        $serviceAuth = $sl->get('Zend\Authentication\AuthenticationService');
        $identity = $serviceAuth->authenticate()->getIdentity();

        if (!$identity) {
            $sl->get('Autenticacao\Service\Autenticacao')->redirecionar();
            return;
        }

        $dbname = $identity->getConexao()->getBanco();
        $host = $identity->getConexao()->getIp();
        $porta = $identity->getConexao()->getPorta();
        $banco = $sl->get('Config')['banco'];

        return new Adapter([
            'driver' => 'Pdo',
            'dsn' => "$banco:dbname=$dbname;host=$host;port=$porta",
            'username' => $identity->getConexao()->getUsuarioBanco(),
            'password' => $identity->getConexao()->getSenhaBanco(),
        ]);
    }
}
