<?php

namespace Autenticacao\Entity;

use Traversable;

abstract class AbstractEntity
{

    function __construct($data = null)
    {
        if ($data) {
            $this->exchangeArray($data);
        }
    }

    public function exchangeArray($data)
    {
        if (!(is_array($data) || $data instanceof Traversable)) {
            throw new \InvalidArgumentException('O parametro deve ser array ou Traversable');
        }

        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
        return $this;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }

}
