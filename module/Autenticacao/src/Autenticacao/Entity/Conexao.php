<?php

namespace Autenticacao\Entity;

class Conexao extends AbstractEntity
{
    protected $id;
    protected $ip;
    protected $porta;
    protected $usuarioBanco;
    protected $banco;
    protected $idSistema;
    protected $idClienteMax;
    protected $senhaBanco;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    public function getIp()
    {
        return $this->ip;
    }

    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    public function getPorta()
    {
        return $this->porta;
    }

    public function setPorta($porta)
    {
        $this->porta = $porta;
        return $this;
    }

    public function getUsuarioBanco()
    {
        return $this->usuarioBanco;
    }

    public function setUsuarioBanco($usuarioBanco)
    {
        $this->usuarioBanco = $usuarioBanco;
        return $this;
    }

    public function getBanco()
    {
        return $this->banco;
    }

    public function setBanco($banco)
    {
        $this->banco = $banco;
        return $this;
    }

    public function getIdSistema()
    {
        return $this->idSistema;
    }

    public function setIdSistema($idSistema)
    {
        $this->idSistema = (int) $idSistema;
        return $this;
    }

    public function getIdClienteMax()
    {
        return $this->idClienteMax;
    }

    public function setIdClienteMax($idClienteMax)
    {
        $this->idClienteMax = (int) $idClienteMax;
        return $this;
    }

    public function getSenhaBanco()
    {
        return $this->senhaBanco;
    }

    public function setSenhaBanco($senhaBanco)
    {
        $this->senhaBanco = $senhaBanco;
        return $this;
    }

}
