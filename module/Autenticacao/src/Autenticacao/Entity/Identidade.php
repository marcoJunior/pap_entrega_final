<?php

namespace Autenticacao\Entity;

use Zend\Db\ResultSet\HydratingResultSet;

class Identidade extends AbstractEntity
{

    /**
     * @var Conexao
     */
    protected $conexao;

    /**
     * @var Usuario
     */
    protected $usuario;

    /**
     * @var Acl
     */
    protected $acl;

    /**
     * @var string
     */
    protected $funcao = 'admin';

    public function __construct(HydratingResultSet $resultSet)
    {
        $resultSet->setObjectPrototype(new Conexao());
        $this->setConexao($resultSet->current());
        $resultSet->setObjectPrototype(new Usuario());
        $this->setUsuario($resultSet->current());
        return $this;
    }

    public function getConexao()
    {
        return $this->conexao;
    }

    public function setConexao(Conexao $conexao)
    {
        $this->conexao = $conexao;
        return $this;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario(Usuario $usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getFuncao()
    {
        return $this->funcao;
    }

    public function setFuncao($funcao)
    {
        $this->funcao = $funcao;
        return $this;
    }

    public function getAcl()
    {
        return $this->acl;
    }

    public function setAcl($acl)
    {
        $this->acl = $acl;
    }

}
