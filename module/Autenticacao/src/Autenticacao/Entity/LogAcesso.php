<?php

namespace Autenticacao\Entity;


class LogAcesso extends AbstractEntity
{
    const METODO_GET = 1;
    const METODO_POST = 2;
    const METODO_PUT = 3;
    const METODO_DELETE = 4;

    protected $id;
    // protected $sistemaId;
    protected $usuarioId;

    /**
     * Método de acesso
     * @return flag METODO_
     */
    protected $metodoId;
    protected $url;
    protected $json;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    // public function getSistemaId()
    // {
    //     return $this->sistemaId;
    // }
    //
    // public function setSistemaId($sistemaId)
    // {
    //     $this->sistemaId = $sistemaId;
    //     return $this;
    // }

    public function getUsuarioId()
    {
        return $this->usuarioId;
    }

    public function setUsuarioId($usuarioId)
    {
        $this->usuarioId = $usuarioId;
        return $this;
    }

    public function getMetodoId()
    {
        return $this->metodoId;
    }

    public function setMetodoId($metodoId)
    {
        $this->metodoId = $metodoId;
        return $this;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    public function getJson()
    {
        return $this->json;
    }

    public function setJson($json)
    {
        $this->json = $json;
        return $this;
    }

}
