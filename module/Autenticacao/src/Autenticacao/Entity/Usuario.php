<?php

namespace Autenticacao\Entity;

class Usuario extends AbstractEntity
{

    protected $id;
    protected $usuario;
    protected $senha;
    protected $idConexao = 1;
    protected $idTipo = 0;
    protected $nomeCompleto;
    protected $tokenLogin;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = (int) $id;
        return $this;
    }

    public function getUsuario()
    {
        return $this->usuario;
    }

    public function setUsuario($usuario)
    {
        $this->usuario = $usuario;
        return $this;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function setSenha($senha)
    {
        $this->senha = $senha;
        return $this;
    }

    public function getIdConexao()
    {
        return $this->idConexao;
    }

    public function setIdConexao($idConexao)
    {
        $this->idConexao = (int) $idConexao;
        return $this;
    }

    public function getTokenLogin()
    {
        return $this->tokenLogin;
    }

    public function setTokenLogin($tokenLogin)
    {
        $this->tokenLogin = $tokenLogin;
        return $this;
    }

    public function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    public function setNomeCompleto($nomeCompleto)
    {
        $this->nomeCompleto = $nomeCompleto;
        return $this;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setIdTipo($idTipo)
    {
        $this->idTipo = (int) $idTipo;
    }

}
