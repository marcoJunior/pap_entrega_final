<?php

namespace Autenticacao\Mapper;

use Autenticacao\Mapper\Conexao as ConexaoEntity;
use Zend\Db\ResultSet\HydratingResultSet;
use ZfcBase\Mapper\AbstractDbMapper;

class Conexao extends AbstractDbMapper
{
    protected $tableName = 'conexao';

    /**
     * Retorna a conexão pelo token
     * @param  string $token
     * @return HydratingResultSet
     */
    public function selecionarIdentidadePorToken($token)
    {
        $select = $this->getSelect();
        $select->join('usuario', 'conexao.id = id_conexao');
        $select->where->equalTo('token_login', $token);
        $select->limit(1);

        return $this->select($select);
    }
}
