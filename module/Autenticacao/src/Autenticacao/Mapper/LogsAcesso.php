<?php

namespace Autenticacao\Mapper;

use Autenticacao\Entity\LogAcesso;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Db\Adapter\Driver\Pdo\Result;
use ZfcBase\Mapper\AbstractDbMapper;

class LogsAcesso extends AbstractDbMapper
{
    protected $tableName = 'log_acesso';

    /**
     * @param  LogAcesso $log
     * @return HydratingResultSet
     */
    public function inserir(LogAcesso $log): Result
    {
        return $this->insert($log);
    }
}
