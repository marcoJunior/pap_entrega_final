<?php

namespace Autenticacao\Mapper;

use Autenticacao\Entity\Usuario;
use ZfcBase\Mapper\AbstractDbMapper;

/**
 * @author Vinicius Meira <vinicius@maxscalla.com.br>
 */
class Usuarios extends AbstractDbMapper
{

    public $tableName = 'usuario';

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function editarPorId(Usuario $usuario)
    {
        return $this->update($usuario, ['id' => $usuario->getId()]);
    }

    /**
     * @param  string nomeOuEmail
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function selecionarPorNome($nome)
    {
        $select = $this->getSelect()->limit(1);
        $select->where->equalTo('usuario', $nome);
        return $this->select($select);
    }

}
