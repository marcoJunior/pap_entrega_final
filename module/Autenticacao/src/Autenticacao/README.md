# Autenticação
Submódulo para centralizar os logins dos projetos da Maxscalla

#### Dependências
 - `Zend >= 2.4`
 - Conexão com o banco csgestor_admin;
 - Configurar uma url para entrar e sair do sistema;
 - Configurar uma module `Autenticacao` no `application.config.php`
 - Definir parametro default `redirectUrl` para definir a url inicial do sistema
 - `$sm->get('Config')['banco']; ` deve fornecer o banco que será acessado `mysql` ou `pgsql` (chave configurada no global.php);
 - `$sm->get('\Autenticacao\Service\Cookie::class'); ` deve fornecer classe com metodos `get`, `set`, `delete` para cookies;

#### Adicionando submódulo a um projeto

```sh
mkdir -p module/Autenticacao/src
git submodule add -f -b master --name autenticacao git@gitlab.com:maxscalla/autenticacao.git module/Autenticacao/src/Autenticacao
git commit -m 'Adicionando submodulo'
git push
```
