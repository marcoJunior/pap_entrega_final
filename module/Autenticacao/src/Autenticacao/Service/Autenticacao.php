<?php

namespace Autenticacao\Service;

use Autenticacao\Entity\Usuario;
use Autenticacao\Exception\RuntimeException;
use Zend\Stdlib\Parameters;

class Autenticacao extends AutenticacaoHelper
{
    private $token;

    public function getToken()
    {
        return $this->token;
    }

    /**
     * Busca o usuário no banco, valida e gera um token para o login
     * @param  string $usuarioOuEmail
     * @param  string $senha
     */
    public function executarLogin(Parameters $data)
    {
        try {
            $usuario = $this->serviceUsuarios()->selecionarPorNome($data->usuario);

            if (!$this->validarUsuario($usuario, $data->senha)) {
                return;
            }

            $this->gerarTokenLogin();
            $this->atualizarTokenLogin($usuario, $this->token);
            if ($data->lembrar) {
                $this->cookie()->set('token_login', $this->token, 10);
            }
        } catch (RuntimeException $err) {
            return;
        }
    }

    /**
     * Verifica se o usuário que está fazendo o login pode acessar o sistema
     * @param  Usuario $usuario
     * @param  string $senha
     * @return boolean
     */
    public function validarUsuario(Usuario $usuario, $senha)
    {
        return $usuario->getSenha() === $senha;
    }

    /**
     * Gera o token do login usado para persistir o login
     * @return string
     */
    private function gerarTokenLogin()
    {
        $this->token = md5(time());
    }

    /**
     * Atualiza o banco
     * @param Usuario $usuario
     * @throws RuntimeException
     */
    private function atualizarTokenLogin(Usuario $usuario, $token)
    {
        $usuario->setTokenLogin($token);
        $result = $this->mapperUsuarios()->editarPorId($usuario);
        if (!$result->getAffectedRows()) {
            throw new RuntimeException("Não foi possível atualizar o token de login", 500);
        }
    }

    /**
     * Envia headers de redirecionamento
     * @param  MvcEvent $e
     */
    public function redirecionar()
    {
        $sm = $this->getServiceManager();
        $response = $sm->get('Response');
        $location = $sm->get('Request')->getBaseUrl() . "/";

        $response->setStatusCode(301);
        $response->getHeaders()->addHeaderLine('Location', $location);
        $response->sendHeaders();
    }

}
