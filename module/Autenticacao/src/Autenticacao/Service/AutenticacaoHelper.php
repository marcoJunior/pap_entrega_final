<?php

namespace Autenticacao\Service;

use Autenticacao\Mapper;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;

class AutenticacaoHelper //implements ServiceManagerAwareInterface
{

    protected $serviceManager;

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @return \Autenticacao\Service\Usuarios
     */
    protected function serviceUsuarios()
    {
        return $this->getServiceManager()->get(Usuarios::class);
    }

    /**
     * @return \Autenticacao\Mapper\Usuarios
     */
    protected function mapperUsuarios()
    {
        return $this->getServiceManager()->get(Mapper\Usuarios::class);
    }

    /**
     * @return \Autenticacao\Service\Conexao
     */
    protected function serviceConexao()
    {
        return $this->getServiceManager()->get(Conexao::class);
    }

    /**
     * @return \Autenticacao\Service\Autenticacao
     */
    protected function serviceAutenticacao()
    {
        return $this->getServiceManager()->get(Autenticacao::class);
    }

    /**
     * @return \Autenticacao\Mapper\Conexao
     */
    protected function mapperConexao()
    {
        return $this->getServiceManager()->get(Mapper\Conexao::class);
    }

    /**
     * @return \Autenticacao\Mapper\LogsAcesso
     */
    protected function mapperLogsAcesso()
    {
        return $this->getServiceManager()->get(Mapper\LogsAcesso::class);
    }

    protected function authenticationService()
    {
        return $this->getServiceManager()->get(\Zend\Authentication\AuthenticationService::class);
        
    }

    public function cookie()
    {
        return $this->getServiceManager()->get(\Autenticacao\Service\Cookie::class);
    }
}
