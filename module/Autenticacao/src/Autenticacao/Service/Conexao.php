<?php

namespace Autenticacao\Service;

use Autenticacao\Exception\RuntimeException;

class Conexao extends AutenticacaoHelper
{

    /**
     * Retonra a conexão pelo token
     * @param  string $token
     * @return \Autenticacao\Entity\Conexao
     * @throws RuntimeException
     */
    public function selecionarIdentidadePorToken($token)
    {
        $conexao = $this->mapperConexao()->selecionarIdentidadePorToken($token);
        if ($conexao->current()) return $conexao;
        throw new RuntimeException("Não foi possível estabelecer conexão com o token $token");
    }
}
