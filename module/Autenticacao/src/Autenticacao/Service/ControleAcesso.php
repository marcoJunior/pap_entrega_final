<?php

namespace Autenticacao\Service;

use Zend\Mvc\MvcEvent;

class ControleAcesso extends AutenticacaoHelper
{

    protected $authenticationService;

    public function __construct($authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    public function getAuthenticationService()
    {
        return $this->authenticationService;
    }

    /**
     * Checa se o usuário pode acessar o recurso solicitado
     * @param  MvcEvent $e
     */
    public function checarIdentidadePermitida(MvcEvent $e)
    {
        $auth = $this->getAuthenticationService();
        $route = $e->getRouteMatch()->getMatchedRouteName();
        $identity = $auth->authenticate()->getIdentity();

        if ($route !== 'loja/apps') {
            return;
        }

        if (
            !$identity &&
            ($route !== 'autenticacao' && $route !== 'autenticacao/entrar' )
        ) {
            $this->serviceAutenticacao()->redirecionar();
        }
    }

}
