<?php

namespace Autenticacao\Service;

use Autenticacao\Entity\LogAcesso;
use Autenticacao\Entity\Usuario;
use Autenticacao\Exception\RuntimeException;
use Zend\Db\ResultSet\HydratingResultSet;
use Zend\Json\Json;
use Zend\Http\Request;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\Parameters;

class LogsAcesso extends AutenticacaoHelper
{

    /**
     * Constroi a o log de acesso de acordo com o request e
     */
    public function gravarLog(MvcEvent $e)
    {
        $identity = $this->authenticationService()->getIdentity();
        if (!$identity) return;
        $log = new LogAcesso();
        $log->setUsuarioId($identity->getUsuario()->getId());

        $metodoId = $this->getMetodoId($e->getRequest());
        if ($metodoId === LogAcesso::METODO_GET) {
            return;
        }
        $log->setMetodoId($metodoId);

        $url = $e->getRequest()->getUriString();
        $log->setUrl($url);

        $json = $this->getJson($e->getRequest());
        if ($json) $log->setJson($json);
        $this->mapperLogsAcesso()->inserir($log);
    }

    private function getMetodoId(Request $request)
    {
        if ($request->isPost()) return LogAcesso::METODO_POST;
        else if ($request->isPut()) return LogAcesso::METODO_PUT;
        else if ($request->isDelete()) return LogAcesso::METODO_DELETE;
        else if ($request->isGet()) return LogAcesso::METODO_GET;
        else return LogAcesso::METODO_GET;
    }

    private function getJson(Request $request)
    {
        if ($request->getContent()) {
            return $request->getContent();
        } else if ($request->isPost()) {
            return Json::encode($request->getPost()->toArray());
        }
    }
}
