<?php

namespace Autenticacao\Service;

use Autenticacao\Entity\Usuario;
use Autenticacao\Exception\RuntimeException;
use Zend\Db\ResultSet\HydratingResultSet;

class Usuarios extends AutenticacaoHelper
{

    /**
     * Busca usuario por nome
     * @param  string  $nomeOuEmail
     * @return Usuario
     */
    public function selecionarPorNome($nome)
    {
        $usuario = $this->mapperUsuarios()->selecionarPorNome($nome)->current();
        if ($usuario) return $usuario;
        throw new RuntimeException("Usuário $nome não existe", 404);
    }
}
