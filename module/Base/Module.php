<?php

namespace Base;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use APISql\Model\Helper;
use APISql\Model\HelperTable;
use APISql\Model\Transaction;
use APISql\Model\TransactionTable;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Charts' => __DIR__ . '/src/Charts',
                    'Maps' => __DIR__ . '/src/Maps',
                    'APIDate' => __DIR__ . '/src/APIDate',
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                //APIDate
                'APIDate\Service\DayHollyday' => 'APIDate\Service\DayHollyday',
                //Maps
                'Mapeamento' => 'Maps\Service\Mapeamento',
                //Charts
                'ChartsService' => 'Charts\Service\Charts',
            ),
            'factories' => array(
                //APISql
                'APISql\Model\HelperTable' => function ($sm) {
                    $tableGateway = $sm->get('HelperTableGateway');
                    $table = new HelperTable($tableGateway);
                    return $table;
                },
                'HelperTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Helper());
                    return new TableGateway('mxcfwcfi', $dbAdapter, null, $resultSetPrototype);
                },
                'APISql\Model\TransactionTable' => function ($sm) {
                    $tableGateway = $sm->get('TransactionTableGateway');
                    $table = new TransactionTable($tableGateway);
                    return $table;
                },
                'TransactionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Transaction());
                    return new TableGateway('mxatucfi', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

}
