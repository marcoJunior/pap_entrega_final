<?php
 
/**
 * Arquivo utilizado para configuração de Url's e Templates
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Maps\Controller\Index' => 'Maps\Controller\IndexController',
        ),
    ),
    'router' => array(
        'routes' => array(
            '/mapeamento' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/mapeamento',
                    'defaults' => array(
                        'controller' => 'Maps\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
        ),
        'template_path_stack' => array(
            'apifiltro' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(),
        ),
    ),
);
