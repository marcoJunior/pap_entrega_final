<?php

namespace APIDate\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{

    public function dayHollydayAction()
    {
        $serviceDate = $this->getServiceLocator()
                ->get('APIDate\Service\DayHollyday');

        $serviceDate->serviceDayHollyday(3);
    }

    public function getTodayAction()
    {
        $serviceDate = $this->getServiceLocator()
                ->get('APIDate\Service\DayHollyday');

        $serviceDate->serviceToday();
    }

}
