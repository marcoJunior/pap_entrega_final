<?php

namespace APIDate\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class DayHollyday implements ServiceManagerAwareInterface
{

    protected $mesCorrente;
    protected $consumidor;

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    public function getXmlHollydays()
    {
        $json = @file_get_contents('http://www.google.com/calendar/feeds/brazilian__pt_br%40holiday.calendar.google.com/public/basic?alt=json');
        $objAux = (array) json_decode($json);
        $object = (array) $objAux["feed"];

        return $object;
    }

    public function serviceDayHollyday($amountOfHolidays)
    {
        $dayHollyday = array();
        if (isset($this->getXmlHollydays()["entry"])) {
            foreach ($this->getXmlHollydays()["entry"] as $key => $value) {
                $aux = (array) $value;
                $title = $this->configTitle((array) $aux["title"]);
                $date = $this->configSummary((array) $aux["summary"]);
                $today = date("d-m-Y");

                if (strtotime($date['date']) >= strtotime($today)) {
                    $dayHollyday[$date['key']]['title'] = $title;
                    $dayHollyday[$date['key']]['date'] = $date['date'];

                    $arrayListOrder[] = (int) $date['key'];
                }
            }

            $auxOrder = $this->bubbleSort($arrayListOrder);

            $resultHrml = '<li>Feriados no Brasil</li>';

            for ($i = 0; $i < $amountOfHolidays; $i++) {
                if (isset($auxOrder[$i])) {
                    $resultHrml .=
                            '<li>' . $dayHollyday[$auxOrder[$i]]['date']
                            . ' - '
                            . $dayHollyday[$auxOrder[$i]]['title']
                            . '</li>';
                }
            }

            echo $resultHrml;
        }
    }

    public function serviceToday()
    {
        $today = date('d F Y l');
        $auxArray = explode(' ', $today);
        echo '<ul class="event-list" style="overflow: hidden; width: auto; ">'
        . '<li><h4>' . $auxArray[0] . ' de ' . $this->translateMonth($auxArray[1]) . ', ' . $auxArray[2] . '</br>'
        . $this->translateDay($auxArray[3]) . '</h4></li>'
        . '</ul>'
        . '</center>';
    }

    public function translateMonth($month)
    {
        $translateMonth = array(
            'January' => 'Janeiro',
            'February' => 'Fevereiro',
            'March' => 'Março',
            'April' => 'Abril',
            'May' => 'Maio',
            'June' => 'Junho',
            'July' => 'Julho',
            'August' => 'Agosto',
            'September' => 'Setembro',
            'October' => 'Outubro',
            'November' => 'Novembro',
            'December' => 'Dezembro',
        );

        return $translateMonth[$month];
    }

    public function translateDay($day)
    {
        $translateDay = array(
            'Sunday' => 'Domingo',
            'Monday' => 'Segunda-feira',
            'Tuesday' => 'Terça-feira',
            'Wednesday' => 'Quarta-feira',
            'Thursday' => 'Quinta-feira',
            'Friday' => 'Sexta-feira',
            'Saturday' => 'Sabado',
        );

        return $translateDay[$day];
    }

    public function bubbleSort($array)
    {
        for ($cont1 = 0; $cont1 < count($array); $cont1++) {

            for ($cont2 = 0; $cont2 < (count($array) - 1); $cont2++) {

                if ($array[$cont2 + 1] <= $array[$cont2]) {
                    $aux = $array[$cont2];
                    $array[$cont2] = $array[$cont2 + 1];
                    $array[$cont2 + 1] = $aux;
                }
            }
        }

        return $array;
    }

    public function configTitle($title)
    {
        return $title['$t'];
    }

    public function configSummary($summary)
    {
        $aux = str_replace('<br>', ' ', $summary['$t']);
        $auxArray = explode(' ', $aux);

        $summary = array();
        $summary['date'] = $auxArray[2] . '-' . $this->getNumberMonthByString($auxArray[3]) . '-' . $auxArray[4];
        $summary['key'] = $auxArray[4] . $this->getNumberMonthByString($auxArray[3]) . (strlen($auxArray[2]) > 1 ? $auxArray[2] : '0' . $auxArray[2]);

        return $summary;
    }

    public function getNumberMonthByString($stringMonth)
    {
        $array = array(
            'jan' => '01',
            'fev' => '02',
            'mar' => '03',
            'abr' => '04',
            'mai' => '05',
            'jun' => '06',
            'jul' => '07',
            'ago' => '08',
            'set' => '09',
            'out' => '10',
            'nov' => '11',
            'dez' => '12',
        );

        return $array[$stringMonth];
    }

    /**
     * Retorna nome do mês
     * @return string
     */
    public function getMes()
    {
        switch (date("m")) {
            case "01": $mes = 'Janeiro';
                break;
            case "02": $mes = 'Fevereiro';
                break;
            case "03": $mes = 'Março';
                break;
            case "04": $mes = 'Abril';
                break;
            case "05": $mes = 'Maio';
                break;
            case "06": $mes = 'Junho';
                break;
            case "07": $mes = 'Julho';
                break;
            case "08": $mes = 'Agosto';
                break;
            case "09": $mes = 'Setembro';
                break;
            case "10": $mes = 'Outubro';
                break;
            case "11": $mes = 'Novembro';
                break;
            case "12": $mes = 'Dezembro';
                break;
        }

        return $mes;
    }

    public function getMonthByNumber($stringMonth)
    {
        $array = array(
            '1' => 'jan',
            '2' => 'fev',
            '3' => 'mar',
            '4' => 'abr',
            '5' => 'mai',
            '6' => 'jun',
            '7' => 'jul',
            '8' => 'ago',
            '9' => 'set',
            '10' => 'out',
            '11' => 'nov',
            '12' => 'dez',
        );

        return ucfirst($array[$stringMonth]);
    }

}
