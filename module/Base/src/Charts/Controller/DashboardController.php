<?php

namespace Charts\Controller;

//use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\JsonModel;

class ChartsController extends AbstractActionController
{

    public function ChartsAction()
    {
        $service = $this->getServiceLocator()->get('ChartsService');
        return new JsonModel((array) $service->servicos($_POST));
    }

}
