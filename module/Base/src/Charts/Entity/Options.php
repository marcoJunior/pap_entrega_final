<?php

namespace Charts\Entity;

/**
 * @author Leandro Machado
 */
class Options
{

    public $title = '';
    public $focusTarget = 'category';
    public $tooltip = ['isHtml' => false];
    public $curveType = 'function';
    public $lineWidth = 2;
    public $pointSize = 0;
    public $legend = 'top';
    public $hAxis = ['title' => '', 'titleTextStyle' => ['color' => '#000']];
    public $vAxis = ['minValue' => 0];
    public $addColumn = [];
    public $series = ['#4285f4'];
    public $colors = ['#4285f4'];
    public $animation = [];
    public $selectionMode = [];    
    public $aggregationTarget = [];
    public $width = [];
    public $axes = [];
    public $bar = [];
    
    function getBar()
    {
        return $this->bar;
    }

    function setBar($bar)
    {
        $this->bar = $bar;
    }
    
    function getAxes()
    {
        return $this->axes;
    }

    function setAxes($axes)
    {
        $this->axes = $axes;
    }
    
    function getWidth()
    {
        return $this->width;
    }

    function setWidth($width)
    {
        $this->width = $width;
    }
    
    function getSelectionMode()
    {
        return $this->selectionMode;
    }

    function getAggregationTarget()
    {
        return $this->aggregationTarget;
    }

    function setSelectionMode($selectionMode)
    {
        $this->selectionMode = $selectionMode;
    }

    function setAggregationTarget($aggregationTarget)
    {
        $this->aggregationTarget = $aggregationTarget;
    }

    function getChartArea()
    {
        return $this->chartArea;
    }

    function setChartArea($chartArea)
    {
        $this->chartArea = $chartArea;
    }

    function getAnimation()
    {
        return $this->animation;
    }

    function setAnimation($animation)
    {
        $this->animation = $animation;
    }

    function getAddColumn()
    {
        return $this->addColumn;
    }

    function getSeries()
    {
        return $this->series;
    }

    function getColors()
    {
        return $this->colors;
    }

    function setAddColumn($addColumn)
    {
        $this->addColumn = $addColumn;
    }

    function setSeries($series)
    {
        $this->series = $series;
    }

    function setColors($colors)
    {
        $this->colors = $colors;
    }

    function getTitle()
    {
        return $this->title;
    }

    function getFocusTarget()
    {
        return $this->focusTarget;
    }

    function getTooltip()
    {
        return $this->tooltip;
    }

    function getCurveType()
    {
        return $this->curveType;
    }

    function getLineWidth()
    {
        return $this->lineWidth;
    }

    function getPointSize()
    {
        return $this->pointSize;
    }

    function getLegend()
    {
        return $this->legend;
    }

    function getHAxis()
    {
        return $this->hAxis;
    }

    function getVAxis()
    {
        return $this->vAxis;
    }

    function setTitle($title)
    {
        $this->title = $title;
    }

    function setFocusTarget($focusTarget)
    {
        $this->focusTarget = $focusTarget;
    }

    function setTooltip($tooltip)
    {
        $this->tooltip = $tooltip;
    }

    function setCurveType($curveType)
    {
        $this->curveType = $curveType;
    }

    function setLineWidth($lineWidth)
    {
        $this->lineWidth = $lineWidth;
    }

    function setPointSize($pointSize)
    {
        $this->pointSize = $pointSize;
    }

    function setLegend($legend)
    {
        $this->legend = $legend;
    }

    function setHAxis($hAxis)
    {
        $this->hAxis = $hAxis;
    }

    function setVAxis($vAxis)
    {
        $this->vAxis = $vAxis;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
