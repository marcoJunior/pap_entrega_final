<?php

namespace Charts\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado
 * 
 * Se o sábio der ouvidos, aumentará seu conhecimento, 
 * e quem tem discernimento obterá orientação (Provérbios 1:5)
 */
class Charts extends Hydrator
{

    protected function getEntity()
    {
        return 'Charts\Entity\Charts';
    }

    public function getMap()
    {
        return array(
        );
    }

    public function getTemporary()
    {
        return array(
        );
    }

}
