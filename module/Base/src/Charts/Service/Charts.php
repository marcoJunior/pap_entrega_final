<?php

namespace Charts\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface,
    Zend\ServiceManager\ServiceManager;

/**
 * @author Leandro Machado dos Santos
 * 
 * Quem me ouvir viverá em segurança e estará tranqüilo, 
 * sem temer nenhum mal (Provérbios 1:33)
 */
class Charts implements ServiceManagerAwareInterface
{

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

}
