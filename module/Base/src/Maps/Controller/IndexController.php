<?php

/**
 * @author Janley Santos Soares
 */

namespace Maps\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class IndexController extends AbstractActionController
{
    
    public function indexAction()
    {
        $this->layout('mapeamento');
        return new ViewModel();
    }

}
