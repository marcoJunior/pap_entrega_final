<?php

/**
 * Description of Mapeamento
 *
 * @author Janley Santos Soares
 */

namespace Maps\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;

class Mapeamento implements ServiceManagerAwareInterface
{
    public $serviceManager;
    
    public function setServiceManager(ServiceManager $serviceManager) {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager() {
        return $this->serviceManager;
    }
    
    
}
