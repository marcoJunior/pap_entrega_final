<?php

namespace Cadastros;

use Zend\Db\Adapter\Adapter;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                //Services
                Service\Empresa::class => function ($sm) {
                    $service = new Service\Empresa();
                    return $service->setServiceManager($sm);
                },
                Service\Cliente::class => function ($sm) {
                    $service = new Service\Cliente();
                    return $service->setServiceManager($sm);
                },
                Service\Usuario::class => function ($sm) {
                    $service = new Service\Usuario();
                    return $service->setServiceManager($sm);
                },
                //NOVAS SERVICES
                Service\CadastrosHelper::class => function ($sm) {
                    $service = new Service\CadastrosHelper();
                    return $service->setServiceManager($sm);
                },
                Service\Clientes::class => function ($sm) {
                    $service = new Service\Clientes();
                    return $service->setServiceManager($sm);
                },
                Service\Empresas::class => function ($sm) {
                    $service = new Service\Empresas();
                    return $service->setServiceManager($sm);
                },
                Service\ModulosValidade::class => function ($sm) {
                    $service = new Service\ModulosValidade();
                    return $service->setServiceManager($sm);
                },
                Service\Forca::class => function ($sm) {
                    $service = new Service\Forca();
                    return $service->setServiceManager($sm);
                },
                //NOVOS CADASTROS
                Service\EmpresasMax::class => function ($sm) {
                    $service = new Service\EmpresasMax();
                    return $service->setServiceManager($sm);
                },
                Service\ConexoesEmpresas::class => function ($sm) {
                    $service = new Service\ConexoesEmpresas();
                    return $service->setServiceManager($sm);
                },
                /////////////////////////////////////////////////
                'Cadastros\Mapper\ForcaEmpresas' => function ($sm) {
                    $mapper = new Mapper\ForcaEmpresas();
                    $dbConfig = $sm->get('Config');
                    $dbAdapter = new Adapter($dbConfig['localweb2']);
                    $mapper->setDbAdapter($dbAdapter)
                        ->setEntityPrototype(new Entity\ForcaEmpresas())
                        ->setHydrator(new Mapper\Hydrator\ForcaEmpresas());
                    return $mapper;
                },
                'Cadastros\Mapper\ForcaSeries' => function ($sm) {
                    $mapper = new Mapper\ForcaSeries();
                    $dbConfig = $sm->get('Config');
                    $dbAdapter = new Adapter($dbConfig['localweb2']);
                    $mapper->setDbAdapter($dbAdapter)
                        ->setEntityPrototype(new Entity\ForcaSeriesTotal())
                        ->setHydrator(new Mapper\Hydrator\ForcaSeries());
                    return $mapper;
                },
                'Cadastros\Mapper\ModulosValidade' => function ($sm) {
                    $mapper = new Mapper\ModulosValidade();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ModulosValidade())
                        ->setHydrator(new Mapper\Hydrator\ModulosValidade());
                    return $mapper;
                },
                'Cadastros\Mapper\Empresas' =>
                function ($sm) {
                    $mapper = new Mapper\Empresas();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Empresas())
                        ->setHydrator(new Mapper\Hydrator\Empresas());
                    return $mapper;
                },
                'Cadastros\Mapper\Empresa' =>
                function ($sm) {
                    $mapper = new Mapper\Empresa();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Empresa())
                        ->setHydrator(new Mapper\Hydrator\Empresa());
                    return $mapper;
                },
                'Cadastros\Mapper\Pagamento' =>
                function ($sm) {
                    $mapper = new \Cadastros\Mapper\Pagamento();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new \Cadastros\Entity\Pagamento())
                        ->setHydrator(new \Cadastros\Mapper\Hydrator\Pagamento());
                    return $mapper;
                },
                'Cadastros\Mapper\Criacao' =>
                function ($sm) {
                    $mapper = new Mapper\Criacao();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Criacao())
                        ->setHydrator(new Mapper\Hydrator\Criacao());
                    return $mapper;
                },
                'Cadastros\Mapper\Clientes' =>
                function ($sm) {
                    $mapper = new Mapper\Clientes();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Clientes())
                        ->setHydrator(new Mapper\Hydrator\Clientes());
                    return $mapper;
                },
                'Cadastros\Mapper\Cliente' =>
                function ($sm) {
                    $mapper = new Mapper\Cliente();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Cliente())
                        ->setHydrator(new Mapper\Hydrator\Cliente());
                    return $mapper;
                },
                'UsuarioMapper' =>
                function ($sm) {
                    $mapper = new Mapper\Usuario();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Usuario())
                        ->setHydrator(new Mapper\Hydrator\Usuario());
                    return $mapper;
                },
                'Cadastros\Mapper\EmpresasMax' =>
                function ($sm) {
                    $mapper = new Mapper\EmpresasMax();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\EmpresasMax())
                        ->setHydrator(new Mapper\Hydrator\EmpresasMax());
                    return $mapper;
                },
                'Cadastros\Mapper\ConexoesEmpresas' =>
                function ($sm) {
                    $mapper = new Mapper\ConexoesEmpresas();
                    $dbConfig = $sm->get('Zend\Db\Adapter\Adapter');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ConexoesEmpresas())
                        ->setHydrator(new Mapper\Hydrator\ConexoesEmpresas());
                    return $mapper;
                },
            ),
        );
    }

}
