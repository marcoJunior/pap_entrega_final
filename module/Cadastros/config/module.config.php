<?php

return array(
    'controllers' => array(
        'factories' => [
            'Controller' => function ($sl) {
                $controller = new \Cadastros\Controller\IndexController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'usuarios-web' => function ($sl) {
                $controller = new \Cadastros\Controller\ClientesController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'Empresas' => function ($sl) {
                $controller = new \Cadastros\Controller\EmpresasController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'Modulos-Validade' => function ($sl) {
                $controller = new \Cadastros\Controller\ModulosValidadeController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'Forca' => function ($sl) {
                $controller = new \Cadastros\Controller\ForcaController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'empresas_max' => function ($sl) {
                $controller = new \Cadastros\Controller\EmpresasMaxController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'conexoes_empresas_max' => function ($sl) {
                $controller = new \Cadastros\Controller\ConexoesEmpresasMaxController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'planos_empresas_max' => function ($sl) {
                $controller = new \Cadastros\Controller\PlanosController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'cadastros' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/cadastros[/:controller[/:action]]',
                    'constraints' => array(
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'action' => 'layout',
                    )
                ),
            ),
            'empresas/selecionar' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/empresas/selecionar',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'SelecionarEmpresa',
                    ),
                ),
            ),
            'empresas/servicosr' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/empresas/servicos',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'ServicosEmpresa',
                    ),
                ),
            ),
            'clientes/selecionar' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/clientes/selecionar',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'SelecionarClientes',
                    ),
                ),
            ),
            'cliente/servicos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/cliente/servicos',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'ServicosCliente',
                    ),
                ),
            ),
            'usuarios/selecionar' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/usuarios/selecionar',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'SelecionarUsuarios',
                    ),
                ),
            ),
            'usuarios/servicos' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/usuarios/servicos',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'ServicosUsuario',
                    ),
                ),
            ),
            'gerar/tabelas' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/gerar/tabelas',
                    'defaults' => array(
                        'controller' => 'Controller',
                        'action' => 'gerarTabelas',
                    ),
                ),
            )
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            /////////VALIDADE DE USO DOS SISTEMAS POR CLIENTE
            'cadastros/modulos-validade' => __DIR__ . '/../view/layout/cadastros/modulos-validade/index.phtml',
            'cadastros/modulos-validade/grid' => __DIR__ . '/../view/layout/cadastros/modulos-validade/grid.phtml',
            'cadastros/modulos-validade/formulario' => __DIR__ . '/../view/layout/cadastros/modulos-validade/formulario.phtml',
            'cadastros/modulos-validade/modal' => __DIR__ . '/../view/layout/cadastros/modulos-validade/modal.phtml',
            /////////EMPRESAS NEW
            'cadastros/empresasMax' => __DIR__ . '/../view/layout/cadastros/empresas-max/index.phtml',
            'cadastros/empresasMax/grid' => __DIR__ . '/../view/layout/cadastros/empresas-max/grid.phtml',
            'cadastros/empresasMax/formulario' => __DIR__ . '/../view/layout/cadastros/empresas-max/formulario.phtml',
            'cadastros/empresasMax/modal' => __DIR__ . '/../view/layout/cadastros/empresas-max/modal.phtml',
            /////////CONEXOES NEW
            'cadastros/conexoesMax' => __DIR__ . '/../view/layout/cadastros/conexoes-max/index.phtml',
            'cadastros/conexoesMax/grid' => __DIR__ . '/../view/layout/cadastros/conexoes-max/grid.phtml',
            'cadastros/conexoesMax/formulario' => __DIR__ . '/../view/layout/cadastros/conexoes-max/formulario.phtml',
            'cadastros/conexoesMax/modal' => __DIR__ . '/../view/layout/cadastros/conexoes-max/modal.phtml',
            /////////EMPRESAS
            'cadastros/empresas' => __DIR__ . '/../view/layout/cadastros/empresas/index.phtml',
            'cadastros/empresas/grid' => __DIR__ . '/../view/layout/cadastros/empresas/grid.phtml',
            'cadastros/empresas/formulario' => __DIR__ . '/../view/layout/cadastros/empresas/formulario.phtml',
            'cadastros/empresas/modal' => __DIR__ . '/../view/layout/cadastros/empresas/modal.phtml',
            /////////CLIENTES
            'cadastros/clientes' => __DIR__ . '/../view/layout/cadastros/clientes/index.phtml',
            'cadastros/clientes/grid' => __DIR__ . '/../view/layout/cadastros/clientes/grid.phtml',
            'cadastros/clientes/formulario' => __DIR__ . '/../view/layout/cadastros/clientes/formulario.phtml',
            'cadastros/clientes/modal' => __DIR__ . '/../view/layout/cadastros/clientes/modal.phtml',
            /////////FORÇA DE VENDAS
            'cadastros/forca/empresas' => __DIR__ . '/../view/layout/cadastros/forca/empresas/index.phtml',
            'cadastros/forca/empresas/grid' => __DIR__ . '/../view/layout/cadastros/forca/empresas/grid.phtml',
            'cadastros/forca/empresas/formulario' => __DIR__ . '/../view/layout/cadastros/forca/empresas/formulario.phtml',
            'cadastros/forca/empresas/modal' => __DIR__ . '/../view/layout/cadastros/forca/empresas/modal.phtml',
            'cadastros/forca/series' => __DIR__ . '/../view/layout/cadastros/forca/series/index.phtml',
            'cadastros/forca/series/grid' => __DIR__ . '/../view/layout/cadastros/forca/series/grid.phtml',
            'cadastros/forca/series/formulario' => __DIR__ . '/../view/layout/cadastros/forca/series/formulario.phtml',
            'cadastros/forca/series/modal' => __DIR__ . '/../view/layout/cadastros/forca/series/modal.phtml',
        ),
        'template_path_stack' => array(
            'cadastro' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
