<?php

namespace Cadastros\Controller;

use Cadastros\View\Clientes;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\View\Application;

class ClientesController extends ControllerPrincipal
{

  public function layoutAction()
  {
      $get = $this->getRequest()->getQuery();

      $phpRenderer = $this->getService()->get('ViewRenderer');
      $viewClientes = new Clientes($phpRenderer);
      $viewClientes->setVariable('titulo','Login de cliente(s) web');
      $viewClientes->grid();
      $viewClientes->formulario();
      $viewClientes->modal();

      $app = new Application($phpRenderer);
      $app->addChild($viewClientes);
      return $app;

  }

    public function selecionarAction()
    {
        $service = $this->serviceClientes();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionar($post,$get));
    }

    public function alterarAction()
    {
        $service = $this->serviceClientes();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceClientes();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
