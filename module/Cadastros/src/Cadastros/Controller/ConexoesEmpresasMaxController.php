<?php

namespace Cadastros\Controller;

use Zend\View\Model\JsonModel;
use Application\View\Application;
use Cadastros\View\ConexoesEmpresas;

class ConexoesEmpresasMaxController extends ControllerPrincipal
{

    public function layoutAction()
    {
        $get = $this->getRequest()->getQuery();

        $phpRenderer = $this->getServiceLocator()->get('ViewRenderer');
        $viewClientes = new ConexoesEmpresas($phpRenderer);
        $viewClientes->setVariable('titulo', 'Manutenção de conexão dos clientes Web');
        $viewClientes->grid();
        $viewClientes->formulario();

        $app = new Application($phpRenderer);
        $app->addChild($viewClientes);
        return $app;
    }

    public function selecionarAction()
    {
        $service = $this->serviceConexoesEmpresas();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionar($post, $get));
    }

    public function alterarAction()
    {
        $service = $this->serviceConexoesEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function adicaoAction()
    {
        $service = $this->serviceConexoesEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceConexoesEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
