<?php

namespace Cadastros\Controller;

use APIGrid\Controller\APIGridController;

class ControllerPrincipal extends APIGridController
{

    /**
    * @return \Cadastros\Service\Forca
    */
    public function serviceForca()
    {
        return $this->getService()->get(\Cadastros\Service\Forca::class);
    }

    /**
    * @return \Cadastros\Service\Clientes
    */
    public function serviceClientes()
    {
        return $this->getService()->get(\Cadastros\Service\Clientes::class);
    }

    /**
    * @return \Cadastros\Service\ModulosValidade
    */
    public function serviceModulosValidade()
    {
        return $this->getService()->get(\Cadastros\Service\ModulosValidade::class);
    }

    /**
    * @return \Cadastros\Service\Empresas
    */
    public function serviceEmpresas()
    {
        return $this->getService()->get(\Cadastros\Service\Empresas::class);
    }

    /**
    * @return \Cadastros\Service\EmpresasMax
    */
    public function serviceEmpresasMax()
    {
        return $this->getService()->get(\Cadastros\Service\EmpresasMax::class);
    }

    /**
    * @return \Cadastros\Service\ConexoesEmpresas
    */
    public function serviceConexoesEmpresas()
    {
        return $this->getService()->get(\Cadastros\Service\ConexoesEmpresas::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
