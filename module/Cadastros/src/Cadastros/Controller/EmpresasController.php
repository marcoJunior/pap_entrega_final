<?php

namespace Cadastros\Controller;

use Cadastros\View\Empresas;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\View\Application;

class EmpresasController extends ControllerPrincipal
{

  public function layoutAction()
  {
      $get = $this->getRequest()->getQuery();

      $phpRenderer = $this->getService()->get('ViewRenderer');
      $viewClientes = new Empresas($phpRenderer);
      $viewClientes->setVariable('titulo','Manutenção de clientes');
      $viewClientes->grid();
      $viewClientes->formulario();

      $app = new Application($phpRenderer);
      $app->addChild($viewClientes);
      return $app;

  }

    public function selecionarAction()
    {
        $service = $this->serviceEmpresas();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionar($post,$get));
    }

    public function alterarAction()
    {
        $service = $this->serviceEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function adicaoAction()
    {
        $service = $this->serviceEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceEmpresas();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
