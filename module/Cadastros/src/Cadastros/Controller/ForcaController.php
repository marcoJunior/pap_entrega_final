<?php

namespace Cadastros\Controller;

use Cadastros\View\ForcaEmpresas;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\View\Application;

class ForcaController extends ControllerPrincipal
{

    public function empresasAction()
    {
        $get = $this->getRequest()->getQuery();

        $phpRenderer = $this->getService()->get('ViewRenderer');
        $viewForcaEmpresas = new ForcaEmpresas($phpRenderer);
        $viewForcaEmpresas->setVariable('titulo', 'Força de vendas');
        $viewForcaEmpresas->grid();
        $viewForcaEmpresas->formulario();
        $viewForcaEmpresas->modal();

        $service = $this->serviceFiltro();

        // $filtros[] = [
        //     'intervaloData' => [
        //         'intervaloData' => $get->toArray(),
        //         'valor' => $get->toArray()
        //     ]
        // ];
        // $viewForcaEmpresas->setVariable('filtros', $service->loadFiltros($filtros));

        $app = new Application($phpRenderer);
        $app->addChild($viewForcaEmpresas);
        return $app;
    }

    public function empresasSerieAction()
    {
        $service = $this->serviceForca();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getQuery();

        return new JsonModel($service->empresasSerie($post, $get));
    }

    public function empresasSelecionarAction()
    {
        $service = $this->serviceForca();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->empresasSelecionar($post, $get));
    }

    public function empresasAlterarAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function empresasAdicaoAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function empresasExcluirAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

    public function empresasAtualizaAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->atualizar($post));
    }

    public function empresasSerieExcluirAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

    public function empresasCodigoSerieLimparAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->empresasSerieLimpar($post));
    }

    public function empresasCodigoSerieExcluirAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->empresasSerieExcluir($post));
    }

    public function empresasCodigoSerieGerarAction()
    {
        $service = $this->serviceForca();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->gerarCodigo($post));
    }

}
