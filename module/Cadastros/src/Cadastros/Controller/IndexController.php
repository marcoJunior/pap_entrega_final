<?php

namespace Cadastros\Controller;

use Zend\Mvc\Controller\AbstractActionController;
//use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IndexController extends AbstractActionController
{

    public function SelecionarEmpresaAction()
    {
        $service = $this->getServiceLocator()->get('Cadastros\Service\Empresa');
        $resultado = $service->selecionarGrid($_POST);
        return new JsonModel((array) $resultado);
    }

    public function ServicosEmpresaAction()
    {
        $service = $this->getServiceLocator()->get('Cadastros\Service\Empresa');
        if (!isset($_POST['acao'])) {
            return false;
        }
        $acao = $_POST['acao'];
        $resultado = $service->{$acao}($_POST);
        return new JsonModel((array) $resultado);
    }

    public function SelecionarClientesAction()
    {
        $service = $this->getServiceLocator()->get('Cadastros\Service\Cliente');
        $resultado = $service->selecionarGrid($_POST);
        return new JsonModel((array) $resultado);
    }

    public function ServicosClienteAction()
    {
        $service = $this->getServiceLocator()->get('Cadastros\Service\Cliente');
        if (!isset($_POST['acao'])) {
            return false;
        }
        $resultado = $service->$_POST['acao']($_POST);
        return new JsonModel((array) $resultado);
    }

    public function SelecionarUsuariosAction()
    {
        $service = $this->getServiceLocator()->get('UsuarioService');
        $resultado = $service->grid($_POST);
        return new JsonModel((array) $resultado);
    }

    public function ServicosUsuarioAction()
    {
        $service = $this->getServiceLocator()->get('UsuarioService');
        if (!isset($_POST['acao'])) {
            return false;
        }
        $resultado = $service->$_POST['acao']($_POST);
        return new JsonModel((array) $resultado);
    }

    public function gerarTabelasAction()
    {
        $service = $this->getServiceLocator()->get('Cadastros\Service\Empresa');
        return new JsonModel((array) $service->gerarTabelas($_POST));
    }

}
