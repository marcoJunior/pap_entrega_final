<?php

namespace Cadastros\Controller;

use Cadastros\View\ModulosValidade;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Application\View\Application;

class ModulosValidadeController extends ControllerPrincipal
{

  public function layoutAction()
  {
      $get = $this->getRequest()->getQuery();

      $phpRenderer = $this->getServiceLocator()->get('ViewRenderer');
      $viewModulosValidade = new ModulosValidade($phpRenderer);
      $viewModulosValidade->setVariable('titulo','Validades por modulos');
      $viewModulosValidade->grid();
      $viewModulosValidade->formulario();
      $viewModulosValidade->modal();

      $service = $this->serviceFiltro();

      $filtros[] = [
          'intervaloData' => [
              'intervaloData' => $get->toArray(),
              'valor' => $get->toArray()
          ]
      ];

      $viewModulosValidade->setVariable('filtros', $service->loadFiltros($filtros));

      $app = new Application($phpRenderer);
      $app->addChild($viewModulosValidade);
      return $app;

  }

    public function selecionarAction()
    {
        $service = $this->serviceModulosValidade();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->getClientes($post,$get));
    }

    public function selecionarUnicoAction()
    {
        $service = $this->serviceModulosValidade();
        $get = $this->getRequest()->getQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->getClientes($post,$get));
    }

    public function alterarAction()
    {
        $service = $this->serviceModulosValidade();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function adicaoAction()
    {
        $service = $this->serviceModulosValidade();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceModulosValidade();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
