<?php

namespace Cadastros\Controller;

use Zend\View\Model\JsonModel;
use Cadastros\View\EmpresasMax;
use Application\View\Application;

class PlanosController extends ControllerPrincipal
{

    public function layoutAction()
    {
        $get = $this->getRequest()->getQuery();

        $phpRenderer = $this->getServiceLocator()->get('ViewRenderer');
        $viewClientes = new EmpresasMax($phpRenderer);
        $viewClientes->setVariable('titulo', 'Manutenção de planos dos clientes Web');
        $viewClientes->grid();
        $viewClientes->formulario();

        $app = new Application($phpRenderer);
        $app->addChild($viewClientes);
        return $app;
    }

    public function selecionarAction()
    {
        $service = $this->serviceEmpresasMax();
        $get = $this->getRequestQuery();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionar($post, $get));
    }

    public function alterarAction()
    {
        $service = $this->serviceEmpresasMax();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->alterar($post));
    }

    public function adicaoAction()
    {
        $service = $this->serviceEmpresasMax();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->adicao($post));
    }

    public function excluirAction()
    {
        $service = $this->serviceEmpresasMax();
        $post = $this->getRequest()->getPost();

        return new JsonModel((array) $service->excluir($post));
    }

}
