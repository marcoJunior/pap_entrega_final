<?php

namespace Cadastros\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Cliente
{

    //Grid
    protected $codigo;
    protected $email;
    protected $serie;
    protected $nivel;
    protected $sistema;
    protected $senha;
    protected $ativo;
    protected $referencia;
    protected $empresa;

    function getEmpresa()
    {
        return $this->empresa;
    }

    function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getSenha()
    {
        return $this->senha;
    }

    function getSerie()
    {
        return $this->serie;
    }

    function getNivel()
    {
        return $this->nivel;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function getReferencia()
    {
        return $this->referencia;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setSenha($senha)
    {
        $this->senha = $senha;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function setNivel($nivel)
    {
        $this->nivel = $nivel;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function setReferencia($referencia)
    {
        $this->referencia = $referencia;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
