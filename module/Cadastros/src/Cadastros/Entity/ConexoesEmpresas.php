<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

class ConexoesEmpresas extends EntityHelper
{

    //CLIENTE
    protected $idClienteMax;
    protected $fantasia;
    protected $documento;
    //CONEXÃO
    protected $id;
    protected $ip;
    protected $porta;
    protected $usuarioBanco;
    protected $senhaBanco;
    protected $banco;

    function getIdClienteMax()
    {
        return $this->idClienteMax;
    }

    function getFantasia()
    {
        return $this->fantasia;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getId()
    {
        return $this->id;
    }

    function getIp()
    {
        return $this->ip;
    }

    function getPorta()
    {
        return $this->porta;
    }

    function getUsuarioBanco()
    {
        return $this->usuarioBanco;
    }

    function getSenhaBanco()
    {
        return $this->senhaBanco;
    }

    function getBanco()
    {
        return $this->banco;
    }

    function setIdClienteMax($idClienteMax)
    {
        $this->idClienteMax = (int) $idClienteMax;
    }

    function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function setPorta($porta)
    {
        $this->porta = $porta;
    }

    function setUsuarioBanco($usuarioBanco)
    {
        $this->usuarioBanco = $usuarioBanco;
    }

    function setSenhaBanco($senhaBanco)
    {
        $this->senhaBanco = $senhaBanco;
    }

    function setBanco($banco)
    {
        $this->banco = $banco;
    }

}
