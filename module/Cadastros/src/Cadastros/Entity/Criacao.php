<?php

namespace Cadastros\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Criacao
{

    //Grid
    protected $codigo;
    protected $empresa;
    protected $ip;
    protected $banco;
    protected $porta;
    protected $qtdLogins;
    protected $serie;
    protected $login;
    protected $iptintometrico;
    protected $bancotintometrico;
    protected $portatintometrico;
    protected $plano;
    protected $projetos;
    protected $qtdClientes;
    protected $dominio;
    protected $subdominio;
    protected $qtdMb;
    protected $cielo;
    protected $moip;
    protected $pagseguro;

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

    function getCodigo()
    {
        return $this->codigo;
    }

    function getEmpresa()
    {
        return $this->empresa;
    }

    function getIp()
    {
        return $this->ip;
    }

    function getBanco()
    {
        return $this->banco;
    }

    function getPorta()
    {
        return $this->porta;
    }

    function getQtdLogins()
    {
        return $this->qtdLogins;
    }

    function getSerie()
    {
        return $this->serie;
    }

    function getLogin()
    {
        return $this->login;
    }

    function getIptintometrico()
    {
        return $this->iptintometrico;
    }

    function getBancotintometrico()
    {
        return $this->bancotintometrico;
    }

    function getPortatintometrico()
    {
        return $this->portatintometrico;
    }

    function getPlano()
    {
        return $this->plano;
    }

    function getProjetos()
    {
        return $this->projetos;
    }

    function getQtdClientes()
    {
        return $this->qtdClientes;
    }

    function getDominio()
    {
        return $this->dominio;
    }

    function getSubdominio()
    {
        return $this->subdominio;
    }

    function getQtdMb()
    {
        return $this->qtdMb;
    }

    function getCielo()
    {
        return $this->cielo;
    }

    function getMoip()
    {
        return $this->moip;
    }

    function getPagseguro()
    {
        return $this->pagseguro;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setEmpresa($empresa)
    {
        $this->empresa = $empresa;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function setBanco($banco)
    {
        $this->banco = $banco;
    }

    function setPorta($porta)
    {
        $this->porta = $porta;
    }

    function setQtdLogins($qtdLogins)
    {
        $this->qtdLogins = $qtdLogins;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function setLogin($login)
    {
        $this->login = $login;
    }

    function setIptintometrico($iptintometrico)
    {
        $this->iptintometrico = $iptintometrico;
    }

    function setBancotintometrico($bancotintometrico)
    {
        $this->bancotintometrico = $bancotintometrico;
    }

    function setPortatintometrico($portatintometrico)
    {
        $this->portatintometrico = $portatintometrico;
    }

    function setPlano($plano)
    {
        $this->plano = $plano;
    }

    function setProjetos($projetos)
    {
        $this->projetos = $projetos;
    }

    function setQtdClientes($qtdClientes)
    {
        $this->qtdClientes = $qtdClientes;
    }

    function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    function setSubdominio($subdominio)
    {
        $this->subdominio = $subdominio;
    }

    function setQtdMb($qtdMb)
    {
        $this->qtdMb = $qtdMb;
    }

    function setCielo($cielo)
    {
        $this->cielo = $cielo;
    }

    function setMoip($moip)
    {
        $this->moip = $moip;
    }

    function setPagseguro($pagseguro)
    {
        $this->pagseguro = $pagseguro;
    }

}
