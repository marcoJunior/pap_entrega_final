<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

class EmpresasMax extends EntityHelper
{

    protected $id;
    protected $razaoSocial;
    protected $fantasia;
    protected $documento;
    protected $telefone;
    protected $email;
    protected $logo;
    protected $apelido;
    protected $dominio;
    protected $subDominio;
    protected $cep;
    protected $rua;
    protected $numero;
    protected $complemento;
    protected $bairro;
    protected $cidade;
    protected $estado;
    protected $ativo;
    protected $idPlano;

    function getId()
    {
        return $this->id;
    }

    function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    function getFantasia()
    {
        return $this->fantasia;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getTelefone()
    {
        return $this->telefone;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getLogo()
    {
        return $this->logo;
    }

    function getApelido()
    {
        return $this->apelido;
    }

    function getDominio()
    {
        return $this->dominio;
    }

    function getSubDominio()
    {
        return $this->subDominio;
    }

    function getCep()
    {
        return $this->cep;
    }

    function getRua()
    {
        return $this->rua;
    }

    function getNumero()
    {
        return $this->numero;
    }

    function getComplemento()
    {
        return $this->complemento;
    }

    function getBairro()
    {
        return $this->bairro;
    }

    function getCidade()
    {
        return $this->cidade;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function getIdPlano()
    {
        return $this->idPlano;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
    }

    function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function setLogo($logo)
    {
        $this->logo = $logo;
    }

    function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    function setSubDominio($subDominio)
    {
        $this->subDominio = $subDominio;
    }

    function setCep($cep)
    {
        $this->cep = $cep;
    }

    function setRua($rua)
    {
        $this->rua = $rua;
    }

    function setNumero($numero)
    {
        $this->numero = $numero;
    }

    function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;

        if (gettype($ativo) == 'string') {
            $this->ativo = $ativo === 'true' ? true : false;
        }
    }

    function setIdPlano($idPlano)
    {
        $this->idPlano = $idPlano;
    }

}
