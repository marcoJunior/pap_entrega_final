<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

class ForcaEmpresas extends EntityHelper
{

    protected $codigo;
    protected $serie;
    protected $fantasia;
    protected $ip;
    protected $banco;
    protected $tipoLoja;
    protected $sistema;
    protected $qtdAdiquirida;
    protected $qtdUso;
    protected $dataValidade;

    /**
    * @return mixed
    */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
    * @param mixed codigo
    */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    /**
    * @return mixed
    */
    public function getSerie()
    {
        return $this->serie;
    }

    /**
    * @param mixed serie
    */
    public function setSerie($serie)
    {
        $this->serie = $serie;
    }

    /**
    * @return mixed
    */
    public function getFantasia()
    {
        return $this->fantasia;
    }

    /**
    * @param mixed fantasia
    */
    public function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    /**
    * @return mixed
    */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed ip
     */
     public function setIp($ip)
     {
         $this->ip = $ip;
     }

    /**
     * @return mixed
     */
    public function getBanco()
    {
        return $this->banco;
    }

    /**
     * @param mixed banco
     */
     public function setBanco($banco)
     {
         $this->banco = $banco;
     }

    /**
     * @return mixed
     */
    public function getTipoLoja()
    {
        return $this->tipoLoja;
    }

    /**
     * @param mixed tipoLoja
     */
     public function setTipoLoja($tipoLoja)
     {
         $this->tipoLoja = $tipoLoja;
     }

    /**
     * @return mixed
     */
    public function getSistema()
    {
        return $this->sistema;
    }

    /**
     * @param mixed sistema
     */
     public function setSistema($sistema)
     {
         $this->sistema = $sistema;
     }

    /**
     * @return mixed
     */
    public function getQtdAdiquirida()
    {
        return $this->qtdAdiquirida;
    }

    /**
     * @param mixed qtdAdiquirida
     */
     public function setQtdAdiquirida($qtdAdiquirida)
     {
         $this->qtdAdiquirida = $qtdAdiquirida;
     }

    /**
     * @return mixed
     */
    public function getQtdUso()
    {
        return $this->qtdUso;
    }

    /**
     * @param mixed qtdUso
     */
     public function setQtdUso($qtdUso)
     {
         $this->qtdUso = $qtdUso;
     }

    /**
     * @return mixed
     */
    public function getDataValidade()
    {
        return $this->dataValidade;
    }

    /**
     * @param mixed dataValidade
     */
     public function setDataValidade($dataValidade)
     {
         $this->dataValidade = $dataValidade;
     }
 }
