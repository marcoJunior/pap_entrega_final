<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

class ForcaSeries extends EntityHelper
{

    protected $codigo;
    protected $serie;
    protected $aparelho;
    protected $representante;
    protected $numForca;

    function getCodigo()
    {
        return $this->codigo;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function getSerie()
    {
        return $this->serie;
    }

    function getAparelho()
    {
        return $this->aparelho;
    }

    function getRepresentante()
    {
        return $this->representante;
    }

    function getNumForca()
    {
        return $this->numForca;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function setAparelho($aparelho)
    {
        $this->aparelho = $aparelho;
    }

    function setRepresentante($representante)
    {
        $this->representante = $representante;
    }

    function setNumForca($numForca)
    {
        $this->numForca = $numForca;
    }

}
