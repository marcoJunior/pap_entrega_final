<?php

namespace Cadastros\Entity;

class ForcaSeriesTotal extends ForcaSeries
{

    protected $total;

    function getTotal()
    {
        return $this->total;
    }

    function setTotal($total)
    {
        $this->total = $total;
    }

}
