<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

class ModulosValidade extends EntityHelper
{

    protected $serie;
    protected $fantasia;
    protected $modulo;
    protected $data;
    protected $dataValidade;
    protected $dias;
    protected $diasVencer;

    function getSerie()
    {
        return $this->serie;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function getFantasia()
    {
        return $this->fantasia;
    }

    function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    function getModulo()
    {
        return $this->modulo;
    }

    function setModulo($modulo)
    {
        $this->modulo = $modulo;
    }

    function getData()
    {
        return $this->data;
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function getDataValidade()
    {
        return $this->dataValidade;
    }

    function setDataValidade($dataValidade)
    {
        $this->dataValidade = $dataValidade;
    }

    function getDias()
    {
        return $this->dias;
    }

    function setDias($dias)
    {
        $this->dias = $dias;
    }

    function getDiasVencer()
    {
        return $this->diasVencer;
    }

    function setDiasVencer($diasVencer)
    {
        $this->diasVencer = $diasVencer;
    }

}
