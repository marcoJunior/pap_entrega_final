<?php

namespace Cadastros\Entity;

use Application\Entity\EntityHelper;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Pagamento extends EntityHelper
{

    protected $serie;
    protected $moip;
    protected $pagseguro;
    protected $cielo;

    function getSerie()
    {
        return $this->serie;
    }

    function getMoip()
    {
        return $this->moip;
    }

    function getPagseguro()
    {
        return $this->pagseguro;
    }

    function getCielo()
    {
        return $this->cielo;
    }

    function setSerie($serie)
    {
        $this->serie = (int) $serie;
    }

    function setMoip($moip)
    {
        $this->moip = (int) $moip;
    }

    function setPagseguro($pagseguro)
    {
        $this->pagseguro = (int) $pagseguro;
    }

    function setCielo($cielo)
    {
        $this->cielo = (int) $cielo;
    }

}
