<?php

namespace Cadastro\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Usuario
{

    protected $codigo;
    protected $ativacao;
    protected $nome;
    protected $usuario;
    protected $email;
    protected $senha;
    protected $tipo;

    function getCodigo()
    {
        return $this->codigo;
    }

    function getAtivacao()
    {
        return $this->ativacao;
    }

    function getNome()
    {
        return $this->nome;
    }

    function getUsuario()
    {
        return $this->usuario;
    }

    function getEmail()
    {
        return $this->email;
    }

    function getSenha()
    {
        return $this->senha;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    }

    function setAtivacao($ativacao)
    {
        $this->ativacao = $ativacao;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    function setUsuario($usuario)
    {
        $this->usuario = strtolower($usuario);
    }

    function setEmail($email)
    {
        $this->email = strtolower($email);
    }

    function setSenha($senha)
    {
        $this->senha = $senha;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
