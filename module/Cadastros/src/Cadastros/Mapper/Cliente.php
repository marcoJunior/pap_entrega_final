<?php

namespace Cadastros\Mapper;

use ZfcBase\Mapper\AbstractDbMapper;
use Zend\Db\Sql\Where;
use Cadastro\Entity\Cliente as EntidadeCliente;
use Zend\Db\Sql\Expression;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Cliente extends AbstractDbMapper
{

    /**
     * Verifica se registro existe conexao no banco
     * @param array
     * @return boolean
     */
    public function verificarRegistro($parametros)
    {
        $where = new Where();
        $where->equalTo($parametros['coluna'], strtoupper($parametros['valor']));

        $select = $this->getSelect()->from("mxlgncfi");
        $select->columns(['*']);
        $select->where($where);

        return empty($this->select($select)->toArray());
    }

    /**
     * Inserindo produto no banco
     * @param \Cadastro\Entity\Cliente
     * @return boolean
     */
    public function adicionar(EntidadeCliente $cliente)
    {
        $cliente->setCodigo($this->gerarCodigo());
        $result = $this->insert($cliente, "mxlgncfi");
        return $result->getAffectedRows() ? $cliente->getCodigo() : false;
    }

    /**
     * @return int Id do novo produto
     */
    public function gerarCodigo()
    {
        $select = $this->getSelect()->
                columns([
                    'cficodlgn' => new Expression('MAX(CAST(cficodlgn AS INT))'),
                ])->
                from("mxlgncfi");

        $resultado = $this->select($select);
        $retorno = $resultado->getDataSource()->current()["cficodlgn"] + 1;

        return $retorno;
    }

    /**
     * Editando (update) em produto
     * @param \Cadastro\Entity\Cliente
     * @return boolean
     */
    public function editar(EntidadeCliente $cliente)
    {
        $where = new Where();
        $where->equalTo("cficodlgn", $cliente->getCodigo());

        $resultado = $this->update($cliente, $where, "mxlgncfi");
        return $resultado->getAffectedRows() ? (int) $cliente->getCodigo() : false;
    }

    /**
     * Excluindo empresa
     * @param \Cadastro\Entity\Cliente
     * @return boolean
     */
    public function remover(EntidadeCliente $cliente)
    {
        $where = new Where();
        $where->equalTo("cficodlgn", $cliente->getCodigo());
        $this->delete($where, "mxlgncfi");

        return [true];
    }

    /**
     * Responsável apenas para atualizar campo de ativação
     * @param \Cadastro\Entity\Cliente
     * @return boolean
     */
    public function atualizarUsuario(EntidadeCliente $usuario)
    {
        $where = new Where();
        $where->equalTo("cficodlgn", $usuario->getCodigo());

        $resultado = $this->update(['cfiatvlgn' => $usuario->getAtivo()], $where, "mxlgncfi");
        return $resultado->getAffectedRows() ? (int) $usuario->getCodigo() : false;
    }

    /**
     * Selecionando produtos
     * @param Where $where
     * @return obj $retorno dados selecionados
     */
    public function selecionar(Where $where, $config)
    {
        $selectCont = $this->getSelect()->from("mxlgncfi");
        $selectCont->columns(array("total" => new Expression('count(*)')));
        $selectCont->where($where);

        $coluns = array(
            '*',
            'totalTable' => new Expression('?', array($selectCont)),
        );

        $select = $this->getSelect()->from("mxlgncfi");
        $select->columns($coluns);
        $select->where($where);

        if (isset($config['order']) && !empty($config['order'])) {
            $select->order($config['order']['coluna'] . ' ' . $config['order']['valor']);
        }
        if (isset($config['offset']) && !empty($config['offset'])) {
            $select->offset($config['offset']);
        }
        if (isset($config['limit']) && !empty($config['limit'])) {
            $select->limit($config['limit']);
        }

        return $this->select($select);
    }

}
