<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Entity\Clientes as ClientesEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Clientes extends APIGrid
{
    public $tableName = 'mxlgncfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\Clientes';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setLimitOffset(true);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'cficodlgn',
            'cfiemllgn',
            'cfisenlgn',
            'cfiapelgn',
            'cfiserlgn',
            'cfinivlgn',
            'cfisislgn',
            'cfiatvlgn',
            'cfireflgn'
        ];
    }

    public function alterar($formulario)
    {
        $entityFormaulario = new  ClientesEntity();
        $entityFormaulario->exchangeArray($formulario);

        $empresa = explode(' - ', strtolower($entityFormaulario->getEmpresa()))[1];
        $entityFormaulario->setEmpresa($empresa);

        $where = new Where();
        $where->equalTo('cficodlgn', $entityFormaulario->getCodigo());

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        return $this->update($lista, $where)->getAffectedRows();
    }

    public function excluir($codigo)
    {
        $where = new Where();
        $where->equalTo('cficodlgn', $codigo);

        return $this->delete($where)->getAffectedRows();
    }

}
