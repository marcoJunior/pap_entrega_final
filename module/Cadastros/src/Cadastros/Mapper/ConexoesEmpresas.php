<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Entity\ConexoesEmpresas as ConexoesEmpresasEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexoesEmpresas extends APIGrid
{

    public $tableName = 'conexao';
    public $mapperName = 'Cadastros\Mapper\Hydrator\ConexoesEmpresas';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $this->setJoin([
            'clientes', 'conexao.id_cliente_max = clientes.id',
            ['documento', 'fantasia'],
            'left outer'
        ]);
//        foreach ($filtros->toArray() as $index => $valor) {
//            $valorDescricao = str_replace('_', '',
//                    str_replace('_de', '', ucfirst($index)));
//            $getValor = 'get' . $valorDescricao;
//            $setWhere = 'setWhere' . $valorDescricao;
//            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere}($filtros);
//            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere . "s"}($filtros);
//            }
//        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'id_cliente_max',
            'ip',
            'porta',
            'usuario_banco',
            'senha_banco',
            'banco'
        ];
    }

    public function adicao(ConexoesEmpresasEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(ConexoesEmpresasEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
