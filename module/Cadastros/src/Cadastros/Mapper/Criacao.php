<?php

namespace Cadastros\Mapper;

use APIGrid\Mapper\APIGrid;
use Cadastros\Entity\Empresa as EntidadeEmpresa;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Criacao extends APIGrid
{

    public $conexao;

    /**
     * Abre conexão com novo banco
     * @return pg_connect $con conexão
     */
    public function getConexao(EntidadeEmpresa $entidade)
    {
        $host = $entidade->getIp();
        $porta = $entidade->getPorta();
        $username = strtolower($entidade->getLogin());
        $con = pg_connect("host=$host port=$porta dbname=" . strtolower($entidade->getBanco()) . " user=$username password=maxr2w2e8f4");
        return $con;
    }

    /**
     * Executa sql
     */
    public function executar($conexao, $sql)
    {
        return @pg_query($conexao, $sql);
    }

    /**
     * Método que cria/executa query's do novo banco
     * @param array $entidade Dados do banco de dados criado
     * @return array Execução de query das criações de tabelas
     */
    public function criarTabela($entidade)
    {

        $tabelas = $this->listaTabelas();

        $this->conexao = $this->getConexao($entidade);

        $this->criarColunas();

        foreach ($tabelas as $tabela => $create) {

            $sql = "SELECT 1 FROM pg_class WHERE relname = '$tabela';";

            $result = pg_query($this->conexao, $sql);

            $retorno = '';
            while ($myrow = pg_fetch_assoc($result)) {
                $retorno = $myrow;
            }

            if (!is_array($retorno)) {
                if (!$this->criarSequencia($tabela)) {
                    $this->transition('ROLLBACK');
                    //throw new \Exception("Erro ao criar sequencia na tabela $tabela");
                }
                if (!$this->executar($this->conexao, $create)) {
                    $this->transition('ROLLBACK');
                    //throw new \Exception("Erro ao criar tabela $tabela");
                }
            }
        }

        $this->inserirCeps();

        pg_close($this->conexao);

        return $this->getTabelaDescricao();
    }

    public function getTabelaDescricao()
    {
        return [
            'mxlgncfi' => 'Validação de login',
            'mxvcocfi' => 'Validação do convite para o cliente final',
            'mxcfwcfi' => 'Configuração web (b2b)',
            'mxglocfi' => 'Funcionários',
            'mxtpecfi' => 'Vendas temporárias',
            'mxtitcfi' => 'Produtos das vendas temporárias',
            'mxpdwcfi' => 'Vendas pelo pagseguro/moip',
            'mximacfi' => 'Diretório de imagens do B2B',
            'mxgeocfi' => 'Latitude e longitude dos ceps brasileiros',
            'mxvrfcfi' => 'Vínculo de representante para os funcionários',
        ];
    }

    public function transition($tipo)
    {
        return $this->executar($this->conexao, $tipo);
    }

    /**
     * Método para criar sequencia
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function criarSequencia($tabela)
    {
        $sql = "CREATE SEQUENCE " . $tabela . "_sq
                INCREMENT 1
                MINVALUE 1
                MAXVALUE 9223372036854775807
                START 1
                CACHE 1";
        $this->executar($this->conexao, $sql);
    }

    /**
     * Método para criar sequencia
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function criarColunas()
    {
        $sql = "SELECT 1 FROM pg_class WHERE relname = 'mxemlcfi'";

        $result = $result = pg_query($this->conexao, $sql);
        $retorno = '';
        while ($myrow = pg_fetch_assoc($result)) {
            $retorno = $myrow;
        }

        if (is_array($retorno)) {
            $sql = "SELECT column_name FROM information_schema.columns WHERE table_name='mxemlcfi' and column_name='cfiatveml'";
            $result = $result = pg_query($this->conexao, $sql);
            $retorno = '';
            while ($myrow = pg_fetch_assoc($result)) {
                $retorno = $myrow;
            }
            if (empty($retorno)) {
                $sql = "ALTER TABLE MXEMLCFI ADD cfiatveml numeric(1,0) NOT NULL DEFAULT 0";
                $this->executar($this->conexao, $sql);
            }
        }

        return true;
    }

    /**
     * Método listando tabelas e seus create
     * @return array $tabelas Tabelas e seus create
     */
    public function listaTabelas()
    {
        $tabelas = [
            'mxgeocfi' => "
                        CREATE TABLE mxgeocfi
                        (
                          cficepgeo character varying(8) NOT NULL DEFAULT ''::character varying,
                          cfilatgeo character varying(20) NOT NULL DEFAULT ''::character varying,
                          cfilongeo character varying(20) NOT NULL DEFAULT ''::character varying
                        )
                        WITH (
                          OIDS=FALSE
                        );
                        CREATE INDEX index_cep ON mxgeocfi (cficepgeo);",
            'mxcfwcfi' => "
                        CREATE TABLE mxcfwcfi
                        (
                          cfiatvcfw numeric(9,0) NOT NULL DEFAULT 0,
                          cfiltbcfw text,
                          cfivalcfw text,
                          cfinomcfw character(9) NOT NULL DEFAULT ''::bpchar,
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxcfwcfi_sq'::regclass),

                         CONSTRAINT mxcfwcfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                         OIDS=FALSE,
                         autovacuum_enabled=true
                        );",
            'mxglocfi' => "
                        CREATE TABLE mxglocfi
                        (
                          cficodglo serial NOT NULL,
                          cfinomglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfiemlglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfitelglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfilgnglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfisenglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfirepglo numeric(5,0) NOT NULL DEFAULT 0,
                          cfidtaglo timestamp without time zone DEFAULT now(),
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxglocfi_sq'::regclass),
                          cfistaglo character(5) NOT NULL DEFAULT '0'::bpchar,
                          CONSTRAINT mxglocfi_pkey PRIMARY KEY (cficodglo)
                        )
                        WITH (
                          OIDS=FALSE
                        );
                        ALTER TABLE mxglocfi
                          OWNER TO postgres;
                        ",
            'mxtpecfi' => "
                        CREATE TABLE mxtpecfi
                        (
                          cficodped numeric(7,0) NOT NULL DEFAULT 0,
                          cfinnfped numeric(6,0) NOT NULL DEFAULT 0,
                          cfincfped numeric(6,0) NOT NULL DEFAULT 0,
                          cfiecfped numeric(4,0) NOT NULL DEFAULT 0,
                          cficliped numeric(9,0) NOT NULL DEFAULT 0,
                          cfifanped character(15) NOT NULL DEFAULT ''::bpchar,
                          cfinclped character varying(40) NOT NULL DEFAULT ''::character varying,
                          cfiendped character varying(35) NOT NULL DEFAULT ''::character varying,
                          cfibaiped character varying(20) NOT NULL DEFAULT ''::character varying,
                          cficidped character varying(60) NOT NULL DEFAULT ''::character varying,
                          cfiestped character(2) NOT NULL DEFAULT ''::bpchar,
                          cficepped character(9) NOT NULL DEFAULT ''::bpchar,
                          cficpfped character varying(19) NOT NULL DEFAULT ''::character varying,
                          cfinrgped character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfifonped character varying(12) NOT NULL DEFAULT ''::character varying,
                          cficlbped character varying(19) NOT NULL DEFAULT ''::character varying,
                          cfidtcped date,
                          cfidteped date,
                          cfihorped character(8) NOT NULL DEFAULT ''::bpchar,
                          cfistaped character(5) NOT NULL DEFAULT ''::bpchar,
                          cfitpnped character(4) NOT NULL DEFAULT ''::bpchar,
                          cfivalped numeric(15,2) NOT NULL DEFAULT 0,
                          cfivnfped numeric(15,2) NOT NULL DEFAULT 0,
                          cfidscped numeric(12,2) NOT NULL DEFAULT 0,
                          cfifreped numeric(12,2) NOT NULL DEFAULT 0,
                          cfisegped numeric(12,2) NOT NULL DEFAULT 0,
                          cficpgped numeric(3,0) NOT NULL DEFAULT 0,
                          cfidesped character varying(30) NOT NULL DEFAULT ''::character varying,
                          cfirefped character(3) NOT NULL DEFAULT ''::bpchar,
                          cfiparped numeric(2,0) NOT NULL DEFAULT 0,
                          cfiindped numeric(8,4) NOT NULL DEFAULT 0,
                          cfidiaped numeric(3,0) NOT NULL DEFAULT 0,
                          cficarped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp1ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp2ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp3ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp4ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp5ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp6ped numeric(3,0) NOT NULL DEFAULT 0,
                          cfifpgped numeric(1,0) NOT NULL DEFAULT 0,
                          cfilisped numeric(3,0) NOT NULL DEFAULT 0,
                          cfirepped numeric(5,0) NOT NULL DEFAULT 0,
                          cfitraped numeric(5,0) NOT NULL DEFAULT 0,
                          cfinatped numeric(4,2) NOT NULL DEFAULT 0,
                          cfina2ped numeric(4,2) NOT NULL DEFAULT 0,
                          cfinopped character(6) NOT NULL DEFAULT ''::bpchar,
                          cfino2ped character(6) NOT NULL DEFAULT ''::bpchar,
                          cfiqtdped numeric(6,0) NOT NULL DEFAULT 0,
                          cfiespped character varying(20) NOT NULL DEFAULT ''::character varying,
                          cfiviaped character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfiseuped character(10) NOT NULL DEFAULT ''::bpchar,
                          cfirecped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfifedped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfiob1ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob2ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob3ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob4ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob5ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfirotped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfincxped numeric(3,0) NOT NULL DEFAULT 0,
                          cfinljped numeric(3,0) NOT NULL DEFAULT 0,
                          cfipetped numeric(7,0) NOT NULL DEFAULT 0,
                          cfiemlped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfioutped numeric(12,2) NOT NULL DEFAULT 0,
                          cfistoped character(5) NOT NULL DEFAULT ''::bpchar,
                          cficreped numeric(7,0) NOT NULL DEFAULT 0,
                          cficpiped character(19) NOT NULL DEFAULT ''::bpchar,
                          cfiljfped numeric(3,0) NOT NULL DEFAULT 0,
                          cfiusuped character varying(35) NOT NULL DEFAULT ''::character varying,
                          cfioriped character(1) NOT NULL DEFAULT ''::bpchar,
                          cficplped character varying(30) NOT NULL DEFAULT ''::character varying,
                          cficudped numeric(5,0) NOT NULL DEFAULT 0,
                          cfigrjped numeric(12,2) NOT NULL DEFAULT 0,
                          cfinenped character(10) NOT NULL DEFAULT ''::bpchar,
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxtpecfi_sq'::regclass),
                          indkey_001 character varying(254),
                          cfitcfped character(3) NOT NULL DEFAULT ''::bpchar,
                          cfirpaped numeric(5,0) NOT NULL DEFAULT 0,
                          cfinseped character varying(15) NOT NULL DEFAULT ''::character varying,
                          CONSTRAINT mxtpecfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE,
                          autovacuum_enabled=true
                        );
                        ALTER TABLE mxtpecfi
                          OWNER TO postgres;

                        CREATE INDEX mxtpecfi_mxtpec10
                          ON mxtpecfi
                          USING btree
                          (cfincxped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpec11
                          ON mxtpecfi
                          USING btree
                          (cficodped, cfinljped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpec12
                          ON mxtpecfi
                          USING btree
                          (cficreped, cfinljped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf1
                          ON mxtpecfi
                          USING btree
                          (cficodped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf2
                          ON mxtpecfi
                          USING btree
                          (cfirepped, cfidteped, sr_recno);


                        CREATE INDEX mxtpecfi_mxtpecf3
                          ON mxtpecfi
                          USING btree
                          (cfidteped, cfinljped, cficodped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf4
                          ON mxtpecfi
                          USING btree
                          (cficliped, cfidteped, sr_recno);

                        -- Index: mxtpecfi_mxtpecf5

                        -- DROP INDEX mxtpecfi_mxtpecf5;

                        CREATE INDEX mxtpecfi_mxtpecf5
                          ON mxtpecfi
                          USING btree
                          (cfifanped, cficodped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf6
                          ON mxtpecfi
                          USING btree
                          (cfistaped, cficliped, cfidteped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf7
                          ON mxtpecfi
                          USING btree
                          (cfincfped, cfiecfped, cfidteped, sr_recno);

                        CREATE INDEX mxtpecfi_mxtpecf8
                          ON mxtpecfi
                          USING btree
                          (indkey_001);

                        -- Index: mxtpecfi_mxtpecf9

                        -- DROP INDEX mxtpecfi_mxtpecf9;

                        CREATE INDEX mxtpecfi_mxtpecf9
                          ON mxtpecfi
                          USING btree
                          (cficpiped, cfidteped, sr_recno);

                        CREATE INDEX mxtpecfi_sr
                          ON mxtpecfi
                          USING btree
                          (sr_recno);",
            'mxtitcfi' => "
                        CREATE TABLE mxtitcfi
                        (
                          cfipedite numeric(7,0) NOT NULL DEFAULT 0,
                          cfiproite character(9) NOT NULL DEFAULT ''::bpchar,
                          cfidesite character(40) NOT NULL DEFAULT ''::bpchar,
                          cfiembite character(5) NOT NULL DEFAULT ''::bpchar,
                          cfifabite character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfitriite character(3) NOT NULL DEFAULT ''::bpchar,
                          cfimaqite character(1) NOT NULL DEFAULT ''::bpchar,
                          cfivenite character(1) NOT NULL DEFAULT ''::bpchar,
                          cfiobsite character varying(25) NOT NULL DEFAULT ''::character varying,
                          cfidatite date,
                          cfiqtdite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiprcite numeric(13,4) NOT NULL DEFAULT 0,
                          cfiprlite numeric(13,4) NOT NULL DEFAULT 0,
                          cfidscite numeric(12,4) NOT NULL DEFAULT 0,
                          cfiacrite numeric(12,4) NOT NULL DEFAULT 0,
                          cfiratite numeric(9,3) NOT NULL DEFAULT 0,
                          cfiipiite numeric(5,2) NOT NULL DEFAULT 0,
                          cfiicmite numeric(5,2) NOT NULL DEFAULT 0,
                          cfiricite numeric(5,2) NOT NULL DEFAULT 0,
                          cfibruite numeric(8,3) NOT NULL DEFAULT 0,
                          cfiliqite numeric(8,3) NOT NULL DEFAULT 0,
                          cfiqaeite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiqprite numeric(13,6) NOT NULL DEFAULT 0,
                          cficorite character(30) NOT NULL DEFAULT ''::bpchar,
                          cfinljite numeric(3,0) NOT NULL DEFAULT 0,
                          cfipcuite numeric(13,4) NOT NULL DEFAULT 0,
                          cfiqdvite numeric(13,6) NOT NULL DEFAULT 0,
                          cfimotite character varying(27) NOT NULL DEFAULT ''::character varying,
                          cfilisite numeric(3,0) NOT NULL DEFAULT 0,
                          cfinpdite character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfiipdite numeric(6,0) NOT NULL DEFAULT 0,
                          cfialiite numeric(5,2) NOT NULL DEFAULT 0,
                          cfimvaite numeric(6,2) NOT NULL DEFAULT 0,
                          cfirfrite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirsgite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirdsite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirouite numeric(12,2) NOT NULL DEFAULT 0,
                          cfincmite character(8) NOT NULL DEFAULT ''::bpchar,
                          cfivnmite numeric(3,0) NOT NULL DEFAULT 0,
                          cfiqsaite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiaesite numeric(5,2) NOT NULL DEFAULT 0,
                          cfichvite character(10) NOT NULL DEFAULT ''::bpchar,
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxtitcfi_sq'::regclass),
                          CONSTRAINT mxtitcfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE,
                          autovacuum_enabled=true
                        );
                        ALTER TABLE mxtitcfi
                          OWNER TO postgres;

                        CREATE INDEX mxtitcfi_mxtitcf1
                          ON mxtitcfi
                          USING btree
                          (cfipedite, cfiproite, cfidesite, cfiembite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf2
                          ON mxtitcfi
                          USING btree
                          (cfiproite, cfidatite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf3
                          ON mxtitcfi
                          USING btree
                          (cfipedite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf4
                          ON mxtitcfi
                          USING btree
                          (cfidatite, cfiproite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf5
                          ON mxtitcfi
                          USING btree
                          (cfipedite, cfinljite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf6
                          ON mxtitcfi
                          USING btree
                          (cfidatite, cfinljite, sr_recno);

                        CREATE INDEX mxtitcfi_mxtitcf7
                          ON mxtitcfi
                          USING btree
                          (cfiproite, cfidesite, cfiembite, cficorite, sr_recno);

                        CREATE INDEX mxtitcfi_sr
                          ON mxtitcfi
                          USING btree
                          (sr_recno);",
//            'mxloncfi' => "
//                        CREATE TABLE mxloncfi
//                        (
//                          cficodlon serial NOT NULL,
//                          cfinuslon character(35) NOT NULL DEFAULT ''::bpchar,
//                          cfiemllon character(40) NOT NULL DEFAULT ''::bpchar,
//                          cfiipllon character(35) NOT NULL DEFAULT ''::bpchar,
//                          cfidtalon timestamp without time zone DEFAULT now(),
//                          CONSTRAINT mxloncfi_pkey PRIMARY KEY (cficodlon)
//                        )
//                        WITH (
//                          OIDS=FALSE
//                        );
//                        ALTER TABLE mxloncfi
//                          OWNER TO postgres;",
            'mxvcocfi' => "
            CREATE TABLE mxvcocfi
                    (
                      cfikeyvco character(32) NOT NULL DEFAULT ''::bpchar,
                      cfiatvvco numeric(1,0) NOT NULL DEFAULT 0,
                      cficlivco numeric(9,0) NOT NULL DEFAULT 0,
                      cfidtavco date not null default CURRENT_DATE,
                      sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxvcocfi_sq'::regclass),
                      CONSTRAINT mxvcocfi_sr_recno_key UNIQUE (sr_recno)
                    )
                    WITH (
                      OIDS=FALSE
                    );
                    ALTER TABLE mxvcocfi
                      OWNER TO postgres;
                      ",
            'mximacfi' => "
                        CREATE TABLE mximacfi
                        (
                          cficodima character(9),
                          cfidirima text,
                          cfitipima character(15),
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mximacfi_sq'::regclass),
                          CONSTRAINT mximacfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE
                        );
                        ALTER TABLE mximacfi
                          OWNER TO postgres;",
            'mxlgncfi' => "
                        CREATE TABLE mxlgncfi
                        (
                            cfitknlgn character(40) NOT NULL DEFAULT ''::bpchar,
                            cficodlgn numeric(9,0) NOT NULL DEFAULT 0,
                            sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxlgncfi_sq'::regclass),
                            CONSTRAINT mxlgncfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE
                        );
                        ALTER TABLE mxlgncfi
                            OWNER TO postgres;",
            'mxvrfcfi' => "
                CREATE TABLE mxvrfcfi
                (
                  cficodvrf numeric(9,0) NOT NULL DEFAULT nextval('mxglocfi_sq'::regclass),
                  cfirepvrf numeric(9,0) NOT NULL DEFAULT 0,
                  sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxvrfcfi_sq'::regclass),
                  CONSTRAINT mxvrfcfi_sr_recno_key UNIQUE (sr_recno)
                )
                WITH (
                  OIDS=FALSE
                );

                 CREATE INDEX mxvrfcfi_index
                   ON mxvrfcfi
                   USING btree
                   (cficodvrf, cfirepvrf);",
            'mxpdwcfi' => "CREATE TABLE mxpdwcfi
                            (
                              cfipedpdw numeric(9,0) NOT NULL DEFAULT 0,
                              cfirecpdw character varying(32),
                              cfitrapdw character varying(44),
                              cfinotpdw character varying(60),
                              cfitippdw character varying(18),
                              cfistapdw character varying(22),
                              cfidtgpdw date DEFAULT ('now'::text)::date,
                              cfidtppdw date,
                              cfidtvpdw date DEFAULT (('now'::text)::date + 10),
                              sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxpdwcfi_sq'::regclass),
                              CONSTRAINT mxpdwcfi_sr_recno_key UNIQUE (sr_recno)
                            )
                            WITH (
                              OIDS=FALSE
                            );
                            ALTER TABLE mxpdwcfi
                              OWNER TO postgres;"
        ];
        return $tabelas;
    }

    public function inserirCeps()
    {
        $retorno = '';
        $sql = "SELECT count(1) FROM mxgeocfi";
        $resultado = pg_query($this->conexao, $sql);

        while ($myrow = pg_fetch_assoc($resultado)) {
            $retorno = $myrow;
        }

        if (isset($retorno['count']) && (int) $retorno['count'] === 0) {
            ini_set('memory_limit', '-1');
            $file = file(__DIR__ . '/mxgeocfi.csv', FILE_IGNORE_NEW_LINES);

            pg_exec($this->conexao, "COPY mxgeocfi FROM stdin");


            foreach ($file as $line) {

                $tmp = explode(",", $line);

                pg_put_line($this->conexao, sprintf("%d\t%s\t%s\n", $tmp[0], $tmp[1], $tmp[2]));
            }

            pg_put_line($this->conexao, "\\.\n");
            pg_end_copy($this->conexao);
        }

        return true;
    }

}
