<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use ZfcBase\Mapper\AbstractDbMapper;
use Cadastros\Entity\Empresa as EntidadeEmpresa;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Empresa extends AbstractDbMapper
{

    protected $conexao = [];

    public function setConexao(EntidadeEmpresa $empresa)
    {
        $this->conexao = [
            'host' => $empresa->getIp(),
            'port' => $empresa->getPorta(),
            'dbname' => $empresa->getBanco(),
            'user' => $empresa->getLogin(),
            'password' => 'maxr2w2e8f4'
        ];

        return $this->testarConexao($empresa);
    }

    public function testarConexao()
    {
        $ping = pg_ping(@pg_connect(
                        "host=" . $this->conexao['host'] . "
                port=" . $this->conexao['port'] . "
                dbname=" . strtolower($this->conexao['dbname']) . "
                user=" . strtolower($this->conexao['user']) . "
                password=" . $this->conexao['password'] . " connect_timeout=10"
        ));

        return $ping;
    }

    /**
     * Verifica se registro existe conexao no banco
     * @param array
     * @return boolean
     */
    public function verificarRegistro($parametros)
    {
        $where = new Where();
        $where->equalTo($parametros['coluna'], strtoupper($parametros['valor']));

        $select = $this->getSelect()->from("mxvldcfi");
        $select->columns(['*']);
        $select->where($where);

        return empty($this->select($select)->toArray());
    }

    /**
     * Inserindo produto no banco
     * @param \Cadastro\Entity\Empresa
     * @return boolean
     */
    public function adicionar(EntidadeEmpresa $empresa)
    {
        $empresa->setCodigo($this->gerarCodigo());
        $array = $this->entityToArray($empresa);

        unset($array['cficiepoe']);
        unset($array['cfipagpoe']);
        unset($array['cfimoipoe']);

        $result = $this->insert($array, "mxvldcfi");
        return $result->getAffectedRows() ? $empresa->getCodigo() : false;
    }

    /**
     * @return int id do novo produto
     */
    public function gerarCodigo()
    {
        $select = $this->getSelect()->
                columns([
                    'sr_recno' => new Expression('MAX(CAST(sr_recno AS INT))'),
                ])->
                from("mxvldcfi");

        $resultado = $this->select($select);
        $retorno = $resultado->getDataSource()->current()["sr_recno"] + 1;

        return $retorno;
    }

    /**
     * Editando (update) em produto
     * @param \Cadastro\Entity\Empresa
     * @return boolean
     */
    public function editar(EntidadeEmpresa $empresa)
    {
        $where = new Where();

        $where->equalTo("sr_recno", $empresa->getCodigo());
        $array = $this->entityToArray($empresa);

        unset($array['cficiepoe']);
        unset($array['cfipagpoe']);
        unset($array['cfimoipoe']);

        $resultado = $this->update($array, $where, "mxvldcfi");
        return $resultado->getAffectedRows() ? $empresa->getEmpresa() : false;
    }

    /**
     * Excluindo empresa
     * @param \Cadastro\Entity\Empresa
     * @return boolean
     */
    public function remover(EntidadeEmpresa $empresa)
    {
        $where = new Where();
        $where->equalTo("sr_recno", $empresa->getCodigo());
        $this->delete($where, "mxvldcfi");

        return [true];
    }

    /**
     * Selecionando produtos
     * @param Where $where
     * @return obj $retorno dados selecionados
     */
    public function selecionar(Where $where, $config)
    {
        $selectCont = $this->getSelect()->from("mxvldcfi");
        $selectCont->columns(array("total" => new Expression('count(*)')));
        $selectCont->where($where);

        $coluns = array(
            'sr_recno',
            'cfiservld',
            'cfiapevld',
            'cfilogvld',
            'cfinipvld',
            'cfidbsvld',
            'cfiporvld',
            'cfinitvld',
            'cfidbtvld',
            'cfipotvld',
            'cfiqtlvld',
            'cfitusvld',
            'cfiprovld',
            'cfiqclvld',
            'cfiqmbvld',
            'cfidomvld',
            'cfisubvld',
            'totalTable' => new Expression('?', array($selectCont)),
        );

        $select = $this->getSelect()->from("mxvldcfi");
        $select->columns($coluns);
        $select->where($where);
        $select->join('mxpoecfi', 'cficodpoe = cfiservld', ['cficiepoe', 'cfimoipoe', 'cfipagpoe'], 'left');

        if (isset($config['order']) && !empty($config['order'])) {
            $select->order($config['order']['coluna'] . ' ' . $config['order']['valor']);
        }
        if (isset($config['offset']) && !empty($config['offset'])) {
            $select->offset($config['offset']);
        }
        if (isset($config['limit']) && !empty($config['limit'])) {
            $select->limit($config['limit']);
        }

        return $this->select($select);
    }

}
