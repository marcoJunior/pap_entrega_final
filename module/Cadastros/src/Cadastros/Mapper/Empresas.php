<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cadastros\Entity\Empresas as EntidadeEmpresas;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Empresas extends APIGrid
{

    public $tableName = 'mxvldcfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\Empresas';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setJoin(new APIGridJoinEntity(
                'mxpoecfi', 'mxpoecfi.cficodpoe = mxvldcfi.cfiservld',
                ['cficiepoe', 'cfimoipoe', 'cfipagpoe'], 'left outer'
        ));

        $this->setLimitOffset(true);

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '',
                    str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Intervalo de data
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereIntervalodata_bkp(FiltrosEntity $filtros)
    {

        $where = new Where();
        $inicial = date($filtros->getIntervaloData()->getInicial());
        $final = date($filtros->getIntervaloData()->getFinal());

        $where->between('cfidtemva', $inicial, $final);
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'sr_recno',
            'cfiservld',
            'cfiapevld',
            'cfinipvld',
            'cfidbsvld',
            'cfiporvld',
            'cfidomvld',
            'cfilogvld',
            'cfinitvld',
            'cfidbtvld',
            'cfipotvld',
            'cfiqtlvld',
            'cfitusvld',
            'cfiprovld',
            'cfiqclvld',
            'cfiqmbvld',
            'cfisubvld',
        ];
    }

    /**
     * @return int id do novo produto
     */
    public function gerarCodigo()
    {
        $select = $this->getSelect()->
                columns([
            'sr_recno' => new Expression('MAX(CAST(sr_recno AS INT))'),
        ]);

        $resultado = $this->select($select);
        $retorno = $resultado->getDataSource()->current()["sr_recno"] + 1;

        return $retorno;
    }

    public function adicao($formulario, APIGridEntityAction $postEntity)
    {
        $entityFormaulario = new EntidadeEmpresas();
        $entityFormaulario->exchangeArray($formulario);
        $entityFormaulario->setCodigo($this->gerarCodigo());
        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cficiepoe']);
        unset($lista['cfipagpoe']);
        unset($lista['cfimoipoe']);

        try {

            $select = $this->getSelect()
                    ->columns($this->getColunas());

            $where = new Where();
            $where->equalTo('cfiservld', $entityFormaulario->getSerie());
            $where->equalTo('cfiapevld', $entityFormaulario->getEmpresa());

            $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

            if (count($dbVerificaExistencia) > 0) {
                return "Cadastro já existente !";
            }
        } catch (Exception $exc) {
            return false;
        }

        return $this->insert($lista)->getAffectedRows();
    }

    public function alterar($formulario)
    {
        $entityFormaulario = new EntidadeEmpresas();
        $entityFormaulario->exchangeArray($formulario);

        $where = new Where();
        $where->equalTo('sr_recno', $entityFormaulario->getCodigo());

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cficiepoe']);
        unset($lista['cfipagpoe']);
        unset($lista['cfimoipoe']);

        return $this->update($lista, $where)->getAffectedRows();
    }

    public function excluir($codigo, $fantasia)
    {
        $where = new Where();
        $where->equalTo('cfiservld', $codigo);
        $where->equalTo('cfiapevld', $fantasia);

        return $this->delete($where)->getAffectedRows();
    }

}
