<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cadastros\Entity\EmpresasMax as EmpresasMaxEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class EmpresasMax extends APIGrid
{

    public $tableName = 'clientes';
    public $mapperName = 'Cadastros\Mapper\Hydrator\EmpresasMax';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

//        $this->setJoin([
//            'mxpoecfi', 'mxpoecfi.cficodpoe = mxvldcfi.cfiservld',
//            ['cficiepoe', 'cfimoipoe', 'cfipagpoe'],
//            'left outer'
//        ]);
//        foreach ($filtros->toArray() as $index => $valor) {
//            $valorDescricao = str_replace('_', '',
//                    str_replace('_de', '', ucfirst($index)));
//            $getValor = 'get' . $valorDescricao;
//            $setWhere = 'setWhere' . $valorDescricao;
//            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere}($filtros);
//            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere . "s"}($filtros);
//            }
//        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'razao_social',
            'fantasia',
            'documento',
            'logo',
            'ativo'
        ];
    }

    public function validaExistencia(EmpresasMaxEntity $entity)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('documento', $entity->getDocumento());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaExistenciaPorId($id)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(EmpresasMaxEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(EmpresasMaxEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
