<?php

namespace Cadastros\Mapper;

use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Entity\ForcaEmpresas as ForcaEmpresasEntity;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Where;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ForcaEmpresas extends APIGrid
{

    public $tableName = 'mxcamcfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\ForcaEmpresas';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setLimitOffset(true);

        $where = new Where();
        $where->equalTo('cfisiscam', 'csafv');
        $this->setWhere($where);

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '', str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereSerie(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('cfisercam', $filtros->getSerie());
        $this->setWhere($where);
        return $where;
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Intervalo de data
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereIntervalodata(FiltrosEntity $filtros)
    {

        $where = new Where();
        $inicial = new \DateTime(str_replace('/', '-', $filtros->getIntervaloData()->getInicial()));
        $final = new \DateTime(str_replace('/', '-', $filtros->getIntervaloData()->getFinal()));

        $where->between('cfidtacam', $inicial->format('d/m/Y'), $final->format('d/m/Y'));
        // $where->in('cfidtacam', ['']);
        // $this->setWhere($where);
        // return $where;
    }

    public function getColunas()
    {
        return [
            'cficodcam',
            'cfisercam',
            'cficlicam',
            'cfinipcam',
            'cfindbcam',
            'cfitipcam',
            'cfisiscam',
            'cfiqalcam',
            'cfiqaacam',
            'cfidtacam' => new Expression("
            CASE cfidtacam
                WHEN '' THEN 'Contratado'
                ELSE to_char(to_date(cfidtacam, 'YYYY-MM-DD'), 'DD/MM/YYYY')
            END"),
        ];
    }

    public function selecionarEmpresa($formulario)
    {
        $entityFormaulario = new ForcaEmpresasEntity();
        $entityFormaulario->exchangeArray($formulario);
        $entityFormaulario->setSistema("csafv");

        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('cfisercam', $entityFormaulario->getSerie());
        $where->equalTo('cfisiscam', $entityFormaulario->getSistema());

        return ConvertObject::convertObject($this->select($select->where($where)));
    }

    public function adicao($formulario)
    {
        $entityFormaulario = new ForcaEmpresasEntity();
        $entityFormaulario->exchangeArray($formulario);
        $entityFormaulario->setSistema("csafv");

        $where = new Where();
        $where->equalTo('cfisercam', $entityFormaulario->getSerie());
        $where->equalTo('cfisiscam', 'csafv');

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        try {

            $select = $this->getSelect()
                ->columns($this->getColunas());

            $where = new Where();
            $where->equalTo('cfisercam', $entityFormaulario->getSerie());
            $where->equalTo('cfisiscam', 'csafv');

            $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

            if (count($dbVerificaExistencia) > 0) {
                return "Cadastro já existente !";
            }
        } catch (Exception $exc) {
            return false;
        }

        return $this->insert($lista)->getAffectedRows();
    }

    public function alterar($formulario, $config = null, $tipo = 'localweb2')
    {

        if ($config === null) {
            return;
        }

        $dbAdapter = new Adapter($config[$tipo]);
        $this->setDbAdapter($dbAdapter);

        $entityFormaulario = new ForcaEmpresasEntity();
        $entityFormaulario->exchangeArray($formulario);
        $entityFormaulario->setSistema("csafv");

        $where = new Where();
        $where->equalTo('cfisercam', $entityFormaulario->getSerie());
        $where->equalTo('cfisiscam', 'csafv');

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        // unset($lista['cfiapemva']);
        if ($tipo === 'localweb2') {
            return $this->update($lista, $where)->getAffectedRows();
        } else if ($tipo === 'cliente') {
            $this->getDbAdapter()->query('ALTER TABLE MXCAMCFI ALTER COLUMN CFIDTACAM TYPE VARCHAR(60)')->execute();
            $this->delete(new Where())->getAffectedRows();
            return $this->insert($lista)->getAffectedRows();
        }
    }

    public function excluir($codigo, $modulo)
    {
        $where = new Where();
        $where->equalTo('cfisercam', $codigo);
        $where->equalTo('cfisiscam', $modulo);

        return $this->delete($where)->getAffectedRows();
    }

}
