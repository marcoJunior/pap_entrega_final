<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use APIGrid\Mapper\APIGrid;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cadastros\Entity\Empresa as EntidadeEmpresa;
use Cadastros\Entity\ForcaEmpresas as ForcaEmpresasEntity;
use Cadastros\Entity\ForcaSeries as ForcaSeriesEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Mapper\Hydrator\ForcaSeries as ForcaSeriesHydrator;
use Zend\Db\Adapter\Exception\RuntimeException;
use Zend\Db\Adapter\Exception\InvalidQueryException;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ForcaSeries extends APIGrid
{

    protected $conexao = [];
    public $tableName = 'mxcelcfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\ForcaSeries';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros, $conexaoDb)
    {

        $dbAdapter = new Adapter($conexaoDb);
        $this->setDbAdapter($dbAdapter);

        $this->atualizaTabela();

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setLimitOffset(true);

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '', str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity, $conexaoDb);
        } catch (Exception $exc) {
            return false;
        }
    }

    function atualizaTabela()
    {
        try {
            $updateTabela = 'ALTER TABLE MXCELCFI ADD COLUMN cficodcel bigserial primary key;';
            $this->getDbAdapter()->query($updateTabela)->execute();
        } catch (InvalidQueryException $exc) {
            $this->criaTabela();
        } catch (RuntimeException $error) {
            $msg = 'Não foi possivel estabelecer conexão com o servidor de banco de dados!<br>'
                    . 'Verifique as informações e tente novamente!';
            return [
                'mensagem' => $msg
            ];
        }
    }

    function criaTabela()
    {
        try {
            $createTabela = "CREATE TABLE mxcelcfi (" .
                    "cficodcel bigserial primary key," .
                    "cfisercel character varying(20) NOT NULL DEFAULT ''::character varying," .
                    "cfisiscel character varying(20) NOT NULL DEFAULT ''::character varying," .
                    "cficidcel character varying(100) NOT NULL DEFAULT ''::character varying," .
                    "cfirepcel integer NOT NULL DEFAULT 0," .
                    "cfiaticel character varying(20) NOT NULL DEFAULT ''::character varying" .
                    ");";
            $this->getDbAdapter()->query($createTabela)->execute();
        } catch (InvalidQueryException $ex) {

        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereCodigo(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('cfisercel', $filtros->getCodigo());
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'cficodcel',
            'cfisercel',
            'cficidcel',
            'cfirepcel',
            'cfiaticel',
        ];
    }

    public function gerarListaSeries($serie, $nCodigos = 1)
    {

        $arraySeries = [];
        while ($nCodigos >= $nCont) {
            if ($nCont == 10 || $nCont == 100 || $nCont == 1000) {
                $nCodigos++;
                $nCont++;
            }
            array_push($arraySeries, $this->gerarCodigo($serie, $nCont));
            $nCont++;
        }

        return $arraySeries;
    }

    public function gerarCodigo($serie = '', $conexaoDb)
    {

        $dbAdapter = new Adapter($conexaoDb);
        $this->setDbAdapter($dbAdapter);

        if (!isset($serie)) {
            return 'Lista de codigos de serie, só pode ser gerada, há partir de um código de série do Control Shop !';
        }

        $select = $this->getSelect()
                ->columns(['total' => new Expression('count(*)')]);

        $where = new Where();
        $where->equalTo('cfisercel', $serie);
        $where->equalTo('cfisiscel', 'csafv');

        $valorString = '';
        $nCont = (int) $this->select($select->where($where))->current()->getTotal() + 1;
        $valor1 = ($nCont * ((int) $serie) * 123);

        $i = 1;
        $nTot = strlen((string) $valor1);

        while ($nTot > $i) {
            $valorString .= substr($valor1, 0, $nTot);
            $i++;
        }

        $valorString = (int) substr($valorString, 0, 6);
        $cRet = substr((string) ($valorString * 975), 0, 6);

        $insertSerieNew['cfisercel'] = $serie;
        $insertSerieNew['cfisiscel'] = 'csafv';
        $insertSerieNew['cfiaticel'] = $cRet;

        $this->insert($insertSerieNew);
        return $cRet;
    }

    public function adicao($formulario, APIGridEntityAction $postEntity)
    {
        $entityFormaulario = new ForcaSeriesEntity();
        $entityFormaulario->exchangeArray($formulario);
        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        try {

            $select = $this->getSelect()
                    ->columns($this->getColunas());

            $where = new Where();
            $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
            $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

            $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

            if (count($dbVerificaExistencia) > 0) {
                return "Cadastro já existente !";
            }
        } catch (Exception $exc) {
            return false;
        }

        return $this->insert($lista)->getAffectedRows();
    }

    public function alterar($formulario)
    {
        $entityFormaulario = new ForcaSeriesEntity();
        $entityFormaulario->exchangeArray($formulario);

        $where = new Where();
        $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
        $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        return $this->update($lista, $where)->getAffectedRows();
    }

    public function excluir($codigo, $modulo)
    {
        $where = new Where();
        $where->equalTo('cfivldmva', $codigo);
        $where->equalTo('cfimdlmva', $modulo);

        return $this->delete($where)->getAffectedRows();
    }

    public function empresasSerieExcluir($post, $conexaoDb)
    {
        $dbAdapter = new Adapter($conexaoDb);
        $this->setDbAdapter($dbAdapter);

        $where = new Where();
        $where->equalTo('cficodcel', (integer) $post->get('codigo'));

        return $this->delete($where)->getAffectedRows();
    }

    public function empresasSerieLimpar($post, $conexaoDb)
    {
        $dbAdapter = new Adapter($conexaoDb);
        $this->setDbAdapter($dbAdapter);

        $where = new Where();
        $where->equalTo('cficodcel', (integer) $post->get('codigo'));

        $lista = [
            'cficidcel' => '',
            'cfirepcel' => 0,
        ];

        return $this->update($lista, $where, 'mxcelcfi')->getAffectedRows();
    }

}
