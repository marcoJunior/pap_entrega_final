<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Cliente extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Cliente';
    }

    public function getMap()
    {
        return [
            'codigo' => 'cficodlgn',
            'email' => 'cfiemllgn',
            'senha' => 'cfisenlgn',
            'empresa' => 'cfiapelgn',
            'serie' => 'cfiserlgn',
            'nivel' => 'cfinivlgn',
            'sistema' => 'cfisislgn',
            'ativo' => 'cfiatvlgn',
            'referencia' => 'cfireflgn',
        ];
    }
    public static function getColuna($coluna)
    {
        $mapa = new Cliente();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
