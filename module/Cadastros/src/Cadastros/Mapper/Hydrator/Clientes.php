<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Clientes extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Clientes';
    }

    public function getMap()
    {
        return [
            'codigo' => 'cficodlgn',
            'email' => 'cfiemllgn',
            'senha' => 'cfisenlgn',
            'empresa' => 'cfiapelgn',
            'serie' => 'cfiserlgn',
            'nivel' => 'cfinivlgn',
            'sistema' => 'cfisislgn',
            'ativo' => 'cfiatvlgn',
            'referencia' => 'cfireflgn',
        ];
    }
    public static function getColuna($coluna)
    {
        $mapa = new Clientes();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
