<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexoesEmpresas extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\ConexoesEmpresas';
    }

    public function getMap()
    {
        return [];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'documento',
            'fantasia',
            'iterator'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ConexoesEmpresas();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
