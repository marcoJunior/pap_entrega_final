<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Criacao extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Criacao';
    }

    protected function getMap()
    {
        return [
            'codigo' => 'sr_recno',
            'serie' => 'cfiservld',
            'empresa' => 'cfiapevld',
            'login' => 'cfilogvld',
            'ip' => 'cfinipvld',
            'banco' => 'cfidbsvld',
            'porta' => 'cfiporvld',
            'iptintometrico' => 'cfinitvld',
            'bancotintometrico' => 'cfidbtvld',
            'portatintometrico' => 'cfipotvld',
            'qtdLogins' => 'cfiqtlvld',
            'plano' => 'cfitusvld',
            'projetos' => 'cfiprovld',
            'qtdClientes' => 'cfiqclvld',
            'dominio' => 'cfidomvld',
            'subdominio' => 'cfisubvld',
            'qtdMb' => 'cfiqmbvld',
            'cielo' => 'cficiepoe',
            'pagseguro' => 'cfipagpoe',
            'moip' => 'cfimoipoe',
        ];
    }

}
