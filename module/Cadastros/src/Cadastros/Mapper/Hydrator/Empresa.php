<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Empresa extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Empresa';
    }

    public function getMap()
    {
        return [
            'codigo' => 'sr_recno',
            'serie' => 'cfiservld',
            'empresa' => 'cfiapevld',
            'login' => 'cfilogvld',
            'ip' => 'cfinipvld',
            'banco' => 'cfidbsvld',
            'porta' => 'cfiporvld',
            'iptintometrico' => 'cfinitvld',
            'bancotintometrico' => 'cfidbtvld',
            'portatintometrico' => 'cfipotvld',
            'qtdLogins' => 'cfiqtlvld',
            'plano' => 'cfitusvld',
            'projetos' => 'cfiprovld',
            'qtdClientes' => 'cfiqclvld',
            'dominio' => 'cfidomvld',
            'subdominio' => 'cfisubvld',
            'qtdMb' => 'cfiqmbvld',
            'cielo' => 'cficiepoe',
            'pagseguro' => 'cfipagpoe',
            'moip' => 'cfimoipoe',
        ];
    }

}
