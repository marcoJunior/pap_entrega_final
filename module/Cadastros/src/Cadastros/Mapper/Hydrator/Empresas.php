<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Empresas extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Empresas';
    }

    public function getMap()
    {
        return [
            'codigo'                          => 'sr_recno',
            'serie'                             => 'cfiservld',
            'empresa'                      => 'cfiapevld',
            'login'                              => 'cfilogvld',
            'ip'                                   => 'cfinipvld',
            'banco'                           => 'cfidbsvld',
            'porta'                            => 'cfiporvld',
            'iptintometrico'              => 'cfinitvld',
            'bancotintometrico'      => 'cfidbtvld',
            'portatintometrico'       => 'cfipotvld',
            'qtdLogins'                     => 'cfiqtlvld',
            'plano'                            => 'cfitusvld',
            'projetos'                       => 'cfiprovld',
            'qtdClientes'                  => 'cfiqclvld',
            'dominio'                        => 'cfidomvld',
            'subdominio'                  => 'cfisubvld',
            'qtdMb'                          => 'cfiqmbvld',
            'cielo'                              => 'cficiepoe',
            'pagseguro'                   => 'cfipagpoe',
            'moip'                             => 'cfimoipoe',
        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Empresas();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
