<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class EmpresasMax extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\EmpresasMax';
    }

    public function getMap()
    {
        return [
//            'razaoSocial' => 'razao_social'
        ];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'idPlano',
            'iterator'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new EmpresasMax();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
