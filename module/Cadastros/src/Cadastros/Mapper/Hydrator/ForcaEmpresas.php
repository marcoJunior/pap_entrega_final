<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ForcaEmpresas extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\ForcaEmpresas';
    }

    public function getMap()
    {
        return [
            'codigo'               => 'cficodcam',
            'serie'                  => 'cfisercam',
            'fantasia'            => 'cficlicam',
            'ip'                       => 'cfinipcam',
            'banco'               => 'cfindbcam',
            'tipoLoja'            => 'cfitipcam',
            'sistema'            => 'cfisiscam',
            'qtdAdiquirida'   => 'cfiqalcam',
            'qtdUso'             => 'cfiqaacam',
            'dataValidade'   => 'cfidtacam',
        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ForcaEmpresas();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
