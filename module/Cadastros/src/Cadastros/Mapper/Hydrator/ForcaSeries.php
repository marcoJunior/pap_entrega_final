<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ForcaSeries extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\ForcaSeriesTotal';
    }

    public function getMap()
    {
        return [
            'codigo'        => 'cficodcel',
            'serie'         => 'cfisercel',
            'aparelho'      => 'cficidcel',
            'representante' => 'cfirepcel',
            'numForca'      => 'cfiaticel',
        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ForcaSeries();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
