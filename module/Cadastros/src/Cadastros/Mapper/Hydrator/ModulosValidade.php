<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ModulosValidade extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\ModulosValidade';
    }

    public function getMap()
    {
        return [
            'serie' => 'cfivldmva',
            'fantasia' => 'cfiapemva',
            'modulo' => 'cfimdlmva',
            'data' => 'cfidtemva',
            'dataValidade' => 'cfivalmva',
            'dias' => 'cfidvlmva',
            'diasVencer' => 'cfidivmva',
        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ModulosValidade();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
