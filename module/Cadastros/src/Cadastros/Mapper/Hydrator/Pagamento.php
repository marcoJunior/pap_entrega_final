<?php

namespace Cadastros\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Pagamento extends Hydrator
{

    protected function getEntity()
    {
        return 'Cadastros\Entity\Pagamento';
    }

    public function getMap()
    {
        return [
            'serie' => 'cficodpoe',
            'cielo' => 'cficiepoe',
            'pagseguro' => 'cfipagpoe',
            'moip' => 'cfimoipoe'
        ];
    }

    protected function getTemporary()
    {
        return [];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Pagamento();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
