<?php

namespace Cadastro\Mapper\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Usuario extends \APISql\Mapper\Hydrator\Hydrator
{

    protected function getEntity()
    {
        return 'Cadastro\Entity\Usuario';
    }

    public function getMap()
    {
        return [
            'codigo' => 'cficodusu',
            'ativacao' => 'cfiatvusu',
            'nome' => 'cfinomusu',
            'usuario' => 'cfiusuusu',
            'email' => 'cfiemlusu',
            'senha' => 'cfisenusu',
            'tipo' => 'cfitipusu',
        ];
    }

}
