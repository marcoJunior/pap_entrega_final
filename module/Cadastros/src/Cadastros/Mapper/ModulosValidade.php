<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Entity\ModulosValidade as ModulosValidadeEntity;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ModulosValidade extends APIGrid
{

    public $tableName = 'mxmvacfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\ModulosValidade';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());

        $this->setJoin(new APIGridJoinEntity('mxvldcfi',
                'mxmvacfi.cfivldmva = mxvldcfi.cfiservld',
                [
            'cfiapemva' => new Expression("mxvldcfi.cfiapevld"),
                ]
        ));

        $this->setLimitOffset(true);

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '',
                    str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereSerie(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('cfivldmva', $filtros->getSerie());
        $this->setWhere($where);
        return $where;
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por modulo do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereModulo(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('cfimdlmva', $filtros->getModulo());
        $this->setWhere($where);
        return $where;
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Intervalo de data
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereIntervalodata(FiltrosEntity $filtros)
    {

        $where = new Where();
        $inicial = new \DateTime(str_replace('/', '-',
                        $filtros->getIntervaloData()->getInicial()));
        $final = new \DateTime(str_replace('/', '-',
                        $filtros->getIntervaloData()->getFinal()));

        $where->between('cfidtemva', $inicial->format('d/m/Y'),
                $final->format('d/m/Y'));
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'cfivldmva',
            'cfimdlmva',
            'cfidtemva' => new Expression(" to_char(cfidtemva, 'DD/MM/YYYY') "),
            'cfidvlmva',
            'cfidivmva' => new Expression("to_char((cfidtemva + cfidvlmva::integer) - CURRENT_TIMESTAMP, 'DD')"),
            'cfivalmva' => new Expression(" to_char((cfidtemva + cfidvlmva::integer), 'DD/MM/YYYY') "),
        ];
    }

    public function adicao($formulario, APIGridEntityAction $postEntity)
    {
        $entityFormaulario = new ModulosValidadeEntity();
        $entityFormaulario->exchangeArray($formulario);
        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        try {

            $select = $this->getSelect()
                    ->columns($this->getColunas());

            $where = new Where();
            $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
            $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

            $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

            if (count($dbVerificaExistencia) > 0) {
                return "Cadastro já existente !";
            }
        } catch (Exception $exc) {
            return false;
        }

        return $this->insert($lista)->getAffectedRows();
    }

    public function alterar($formulario)
    {
        $entityFormaulario = new ModulosValidadeEntity();
        $entityFormaulario->exchangeArray($formulario);

        $where = new Where();
        $where->equalTo('cfivldmva', $entityFormaulario->getSerie());
        $where->equalTo('cfimdlmva', $entityFormaulario->getModulo());

        $arrayDb = $entityFormaulario->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        unset($lista['cfiapemva']);
        unset($lista['cfivalmva']);
        unset($lista['cfidivmva']);

        return $this->update($lista, $where)->getAffectedRows();
    }

    public function excluir($codigo, $modulo)
    {
        $where = new Where();
        $where->equalTo('cfivldmva', $codigo);
        $where->equalTo('cfimdlmva', $modulo);

        return $this->delete($where)->getAffectedRows();
    }

}
