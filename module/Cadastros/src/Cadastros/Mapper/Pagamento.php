<?php

namespace Cadastros\Mapper;

use Zend\Db\Sql\Where;
use ZfcBase\Mapper\AbstractDbMapper;
use Cadastros\Entity\Pagamento as PagamentoEntity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Pagamento extends AbstractDbMapper
{

    public $tableName = 'mxpoecfi';
    public $mapperName = 'Cadastros\Mapper\Hydrator\Pagamento';

    /**
     * Inserindo produto no banco
     * @param PagamentoEntity
     * @return boolean
     */
    public function adicionar(PagamentoEntity $pagamento)
    {
        $this->remover($pagamento);
        $arrayDb = $pagamento->toArray();

        $lista = [];
        foreach ((array) $arrayDb as $key => $value) {
            $mapper = $this->mapperName;
            $lista[$mapper::getColuna($key)] = isset($value) ? $value : $value;
        }

        return $this->insert($lista)->getAffectedRows() ;
    }

    /**
     * Inserindo produto no banco
     * @param PagamentoEntity
     * @return boolean
     */
    public function remover(PagamentoEntity $pagamento)
    {
        $where = new Where();
        $where->equalTo("cficodpoe", $pagamento->getSerie());
        $this->delete($where);

        return true;
    }

}
