<?php

namespace Cadastro\Mapper;

use ZfcBase\Mapper\AbstractDbMapper,
    Zend\Db\Sql\Where,
    Cadastro\Entity\Usuario as UsuarioEntity,
    Zend\Db\Sql\Expression;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Usuario extends AbstractDbMapper
{

    /**
     * Verifica se registro existe
     * @param array
     * @return boolean 
     */
    public function verificar($parametros)
    {
        $where = new Where();
        $where->equalTo('cfiemlusu', $parametros['email']);
        $where->Or->equalTo('cfiusuusu', $parametros['usuario']);

        $select = $this->getSelect()->from("mxusucfi");
        $select->columns(['*']);
        $select->where($where);

        return !empty($this->select($select)->toArray());
    }

    /**
     * Inserindo produto no banco
     * @param \Cadastro\Entity\Usuario
     * @return boolean 
     */
    public function adicionar(UsuarioEntity $usuario)
    {
        $usuario->setCodigo($this->gerarCodigo());
        $result = $this->insert($usuario, "mxusucfi");
        return $result->getAffectedRows() ? $usuario->getCodigo() : false;
    }

    /**
     * @return int Id do novo produto
     */
    public function gerarCodigo()
    {
        $select = $this->getSelect()->
                columns([
                    'cficodusu' => new Expression('MAX(CAST(cficodusu AS INT))'),
                ])->
                from("mxusucfi");

        $resultado = $this->select($select);
        $retorno = $resultado->getDataSource()->current()["cficodusu"] + 1;

        return $retorno;
    }

    /**
     * Editando (update) em produto
     * @param \Cadastro\Entity\Usuario
     * @return boolean 
     */
    public function editar(UsuarioEntity $usuario)
    {
        $where = new Where();
        $where->equalTo("cficodusu", $usuario->getCodigo());

        $resultado = $this->update($usuario, $where, "mxusucfi");
        return $resultado->getAffectedRows() ? (int) $usuario->getCodigo() : false;
    }

    /**
     * Excluindo empresa
     * @param \Cadastro\Entity\Usuario 
     * @return boolean 
     */
    public function remover(UsuarioEntity $usuario)
    {
        $where = new Where();
        $where->equalTo("cficodusu", $usuario->getCodigo());
        $this->delete($where, "mxusucfi");

        return [true];
    }

    /**
     * Responsável apenas para atualizar campo de ativação
     * @param \Cadastro\Entity\Usuario
     * @return boolean 
     */
    public function atualizarUsuario(UsuarioEntity $usuario)
    {
        $where = new Where();
        $where->equalTo("cficodusu", $usuario->getCodigo());

        $resultado = $this->update(['cfiatvusu' => $usuario->getAtivacao()], $where, "mxusucfi");
        return $resultado->getAffectedRows() ? (int) $usuario->getCodigo() : false;
    }

    /**
     * Selecionando produtos
     * @param Where $where
     * @return obj $retorno dados selecionados 
     */
    public function selecionar(Where $where, $config)
    {
        $coluns = array(
            '*',
        );

        $select = $this->getSelect()->from("mxusucfi");
        $select->columns($coluns);
        $select->where($where);

        if (isset($config['order']) && !empty($config['order'])) {
            $select->order($config['order']['coluna'] . ' ' . $config['order']['valor']);
        }
        if (isset($config['offset']) && !empty($config['offset'])) {
            $select->offset($config['offset']);
        }
        if (isset($config['limit']) && !empty($config['limit'])) {
            $select->limit($config['limit']);
        }

        return $this->select($select);
    }

}
