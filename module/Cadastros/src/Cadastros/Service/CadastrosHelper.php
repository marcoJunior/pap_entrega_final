<?php

namespace Cadastros\Service;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class CadastrosHelper extends \APIGrid\Service\APIGrid
{

    /**
    * @return \Cadastros\Mapper\ForcaEmpresas
    */
    public function mapperForcaEmpresas()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\ForcaEmpresas');
    }

    /**
    * @return \Cadastros\Mapper\ForcaSeries
    */
    public function mapperForcaSeries()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\ForcaSeries');
    }

    /**
     * @return \Cadastros\Mapper\Criacao
     */
    public function mapperCriacao()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Criacao');
    }

    /**
     * @return Cadastros\Mapper\Clientes
     */
    public function mapperClientes()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Clientes');
    }

    /**
     * @return Cadastros\Mapper\Empresas
     */
    public function mapperEmpresas()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Empresas');
    }
    /**
     * @return Cadastros\Mapper\EmpresasMax
     */
    public function mapperEmpresasMax()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\EmpresasMax');
    }
    /**
     * @return Cadastros\Mapper\ConexoesEmpresas
     */
    public function mapperConexoesEmpresas()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\ConexoesEmpresas');
    }

    /**
     * @return Cadastros\Mapper\Empresa
     */
    public function getMapperEmpresa()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Empresa');
    }

    /**
     * @return Grid\Mapper\Grid
     */
    public function getServiceGrid()
    {
        return $this->getServiceManager()->get('Grid\Service\Grid');
    }

    /**
     * @return Cpanel\Service\Cpanel
     */
    public function getServiceCpanel()
    {
        return $this->getServiceManager()->get('Cpanel\Service\Cpanel');
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

    /**
     * @return \Clientes\Mapper\ModulosValidade
     */
    public function mapperModulosValidade()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\ModulosValidade');
    }

}
