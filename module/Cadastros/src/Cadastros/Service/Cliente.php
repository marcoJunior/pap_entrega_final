<?php

namespace Cadastros\Service;

use APISql\Service\ConvertObject;
use  Cadastros\Entity\Cliente as ClienteEntity;
use APIHelper\Service\ServiceHelper;

class Cliente extends ServiceHelper
{

    /**
     * @return \Cadastros\Service\Cliente
     */
    public function getMapperCliente()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Cliente');
    }

    /**
     * @return Grid\Mapper\Grid
     */
    public function getServiceGrid()
    {
        return $this->getServiceManager()->get('Grid\Service\Grid');
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

    /**
     * @param array
     * @return array
     */
    public function serializeParametros($parametro = [])
    {
        $retorno = [];

        foreach ($parametro as $value) {
            $retorno[$value['name']] = $value["value"];
        }

        return $retorno;
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function adicionar($parametros)
    {
        $mapper = $this->getMapperCliente();
        $dados = $this->serializeParametros($parametros['dados']);
        $entidade = new ClienteEntity();
        $entidade->exchangeArray($dados);

        return $mapper->adicionar($entidade);
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function editar($parametros)
    {
        $mapper = $this->getMapperCliente();

        $dados = $this->serializeParametros($parametros['dados']);
        $entidade = new ClienteEntity();
        $entidade->exchangeArray($dados);

        return $mapper->editar($entidade);
    }

    /**
     * @param array ['codigo', 'acao']
     * @return bool
     */
    public function remover($parametros)
    {
        $mapper = $this->getMapperCliente();
        $entidade = new ClienteEntity();
        $entidade->setCodigo($parametros['codigo']);

        return $mapper->remover($entidade);
    }

    /**
     * Responsável apenas para atualizar campo de ativação.
     * @param array ['dados','acao']
     * @return bool
     */
    public function atualizarUsuario($parametros)
    {
        $mapper = $this->getMapperCliente();
        $entidade = new ClienteEntity();

        $entidade->setCodigo($parametros['dados']['codigo']);
        $entidade->setAtivo($parametros['dados']['ativacao']);

        return $mapper->atualizarUsuario($entidade);
    }

    public function selecionarGrid($config)
    {
        $configPadrao = $this->getConfig($config['gridAjax']['tela']);
        $mapperGrid = $this->getServiceGrid();

        $where = $mapperGrid->montaWhere($config['gridAjax']['parametro']['where'], 'Cadastros\Mapper\Hydrator\Cliente', $this->getConfig('')['gridPhp']['colunas']);
        $select = $mapperGrid->traduzirParametros($config['gridAjax']['parametro']['select'], 'Cadastros\Mapper\Hydrator\Cliente', $this->getConfig('')['gridPhp']['colunas']);
        $mapperEmpresa = $this->getMapperCliente();

        $retorno = $mapperEmpresa->selecionar($where, $select);

        $convert = new ConvertObject();
        $array = $convert->convertObject($retorno);

        $configPadrao['gridPhp']['data'] = $array;
        $configPadrao['gridPhp']['contData'] = $this->getServiceHelperSql()->contaLinhasTabelaBanco('mxlgncfi', $mapperGrid->getMapHydrator('Cadastros\Mapper\Hydrator\Cliente'), $this->getConfig('')['gridPhp']['colunas'], $where);

        return $configPadrao;
    }

    public function getConfig()
    {

        $colunas = [
            'btn-detalhes-primeiro' => [
                'tipo' => 'int-data',
                'titulo' => 'Ativação',
                'mask' => '####',
                'exibir' => 'true',
                0 => [
                    'tipo' => 'input type="checkbox"',
                    'icon' => '',
                    'class' => '',
                    'href' => '',
                    'onclick' => '',
                    'value' => 'codigo',
                    'title' => 'Ativar/desativar cliente',
                ],
            ],
            'codigo' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Código', 'mask' => '#####################'],
            'email' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Email', 'mask' => '#####################'],
            'senha' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => 'Senha', 'mask' => '#####################'],
            'empresa' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Empresa', 'mask' => '#####################'],
            'btn-detalhes' => [
                'exibir' => 'true',
                'titulo' => 'Opções',
                0 => [
                    'tipo' => 'a',
                    'icon' => 'fa-edit',
                    'class' => '',
                    'href' => '',
                    'onclick' => 'Servicos.editar',
                    'value' => 'codigo',
                    'title' => 'Editar registro',
                    'titulo' => 'Editar',
                    'style' => 'font-size: 20px; color:#2980BA',
                ]
            ],
        ];

        return $config = [
            'gridPhp' => [
                'titulo' => 'Clientes',
                'colunas' => $colunas,
                'data' => '',
                'configParam' => [
                ],
            ],
        ];
    }

}
