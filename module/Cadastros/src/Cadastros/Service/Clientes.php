<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Clientes extends CadastrosHelper
{

    public function selecionar(Parameters $post, $get = null)
    {

        $mapper = $this->mapperClientes();
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $mapper->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function alterar(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperClientes();
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

        return $mapper->alterar($formulario);
    }

    public function excluir(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperClientes();
        $codigo = $post->get('codigo', null);

        if ($codigo === null) {
            return false;
        }
        return $mapper->excluir($codigo);
    }

}
