<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\APIGridReturn as APIGridReturnEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Cadastros\Entity\ConexoesEmpresas as ConexoesEmpresasEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexoesEmpresas extends CadastrosHelper
{

    public function selecionar(Parameters $post, $get = null)
    {
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retornoBanco = $this->mapperConexoesEmpresas()->selecionar($postTratado,
                $filtros);

        $retorno = new APIGridReturnEntity(
                $post->get('draw', 1), $retornoBanco['count'],
                $retornoBanco['count'],
                ConvertObject::convertObject($retornoBanco['db'])
        );

        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperConexoesEmpresas();

        $entidade = new ConexoesEmpresasEntity();
        $entidade->exchangeArray($formulario);

        if (!$this->validaExistencia($entidade)) {
            return ['mensagem' => 'Não foi selecionado uma empresa existente!'];
        }

        return $mapper->adicao($entidade);
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperConexoesEmpresas();

        $entidade = new ConexoesEmpresasEntity();
        $entidade->exchangeArray($formulario);

        return $mapper->alterar($entidade);
    }

    public function excluir(Parameters $post)
    {
        $mapper = $this->mapperConexoesEmpresas();
        $id = $post->get('id', null);

        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $mapper->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * Verifica se já existe empresa registrada
     * @return bool
     */
    public function validaExistencia(ConexoesEmpresasEntity $entidade)
    {
        $mapper = $this->mapperEmpresasMax();
        return $mapper->validaExistenciaPorId($entidade->getIdClienteMax());
    }

}
