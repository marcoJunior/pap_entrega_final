<?php

namespace Cadastros\Service;

use APISql\Service\ConvertObject;
use Cadastros\Entity\Empresa as EmpresaEntity;
use Cadastros\Entity\Pagamento as PagamentoEntity;
use Cpanel\Entity\SubDominio as SubdominioEntity;
use Cpanel\Entity\Dominio as DominioEntity;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

class Empresa extends CadastrosHelper
{

    public function selecionar(Parameters $post, $get = null)
    {

        $mapper = $this->mapperEmpresas();
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array)$post->get("filtros"));
        $filtros->exchangeArray((array)$get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retornoBanco = $mapper->selecionar($postTratado, $filtros);

        return [
            "draw" => $post->get('draw', 1),
            "recordsTotal" => $retornoBanco['count'],
            "recordsFiltered" => $retornoBanco['count'],
            "data" => ConvertObject::convertObject($retornoBanco['db'])
        ];
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function adicionar($parametros)
    {
        $mapper = $this->getMapperEmpresa();
        $dados = $this->serializeParametros($parametros['dados']);
        $entidade = new EmpresaEntity();
        $entidade->exchangeArray($dados);

        if (!$this->verificarRegistro(['coluna' => 'cfiapevld', 'valor' => $entidade->getEmpresa()])) {
            return false;
        }

        // cria subdominio se ativado o csb2b
        if (is_int(strpos(explode(",", $dados["projetos"])[0], "a"))) {
            $this->adicionarSubDominio($entidade);
        }

        if ($entidade->getDominio()) {
            $this->adicionarDominio($entidade);
        }

        $this->salvarPagamentoConfigurado($entidade, $dados);

        $mapper->adicionar($entidade);
        $this->gerarTabelas($entidade);
        return true;
    }

    /**
     * @return \Cadastro\Mapper\Pagamento
     */
    public function getMapperPagamento()
    {
        return $this->getServiceManager()->get('Cadastro\Mapper\Pagamento');
    }

    /**
     * @param EmpresaEntity $empresa
     * @param array $array
     * @return BOOLEAN
     */
    public function salvarPagamentoConfigurado(EmpresaEntity $empresa, $array)
    {
        $pagamento = new PagamentoEntity();
        $pagamento->setCielo(!isset($array['cielo']) ? 0 : $array['cielo']);
        $pagamento->setMoip(!isset($array['moip']) ? 0 : $array['moip']);
        $pagamento->setPagseguro(!isset($array['pagseguro']) ? 0 : $array['pagseguro']);
        $pagamento->setSerie($empresa->getSerie());

        return $this->getMapperPagamento()->adicionar($pagamento);
    }

    public function gerarTabelas($parametro)
    {

        if (!$this->testarConexao($parametro)['teste']) {
            return ['erro'];
        }

        if ($parametro instanceof EmpresaEntity) {
            $empresa = $parametro;
        } else {
            $empresa = new EmpresaEntity();
            $empresa->exchangeArray($this->serializeParametros($parametro['dados']));
        }

        $mapperCriacao = $this->getServiceManager()->get('Cadastros\Mapper\Criacao');
        return $mapperCriacao->criarTabela($empresa);
    }

    public function adicionarDominio(EmpresaEntity $empresa)
    {
        //Vai ser preciso encontrar uma forma de adicionar
        //Dominios de complemento para que isso seja automatico
        //Adicionar o dominio normalmente não funciona
        //Porque todas as url vão precisar ser incluidas
        //dentro do csgestor
//        $servico = $this->getServiceCpanel();
//        $dominioEntity = new DominioEntity();
//        $dominioEntity->setNovoDominio(strtolower($empresa->getDominio()));
//        $dominioEntity->setNovoUsuario(strtolower($empresa->getEmpresa()));
//        $dominioEntity->setNovaSenha('amdsdl7586');
//
//        return $servico->adicionarDominio($dominioEntity);
    }

    public function adicionarSubDominio(EmpresaEntity $empresa)
    {
        $servico = $this->getServiceCpanel();
        $subdominioEntity = new SubdominioEntity();
        $subdominioEntity->setSubDominio(strtolower($empresa->getEmpresa()));

        return $servico->adicionarSubDominio($subdominioEntity);
    }

    /**
     * Verifica se já existe empresa registrada
     * @param array ['dados'] chave = coluna , valor = valor da coluna
     * @return bool
     */
    public function verificarRegistro($parametros = [])
    {
        $mapper = $this->getMapperEmpresa();
        if (isset($parametros['dados'])) {
            $parametros = $parametros['dados'];
        }
        return $mapper->verificarRegistro($parametros);
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function editar($parametros)
    {
        $mapper = $this->getMapperEmpresa();
        $dados = $this->serializeParametros($parametros['dados']);

        $entidade = new EmpresaEntity();
        $entidade->exchangeArray($dados);

        $this->salvarPagamentoConfigurado($entidade, $dados);
        // cria subdominio se ativado o csb2b
        if (is_int(strpos(explode(",", $dados["projetos"])[0], "a"))) {
            $this->adicionarSubDominio($entidade);
        }

        if ($entidade->getDominio()) {
            $this->adicionarDominio($entidade);
        }

        return $mapper->editar($entidade);
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function remover($parametros)
    {
        $mapper = $this->getMapperEmpresa();
        $entidade = new EmpresaEntity();
        $entidade->exchangeArray($parametros['dados']);

        $this->removerSubDominio($entidade);

        return $mapper->remover($entidade);
    }

    public function removerSubDominio($empresa)
    {
        $servico = $this->getServiceCpanel();
        $subdominioEntity = new SubdominioEntity();
        $subdominioEntity->setSubDominio(strtolower($empresa->getEmpresa()));

        return $servico->removerSubDominio($subdominioEntity);
    }

    public function selecionarGrid($config)
    {
        $configPadrao = $this->getConfig($config['gridAjax']['tela']);
        $mapperGrid = $this->getServiceGrid();

        $where = $mapperGrid->montaWhere($config['gridAjax']['parametro']['where'], 'Cadastros\Mapper\Hydrator\Empresa', $this->getConfig('')['gridPhp']['colunas']);
        $select = $mapperGrid->traduzirParametros($config['gridAjax']['parametro']['select'], 'Cadastros\Mapper\Hydrator\Empresa', $this->getConfig('')['gridPhp']['colunas']);
        $mapperEmpresa = $this->getMapperEmpresa();

        $retorno = $mapperEmpresa->selecionar($where, $select);

        $convert = new ConvertObject();
        $array = $convert->convertObject($retorno);

        $configPadrao['gridPhp']['data'] = $array;
        $configPadrao['gridPhp']['contData'] = $this->getServiceHelperSql()->contaLinhasTabelaBanco('mxvldcfi', $mapperGrid->getMapHydrator('Cadastros\Mapper\Hydrator\Empresa'), $this->getConfig('')['gridPhp']['colunas'], $where);

        return $configPadrao;
    }

    public function getConfig()
    {

        $colunas = [
            'codigo' => ['exibir' => 'false', 'tipo' => 'int', 'titulo' => 'Código', 'mask' => '#####################'],
            'serie' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Série', 'mask' => '#####################'],
            'empresa' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Empresa', 'mask' => '#####################'],
            'login' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => 'Login', 'mask' => '#####################'],
            'ip' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'IP', 'mask' => '#####################'],
            'porta' => ['exibir' => 'false', 'tipo' => 'int', 'titulo' => 'Porta', 'mask' => '#####################'],
            'banco' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Banco', 'mask' => '#####################'],
            'iptintometrico' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'IP tinto.', 'mask' => '#####################'],
            'portatintometrico' => ['exibir' => 'false', 'tipo' => 'int', 'titulo' => 'Porta tinto.', 'mask' => '#####################'],
            'bancotintometrico' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Banco tinto.', 'mask' => '#####################'],
            'qtdLogins' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Funcionarios', 'mask' => '#####################'],
            'qtdClientes' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Clientes', 'mask' => '#################'],
            'plano' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Plano', 'mask' => '#################'],
            'projetos' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => 'Projetos', 'mask' => '#################'],
            'dominio' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Dominio', 'mask' => '###############################################'],
            'subdominio' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Subdominio', 'mask' => '###############################################'],
            'qtdMb' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => 'Qtd. mb', 'mask' => '###############################################'],
            'cielo' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => '', 'mask' => '###############################################'],
            'moip' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => '', 'mask' => '###############################################'],
            'pagseguro' => ['exibir' => 'false', 'tipo' => 'string', 'titulo' => '', 'mask' => '###############################################'],
            'btn-detalhes' => [
                'exibir' => 'true',
                'titulo' => 'Opções',
                0 => [
                    'tipo' => 'a',
                    'icon' => 'fa-edit',
                    'class' => '',
                    'href' => '',
                    'onclick' => 'editar',
                    'value' => 'codigo',
                    'title' => 'Editar registro',
                    'titulo' => 'Editar',
                    'style' => 'font-size: 20px; color:#2980BA',
                ],
                1 => [
                    'tipo' => 'a',
                    'icon' => 'fa-remove',
                    'class' => '',
                    'href' => '',
                    'onclick' => 'remover',
                    'value' => 'codigo',
                    'title' => 'Remover registro',
                    'titulo' => 'Remover    ',
                    'style' => 'font-size: 22px; color:#E80000',
                ],
            ],
        ];

        return $config = [
            'gridPhp' => [
                'titulo' => 'Empresas',
                'colunas' => $colunas,
                'data' => '',
                'configParam' => [
                ],
            ],
        ];
    }

    /**
     * Testa conexão de empresa
     * @param EmpresaEntity $conexao
     * @return array
     */
    public function testarConexao($conexao)
    {
        $empresaEntity = new EmpresaEntity();

        if (!$conexao instanceof EmpresaEntity) {
            $dados = $this->serializeParametros($conexao['dados']);
            $empresaEntity->exchangeArray($dados);
        } else {
            $empresaEntity = $conexao;
        }

        return ['teste' => $this->
                    getMapperEmpresa()->
                    setConexao($empresaEntity)
        ];
    }

    /**
     * @param array
     * @return array
     */
    public function serializeParametros($parametro = [])
    {
        $retorno = [];

        foreach ($parametro as $value) {
            $retorno[$value['name']] = $value["value"];
        }

        return $retorno;
    }

}
