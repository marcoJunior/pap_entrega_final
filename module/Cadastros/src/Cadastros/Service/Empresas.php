<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cpanel\Entity\Dominio as DominioEntity;
use Cadastros\Entity\Empresa as EmpresaEntity;
use Cpanel\Entity\SubDominio as SubdominioEntity;
use Cadastros\Entity\Empresas as EntidadeEmpresas;
use Cadastros\Entity\Pagamento as PagamentoEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Empresas extends CadastrosHelper
{

    public function selecionar(Parameters $post, $get = null)
    {
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperEmpresas()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperEmpresas();
        $postTratado = $this->getPostTratado($post);
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

        $entidade = new EmpresaEntity();
        $entidade->exchangeArray($formulario);

        if (!$this->verificarRegistro(['coluna' => 'cfiapevld', 'valor' => $entidade->getEmpresa()])) {
            return false;
        }

        // cria subdominio se ativado o csb2b
        // if (is_int(strpos(explode(",", $formulario["projetos"])[0], "a"))) {
        // $this->adicionarSubDominio($entidade);
        // }
        // if ($entidade->getDominio()) {
        // $this->adicionarDominio($entidade);
        // }

        $this->salvarPagamentoConfigurado($entidade, $formulario);
        $this->gerarTabelas($entidade);

        return $mapper->adicao($formulario, $postTratado);
    }

    public function alterar(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperEmpresas();
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

        $entidade = new EmpresaEntity();
        $entidade->exchangeArray($formulario);

        // cria subdominio se ativado o csb2b
        // if (is_int(strpos(explode(",", $formulario["projetos"])[0], "a"))) {
        //     $this->adicionarSubDominio($entidade);
        // }
        // if ($entidade->getDominio()) {
        //     $this->adicionarDominio($entidade);
        // }

        $this->salvarPagamentoConfigurado($entidade, $formulario);
        $this->gerarTabelas($entidade);

        return $mapper->alterar($formulario);
    }

    public function excluir(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperEmpresas();
        $codigo = $post->get('codigo', null);
        $fantasia = $post->get('empresa', null);

        if ($codigo === null || $fantasia === null) {
            return false;
        }

        $entidade = new EmpresaEntity();
        $entidade->setSerie($codigo);
        $entidade->setEmpresa($fantasia);

        // $this->removerSubDominio($entidade);

        return $mapper->excluir($codigo, $fantasia);
    }

    /**
     * @return \Cadastros\Mapper\Pagamento
     */
    public function getMapperPagamento()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\Pagamento');
    }

    /**
     * @param EmpresaEntity $empresa
     * @param array $array
     * @return BOOLEAN
     */
    public function salvarPagamentoConfigurado(EmpresaEntity $empresa, $array)
    {
        $pagamento = new PagamentoEntity();
        $pagamento->setCielo(!isset($array['cielo']) ? 0 : $array['cielo']);
        $pagamento->setMoip(!isset($array['moip']) ? 0 : $array['moip']);
        $pagamento->setPagseguro(!isset($array['pagseguro']) ? 0 : $array['pagseguro']);
        $pagamento->setSerie($empresa->getSerie());

        return $this->getMapperPagamento()->adicionar($pagamento);
    }

    public function adicionarDominio(EmpresaEntity $empresa)
    {
        //Vai ser preciso encontrar uma forma de adicionar
        //Dominios de complemento para que isso seja automatico
        //Adicionar o dominio normalmente não funciona
        //Porque todas as url vão precisar ser incluidas
        //dentro do csgestor
//        $servico = $this->getServiceCpanel();
//        $dominioEntity = new DominioEntity();
//        $dominioEntity->setNovoDominio(strtolower($empresa->getDominio()));
//        $dominioEntity->setNovoUsuario(strtolower($empresa->getEmpresa()));
//        $dominioEntity->setNovaSenha('amdsdl7586');
//
//        return $servico->adicionarDominio($dominioEntity);
    }

    public function adicionarSubDominio(EmpresaEntity $empresa)
    {
        $servico = $this->getServiceCpanel();
        $subdominioEntity = new SubdominioEntity();
        $subdominioEntity->setSubDominio(strtolower($empresa->getEmpresa()));

        return $servico->adicionarSubDominio($subdominioEntity);
    }

    public function removerSubDominio($empresa)
    {
        $servico = $this->getServiceCpanel();
        $subdominioEntity = new SubdominioEntity();
        $subdominioEntity->setSubDominio(strtolower($empresa->getEmpresa()));

        return $servico->removerSubDominio($subdominioEntity);
    }

    /**
     * Verifica se já existe empresa registrada
     * @param array ['dados'] chave = coluna , valor = valor da coluna
     * @return bool
     */
    public function verificarRegistro($parametros = [])
    {
        $mapper = $this->getMapperEmpresa();
        if (isset($parametros['dados'])) {
            $parametros = $parametros['dados'];
        }
        return $mapper->verificarRegistro($parametros);
    }

    public function gerarTabelas(EmpresaEntity $parametro)
    {

        if (!$this->testarConexao($parametro)['teste']) {
            return ['erro'];
        }

        $mapperCriacao = $this->mapperCriacao();
        return $mapperCriacao->criarTabela($parametro);
    }

    /**
     * Testa conexão de empresa
     * @param EmpresaEntity $conexao
     * @return array
     */
    public function testarConexao(EmpresaEntity $empresaEntity)
    {
        return ['teste' => $this->
                    getMapperEmpresa()->
                    setConexao($empresaEntity)
        ];
    }

}
