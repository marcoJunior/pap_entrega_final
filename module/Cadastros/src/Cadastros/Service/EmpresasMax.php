<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cadastros\Entity\EmpresasMax as EmpresasMaxEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class EmpresasMax extends CadastrosHelper
{

    public function selecionar(Parameters $post, $get = null)
    {
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperEmpresasMax()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperEmpresasMax();

        $entidade = new EmpresasMaxEntity();
        $entidade->exchangeArray($formulario);

        if ($this->validaExistencia($entidade)) {
            return ['mensagem' => 'Já existe um cadastro para está empresa!'];
        }

        return $mapper->adicao($entidade);
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperEmpresasMax();

        $entidade = new EmpresasMaxEntity();
        $entidade->exchangeArray($formulario);

        return $mapper->alterar($entidade);
    }

    public function excluir(Parameters $post)
    {
        $mapper = $this->mapperEmpresasMax();
        $id = $post->get('id', null);

        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $mapper->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * Verifica se já existe empresa registrada
     * @return bool
     */
    public function validaExistencia(EmpresasMaxEntity $entidade)
    {
        $mapper = $this->mapperEmpresasMax();
        return $mapper->validaExistencia($entidade);
    }

}
