<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Forca extends CadastrosHelper
{

    public function empresasSelecionar(Parameters $post, $get = null)
    {

        $mapper = $this->mapperForcaEmpresas();
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $mapper->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function empresasSerie(Parameters $post, $get = null)
    {

        $conexaoDb = $this->getServiceManager()->get('Config')['cliente'];

        $mapper = $this->mapperForcaSeries();
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

//        if (!isset($get)) {
//            $filtros->setCodigo(null);
//        }
//
//        if ($filtros->getCodigo() == null) {
//            $filtros->setCodigo($post->get("codigo", null));
//        }

        $retorno = $mapper->selecionar($postTratado, $filtros, $conexaoDb);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function atualizar(Parameters $post)
    {
        try {

            if (empty($post)) {
                throw new \Exception('Houve um erro!');
            }

            $cliente = null;
            $mapper = $this->mapperForcaSeries();
            $formulario = $post->get('formulario', null);

            if ($formulario === null) {
                return false;
            }

            $tipo = $post->get('tipo', 'Provedor');

            if ($tipo == 'Cliente') {
                $cliente = $this->selecionarEmpresa($post);
            }

            return $mapper->{"setAtualizacao$tipo"}($formulario, $cliente);
        } catch (Exception $ex) {
            return "Não foi possivel executar a atualização !";
        }
    }

    public function selecionarEmpresa(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperForcaEmpresas();
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

        return $mapper->selecionarEmpresa($formulario);
    }

    public function adicao(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperForcaEmpresas();
        $postTratado = $this->getPostTratado($post);
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

//        $mapperSerie = $this->mapperForcaSeries();
//        $mapperSerie->gerarListaSeries($formulario['serie'], $formulario['qtdAdiquirida']);

        $add = $mapper->adicao($formulario, $postTratado);

//        $cliente = $this->selecionarEmpresa($post);
//        $mapperSerie->setAtualizacaoCliente($formulario, $cliente);

        return $add;
    }

    public function alterar(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperForcaEmpresas();
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }

        $tipo = $post->get('tipo', null);

        if ($tipo === null) {
            return false;
        }

        $config = $this->getServiceManager()->get('Config');
        return $mapper->alterar($formulario, $config, $tipo);
    }

    public function excluir(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperForcaEmpresas();
        $codigo = $post->get('codigo', null);
        $modulo = $post->get('modulo', null);

        if ($codigo === null || $modulo === null) {
            return false;
        }
        return $mapper->excluir($codigo, $modulo);
    }

    public function empresasSerieLimpar(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $conexaoDb = $this->getServiceManager()->get('Config')['cliente'];

        $mapperSerie = $this->mapperForcaSeries();
        return $mapperSerie->empresasSerieLimpar($post, $conexaoDb);
    }

    public function empresasSerieExcluir(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $conexaoDb = $this->getServiceManager()->get('Config')['cliente'];

        $mapperSerie = $this->mapperForcaSeries();
        return $mapperSerie->empresasSerieExcluir($post, $conexaoDb);
    }

    public function gerarCodigo(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $serie = $post->get('serie', '');
        $conexaoDb = $this->getServiceManager()->get('Config')['cliente'];

        $mapperSerie = $this->mapperForcaSeries();
        return $mapperSerie->gerarCodigo($serie, $conexaoDb);
    }

}
