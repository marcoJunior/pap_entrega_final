<?php

namespace Cadastros\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ModulosValidade extends CadastrosHelper
{

    public function init()
    {
        $mapper = $this->mapperModulosValidade();
        return array_reverse(ConvertObject::convertObject($mapper->selecionar()));
    }

    public function getClientes(Parameters $post, $get = null)
    {

        $mapper = $this->mapperModulosValidade();
        $postTratado = $this->getPostTratado($post);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $post->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $mapper->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $post->get('draw', 1));
        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperModulosValidade();
        $postTratado = $this->getPostTratado($post);
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }
        return $mapper->adicao($formulario, $postTratado);
    }

    public function alterar(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperModulosValidade();
        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return false;
        }
        return $mapper->alterar($formulario);
    }

    public function excluir(Parameters $post)
    {
        if (empty($post)) {
            throw new \Exception('Houve um erro!');
        }

        $mapper = $this->mapperModulosValidade();
        $codigo = $post->get('codigo', null);
        $modulo = $post->get('modulo', null);

        if ($codigo === null || $modulo === null) {
            return false;
        }
        return $mapper->excluir($codigo, $modulo);
    }

}
