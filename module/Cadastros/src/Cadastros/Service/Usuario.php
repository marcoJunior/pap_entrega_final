<?php

namespace Cadastro\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface,
    Zend\ServiceManager\ServiceManager,
    APISql\Service\ConvertObject,
    Cadastro\Entity\Usuario as UsuarioEntity;

class Usuario implements ServiceManagerAwareInterface
{

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @return Cadastros\Mapper\Empresa
     */
    public function getInstance($instance)
    {
        return $this->getServiceManager()->get($instance);
    }

    /**
     * @param array
     * @return array
     */
    public function serializeParametros($parametro = [])
    {
        $retorno = [];

        foreach ($parametro as $value) {
            $retorno[$value['name']] = $value["value"];
        }

        return $retorno;
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function adicionar($parametros)
    {
        $mapper = $this->getInstance('UsuarioMapper');
        $dados = $this->serializeParametros($parametros['dados']);
        $entidade = new UsuarioEntity();
        $entidade->exchangeArray($dados);

        if ($mapper->verificar(['email' => $entidade->getEmail(), 'usuario' => $entidade->getUsuario()])) {
            return ['erro' => 'Já existe'];
        }

        return $mapper->adicionar($entidade);
    }

    /**
     * @param array ['dados'] serialize array
     * @return bool
     */
    public function editar($parametros)
    {
        $mapper = $this->getInstance('UsuarioMapper');

        $dados = $this->serializeParametros($parametros['dados']);
        $entidade = new UsuarioEntity();
        $entidade->exchangeArray($dados);

        if ($mapper->verificar(['email' => $entidade->getEmail(), 'usuario' => $entidade->getUsuario()])) {
            return ['erro' => 'Já existe'];
        }

        return $mapper->editar($entidade);
    }

    /**
     * @param array ['codigo', 'acao']
     * @return bool
     */
    public function remover($parametros)
    {
        $mapper = $this->getInstance('UsuarioMapper');
        $entidade = new UsuarioEntity();
        $entidade->setCodigo($parametros['codigo']);

        return $mapper->remover($entidade);
    }

    /**
     * Responsável apenas para atualizar campo de ativação.
     * @param array ['dados','acao']
     * @return bool
     */
    public function atualizarUsuario($parametros)
    {
        $mapper = $this->getInstance('UsuarioMapper');
        $entidade = new UsuarioEntity();

        $entidade->setCodigo($parametros['dados']['codigo']);
        $entidade->setAtivacao($parametros['dados']['ativacao']);

        return $mapper->atualizarUsuario($entidade);
    }

    /**
     * Responsável para verificação se já existe o registro no banco
     * @param array ['coluna' => '', 'valor' => '']
     * @return bool
     */
//    public function verificar($parametros)
//    {
//        $mapper = $this->getInstance('UsuarioMapper');
//        $verificado = $mapper->verificar($parametros['dados']);
//
//        return [$verificado];
//    }

    public function grid($config)
    {
        $configPadrao = $this->getConfig($config['gridAjax']['tela']);
        $serviceGrid = $this->getInstance('Grid\Service\Grid');

        $where = $serviceGrid->montaWhere($config['gridAjax']['parametro']['where'], 'Cadastro\Mapper\Hydrator\Usuario', $this->getConfig('')['gridPhp']['colunas']);
        $select = $serviceGrid->traduzirParametros($config['gridAjax']['parametro']['select'], 'Cadastro\Mapper\Hydrator\Usuario', $this->getConfig('')['gridPhp']['colunas']);
        $mapperEmpresa = $this->getInstance('UsuarioMapper');
        $retorno = $mapperEmpresa->selecionar($where, $select);

        $convert = new ConvertObject();
        $array = $convert->convertObject($retorno);

        $configPadrao['gridPhp']['data'] = $array;
        //$configPadrao['gridPhp']['contData'] = $this->getInstance('APISql\Service\HelperSql')->contaLinhasTabelaBanco('mxlgncfi', $mapperGrid->getMapHydrator('Cadastro\Mapper\Hydrator\Cliente'), $this->getConfig('')['gridPhp']['colunas'], $where);

        return $configPadrao;
    }

    public function getConfig()
    {

        $colunas = [
            'btn-detalhes-primeiro' => [
                'tipo' => 'int-data',
                'titulo' => 'Ativação',
                'mask' => '####',
                'exibir' => 'true',
                0 => [
                    'tipo' => 'input type="checkbox"',
                    'icon' => '',
                    'class' => '',
                    'href' => '',
                    'onclick' => '',
                    'value' => 'codigo',
                    'title' => 'Ativar/desativar usuário',
                ],
            ],
            'codigo' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Código', 'mask' => '#####################'],
            'ativacao' => ['exibir' => 'false', 'tipo' => 'int', 'titulo' => 'Código', 'mask' => '#####################'],
            'nome' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Nome', 'mask' => '#####################'],
            'usuario' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Usuário', 'mask' => '#####################'],
            'email' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Email', 'mask' => '#####################'],
            'senha' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Senha', 'mask' => '#####################'],
            'tipo' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Tipo de login', 'mask' => '#####################'],
            'btn-detalhes' => [
                'exibir' => 'true',
                'titulo' => 'Opções',
                0 => [
                    'tipo' => 'a',
                    'icon' => 'fa-edit',
                    'class' => '',
                    'href' => '',
                    'onclick' => 'ServicosGrid.carregar',
                    'value' => 'codigo',
                    'title' => 'Editar registro',
                    'titulo' => 'Editar',
                    'style' => 'font-size: 20px; color:#2980BA',
                ],
                1 => [
                    'tipo' => 'a',
                    'icon' => 'fa-trash',
                    'class' => '',
                    'href' => '',
                    'onclick' => 'ServicosGrid.remover',
                    'value' => 'codigo',
                    'title' => 'Remover registro',
                    'titulo' => 'Remover',
                    'style' => 'font-size: 20px; color:red',
                ],
            ],
        ];

        return $config = [
            'gridPhp' => [
                'titulo' => 'Usuários',
                'colunas' => $colunas,
                'data' => '',
                'configParam' => [
                ],
            ],
        ];
    }

}
