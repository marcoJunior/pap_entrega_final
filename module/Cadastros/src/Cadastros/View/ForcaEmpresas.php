<?php

namespace Cadastros\View;

use Zend\View\Renderer\PhpRenderer;
use Zend\View\Model\ViewModel;

class ForcaEmpresas extends ViewModel
{

    protected $template = 'cadastros/forca/empresas';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [
            '../APIGridGitLab/public/css/api_grid_v2.css',
        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
//            $render->basePath('../APIGridGitLab/public/js/api_grid_v2.js'),
            $render->basePath('../app/cadastros/forca/empresas/index.js'),
            $render->basePath('../app/cadastros/forca/series/index.js'),
        ];
    }

    public function grid()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/forca/empresas/grid");
        $view->setCaptureTo("grid");

        $this->addChild($view);
    }

    public function formulario()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/forca/empresas/formulario");
        $view->setVariable('subTitulo', 'Lincenças');
        $view->setCaptureTo("formulario");

        $viewSubGrid = new ViewModel();
        $viewSubGrid->setTemplate("cadastros/forca/series/grid");
        $viewSubGrid->setCaptureTo("gridSeries");

        $view->addChild($viewSubGrid);
        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/forca/empresas/modal");
        $view->setCaptureTo("modal");

        $this->addChild($view);
    }

}
