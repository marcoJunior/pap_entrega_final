<?php

namespace Cadastros\View;

use Zend\View\Renderer\PhpRenderer;
use Zend\View\Model\ViewModel;

class ModulosValidade extends ViewModel
{

    protected $template = 'cadastros/modulos-validade';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [

        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
            $render->basePath('../app/cadastros/modulos-validade/index.js'),
        ];
    }

    public function grid()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/modulos-validade/grid");
        $view->setCaptureTo("grid");

        $this->addChild($view);
    }
    public function formulario()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/modulos-validade/formulario");
        $view->setCaptureTo("formulario");

        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel();
        $view->setTemplate("cadastros/modulos-validade/modal");
        $view->setCaptureTo("modal");

        $this->addChild($view);
    }

}
