<?php

namespace Cliente;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'BancoDeDados' => __DIR__ . '/src/BancoDeDados'
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Cliente::class => function ($sm) {
                    $service = new Service\Cliente();
                    return $service->setServiceManager($sm);
                },
                \BancoDeDados\Service\BancoDeDados::class => function ($sm) {
                    $service = new \BancoDeDados\Service\BancoDeDados();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                \BancoDeDados\Mapper\BancoDeDados::class =>
                function ($sm) {
                    $mapper = new \BancoDeDados\Mapper\BancoDeDados();
                    $dbConfig = $sm->get('db');
                    $mapper->setDbAdapter($dbConfig);
                    return $mapper;
                },
                'Cliente\Mapper\Cliente' =>
                function ($sm) {
                    $mapper = new Mapper\Cliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ClienteCompleto())
                        ->setHydrator(new Mapper\Hydrator\ClienteCompleto());
                    return $mapper;
                },
                'Cliente\Mapper\ContatoCliente' =>
                function ($sm) {
                    $mapper = new Mapper\ContatoCliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ContatoCliente())
                        ->setHydrator(new Mapper\Hydrator\ContatoCliente());
                    return $mapper;
                },
                'Cliente\Mapper\EnderecoCliente' =>
                function ($sm) {
                    $mapper = new Mapper\EnderecoCliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\EnderecoCliente())
                        ->setHydrator(new Mapper\Hydrator\EnderecoCliente());
                    return $mapper;
                },
            ),
        );
    }

}
