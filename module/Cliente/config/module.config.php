<?php

return array(
    'controllers' => array(
        'factories' => array(
            'clientes' => function ($sl) {
                $controller = new \Cliente\Controller\ClienteController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'contrato' => function ($sl) {
                $controller = new \Cliente\Controller\ConexoesEmpresasMaxController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'bancos_de_dado' => function ($sl) {
                $controller = new \BancoDeDados\Controller\BancoDeDadosController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ),
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'clientes' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/clientes[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'clientes',
                    )
                ),
            ),
            'clientes/bancos_de_dado' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/bancos_de_dado[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'bancos_de_dado',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
//            'grid' => __DIR__ . '/../view/clientes/grid.phtml',
//            'formulario' => __DIR__ . '/../view/clientes/formulario.phtml',
        ),
        'template_path_stack' => array(
            'clientes' => __DIR__ . '/../view',
            'bancoDeDados' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
