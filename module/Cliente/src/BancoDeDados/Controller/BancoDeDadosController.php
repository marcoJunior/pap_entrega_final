<?php

namespace BancoDeDados\Controller;

use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;

class BancoDeDadosController extends \APIGrid\Controller\APIGridController
{

    public function create($data)
    {
        $service = $this->serviceBancoDeDados();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceBancoDeDados();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
//        $service = $this->serviceBancoDeDados();
//        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \BancoDeDados\Service\BancoDeDados
     */
    public function serviceBancoDeDados()
    {
        return $this->getService()->get(\BancoDeDados\Service\BancoDeDados::class);
    }

}
