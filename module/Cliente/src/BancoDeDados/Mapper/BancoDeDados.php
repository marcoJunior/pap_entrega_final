<?php

namespace BancoDeDados\Mapper;

use Zend\Db\Sql\Where;
use APIGrid\Mapper\APIGrid;
use Zend\Db\Adapter\Adapter;
use BancoDeDados\Mapper\QuerysUpdate;
use Zend\Db\Adapter\Exception\InvalidQueryException;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class BancoDeDados extends APIGrid
{

    public function adicao($idCliente)
    {
        $dbName = "simples_$idCliente";
        $sql = "CREATE DATABASE $dbName TEMPLATE template_simples";
        try {
            $db = $this->getDbAdapter()->query($sql)->execute();
            $db->getAffectedRows();
            $retorno = $dbName;
        } catch (InvalidQueryException $exc) {
            $retorno = ['mensagem' => $exc->getMessage()];
        }

        return $retorno;
    }

    public function alterar(Adapter $configDb)
    {

        $retorno = [];
        $this->setDbAdapter($configDb);
        $querys = new QuerysUpdate();

        foreach ($querys->getQuerys() as $versao => $tables) {
            foreach ($tables as $tabela => $comando) {

                if (is_array($comando)) {

                    foreach ($comando as $posicao => $query) {
                        if (!isset($retorno[$tabela])) {
                            $retorno[$tabela] = [];
                        }

                        try {
                            $db = $this->getDbAdapter()->query($query['SQL'])->execute();
                            $retorno[$tabela][$posicao] = $this->getMensagem($db->getAffectedRows(),
                                    $query['TIPO']);
                        } catch (InvalidQueryException $exc) {
                            $retorno[$tabela][$posicao] = $this->getMensagemErro($exc,
                                    $query['TIPO']);
                        }
                    }
                }
                $retorno[$tabela]['VERSAO'] = $versao;
            }
        }

        return $retorno;
    }

    function getMensagem($linhas, $tipo)
    {
        $mensagens = [];
        $mensagens[0] = 'Não houve alterações!';

        $msgMultiplasLinhas = "Houve algum erro!";
        $msgStatus = "Criado com sucesso!";

        if ($tipo == 'CREATE TABLE') {
            $mensagens[0] = 'Table criada com sucesso!';
            $msgStatus = "OK";
        } else if ($tipo == 'ALTER TABLE') {
            $mensagens[0] = 'Table atualizada com sucesso!';
            $msgStatus = "OK";
        } else if ($tipo == 'CREATE  SEQUENCE') {
            $mensagens[0] = 'Sequencia criada com sucesso!';
            $msgStatus = "Sequencia criada com sucesso!";
        } else if ($tipo == 'CREATE  INDEX') {
            $mensagens[0] = 'Indice criada com sucesso!';
            $msgStatus = "Indice criada com sucesso!";
        } else if ($tipo == 'SELECT') {
            $mensagens[0] = 'Nenhum registro retornado!';
            $msgMultiplasLinhas = "Houve $linhas retornados!";
        } else {
            $mensagens[0] = 'Não foi possivel identificar status!';
            $msgStatus = "Atenção";
            $msgMultiplasLinhas = "Houve $linhas linha(s) alterada(s)!";
        }

        return ['DESCRICAO' => isset($mensagens[$linhas]) ? $mensagens[$linhas] : $msgMultiplasLinhas,
            'STATUS' => $msgStatus];
    }

    function getMensagemErro(InvalidQueryException $exc, $tipo)
    {
        $status = 'Erro';
        $mensagens = [
            '42P07' => '{1} {2} já existente!',
            '42601' => 'Não é posivel executar dois ou mais comandos na mesma sessão!',
            '42703' => 'Campo {1} não existente na tabela!',
        ];

        $retorno = isset($mensagens[$exc->getPrevious()->getCode()]) ? $mensagens[$exc->getPrevious()->getCode()] : $exc->getMessage();
        if ($exc->getPrevious()->getCode() == 42703) {
            $msg = $exc->getMessage();
            $explode = explode('column "', $msg);
            if (count($explode) >= 1) {
                $retorno = str_replace('{1}', explode('"', $explode[1])[0],
                        $retorno);
            }
        } else if ($exc->getPrevious()->getCode() == '42P07') {
            $status = 'OK';
            $msg = $exc->getMessage();
            $local = substr($msg, strpos($msg, '"') + 1, 11);

            $retorno = str_replace('{2}', $local,
                    str_replace('{1}', $tipo, $retorno));
        }
        return ['DESCRICAO' => $retorno,
            'STATUS' => $status];
    }

    public function excluir($id)
    {
        $this->inicializaManutencao();
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
