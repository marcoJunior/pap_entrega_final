<?php

namespace Cliente\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class BancoDeDados extends Hydrator
{

    protected function getEntity()
    {
        return 'Cliente\Entity\BancoDeDados';
    }

    public function getMap()
    {
        $arrayMap = [
            // 'razaoSocial' => 'razao_social'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new BancoDeDados();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
