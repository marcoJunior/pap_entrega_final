<?php

namespace BancoDeDados\Mapper;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class QuerysUpdate
{

    protected $tabelas = [];
    protected $querys = [];

    function getTabelas()
    {
        return $this->tabelas;
    }

    function setTabelas($tabelas)
    {
        $this->tabelas = $tabelas;
    }

    function getQuerys()
    {
        return $this->querys;
    }

    function setQuerys($querys)
    {
        $this->querys = $querys;
    }

    function __construct()
    {
        self::setTabelas([
            //V1
            'MXGEOCFI', 'MXCFWCFI', 'MXGLOCFI', 'MXTPECFI', 'MXTITCFI', 'MXVCOCFI',
            'MXIMACFI', 'MXVRFCFI',
            //V2
            'MXFCPCFI', 'MXFPPCFI', 'MXFPECFI', 'MXEPCCFI', 'MXMENCFI', 'MXFSECFI',
            'MXBPECFI', 'MXENECFI'
        ]);
        self::setQuerys([
            'V1' => self::getV1(),
            'V2' => self::getV2(),
        ]);
    }

    public function getV1()
    {
        return [
            'MXGEOCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXGEOCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXGEOCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXGEOCFI', 'CFICEPGEO')],
            ],
            'MXCFWCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXCFWCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXCFWCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXCFWCFI', 'sr_recno')],
            ],
            'MXGLOCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXGLOCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXGLOCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXGLOCFI', 'cficodglo')],
            ],
            'MXTPECFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXTPECFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXTPECFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpec10 ON mxtpecfi USING btree (cfincxped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpec11 ON mxtpecfi USING btree (cficodped, cfinljped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpec12 ON mxtpecfi USING btree (cficreped, cfinljped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf1 ON mxtpecfi USING btree (cficodped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf2 ON mxtpecfi USING btree (cfirepped, cfidteped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf3 ON mxtpecfi USING btree (cfidteped, cfinljped, cficodped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf4 ON mxtpecfi USING btree (cficliped, cfidteped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf5 ON mxtpecfi USING btree (cfifanped, cficodped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf6 ON mxtpecfi USING btree (cfistaped, cficliped, cfidteped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf7 ON mxtpecfi USING btree (cfincfped, cfiecfped, cfidteped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf8 ON mxtpecfi USING btree (indkey_001);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_mxtpecf9 ON mxtpecfi USING btree (cficpiped, cfidteped, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtpecfi_sr ON mxtpecfi USING btree (sr_recno);']
            ],
            'MXTITCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXTITCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXTITCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf1 ON mxtitcfi USING btree (cfipedite, cfiproite, cfidesite, cfiembite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf2 ON mxtitcfi USING btree (cfiproite, cfidatite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf3 ON mxtitcfi USING btree (cfipedite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf4 ON mxtitcfi USING btree (cfidatite, cfiproite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf5 ON mxtitcfi USING btree (cfipedite, cfinljite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf6 ON mxtitcfi USING btree (cfidatite, cfinljite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_mxtitcf7 ON mxtitcfi USING btree (cfiproite, cfidesite, cfiembite, cficorite, sr_recno);'],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX index_mxtitcfi_sr ON mxtitcfi USING btree (sr_recno);']
            ],
            'MXVCOCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXVCOCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXVCOCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXVCOCFI', 'sr_recno')],
            ],
            'MXIMACFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXIMACFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXIMACFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXIMACFI', 'sr_recno')],
            ],
            'MXLGNCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXLGNCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXLGNCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXLGNCFI', 'sr_recno')],
            ],
            'MXVRFCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXVRFCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXLGNCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX mxvrfcfi_index ON mxvrfcfi USING btree (cficodvrf, cfirepvrf);']
            ],
            'MXPDWCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXPDWCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXPDWCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXPDWCFI', 'sr_recno')],
            ],
        ];
    }

    public function getV2()
    {
        return [
            'MXFCPCFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXFCPCFI')],
                ['TIPO' => 'CREATE  TABLE', 'SQL' => self::getCriacaoMXFCPCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => self::getCriacaoIndex('MXFCPCFI', 'sr_recno, cficodfcp, cfiblofcp')],
            ],
            'MXEPCCFI' => [
                ['TIPO' => 'CREATE  TABLE', 'SQL' => 'CREATE TABLE MXEPCCFI (CFICODEPC SERIAL PRIMARY KEY,'
                    . 'CFIDESEPC VARCHAR(120), CFIFCPEPC INT REFERENCES MXFCPCFI(CFICODFCP),'
                    . 'CFIFPPEPC INT REFERENCES MXFPPCFI(CFICODFPP),CFIFPEEPC INT REFERENCES MXFPECFI(CFICODFPE));'
                ]
            ],
            'MXFSECFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXFSECFI')],
                ['TIPO' => 'CREATE  TABLE', 'SQL' => self::getCriacaoMXFSECFI()]
            ],
            'MXMENCFI' => [
//                ['TIPO' => 'DROP TABLE', 'SQL' => self::getDeleteTabela('MXMENCFI')],
//                ['TIPO' => 'DROP  SEQUENCE', 'SQL' => self::getDeleteSequencia('MXMENCFI')],
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXMENCFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXMENCFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX mxmencfi_mxmencf1 ON mxmencfi USING btree (cficodmen, sr_recno);'],
            ],
            'MXBPECFI' => [
                [
                    'TIPO' => 'CREATE TABLE',
                    'SQL' => self::getCriacaoMXBPECFI(),
                ]
            ],
            'MXENECFI' => [
                ['TIPO' => 'CREATE  SEQUENCE', 'SQL' => self::getCriacaoSequencia('MXENECFI')],
                ['TIPO' => 'CREATE TABLE', 'SQL' => self::getCriacaoMXENECFI()],
                ['TIPO' => 'CREATE INDEX', 'SQL' => 'CREATE INDEX mxenecfi_mxenecf1 ON mxenecfi  USING btree  (cficliene, cficepene, sr_recno);'],
            ]
        ];
    }

    /**
     * Método para criar sequencia
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function getCriacaoSequencia($tabela)
    {
        return "CREATE SEQUENCE $tabela" . "_sq
                INCREMENT 1
                MINVALUE 1
                MAXVALUE 9223372036854775807
                START 1
                CACHE 1";
    }

    /**
     * Método para deletar sequencia
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function getDeleteSequencia($tabela)
    {
        return "DROP SEQUENCE $tabela" . "_sq";
    }

    /**
     * Método para deletar tabela
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function getDeleteTabela($tabela)
    {
        return "DROP TABLE $tabela";
    }

    /**
     * Método para criar sequencia
     * @param string $tabela Tabela a ser criada sequencia
     */
    public function getCriacaoIndex($tabela, $coluna)
    {
        return "CREATE INDEX index_" . "$tabela ON $tabela ($coluna);";
    }

    public function getCriacaoMXGEOCFI()
    {
        return "CREATE TABLE mxgeocfi
                    (
                      cficepgeo character varying(8) NOT NULL DEFAULT ''::character varying,
                      cfilatgeo character varying(20) NOT NULL DEFAULT ''::character varying,
                      cfilongeo character varying(20) NOT NULL DEFAULT ''::character varying
                    )
                    WITH (
                      OIDS=FALSE,
                      autovacuum_enabled=true
                    );";
    }

    public function getCriacaoMXCFWCFI()
    {
        return "CREATE TABLE mxcfwcfi
                    (
                      cfiatvcfw numeric(9,0) NOT NULL DEFAULT 0,
                      cfiltbcfw text,
                      cfivalcfw text,
                      cfinomcfw character(9) NOT NULL DEFAULT ''::bpchar,
                      sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxcfwcfi_sq'::regclass),

                     CONSTRAINT mxcfwcfi_sr_recno_key UNIQUE (sr_recno)
                    )
                    WITH (
                     OIDS=FALSE,
                     autovacuum_enabled=true
                    );";
    }

    public function getCriacaoMXGLOCFI()
    {
        return "CREATE TABLE mxglocfi
                        (
                          cficodglo serial NOT NULL,
                          cfinomglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfiemlglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfitelglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfilgnglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfisenglo character(40) NOT NULL DEFAULT ''::bpchar,
                          cfirepglo numeric(5,0) NOT NULL DEFAULT 0,
                          cfidtaglo timestamp without time zone DEFAULT now(),
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxglocfi_sq'::regclass),
                          cfistaglo character(5) NOT NULL DEFAULT '0'::bpchar,
                          CONSTRAINT mxglocfi_pkey PRIMARY KEY (cficodglo)
                        )
                        WITH (
                            OIDS=FALSE,
                            autovacuum_enabled=true
                        );";
    }

    public function getCriacaoMXTPECFI()
    {
        RETURN "CREATE TABLE mxtpecfi
                        (
                          cficodped numeric(7,0) NOT NULL DEFAULT 0,
                          cfinnfped numeric(6,0) NOT NULL DEFAULT 0,
                          cfincfped numeric(6,0) NOT NULL DEFAULT 0,
                          cfiecfped numeric(4,0) NOT NULL DEFAULT 0,
                          cficliped numeric(9,0) NOT NULL DEFAULT 0,
                          cfifanped character(15) NOT NULL DEFAULT ''::bpchar,
                          cfinclped character varying(40) NOT NULL DEFAULT ''::character varying,
                          cfiendped character varying(35) NOT NULL DEFAULT ''::character varying,
                          cfibaiped character varying(20) NOT NULL DEFAULT ''::character varying,
                          cficidped character varying(60) NOT NULL DEFAULT ''::character varying,
                          cfiestped character(2) NOT NULL DEFAULT ''::bpchar,
                          cficepped character(9) NOT NULL DEFAULT ''::bpchar,
                          cficpfped character varying(19) NOT NULL DEFAULT ''::character varying,
                          cfinrgped character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfifonped character varying(12) NOT NULL DEFAULT ''::character varying,
                          cficlbped character varying(19) NOT NULL DEFAULT ''::character varying,
                          cfidtcped date,
                          cfidteped date,
                          cfihorped character(8) NOT NULL DEFAULT ''::bpchar,
                          cfistaped character(5) NOT NULL DEFAULT ''::bpchar,
                          cfitpnped character(4) NOT NULL DEFAULT ''::bpchar,
                          cfivalped numeric(15,2) NOT NULL DEFAULT 0,
                          cfivnfped numeric(15,2) NOT NULL DEFAULT 0,
                          cfidscped numeric(12,2) NOT NULL DEFAULT 0,
                          cfifreped numeric(12,2) NOT NULL DEFAULT 0,
                          cfisegped numeric(12,2) NOT NULL DEFAULT 0,
                          cficpgped numeric(3,0) NOT NULL DEFAULT 0,
                          cfidesped character varying(30) NOT NULL DEFAULT ''::character varying,
                          cfirefped character(3) NOT NULL DEFAULT ''::bpchar,
                          cfiparped numeric(2,0) NOT NULL DEFAULT 0,
                          cfiindped numeric(8,4) NOT NULL DEFAULT 0,
                          cfidiaped numeric(3,0) NOT NULL DEFAULT 0,
                          cficarped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp1ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp2ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp3ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp4ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp5ped numeric(3,0) NOT NULL DEFAULT 0,
                          cficp6ped numeric(3,0) NOT NULL DEFAULT 0,
                          cfifpgped numeric(1,0) NOT NULL DEFAULT 0,
                          cfilisped numeric(3,0) NOT NULL DEFAULT 0,
                          cfirepped numeric(5,0) NOT NULL DEFAULT 0,
                          cfitraped numeric(5,0) NOT NULL DEFAULT 0,
                          cfinatped numeric(4,2) NOT NULL DEFAULT 0,
                          cfina2ped numeric(4,2) NOT NULL DEFAULT 0,
                          cfinopped character(6) NOT NULL DEFAULT ''::bpchar,
                          cfino2ped character(6) NOT NULL DEFAULT ''::bpchar,
                          cfiqtdped numeric(6,0) NOT NULL DEFAULT 0,
                          cfiespped character varying(20) NOT NULL DEFAULT ''::character varying,
                          cfiviaped character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfiseuped character(10) NOT NULL DEFAULT ''::bpchar,
                          cfirecped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfifedped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfiob1ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob2ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob3ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob4ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfiob5ped character varying(50) NOT NULL DEFAULT ''::character varying,
                          cfirotped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfincxped numeric(3,0) NOT NULL DEFAULT 0,
                          cfinljped numeric(3,0) NOT NULL DEFAULT 0,
                          cfipetped numeric(7,0) NOT NULL DEFAULT 0,
                          cfiemlped character(1) NOT NULL DEFAULT ''::bpchar,
                          cfioutped numeric(12,2) NOT NULL DEFAULT 0,
                          cfistoped character(5) NOT NULL DEFAULT ''::bpchar,
                          cficreped numeric(7,0) NOT NULL DEFAULT 0,
                          cficpiped character(19) NOT NULL DEFAULT ''::bpchar,
                          cfiljfped numeric(3,0) NOT NULL DEFAULT 0,
                          cfiusuped character varying(35) NOT NULL DEFAULT ''::character varying,
                          cfioriped character(1) NOT NULL DEFAULT ''::bpchar,
                          cficplped character varying(30) NOT NULL DEFAULT ''::character varying,
                          cficudped numeric(5,0) NOT NULL DEFAULT 0,
                          cfigrjped numeric(12,2) NOT NULL DEFAULT 0,
                          cfinenped character(10) NOT NULL DEFAULT ''::bpchar,
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxtpecfi_sq'::regclass),
                          indkey_001 character varying(254),
                          cfitcfped character(3) NOT NULL DEFAULT ''::bpchar,
                          cfirpaped numeric(5,0) NOT NULL DEFAULT 0,
                          cfinseped character varying(15) NOT NULL DEFAULT ''::character varying,
                          CONSTRAINT mxtpecfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE,
                          autovacuum_enabled=true
                        );";
    }

    public function getCriacaoMXTITCFI()
    {
        return "CREATE TABLE mxtitcfi
                        (
                          cfipedite numeric(7,0) NOT NULL DEFAULT 0,
                          cfiproite character(9) NOT NULL DEFAULT ''::bpchar,
                          cfidesite character(40) NOT NULL DEFAULT ''::bpchar,
                          cfiembite character(5) NOT NULL DEFAULT ''::bpchar,
                          cfifabite character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfitriite character(3) NOT NULL DEFAULT ''::bpchar,
                          cfimaqite character(1) NOT NULL DEFAULT ''::bpchar,
                          cfivenite character(1) NOT NULL DEFAULT ''::bpchar,
                          cfiobsite character varying(25) NOT NULL DEFAULT ''::character varying,
                          cfidatite date,
                          cfiqtdite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiprcite numeric(13,4) NOT NULL DEFAULT 0,
                          cfiprlite numeric(13,4) NOT NULL DEFAULT 0,
                          cfidscite numeric(12,4) NOT NULL DEFAULT 0,
                          cfiacrite numeric(12,4) NOT NULL DEFAULT 0,
                          cfiratite numeric(9,3) NOT NULL DEFAULT 0,
                          cfiipiite numeric(5,2) NOT NULL DEFAULT 0,
                          cfiicmite numeric(5,2) NOT NULL DEFAULT 0,
                          cfiricite numeric(5,2) NOT NULL DEFAULT 0,
                          cfibruite numeric(8,3) NOT NULL DEFAULT 0,
                          cfiliqite numeric(8,3) NOT NULL DEFAULT 0,
                          cfiqaeite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiqprite numeric(13,6) NOT NULL DEFAULT 0,
                          cficorite character(30) NOT NULL DEFAULT ''::bpchar,
                          cfinljite numeric(3,0) NOT NULL DEFAULT 0,
                          cfipcuite numeric(13,4) NOT NULL DEFAULT 0,
                          cfiqdvite numeric(13,6) NOT NULL DEFAULT 0,
                          cfimotite character varying(27) NOT NULL DEFAULT ''::character varying,
                          cfilisite numeric(3,0) NOT NULL DEFAULT 0,
                          cfinpdite character varying(15) NOT NULL DEFAULT ''::character varying,
                          cfiipdite numeric(6,0) NOT NULL DEFAULT 0,
                          cfialiite numeric(5,2) NOT NULL DEFAULT 0,
                          cfimvaite numeric(6,2) NOT NULL DEFAULT 0,
                          cfirfrite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirsgite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirdsite numeric(12,2) NOT NULL DEFAULT 0,
                          cfirouite numeric(12,2) NOT NULL DEFAULT 0,
                          cfincmite character(8) NOT NULL DEFAULT ''::bpchar,
                          cfivnmite numeric(3,0) NOT NULL DEFAULT 0,
                          cfiqsaite numeric(13,6) NOT NULL DEFAULT 0,
                          cfiaesite numeric(5,2) NOT NULL DEFAULT 0,
                          cfichvite character(10) NOT NULL DEFAULT ''::bpchar,
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxtitcfi_sq'::regclass),
                          CONSTRAINT mxtitcfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                          OIDS=FALSE,
                          autovacuum_enabled=true
                        );";
    }

    public function getCriacaoMXVCOCFI()
    {
        return "CREATE TABLE mxvcocfi
                    (
                      cfikeyvco character(32) NOT NULL DEFAULT ''::bpchar,
                      cfiatvvco numeric(1,0) NOT NULL DEFAULT 0,
                      cficlivco numeric(9,0) NOT NULL DEFAULT 0,
                      cfidtavco date not null default CURRENT_DATE,
                      sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxvcocfi_sq'::regclass),
                      CONSTRAINT mxvcocfi_sr_recno_key UNIQUE (sr_recno)
                    )
                    WITH (
                      OIDS=FALSE,
                      autovacuum_enabled=true
                    );";
    }

    public function getCriacaoMXIMACFI()
    {
        return "CREATE TABLE mximacfi
                        (
                          cficodima character(9),
                          cfidirima text,
                          cfitipima character(15),
                          sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mximacfi_sq'::regclass),
                          CONSTRAINT mximacfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                            OIDS=FALSE,
                            autovacuum_enabled=true
                        );";
    }

    public function getCriacaoMXLGNCFI()
    {
        return "CREATE TABLE mxlgncfi
                        (
                            cfitknlgn character(40) NOT NULL DEFAULT ''::bpchar,
                            cficodlgn numeric(9,0) NOT NULL DEFAULT 0,
                            sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxlgncfi_sq'::regclass),
                            CONSTRAINT mxlgncfi_sr_recno_key UNIQUE (sr_recno)
                        )
                        WITH (
                            OIDS=FALSE,
                            autovacuum_enabled=true
                        );";
    }

    public function getCriacaoMXVRFCFI()
    {
        return "CREATE TABLE mxvrfcfi
                (
                  cficodvrf numeric(9,0) NOT NULL DEFAULT nextval('mxglocfi_sq'::regclass),
                  cfirepvrf numeric(9,0) NOT NULL DEFAULT 0,
                  sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxvrfcfi_sq'::regclass),
                  CONSTRAINT mxvrfcfi_sr_recno_key UNIQUE (sr_recno)
                )
                WITH (
                   OIDS=FALSE,
                   autovacuum_enabled=true
                );";
    }

    public function getCriacaoMXPDWCFI()
    {
        return "CREATE TABLE mxpdwcfi
                            (
                              cfipedpdw numeric(9,0) NOT NULL DEFAULT 0,
                              cfirecpdw character varying(32),
                              cfitrapdw character varying(44),
                              cfinotpdw character varying(60),
                              cfitippdw character varying(18),
                              cfistapdw character varying(22),
                              cfidtgpdw date DEFAULT ('now'::text)::date,
                              cfidtppdw date,
                              cfidtvpdw date DEFAULT (('now'::text)::date + 10),
                              sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxpdwcfi_sq'::regclass),
                              CONSTRAINT mxpdwcfi_sr_recno_key UNIQUE (sr_recno)
                            )
                            WITH (
                              OIDS=FALSE,
                              autovacuum_enabled=true
                            );";
    }

    public function getCriacaoMXFCPCFI()
    {
        return "CREATE TABLE mxfcpcfi
                (
                  cficodfcp numeric(5,0) NOT NULL DEFAULT 0,
                  cfidesfcp character(40) NOT NULL DEFAULT ''::bpchar,
                  cficinfcp character varying(9) NOT NULL DEFAULT ''::character varying,
                  cficfmfcp character varying(9) NOT NULL DEFAULT ''::character varying,
                  cfipinfcp numeric(8,3) NOT NULL DEFAULT 0,
                  cfipfmfcp numeric(8,3) NOT NULL DEFAULT 0,
                  cfivinfcp numeric(15,2) NOT NULL DEFAULT 0,
                  cfivfmfcp numeric(15,2) NOT NULL DEFAULT 0,
                  cfiprzfcp numeric(12,2) NOT NULL DEFAULT 0,
                  cfivalfcp numeric(15,2) NOT NULL DEFAULT 0,
                  cfiblofcp character(1) NOT NULL DEFAULT 'N'::bpchar,
                  sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxfcpcfi_sq'::regclass),
                  CONSTRAINT mxfcpcfi_sr_recno_key UNIQUE (sr_recno)
                )
                WITH (
                  OIDS=FALSE,
                   autovacuum_enabled=true
                );";
    }

    public function getCriacaoMXFSECFI()
    {
        return "CREATE TABLE mxfsecfi
                (
                    cficodfse SERIAL PRIMARY KEY, -- Código do frete selecionado
                    cfilojfse numeric(2) NOT NULL DEFAULT 0, -- Loja
                    cfipedfse numeric(7,0) NOT NULL DEFAULT 0, -- Código do pedido
                    cficlifse numeric(9,0) NOT NULL DEFAULT 0, -- Código do cliente
                    cfiendfse numeric(9,0) NOT NULL DEFAULT 0, -- Código do endereço
                    cfiprzfse numeric(12,2) NOT NULL DEFAULT 0, -- Prazo do frete selecionado
                    cficscfse numeric(5,0) NOT NULL DEFAULT 0, -- Código de serviço do frete
                    cfidesfse character(40) NOT NULL DEFAULT ''::bpchar, -- Descrição do frete
                    cfidtafse date, -- Data agendada
                    cfidtvfse date, -- Data de vencimento
                    cfiretfse character(1) NOT NULL DEFAULT 'N'::bpchar, -- É retirada na loja?
                    cfiorifse character(8) NOT NULL DEFAULT ''::bpchar, -- Origem do pedido
                    sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxfsecfi_sq'::regclass), -- PostgreSql Corporation Standard on Max Scalla
                    CONSTRAINT mxfsecfi_sr_recno_key UNIQUE (sr_recno)
                )
                WITH (
                  OIDS=FALSE,
                   autovacuum_enabled=true
                );";
    }

    public function getCriacaoMXBPECFI()
    {
        return " CREATE TABLE mxbpecfi
                (
                    cficodbpe SERIAL PRIMARY KEY, -- Código
                    cfiprobpe character(9), -- Código do produto
                    cficrrbpe int DEFAULT 0, -- É correios
                    cfifxfbpe int DEFAULT 0, -- É faixa de frete
                    cfigrpbpe varchar(5), -- Código do Grupo
                    cfidepbpe int, -- Prazo do frete selecionado
                    cfictgbpe int, -- Categoria
                    cfistgbpe int, -- SubCategoria
                    cfifabbpe int -- Fabricante
                )
                WITH (
                    OIDS=FALSE,
                    autovacuum_enabled=true
                );";
    }

    public function getCriacaoMXENECFI()
    {
        return "CREATE TABLE mxenecfi
                (
                  cficliene numeric(9,0) NOT NULL DEFAULT 0,
                  cfinclene character(40) NOT NULL DEFAULT ''::bpchar,
                  cfilogene character varying(35) NOT NULL DEFAULT ''::character varying,
                  cfibaiene character varying(40) NOT NULL DEFAULT ''::character varying,
                  cficidene character varying(20) NOT NULL DEFAULT ''::character varying,
                  cfiestene character(2) NOT NULL DEFAULT ''::bpchar,
                  cficepene character(9) NOT NULL DEFAULT ''::bpchar,
                  cfirefene character varying(40) NOT NULL DEFAULT ''::character varying,
                  cfiatuene character(1) NOT NULL DEFAULT ''::bpchar,
                  cfipriene character(1) NOT NULL DEFAULT ''::bpchar,
                  cficplene character varying(30) NOT NULL DEFAULT ''::character varying,
                  cficodene numeric(9,0) NOT NULL DEFAULT 0,
                  cfinumene character(10) NOT NULL DEFAULT ''::bpchar,
                  cfitabene character(10) NOT NULL DEFAULT ''::bpchar,
                  cficreene character(6) NOT NULL DEFAULT ''::bpchar,
                  sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxenecfi_sq'::regclass),
                  CONSTRAINT mxenecfi_sr_recno_key UNIQUE (sr_recno)
                )
                WITH (
                  OIDS=FALSE,
                  autovacuum_enabled=true
                );";
    }

    public function getCriacaoMXMENCFI()
    {
        return "CREATE TABLE mxmencfi
                    (
                      cficodmen numeric(5,0) NOT NULL DEFAULT 0,
                      cfifabmen numeric(5,0) NOT NULL DEFAULT 0,
                      cfidepmen numeric(5,0) NOT NULL DEFAULT 0,
                      cfictgmen numeric(5,0) NOT NULL DEFAULT 0,
                      cfiordmen numeric(5,0) NOT NULL DEFAULT 0,
                      cfipaimen numeric(5,0) NOT NULL DEFAULT 0,
                      cfinommen character varying(60) NOT NULL DEFAULT ''::character varying,
                      sr_recno numeric(15,0) NOT NULL DEFAULT nextval('mxmencfi_sq'::regclass),
                      CONSTRAINT mxmencfi_sr_recno_key UNIQUE (sr_recno)
                    )
                    WITH (
                      OIDS=FALSE
                    );";
    }
}
