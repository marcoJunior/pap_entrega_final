<?php

namespace BancoDeDados\Service;

use Zend\Stdlib\Parameters;
use Zend\Db\Adapter\Adapter;
use ConexaoClienteMaxScalla\Entity\Conexao;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class BancoDeDados extends \APIGrid\Service\APIGrid
{

    public function adicao(Parameters $post)
    {
        $idCliente = $post->get('idCliente', null);
        return $this->mapperBancoDeDados()->adicao($idCliente);
    }

    public function alterar(Parameters $post)
    {
        $config = $this->getServiceManager()->get('Config')['db'];
        $formularioConexao = new Conexao($post->get('formulario', []));

        $config['dsn'] = 'pgsql:dbname=' . $formularioConexao->getBanco() .
            ';host=' . $formularioConexao->getIp() .
            ';port=' . $formularioConexao->getPorta();

        $config['username'] = $formularioConexao->getUsuarioBanco();
        $config['password'] = $formularioConexao->getSenhaBanco();

        return $this->mapperBancoDeDados()->alterar(new Adapter($config));
    }

    public function excluir($id)
    {

    }

    /**
     * @return \BancoDeDados\Mapper\BancoDeDados
     */
    public function mapperBancoDeDados()
    {
        return $this->getServiceManager()->get(\BancoDeDados\Mapper\BancoDeDados::class);
    }

}
