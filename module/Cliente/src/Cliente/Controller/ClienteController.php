<?php

namespace Cliente\Controller;

use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use Cliente\View\Cliente;

class ClienteController extends \APIGrid\Controller\APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewCliente = new Cliente($phpRenderer);
            $viewCliente->setVariable('titulo', 'Clientes');
            $viewCliente->grid();
            $viewCliente->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewCliente);

            return $app;
        }

        $service = $this->serviceEmpresasMax();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceEmpresasMax();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceEmpresasMax();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceEmpresasMax();
        $get = new Parameters($data);

//        var_dump($get);die();

        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceEmpresasMax();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \Cliente\Service\Cliente
     */
    public function serviceEmpresasMax()
    {
        return $this->getService()->get(\Cliente\Service\Cliente::class);
    }

    /**
     * @return \Cliente\Service\ConexaoCliente
     */
    public function serviceConexaoCliente()
    {
        return $this->getService()->get(\Cliente\Service\ConexaoCliente::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
