<?php

namespace Cliente\Entity;

use APIHelper\Entity\AbstractEntity;

class ClienteCompleto extends AbstractEntity
{

    protected $id;
    protected $serie;
    protected $razaoSocial;
    protected $fantasia;
    protected $documento;
    protected $logo;
    protected $ativo;
    protected $endereco;
    protected $contato;

    public function __construct($data = [])
    {
        parent::__construct($data);
        $this->endereco = new EnderecoCliente($data);
        $this->contato = new ContatoCliente($data);
    }

    public function __call($method, $args)
    {
        if (method_exists($this->endereco, $method)) {
            $this->endereco->{$method}($args[0]);
        }
        if (method_exists($this->contato, $method)) {
            $this->contato->{$method}($args[0]);
        }
    }

    function getSerie()
    {
        return $this->serie;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function getId()
    {
        return $this->id;
    }

    function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    function getFantasia()
    {
        return $this->fantasia;
    }

    function getDocumento()
    {
        return $this->documento;
    }

    function getLogo()
    {
        return $this->logo;
    }

    function getAtivo()
    {
        return $this->ativo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setRazaoSocial($razaoSocial)
    {
        $this->razaoSocial = $razaoSocial;
    }

    function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
    }

    function setDocumento($documento)
    {
        $this->documento = $documento;
    }

    function setLogo($logo)
    {
        $this->logo = $logo;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function getEndereco()
    {
        return $this->endereco;
    }

    function getContato()
    {
        return $this->contato;
    }

}
