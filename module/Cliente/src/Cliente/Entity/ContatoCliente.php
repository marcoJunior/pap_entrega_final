<?php

namespace Cliente\Entity;

use APIHelper\Entity\AbstractEntity;

class ContatoCliente extends AbstractEntity
{

    public $nome = '';
    public $telefone = '';
    public $email = '';
    public $idCliente;

    function getNome()
    {
        return $this->nome;
    }

    function getTelefone()
    {
        return $this->telefone;
    }

    function getEmail()
    {
        return $this->email;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    function setEmail($email)
    {
        $this->email = $email;
    }

    function getidCliente()
    {
        return $this->idCliente;
    }

    function setidCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

}
