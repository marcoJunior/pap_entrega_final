<?php

namespace Cliente\Entity;

use APIHelper\Entity\AbstractEntity;

class EnderecoCliente extends AbstractEntity
{

    public $cep;
    public $rua;
    public $numero;
    public $complemento;
    public $bairro;
    public $cidade;
    public $estado;
    public $idCliente;

    function getCep()
    {
        return $this->cep;
    }

    function getRua()
    {
        return $this->rua;
    }

    function getNumero()
    {
        return $this->numero;
    }

    function getComplemento()
    {
        return $this->complemento;
    }

    function getBairro()
    {
        return $this->bairro;
    }

    function getCidade()
    {
        return $this->cidade;
    }

    function getEstado()
    {
        return $this->estado;
    }

    function setCep($cep)
    {
        $this->cep = $cep;
    }

    function setRua($rua)
    {
        $this->rua = $rua;
    }

    function setNumero($numero)
    {
        $this->numero = $numero;
    }

    function setComplemento($complemento)
    {
        $this->complemento = $complemento;
    }

    function setBairro($bairro)
    {
        $this->bairro = $bairro;
    }

    function setCidade($cidade)
    {
        $this->cidade = $cidade;
    }

    function setEstado($estado)
    {
        $this->estado = $estado;
    }

    function getidCliente()
    {
        return $this->idCliente;
    }

    function setidCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

}
