<?php

namespace Cliente\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ClienteCompleto extends Hydrator
{

    protected function getEntity()
    {
        return 'Cliente\Entity\ClienteCompleto';
    }

    public function getMap()
    {
        $arrayMap = [
            'razaoSocial' => 'razao_social'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ClienteCompleto();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
