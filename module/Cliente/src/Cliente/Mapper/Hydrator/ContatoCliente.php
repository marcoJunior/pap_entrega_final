<?php

namespace Cliente\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ContatoCliente extends Hydrator
{

    protected function getEntity()
    {
        return 'Cliente\Entity\ContatoCliente';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ContatoCliente();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
