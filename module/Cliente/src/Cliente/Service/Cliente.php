<?php

namespace Cliente\Service;

use Zend\Stdlib\Parameters;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Cliente\Entity\Cliente as ClienteEntity;
use Cliente\Entity\ClienteCompleto as ClienteCompletoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Cliente extends \APIGrid\Service\APIGrid
{

    public function selecionar(Parameters $get)
    {

        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperCliente()
                ->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperCliente()->selecionarId($id);
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $clienteCompleto = new ClienteCompletoEntity($formulario);

        $cliente = new ClienteEntity($formulario);
        $contato = $clienteCompleto->getContato();
        $endereco = $clienteCompleto->getEndereco();

        $form = $post->get('formulario');
        if (isset($form['empresa'])) {
            $cliente->setFantasia(strtoupper($form['empresa']));
        }

        $validacao = $this->validaExistencia($cliente, $contato, $endereco);

        if (!isset($validacao['mensagem'])) {

            $retorno[0] = $this->mapperCliente()->adicao($cliente);

            if (!isset($form['empresa'])) {
                $contato->setidCliente($retorno[0]);
                $endereco->setidCliente($retorno[0]);

                $retorno[1] = $this->mapperContatoCliente()->adicao($contato);
                $retorno[2] = $this->mapperEnderecoCliente()->adicao($endereco);
            }
            return $retorno;
        }

        return $validacao;
    }

    public function validaExistencia($cliente, $contato, $endereco)
    {

        if ($this->mapperCliente()->validaExistencia($cliente)) {
            return ['mensagem' => 'Já existe um cadastro para está empresa!'];
        }

        if ($this->mapperContatoCliente()->validaExistencia($contato)) {
            return ['mensagem' => 'Já existe um cadastro de CONTATO para está empresa!'];
        }

        if ($this->mapperEnderecoCliente()->validaExistencia($endereco)) {
            return ['mensagem' => 'Já existe um cadastro de ENDEREÇO para está empresa!'];
        }
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $clienteCompleto = new ClienteCompletoEntity($formulario);

        $cliente = new ClienteEntity($formulario);

        $contato = $clienteCompleto->getContato();
        $contato->setidCliente($clienteCompleto->getId());

        $endereco = $clienteCompleto->getEndereco();
        $endereco->setidCliente($clienteCompleto->getId());

        $form = $post->get('formulario');
        if (isset($form['empresa'])) {
            $cliente->setFantasia(strtoupper($form['empresa']));
        }

        $retorno[0] = $this->mapperCliente()->alterar($cliente);

        if (!isset($form['empresa'])) {
            $retorno[1] = $this->mapperContatoCliente()->alterar($contato);
            $retorno[2] = $this->mapperEnderecoCliente()->alterar($endereco);
        }

        return $retorno;
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        $retorno[0] = $this->mapperEnderecoCliente()->excluir($id);
        $retorno[1] = $this->mapperContatoCliente()->excluir($id);
        $retorno[2] = $this->mapperCliente()->excluir($id);

        return $retorno;
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return \Cliente\Mapper\Cliente
     */
    public function mapperCliente()
    {
        return $this->getServiceManager()->get('Cliente\Mapper\Cliente');
    }

    /**
     * @return Cliente\Mapper\EnderecoCliente
     */
    public function mapperEnderecoCliente()
    {
        return $this->getServiceManager()->get('Cliente\Mapper\EnderecoCliente');
    }

    /**
     * @return Cliente\Mapper\ContatoCliente
     */
    public function mapperContatoCliente()
    {
        return $this->getServiceManager()->get('Cliente\Mapper\ContatoCliente');
    }

}
