<?php

namespace ConexaoClienteMaxScalla;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Conexao::class => function ($sm) {
                    $service = new Service\Conexao();
                    return $service->setServiceManager($sm);
                },
                Service\ConexaoClienteMaxScalla::class => function ($sm) {
                    $service = new Service\ConexaoClienteMaxScalla();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'ConexaoClienteMaxScalla\Mapper\ConexaoClienteMaxScalla' =>
                function ($sm) {
                    $mapper = new Mapper\ConexaoClienteMaxScalla();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\ConexaoClienteMaxScalla())
                        ->setHydrator(new Mapper\Hydrator\ConexaoClienteMaxScalla());
                    return $mapper;
                },
                'ConexaoClienteMaxScalla\Mapper\Conexao' =>
                function ($sm) {
                    $mapper = new Mapper\Conexao();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Conexao())
                        ->setHydrator(new Mapper\Hydrator\Conexao());
                    return $mapper;
                },
            ),
        );
    }

}
