<?php

return array(
    'controllers' => array(
        'factories' => array(
            'conexao' => function ($sl) {
                $controller = new \ConexaoClienteMaxScalla\Controller\ConexaoController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'contrato' => function ($sl) {
                $controller = new \ConexaoClienteMaxScalla\Controller\ConexaoClienteMaxScallaController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ),
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'contrato' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/contrato[/:id][/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'contrato',
                    )
                ),
            ),
            'conexao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/conexao[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'conexao',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'conexao_cliente_maxscalla/grid' => __DIR__ . '/../view/conexao_cliente_maxscalla/grid.phtml',
            'conexao_cliente_maxscalla/formulario' => __DIR__ . '/../view/conexao_cliente_maxscalla/formulario.phtml',
            'conexao_cliente_maxscalla/modal' => __DIR__ . '/../view/conexao_cliente_maxscalla/modal.phtml',
        ),
        'template_path_stack' => array(
            'conexao_cliente_maxscalla' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
