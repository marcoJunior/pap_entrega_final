<?php

namespace ConexaoClienteMaxScalla\Controller;

use APIGrid\Controller\APIGridController;

class AuxiliarController extends APIGridController
{

    /**
     * @return \ConexaoClienteMaxScalla\Service\Sistema
     */
    public function serviceSistema()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\Sistema');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\Plano
     */
    public function servicePlano()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\Plano');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\RecursoDescricao
     */
    public function serviceRecursoDescricao()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\RecursoDescricao');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\Recurso
     */
    public function serviceRecurso()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\Recurso');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\ClienteMaxScalla
     */
    public function serviceEmpresasMax()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\ClienteMaxScalla');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\TempoExperienciaCliente
     */
    public function serviceTempoExperienciaCliente()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\TempoExperienciaCliente');
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\ConexaoClienteMaxScalla
     */
    public function serviceConexaoClienteMaxScalla()
    {
        return $this->getService()->get('ConexaoClienteMaxScalla\Service\ConexaoClienteMaxScalla');
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get('APIFiltro\Service\Init');
    }

}
