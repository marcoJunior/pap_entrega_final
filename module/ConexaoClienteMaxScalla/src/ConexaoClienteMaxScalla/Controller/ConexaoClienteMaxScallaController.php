<?php

namespace ConexaoClienteMaxScalla\Controller;

use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use ConexaoClienteMaxScalla\View\ConexaoClienteMaxScalla;

class ConexaoClienteMaxScallaController extends AuxiliarController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewClientes = new ConexaoClienteMaxScalla($phpRenderer);
            $viewClientes->setVariable('titulo', 'Contratos');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        $service = $this->serviceConexaoClienteMaxScalla();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        return new JsonModel((array) $service->selecionarId($id));
    }

    public function create($data)
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        $get = new Parameters($data);

        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        return new JsonModel((array) $service->excluir($id));
    }

    public function selecionarContratoPorClienteAction()
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        $post = $this->getRequest()->getPost();

        if ($post->get('idCliente') != null) {
            $id = $post->get('idCliente');
        } else {
            $id = explode("/", $this->getRequest()->getRequestUri())[2];
        }

        return new JsonModel((array) $service->selecionarContratoPorCliente($id));
    }

    public function carregarPlanosAction()
    {
        $service = $this->serviceConexaoClienteMaxScalla();
        $post = $this->getRequest()->getPost();

        $return = [
            'quantidadePlano' => $service->qtdTotalPlano($post),
            'precoTotal' => $service->valorTotalPlano($post)
        ];

        return new JsonModel((array) $return);
    }

}
