<?php

namespace ConexaoClienteMaxScalla\Controller;

use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use ConexaoClienteMaxScalla\View\ConexaoClienteMaxScalla;

class ConexaoController extends AuxiliarController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        $service = $this->serviceConexao();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
//        $service = $this->serviceSistema();
//        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function delete($id)
    {
        $service = $this->serviceConexao();

        try {
            $retorno = $service->excluir($id);
        } catch (\Zend\Db\Adapter\Exception\InvalidQueryException $exc) {
            if (count(explode('Cannot delete or update a parent row: a foreign key constraint fails', $exc->getMessage())) > 0) {
                $retorno = ['mensagem' => 'Não é posivel excluir uma conexão que esteja vinculada há um ou mais contratos!'];
            }
        }

        return new JsonModel((array) $retorno);
    }

    /**
     * @return \ConexaoClienteMaxScalla\Service\Conexao
     */
    public function serviceConexao()
    {
        return $this->getService()->get(\ConexaoClienteMaxScalla\Service\Conexao::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
