<?php

namespace ConexaoClienteMaxScalla\Entity;

use Application\Entity\EntityHelper;

class ClientePlanoConexao extends EntityHelper
{

    protected $id;
    protected $apelido;
    protected $subDominio;
    protected $ativo;
    protected $idConexao;
    protected $idPlano;
    protected $idCliente;

    function getId()
    {
        return $this->id;
    }

    function getApelido()
    {
        return $this->apelido;
    }

    function getSubDominio()
    {
        return $this->subDominio;
    }

    function getAtivo()
    {
        if ($this->ativo === '0' || $this->ativo === '1') {
            return (int) $this->ativo;
        }

        if (gettype($this->ativo) == 'string') {
            return $this->ativo == 'true' ? 1 : 0;
        }

        return $this->ativo;
    }

    function getIdConexao()
    {
        return $this->idConexao;
    }

    function getIdPlano()
    {
        return $this->idPlano;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    function setSubDominio($subDominio)
    {
        $this->subDominio = $subDominio;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function setIdConexao($idConexao)
    {
        $this->idConexao = $idConexao;
    }

    function setIdPlano($idPlano)
    {
        $this->idPlano = $idPlano;
    }

    function getIdCliente()
    {
        return $this->idCliente;
    }

    function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }




}
