<?php

namespace ConexaoClienteMaxScalla\Entity;

use Application\Entity\EntityHelper;

class Conexao extends EntityHelper
{

    protected $id;
    protected $dominio;
    protected $ip;
    protected $porta;
    protected $usuarioBanco;
    protected $senhaBanco;
    protected $banco;
    protected $ipTintometrico;
    protected $portaTintometrico;
    protected $usuarioBancoTintometrico;
    protected $senhaBancoTintometrico;
    protected $bancoTintometrico;
    protected $idCliente;

    function getId()
    {
        return $this->id;
    }

    function getDominio()
    {
        return $this->dominio;
    }

    function getIp()
    {
        return $this->ip;
    }

    function getPorta()
    {
        return $this->porta;
    }

    function getUsuarioBanco()
    {
        return $this->usuarioBanco;
    }

    function getSenhaBanco()
    {
        return $this->senhaBanco;
    }

    function getBanco()
    {
        return $this->banco;
    }

    function getIpTintometrico()
    {
        return $this->ipTintometrico;
    }

    function getPortaTintometrico()
    {
        return $this->portaTintometrico;
    }

    function getUsuarioBancoTintometrico()
    {
        return $this->usuarioBancoTintometrico;
    }

    function getSenhaBancoTintometrico()
    {
        return $this->senhaBancoTintometrico;
    }

    function getBancoTintometrico()
    {
        return $this->bancoTintometrico;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function setPorta($porta)
    {
        $this->porta = $porta;
    }

    function setUsuarioBanco($usuarioBanco)
    {
        $this->usuarioBanco = $usuarioBanco;
    }

    function setSenhaBanco($senhaBanco)
    {
        $this->senhaBanco = $senhaBanco;
    }

    function setBanco($banco)
    {
        $this->banco = $banco;
    }

    function setIpTintometrico($ipTintometrico)
    {
        $this->ipTintometrico = $ipTintometrico;
    }

    function setPortaTintometrico($portaTintometrico)
    {
        $this->portaTintometrico = $portaTintometrico;
    }

    function setUsuarioBancoTintometrico($usuarioBancoTintometrico)
    {
        $this->usuarioBancoTintometrico = $usuarioBancoTintometrico;
    }

    function setSenhaBancoTintometrico($senhaBancoTintometrico)
    {
        $this->senhaBancoTintometrico = $senhaBancoTintometrico;
    }

    function setBancoTintometrico($bancoTintometrico)
    {
        $this->bancoTintometrico = $bancoTintometrico;
    }

    function getIdCliente()
    {
        return $this->idCliente;
    }

    function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }


}
