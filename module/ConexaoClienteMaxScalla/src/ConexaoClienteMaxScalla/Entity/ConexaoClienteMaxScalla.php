<?php

namespace ConexaoClienteMaxScalla\Entity;

use Application\Entity\EntityHelper;

class ConexaoClienteMaxScalla extends EntityHelper
{

    protected $id;
    protected $cliente;
    protected $idConexao;
    protected $plano;
    protected $idPlano;
    protected $idCliente;
    protected $apelido;
    protected $dominio;
    protected $subDominio;
    protected $ip;
    protected $porta;
    protected $usuarioBanco;
    protected $senhaBanco;
    protected $banco;
    protected $preco;
    protected $idSistema;
    protected $sistema;
    protected $sistemaChave;
    protected $ativo;
    protected $ipTintometrico;
    protected $portaTintometrico;
    protected $usuarioBancoTintometrico;
    protected $senhaBancoTintometrico;
    protected $bancoTintometrico;

    function getId()
    {
        return $this->id;
    }

    function getIdConexao()
    {
        return $this->idConexao;
    }

    function getIdPlano()
    {
        return $this->idPlano;
    }

    function getIdCliente()
    {
        return $this->idCliente;
    }

    function getApelido()
    {
        return $this->apelido;
    }

    function getDominio()
    {
        return $this->dominio;
    }

    function getSubDominio()
    {
        return $this->subDominio;
    }

    function getIp()
    {
        return $this->ip;
    }

    function getPorta()
    {
        return (int) $this->porta;
    }

    function getUsuarioBanco()
    {
        return $this->usuarioBanco;
    }

    function getSenhaBanco()
    {
        return $this->senhaBanco;
    }

    function getBanco()
    {
        return $this->banco;
    }

    function getAtivo()
    {
        if (gettype($this->ativo) == 'string') {
            $this->ativo = $this->ativo == 'true' ? 1 : 0;
        }

        return $this->ativo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setIdConexao($idConexao)
    {
        $this->idConexao = (int) $idConexao;
    }

    function setIdPlano($idPlano)
    {
        $this->idPlano = (int) $idPlano;
    }

    function setIdCliente($idCliente)
    {
        $this->idCliente = (int) $idCliente;
    }

    function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    function setDominio($dominio)
    {
        $this->dominio = $dominio;
    }

    function setSubDominio($subDominio)
    {
        $this->subDominio = $subDominio;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function setPorta($porta)
    {
        $this->porta = $porta;
    }

    function setUsuarioBanco($usuarioBanco)
    {
        $this->usuarioBanco = $usuarioBanco;
    }

    function setSenhaBanco($senhaBanco)
    {
        $this->senhaBanco = $senhaBanco;
    }

    function setBanco($banco)
    {
        $this->banco = $banco;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function getPlano()
    {
        return $this->plano;
    }

    function setPlano($plano)
    {
        $this->plano = $plano;
    }

    function getIpTintometrico()
    {
        return $this->ipTintometrico;
    }

    function getPortaTintometrico()
    {
        return $this->portaTintometrico;
    }

    function getUsuarioBancoTintometrico()
    {
        return $this->usuarioBancoTintometrico;
    }

    function getSenhaBancoTintometrico()
    {
        return $this->senhaBancoTintometrico;
    }

    function getBancoTintometrico()
    {
        return $this->bancoTintometrico;
    }

    function setIpTintometrico($ipTintometrico)
    {
        $this->ipTintometrico = $ipTintometrico;
    }

    function setPortaTintometrico($portaTintometrico)
    {
        $this->portaTintometrico = $portaTintometrico;
    }

    function setUsuarioBancoTintometrico($usuarioBancoTintometrico)
    {
        $this->usuarioBancoTintometrico = $usuarioBancoTintometrico;
    }

    function setSenhaBancoTintometrico($senhaBancoTintometrico)
    {
        $this->senhaBancoTintometrico = $senhaBancoTintometrico;
    }

    function setBancoTintometrico($bancoTintometrico)
    {
        $this->bancoTintometrico = $bancoTintometrico;
    }

    function getPreco()
    {
        return $this->preco;
    }

    function setPreco($preco)
    {
        $this->preco = $preco;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function getSistemaChave()
    {
        return $this->sistemaChave;
    }

    function setSistemaChave($sistemaChave)
    {
        $this->sistemaChave = $sistemaChave;
    }

    function getIdSistema()
    {
        return $this->idSistema;
    }

    function setIdSistema($idSistema)
    {
        $this->idSistema = $idSistema;
    }

}
