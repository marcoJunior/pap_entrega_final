<?php

namespace ConexaoClienteMaxScalla\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use ConexaoClienteMaxScalla\Entity\ClienteMaxScalla as ClienteMaxScallaEntity;
use ConexaoClienteMaxScalla\Entity\ClienteMaxScallaCompleto as ClienteMaxScallaCompletoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ClienteMaxScalla extends APIGrid
{

    public $tableName = 'cliente_completo';
    public $mapperName = 'ConexaoClienteMaxScalla\Mapper\Hydrator\ClienteMaxScallaCompleto';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

//        foreach ($filtros->toArray() as $index => $valor) {
//            $valorDescricao = str_replace('_', '',
//                    str_replace('_de', '', ucfirst($index)));
//            $getValor = 'get' . $valorDescricao;
//            $setWhere = 'setWhere' . $valorDescricao;
//            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere}($filtros);
//            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere . "s"}($filtros);
//            }
//        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'serie',
            'razao_social',
            'fantasia',
            'documento',
            'logo',
            'ativo',
            //endereço
            'cep',
            'rua',
            'numero',
            'complemento',
            'bairro',
            'cidade',
            'estado',
            //contato
            'nome',
            'telefone',
            'email'
        ];
    }

    public function validaExistencia(ClienteMaxScallaEntity $entity)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('documento', $entity->getDocumento());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaExistenciaPorId($id)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(ClienteMaxScallaEntity $entity)
    {
        $this->inicializaManutencao();
        return $this->insert($entity)->getGeneratedValue();
    }

    public function alterar(ClienteMaxScallaEntity $entity)
    {
        $this->inicializaManutencao();
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $this->inicializaManutencao();
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

    public function inicializaManutencao()
    {
        $this->inicializar('clientes',
                'ConexaoClienteMaxScalla\Mapper\Hydrator\ClienteMaxScalla');

        $this->setEntityPrototype(new ClienteMaxScallaEntity);
        $this->setHydrator(new Hydrator\ClienteMaxScalla);
    }

}
