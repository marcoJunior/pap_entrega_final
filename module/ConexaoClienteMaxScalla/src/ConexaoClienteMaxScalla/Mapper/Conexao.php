<?php

namespace ConexaoClienteMaxScalla\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use ConexaoClienteMaxScalla\Entity\Conexao as ConexaoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Conexao extends APIGrid
{

    public $tableName = 'conexao';
    public $mapperName = 'ConexaoClienteMaxScalla\Mapper\Hydrator\Conexao';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $where = new Where();
        $where->notEqualTo('id', 1);
        $this->setWhere($where);

//        foreach ($filtros->toArray() as $index => $valor) {
//            $valorDescricao = str_replace('_', '',
//                    str_replace('_de', '', ucfirst($index)));
//            $getValor = 'get' . $valorDescricao;
//            $setWhere = 'setWhere' . $valorDescricao;
//            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere}($filtros);
//            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere . "s"}($filtros);
//            }
//        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereCodigo(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('id_cliente', $filtros->getCodigo());
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'id',
            'dominio',
            'ip',
            'porta',
            'usuario_banco',
            'senha_banco',
            'banco',
            //Tintometrico
            'ip_tintometrico',
            'porta_tintometrico',
            'usuario_banco_tintometrico',
            'senha_banco_tintometrico',
            'banco_tintometrico'
        ];
    }

    public function adicao(ConexaoEntity $entity)
    {
        return $this->insert($entity)->getGeneratedValue();
    }
    public function alterar(ConexaoEntity $entity)
    {

        $whereConexao = new Where();
        $whereConexao->equalTo('id', $entity->getIdConexao());

        return $this->update($entity, $whereConexao)->getAffectedRows();
    }

    public function excluir($id)
    {
        $whereConexao = new Where();
        $whereConexao->equalTo('id', $id);

        return $this->delete($whereConexao)->getAffectedRows();
    }

}
