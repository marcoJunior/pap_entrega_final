<?php

namespace ConexaoClienteMaxScalla\Mapper;

use Zend\Stdlib\Parameters;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use ConexaoClienteMaxScalla\Entity\Conexao as ConexaoEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use ConexaoClienteMaxScalla\Entity\ClientePlanoConexao as ClientePlanoConexao;
use ConexaoClienteMaxScalla\Entity\ConexaoClienteMaxScalla as ConexaoClienteMaxScallaEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexaoClienteMaxScalla extends APIGrid
{

    public $tableName = 'contrato_cliente';
    public $mapperName = 'ConexaoClienteMaxScalla\Mapper\Hydrator\ConexaoClienteMaxScalla';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '', str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function selecionarId($id)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $id);

        $returnDb = $this->select($select->where($where));

        $url = filter_input(INPUT_SERVER, 'REDIRECT_URL');
        if (!count(explode('selecionarContratoPorCliente', $url))) {
            return ConvertObject::convertObject($returnDb);
        }

        $retorno = $returnDb->current();
        if (count($returnDb) > 0) {
            $planos = [];
            array_push($planos, $retorno->getIdPlano());
            for ($i = 1; count($returnDb) > $i; $i++) {
                $returnDb->next();
                array_push($planos, $returnDb->current()->getIdPlano());
            }
            $retorno->setPlano($planos);
        }

        return $retorno;
    }

    public function selecionarContratoPorCliente($id)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $id);
        $where->equalTo('ativo', 1);

        return ConvertObject::convertObject($this->select($select->where($where)));
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereCodigo(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('id_cliente', $filtros->getCodigo());
        $this->setWhere($where);
        return $where;
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereClientes(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('id_cliente', $filtros->getClientes());
        $this->setWhere($where);
        return $where;
    }

    public function getColunas()
    {
        return [
            'id',
            'id_conexao',
            'id_plano',
            'id_cliente',
            'apelido',
            'dominio',
            'sub_dominio',
            'ip',
            'porta',
            'usuario_banco',
            'senha_banco',
            'banco',
            'ativo',
            'cliente',
            'plano',
//            'preco',
            'id_sistema',
            'sistema',
            'sistema_chave',
            //Tintometrico
            'ip_tintometrico',
            'porta_tintometrico',
            'usuario_banco_tintometrico',
            'senha_banco_tintometrico',
            'banco_tintometrico'
        ];
    }

    public function qtdTotalSistema(ConexaoClienteMaxScallaEntity $entity)
    {
        $select = $this->getSelect()
            ->columns(['count(1)']);

        $where = new Where();
        $where->equalTo('id_cliente', $entity->getId());

        return ConvertObject::convertObject($this->select($select->where($where)));
    }

    public function qtdTotalPlano(ConexaoClienteMaxScallaEntity $entity)
    {
        $select = $this->getSelect()
            ->columns(['plano' => new Expression('count(1)')]);

        $where = new Where();
        $where->equalTo('id_cliente', $entity->getId());
        return $this->select($select->where($where))->current()->getPlano();
    }

    public function valorTotalPlano(ConexaoClienteMaxScallaEntity $entity)
    {
        $select = $this->getSelect()
            ->columns(['preco' => new Expression('sum(preco)')]);

        $where = new Where();
        $where->equalTo('id_cliente', $entity->getId());
        return $this->select($select->where($where))->current()->getPreco();
    }

    public function adicao(ConexaoClienteMaxScallaEntity $entity)
    {
        $codigoConexao = $entity->getIdConexao();

//        if ($codigoConexao == 0) {
//            $this->inicializaConexao();
//            $conexao = new ConexaoEntity($entity->toArray());
//            $return[1] = $codigoConexao = $this->insert($conexao)->getGeneratedValue();
//        } else {
//            $this->inicializaConexao();
//            $whereConexao = new Where();
//            $whereConexao->equalTo('id', $codigoConexao);
//
//            $conexao = new ConexaoEntity($entity->toArray());
//            $return[1] = $this->update($conexao, $whereConexao)->getAffectedRows();
//        }

        $this->inicializaConexaoClientePlano();
        $clientePlanoConexao = new ClientePlanoConexao($entity->toArray());
        $clientePlanoConexao->setIdConexao((int) $codigoConexao);

        return $this->insert($clientePlanoConexao)->getAffectedRows();
    }

    public function alterar(ConexaoClienteMaxScallaEntity $entity)
    {

        $codigoConexao = $entity->getIdConexao();

//        var_dump($codigoConexao);die();

//        $this->inicializaConexao();
//        if ($codigoConexao == 0) {
//            $conexao = new ConexaoEntity($entity->toArray());
//            $conexao->setId(null);
//            $retorno[0] = $codigoConexao = $this->insert($conexao)->getGeneratedValue();
//        } else {
//            $whereConexao = new Where();
//            $whereConexao->equalTo('id', $codigoConexao);
//
//            $conexao = new ConexaoEntity($entity->toArray());
//            $retorno[0] = $this->update($conexao, $whereConexao);
//        }
//
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        $this->inicializaConexaoClientePlano();
        $clientePlanoConexao = new ClientePlanoConexao($entity->toArray());
        $clientePlanoConexao->setIdConexao($codigoConexao);
        $retorno[1] = $this->update($clientePlanoConexao, $where)->getAffectedRows();

        return $retorno;
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        $this->inicializaConexaoClientePlano();
        return $this->delete($where)->getAffectedRows();
    }

    public function validaExistenciaDeSistema(ConexaoClienteMaxScallaEntity $entidade)
    {

        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $entidade->getIdCliente());
        $where->in('sistema_chave', ['csb2b', 'csb2c', 'orcamentos']);

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaSistemaJaContratado(ConexaoClienteMaxScallaEntity $entidade)
    {

        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_cliente', $entidade->getIdCliente());
        $where->equalTo('id_sistema', $entidade->getIdSistema());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function inicializaConexao()
    {
        $this->inicializar('conexao', 'ConexaoClienteMaxScalla\Mapper\Hydrator\Conexao');

        $this->setEntityPrototype(new ConexaoEntity);
        $this->setHydrator(new Hydrator\Conexao);
    }

    public function inicializaConexaoClientePlano()
    {
        $this->inicializar('contrato', 'ConexaoClienteMaxScalla\Mapper\Hydrator\ClientePlanoConexao');

        $this->setEntityPrototype(new ClientePlanoConexao);
        $this->setHydrator(new Hydrator\ClientePlanoConexao);
    }

}
