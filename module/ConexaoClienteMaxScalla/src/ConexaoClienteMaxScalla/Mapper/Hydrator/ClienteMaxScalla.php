<?php

namespace ConexaoClienteMaxScalla\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ClienteMaxScalla extends Hydrator
{

    protected function getEntity()
    {
        return 'ConexaoClienteMaxScalla\Entity\ClienteMaxScalla';
    }

    public function getMap()
    {
        $arrayMap = [
            // 'razaoSocial' => 'razao_social'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ClienteMaxScalla();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
