<?php

namespace ConexaoClienteMaxScalla\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ClientePlanoConexao extends Hydrator
{

    protected function getEntity()
    {
        return 'ConexaoClienteMaxScalla\Entity\ClientePlanoConexao';
    }

    public function getMap()
    {
        return [];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'iterator',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ClientePlanoConexao();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
