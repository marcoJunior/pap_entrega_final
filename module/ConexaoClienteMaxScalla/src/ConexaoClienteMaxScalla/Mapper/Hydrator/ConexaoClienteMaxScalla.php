<?php

namespace ConexaoClienteMaxScalla\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexaoClienteMaxScalla extends Hydrator
{

    protected function getEntity()
    {
        return 'ConexaoClienteMaxScalla\Entity\ConexaoClienteMaxScalla';
    }

    public function getMap()
    {
        return [
            'idConexao' => 'id_conexao',
            'idPlano' => 'id_plano',
            'idCliente' => 'id_cliente',
            'idSistema' => 'id_sistema',
            'subDominio' => 'sub_dominio',
            'usuarioBanco' => 'usuario_banco',
            'senhaBanco' => 'senha_banco',
            'sistemaChave' => 'sistema_chave',
        ];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'plano',
            'preco',
            'cliente',
            'sistema',
            'sistema_chave',
            'iterator',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ConexaoClienteMaxScalla();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
