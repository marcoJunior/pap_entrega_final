<?php

namespace ConexaoClienteMaxScalla\Service;

use Zend\Stdlib\Parameters;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Zend\Db\Adapter\Exception\InvalidQueryException;
use Zend\Db\Adapter\Exception\ErrorException;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Conexao extends ManutencaoHelper
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        if ($get->get('paramData', null) !== null) {
            $filtros->setCodigo($get->get('paramData')['codigo']);
        }

        $retorno = $this->mapperConexao()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function excluir($id)
    {
        $mapper = $this->mapperConexao();
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $mapper->excluir($id);
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\Conexao
     */
    public function mapperConexao()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\Conexao');
    }

}
