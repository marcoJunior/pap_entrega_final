<?php

namespace ConexaoClienteMaxScalla\Service;

use Zend\Stdlib\Parameters;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\APIGridReturn as APIGridReturnEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use ConexaoClienteMaxScalla\Entity\ConexaoClienteMaxScalla as ConexaoClienteMaxScallaEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ConexaoClienteMaxScalla extends ManutencaoHelper
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        if (isset($get->get('paramData', null)['codigo'])) {
            $filtros->setCodigo($get->get('paramData')['codigo']);
        }

        if (isset($get->get('paramData', null)['cliente'])) {
            $filtros->setClientes($get->get('paramData')['cliente']);
        }

        $retorno = $this->mapperConexaoClienteMaxScalla()->selecionar($postTratado, $filtros, $get);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperConexaoClienteMaxScalla()->selecionarId($id);
    }

    public function selecionarContratoPorCliente($id)
    {
        return $this->mapperConexaoClienteMaxScalla()->selecionarContratoPorCliente($id);
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperConexaoClienteMaxScalla();
        $entidade = new ConexaoClienteMaxScallaEntity($formulario);

        $entidade->setSistemaChave(
            $this->getServiceSistemas()->selecionarId(
                (int) $entidade->getIdSistema()
            )->getChave()
        );

        $lojaVirtual = ['csb2b', 'csb2c', 'orcamentos'];
        if (in_array($entidade->getSistemaChave(), $lojaVirtual)) {
            if ($this->validaExistencia($entidade)) {
                return ['mensagem' => 'Só é possivel adicionar 1 dos 3 sistemas [csb2b, csb2c, orcamentos]!'];
            }
        }

        if ($this->validaSistemaJaContratado($entidade)) {
            return ['mensagem' => 'Só é possivel adicionar 1 plano por sistema!'];
        }

        return $mapper->adicao($entidade);
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperConexaoClienteMaxScalla();

        $entidade = new ConexaoClienteMaxScallaEntity();
        $entidade->exchangeArray($formulario);

        if ($this->validaSistemaJaContratado($entidade) && !((boolean) $post->get('alteracao'))) {
            return ['mensagem' => 'Só é possivel adicionar 1 plano por sistema!'];
        }

        return $mapper->alterar($entidade);
    }

    public function excluir($id)
    {
        $mapper = $this->mapperConexaoClienteMaxScalla();

        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $mapper->excluir($id);
    }

    public function qtdTotalSistema(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperConexaoClienteMaxScalla();
        $entidade = new ConexaoClienteMaxScallaEntity($formulario);

        return $mapper->qtdTotalSistema($entidade);
    }

    public function qtdTotalPlano(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        $mapper = $this->mapperConexaoClienteMaxScalla();
        $entidade = new ConexaoClienteMaxScallaEntity($formulario);

        return $mapper->qtdTotalPlano($entidade);
    }

    public function valorTotalPlano(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        $mapper = $this->mapperConexaoClienteMaxScalla();
        $entidade = new ConexaoClienteMaxScallaEntity($formulario);

        return $mapper->valorTotalPlano($entidade);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * Verifica se já existe empresa registrada
     * @return bool
     */
    public function validaExistencia(ConexaoClienteMaxScallaEntity $entidade)
    {
        $mapper = $this->mapperConexaoClienteMaxScalla();
        return $mapper->validaExistenciaDeSistema($entidade);
    }

    public function validaSistemaJaContratado(ConexaoClienteMaxScallaEntity $entidade)
    {
        $mapper = $this->mapperConexaoClienteMaxScalla();
        return $mapper->validaSistemaJaContratado($entidade);
    }

}
