<?php

namespace ConexaoClienteMaxScalla\Service;

use APIGrid\Service\APIGrid;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ManutencaoHelper extends APIGrid
{

    /**
     * @return \Sistemas\Service\Sistema
     */
    public function getServiceSistemas()
    {
        return $this->getServiceManager()->get(\Sistemas\Service\Sistema::class);
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\Plano
     */
    public function mapperPlano()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\Plano');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\RecursoDescricao
     */
    public function mapperRecursoDescricao()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\RecursoDescricao');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\Recurso
     */
    public function mapperRecurso()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\Recurso');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\ClienteMaxScalla
     */
    public function mapperClienteMaxScalla()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\ClienteMaxScalla');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\TempoExperienciaCliente
     */
    public function mapperTempoExperienciaCliente()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\TempoExperienciaCliente');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\Sistema
     */
    public function mapperSistema()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\Sistema');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\EnderecoClienteMaxScalla
     */
    public function mapperEnderecoClienteMaxScalla()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\EnderecoClienteMaxScalla');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\ContatoClienteMaxScalla
     */
    public function mapperContatoClienteMaxScalla()
    {
        return $this->getServiceManager()->get('ConexaoClienteMaxScalla\Mapper\ContatoClienteMaxScalla');
    }

    /**
     * @return Cadastros\Mapper\EmpresasMax
     */
    public function mapperEmpresasMax()
    {
        return $this->getServiceManager()->get('Cadastros\Mapper\EmpresasMax');
    }

    /**
     * @return ConexaoClienteMaxScalla\Mapper\ConexaoClienteMaxScalla
     */
    public function mapperConexaoClienteMaxScalla()
    {
        return $this->getServiceManager()->get(\ConexaoClienteMaxScalla\Mapper\ConexaoClienteMaxScalla::class);
    }

}
