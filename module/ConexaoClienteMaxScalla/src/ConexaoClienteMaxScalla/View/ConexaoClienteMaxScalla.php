<?php

namespace ConexaoClienteMaxScalla\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class ConexaoClienteMaxScalla extends ViewModel
{

    protected $template = 'conexao_cliente_maxscalla/get-list';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [
        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
            '../APIGridGitLab/public/js/api_grid_v2.js',
            '../app/manutencao/formulario.js',
            '../app/manutencao/conexao_cliente_maxscalla/index.js',
            '../app/manutencao/conexao_cliente_maxscalla/indexPlanos.js',
            '../app/manutencao/conexao_cliente_maxscalla/indexConexao.js',
            '../app/manutencao/conexao_cliente_maxscalla/indexEmpresas.js',
        ];
    }

    public function grid()
    {
        $view = new ViewModel();
        $view->setTemplate("conexao_cliente_maxscalla/grid");
        $view->setCaptureTo("grid");

        $this->addChild($view);
    }

    public function formulario()
    {
        $view = new ViewModel();
        $view->setTemplate("conexao_cliente_maxscalla/formulario");
        $view->setCaptureTo("formulario");

        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel();
        $view->setTemplate("conexao_cliente_maxscalla/modal");
        $view->setCaptureTo("modal");

        $this->addChild($view);
    }

}
