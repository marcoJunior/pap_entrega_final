<?php

namespace Documentacoes;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                //NOVOS CADASTROS
                'Documentacoes\Service\Documentacao' => 'Documentacoes\Service\Documentacao',
                'Documentacoes\Service\DocumentacaoDescricao' => 'Documentacoes\Service\DocumentacaoDescricao',
            ),
            'factories' => array(
                'Documentacoes\Mapper\Documentacao' =>
                function ($sm) {
                    $mapper = new Mapper\Documentacao();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                            ->setEntityPrototype(new Entity\Documentacao())
                            ->setHydrator(new Mapper\Hydrator\Documentacao());
                    return $mapper;
                },
                'Documentacoes\Mapper\DocumentacaoDescricao' =>
                function ($sm) {
                    $mapper = new Mapper\DocumentacaoDescricao();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                            ->setEntityPrototype(new Entity\DocumentacaoDescricao())
                            ->setHydrator(new Mapper\Hydrator\DocumentacaoDescricao());
                    return $mapper;
                },
            ),
        );
    }

}
