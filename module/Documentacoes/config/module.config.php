<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'documentacao' => 'Documentacoes\Controller\DocumentacaoController',
            'documentacao_descricao' => 'Documentacoes\Controller\DocumentacaoDescricaoController',
        ),
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'documentacao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/documentacoes[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'documentacao',
                    )
                ),
            ),
            'documentacao/descricao' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/documentacao_descricao[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'documentacao_descricao',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'documentacoes/documentacao-descricao/get-list' => __DIR__ . '/../view/index.phtml',
            'documentacao/grid' => __DIR__ . '/../view/documentacao/grid.phtml',
            'documentacao/formulario' => __DIR__ . '/../view/documentacao/formulario.phtml',
            'documentacao/modal' => __DIR__ . '/../view/documentacao/modal.phtml',
        ),
        'template_path_stack' => array(
            'documentacao' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
