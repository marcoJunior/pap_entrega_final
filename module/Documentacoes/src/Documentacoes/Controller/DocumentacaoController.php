<?php

namespace Documentacoes\Controller;

use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use APIGrid\Controller\APIGridController;
use Documentacoes\View\Documentacao;

class DocumentacaoController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getServiceLocator()->get('ViewRenderer');
            $viewClientes = new Documentacao($phpRenderer);
            $viewClientes->setVariable('titulo', 'Documentações');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        $service = $this->serviceDocumentacoes();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceDocumentacoes();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceDocumentacoes();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceDocumentacoes();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceDocumentacoes();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \Documentacoes\Service\Documentacoes
     */
    public function serviceDocumentacoes()
    {
        return $this->getServiceLocator()->get('Documentacoes\Service\Documentacao');
    }

}
