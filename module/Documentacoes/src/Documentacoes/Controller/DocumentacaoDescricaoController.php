<?php

namespace Documentacoes\Controller;

use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;
use APIGrid\Controller\APIGridController;

class DocumentacaoDescricaoController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();
        $service = $this->serviceDocumentacaoDescricao();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceDocumentacaoDescricao();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceDocumentacaoDescricao();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceDocumentacaoDescricao();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceDocumentacaoDescricao();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \Documentacoes\Service\DocumentacaoDescricao
     */
    public function serviceDocumentacaoDescricao()
    {
        return $this->getServiceLocator()->get('Documentacoes\Service\DocumentacaoDescricao');
    }

}
