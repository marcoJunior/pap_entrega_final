<?php

namespace Documentacoes\Entity;

use APIHelper\Entity\AbstractEntity;

class Documentacao extends AbstractEntity
{

    protected $id;
    protected $descricao;
    protected $recurso;
    protected $sistema;
    protected $idRecurso;
    protected $idSistema;

    function getId()
    {
        return $this->id;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getRecurso()
    {
        return $this->recurso;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function getIdRecurso()
    {
        return $this->idRecurso;
    }

    function getIdSistema()
    {
        return $this->idSistema;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setRecurso($recurso)
    {
        $this->recurso = $recurso;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function setIdRecurso($idRecurso)
    {
        $this->idRecurso = (int) $idRecurso;
    }

    function setIdSistema($idSistema)
    {
        $this->idSistema = (int) $idSistema;
    }

}
