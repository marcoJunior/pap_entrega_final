<?php

namespace Documentacoes\Entity;

use APIHelper\Entity\AbstractEntity;

class DocumentacaoDescricao extends AbstractEntity
{

    protected $id;
    protected $funcionalidade;
    protected $descricao;
    protected $idDocumentacao = 0;
    protected $tipo = '';
    protected $idTipoDocumentacao = 0;

    function getId()
    {
        return $this->id;
    }

    function getFuncionalidade()
    {
        return $this->funcionalidade;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setFuncionalidade($funcionalidade)
    {
        $this->funcionalidade = $funcionalidade;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    function getIdDocumentacao()
    {
        return $this->idDocumentacao;
    }

    function getIdTipoDocumentacao()
    {
        return $this->idTipoDocumentacao;
    }

    function setIdDocumentacao($idDocumentacao)
    {
        $this->idDocumentacao = (int) $idDocumentacao;
    }

    function setIdTipoDocumentacao($idTipoDocumentacao)
    {
        $this->idTipoDocumentacao = (int) $idTipoDocumentacao;
    }

}
