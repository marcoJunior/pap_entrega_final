<?php

namespace Documentacoes\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Documentacoes\Entity\Documentacao as DocumentacaoEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Documentacao extends APIGrid
{

    public $tableName = 'documentacoes';
    public $mapperName = 'Documentacoes\Mapper\Hydrator\Documentacao';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $this->setJoin(new APIGridJoinEntity(
                'sistema', 'sistema.id = documentacoes.id_sistema',
                ['sistema' => 'nome'], 'left outer'
        ));

        $this->setJoin(new APIGridJoinEntity(
                'recurso', 'recurso.id = documentacoes.id_recurso',
                ['recurso' => 'descricao'], 'left outer'
        ));


        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'descricao',
            'id_recurso',
            'id_sistema',
        ];
    }

    public function adicao(DocumentacaoEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(DocumentacaoEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
