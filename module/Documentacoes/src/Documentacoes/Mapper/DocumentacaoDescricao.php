<?php

namespace Documentacoes\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use Documentacoes\Entity\DocumentacaoDescricao as DocumentacaoDescricaoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class DocumentacaoDescricao extends APIGrid
{

    public $tableName = 'descricao_documentacao';
    public $mapperName = 'Documentacoes\Mapper\Hydrator\DocumentacaoDescricao';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $this->setJoin(new APIGridJoinEntity(
                'tipo_documentacao',
                'tipo_documentacao.id = descricao_documentacao.id_tipo_documentacao',
                ['tipo' => 'descricao'], 'left outer'
        ));


        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '',
                    str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }


        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'funcionalidade',
            'descricao',
            'id_documentacao',
            'id_tipo_documentacao',
        ];
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código Documentacoes
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereCodigo(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->equalTo('id_documentacao', $filtros->getCodigo());
        $this->setWhere($where);
        return $where;
    }

    public function adicao(DocumentacaoDescricaoEntity $entity)
    {
        $entity->setId(null);
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(DocumentacaoDescricaoEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
