<?php

namespace Documentacoes\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Documentacao extends Hydrator
{

    protected function getEntity()
    {
        return 'Documentacoes\Entity\Documentacao';
    }

    public function getMap()
    {
        return [
//            'palavraChave' => 'palavra_chave'
        ];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'sistema',
            'recurso',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Documentacao();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
