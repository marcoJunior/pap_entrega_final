<?php

namespace Documentacoes\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class DocumentacaoDescricao extends Hydrator
{

    protected function getEntity()
    {
        return 'Documentacoes\Entity\DocumentacaoDescricao';
    }

    public function getMap()
    {
        return [
//            'palavraChave' => 'palavra_chave'
        ];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'tipo',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new DocumentacaoDescricao();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
