<?php

namespace Documentacoes\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Documentacoes\Entity\DocumentacaoDescricao as DocumentacaoDescricaoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class DocumentacaoDescricao extends APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        if ($get->get('paramData', null) !== null) {
            $filtros->setCodigo($get->get('paramData')['codigo']);
        }
        
        $retorno = $this->mapperDocumentacoesDescricao()->selecionar($postTratado,
                $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperDocumentacoesDescricao();
        $entidade = new DocumentacaoDescricaoEntity($formulario);

        return $mapper->adicao($entidade);
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperDocumentacoesDescricao();
        $entidade = new DocumentacaoDescricaoEntity($formulario);

        return $mapper->alterar($entidade);
    }

    public function excluir($id)
    {
        $mapper = $this->mapperDocumentacoesDescricao();

        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $mapper->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return Documentacoes\Mapper\DocumentacaoDescricao
     */
    public function mapperDocumentacoesDescricao()
    {
        return $this->getServiceManager()->get('Documentacoes\Mapper\DocumentacaoDescricao');
    }

}
