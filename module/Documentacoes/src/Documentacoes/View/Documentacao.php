<?php

namespace Documentacoes\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class Documentacao extends ViewModel
{

    protected $template = 'documentacao/get-list';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [
        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
            'http://cdn.ckeditor.com/4.6.2/full-all/ckeditor.js',
            '../APIGridGitLab/public/js/api_grid_v2.js',
            '../app/manutencao/documentacao/index.js',
            '../app/manutencao/documentacao/indexSistema.js',
            '../app/manutencao/documentacao/indexRecurso.js',
            '../app/manutencao/documentacao/indexDocumentacoesDescricao.js',
        ];
    }

    public function grid()
    {
        $view = new ViewModel();
        $view->setTemplate("documentacao/grid");
        $view->setCaptureTo("grid");

        $this->addChild($view);
    }

    public function formulario()
    {
        $view = new ViewModel();
        $view->setTemplate("documentacao/formulario");
        $view->setCaptureTo("formulario");

        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel();
        $view->setTemplate("documentacao/modal");
        $view->setCaptureTo("modal");

        $this->addChild($view);
    }

}
