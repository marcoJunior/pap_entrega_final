<?php

namespace Home;

use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\AbstractFactoryInterface;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Clientes::class => function ($sm) {
                    $service = new Service\Clientes();
                    return $service->setServiceManager($sm);
                },
                Service\Acessos::class => function ($sm) {
                    $service = new Service\Acessos();
                    return $service->setServiceManager($sm);
                },
                Service\ControlShopV2::class => function ($sm) {
                    $service = new Service\ControlShopV2();
                    return $service->setServiceManager($sm);
                },
                ////////
                \Home\Mapper\ControlShopV2::class =>
                function ($sm) {
                    $mapper = new Mapper\ControlShopV2();
                    $dbConfig = $sm->get('Config');
                    $dbAdapter = new Adapter($dbConfig['localweb']);
                    $mapper->setDbAdapter($dbAdapter)
                        ->setEntityPrototype(new Entity\ControlShopV2())
                        ->setHydrator(new Mapper\Hydrator\ControlShopV2());
                    return $mapper;
                },
                'MapperClientes' =>
                function ($sm) {
                    $mapper = new Mapper\Clientes();
                    $dbConfig = $sm->get('Config');
                    $dbAdapter = new Adapter($dbConfig['localweb']);
                    $mapper->setDbAdapter($dbAdapter)
                        ->setEntityPrototype(new Entity\ClientesComplemento())
                        ->setHydrator(new Mapper\Hydrator\Clientes());
                    return $mapper;
                },
                \Home\Mapper\Acessos::class =>
                function ($sm) {
                    $mapper = new Mapper\Acessos();
                    $dbConfig = $sm->get('Config');
                    $dbAdapter = new Adapter($dbConfig['secnet']);
                    $mapper->setDbAdapter($dbAdapter)
                        ->setEntityPrototype(new Entity\Acessos())
                        ->setHydrator(new Mapper\Hydrator\Acessos());
                    return $mapper;
                },
            ),
        );
    }

}
