<?php

return [
    # definir e gerenciar controllers
    'controllers' => [
        'factories' => [
            \Home\Controller\HomeController::class => function ($sl) {
                $controller = new \Home\Controller\HomeController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            \Home\Controller\ControlShopV2Controller::class => function ($sl) {
                $controller = new \Home\Controller\ControlShopV2Controller();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
//            Controller\HomeController::class => function ($sl) {
//                $filtros = $sl->getServiceLocator()->get(\APIFiltro\Service\Init::class];
//
//                $controller = new Controller\HomeController(];
//
//                die('dfslfdkjfdsjlkfsdjlkfdsjkdsfljk'];
//                return $controller;
//            },
//            'controlshopv2' => 'Home\Controller\ControlShopV2Controller'
        ],
    ],
    # definir e gerenciar rotas
    'router' => [
        'routes' => [
            /**
             * SERVIÇOS
             */
            'controlshopv2' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/controlshopv2[/:id]',
                    'constraints' => [
                        'id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => \Home\Controller\ControlShopV2Controller::class,
                    ]
                ],
            ],
            'dashboard' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/dashboard',
                    'defaults' => [
                        'controller' => \Home\Controller\HomeController::class,
                    ]
                ],
            ],
            'dashboard/graficos' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/graficos/dashboard',
                    'defaults' => [
                        'controller' => \Home\Controller\HomeController::class,
                        'action' => 'Graficos',
                    ],
                ],
            ],
            'logs/selecionar' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/logs/selecionar',
                    'defaults' => [
                        'controller' => \Home\Controller\HomeController::class,
                        'action' => 'GridLog',
                    ],
                ],
            ],
        ],
    ],
    # definir e gerenciar servicos
    'service_manager' => [
        'factories' => [
        #'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ],
    ],
    # definir e gerenciar layouts, erros, exceptions, doctype base
    'view_manager' => [
        'template_map' => [
            //CONTROLSHOPV2
            'controlshopv2/grid' => __DIR__ . '/../view/controlshopv2/grid.phtml',
            'controlshopv2/formulario' => __DIR__ . '/../view/controlshopv2/formulario.phtml',
            'controlshopv2/modal' => __DIR__ . '/../view/controlshopv2/modal.phtml',
        ],
        'template_path_stack' => [
            'controlshopv2' => __DIR__ . '/../view/controlshopv2',
            'home' => __DIR__ . '/../view/home',
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
