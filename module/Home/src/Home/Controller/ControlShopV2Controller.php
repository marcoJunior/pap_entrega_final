<?php

namespace Home\Controller;

use Home\View\ControlShopV2;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use APIGrid\Controller\APIGridController;

class ControlShopV2Controller extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();


        $service = $this->serviceControlShopV2();
        if ($get->get('contarClientes', false)) {
            return new JsonModel((array) $service->contarClientes());
        } else if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewClientes = new ControlShopV2($phpRenderer);
            $viewClientes->setVariable('titulo', 'Cadastro de Clientes (LocalWeb)');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        return new JsonModel((array) $service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceControlShopV2();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    /**
     * @return \Home\Service\ControlShopV2
     */
    public function serviceControlShopV2()
    {
        return $this->getService()->get(\Home\Service\ControlShopV2::class);
    }

}
