<?php

namespace Home\Controller;

use Home\View\Dashboard;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use APIGrid\Controller\APIGridController;

class HomeController extends APIGridController
{

    public function getList()
    {
        $initFiltro = $this->getService()->get(\APIFiltro\Service\Init::class);
        $filtros = $initFiltro->loadFiltros([['intervaloData' => '']]);

        $phpRenderer = $this->getService()->get('ViewRenderer');
        $viewClientes = new Dashboard($phpRenderer);
        $viewClientes->setVariable('titulo', 'DashBoard');
        $viewClientes->setVariable('filtros', $filtros);

        $app = new Application($phpRenderer);
        $app->addChild($viewClientes);

        return $app;
    }

    public function SelecionarAction()
    {
        $service = $this->getService()->get(Service\Clientes::class);
        $resultado = $service->selecionarGrid($_POST);
        return new JsonModel((array) $resultado);
    }

    public function GraficosAction()
    {
        $requisicao = $this->getRequest();
        if (!$requisicao->isPost()) {
            return;
        }

        $service = $this->getService()->get(
            $requisicao->getPost()->get('service')
        );
        $resultado = $service->set($_POST);

        return new JsonModel((array) $resultado);
    }

    public function GridLogAction()
    {
        $service = $this->getService()->get(Service\Acessos::class);
        $resultado = $service->selecionarGrid($_POST);
        return new JsonModel((array) $resultado);
    }

}
