<?php

namespace Home\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Acessos
{

    protected $data;
    protected $acessos;
    protected $ip;
    protected $usuario;
    protected $sistema;
    protected $apelido;
    protected $dataDoLog;
    
    function getUsuario()
    {
        return $this->usuario;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function getApelido()
    {
        return $this->apelido;
    }

    function getDataDoLog()
    {
        return $this->dataDoLog;
    }

    function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    function setDataDoLog($dataDoLog)
    {
        $this->dataDoLog = $dataDoLog;
    }
    
    function getIp()
    {
        return $this->ip;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
    }

    function getData()
    {
        return $this->data;
    }

    function getAcessos()
    {
        return $this->acessos;
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function setAcessos($acessos)
    {
        $this->acessos = $acessos;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
