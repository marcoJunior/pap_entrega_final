<?php

namespace Home\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class AcessosGrid
{

    protected $rank;
    protected $apelido;
    protected $acessos;

    function getRank()
    {
        return $this->rank;
    }

    function getApelido()
    {
        return $this->apelido;
    }

    function getAcessos()
    {
        return $this->acessos;
    }

    function setRank($rank)
    {
        $this->rank = $rank;
    }

    function setApelido($apelido)
    {
        $this->apelido = $apelido;
    }

    function setAcessos($acessos)
    {
        $this->acessos = $acessos;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
