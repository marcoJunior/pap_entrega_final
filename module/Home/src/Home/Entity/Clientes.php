<?php

namespace Home\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Clientes
{

    protected $serie;
    protected $versaonfe;
    protected $versaocs;
    protected $cnpj;
    protected $cliente;
    protected $ultimaliberacao;
    protected $ativacao;

    function getAtivacao()
    {
        return $this->ativacao;
    }

    function setAtivacao($ativacao)
    {
        $this->ativacao = $ativacao;
    }

    function getSerie()
    {
        return $this->serie;
    }

    function getVersaonfe()
    {
        return $this->versaonfe;
    }

    function getVersaocs()
    {
        return $this->versaocs;
    }

    function getCnpj()
    {
        return $this->cnpj;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function getUltimaliberacao()
    {
        return $this->ultimaliberacao;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function setVersaonfe($versaonfe)
    {
        $this->versaonfe = $versaonfe;
    }

    function setVersaocs($versaocs)
    {
        $this->versaocs = $versaocs;
    }

    function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function setUltimaliberacao($ultimaliberacao)
    {
        $this->ultimaliberacao = $ultimaliberacao;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
