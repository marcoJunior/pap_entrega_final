<?php

namespace Home\Entity;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class ClientesComplemento extends \Home\Entity\Clientes
{

    protected $inativos;
    protected $ativos;
    protected $todos;

    function getTodos()
    {
        return $this->todos;
    }

    function setTodos($todos)
    {
        $this->todos = $todos;
    }

    function getInativos()
    {
        return $this->inativos;
    }

    function getAtivos()
    {
        return $this->ativos;
    }

    function setInativos($inativos)
    {
        $this->inativos = $inativos;
    }

    function setAtivos($ativos)
    {
        $this->ativos = $ativos;
    }

    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
