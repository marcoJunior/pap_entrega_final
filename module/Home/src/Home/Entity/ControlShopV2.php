<?php

namespace Home\Entity;

use APIHelper\Entity\AbstractEntity;

class ControlShopV2 extends AbstractEntity
{

    protected $serie;
    protected $versaoNfe;
    protected $versaoCs;
    protected $cnpj;
    protected $cliente;
    protected $ultimaLiberacao;
    protected $ativacao;
    protected $todos;
    protected $inativos;
    protected $ativos;

    function getSerie()
    {
        return $this->serie;
    }

    function getVersaoNfe()
    {
        return $this->versaoNfe;
    }

    function getVersaoCs()
    {
        return $this->versaoCs;
    }

    function getCnpj()
    {
        return $this->cnpj;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function getUltimaLiberacao()
    {
        return $this->ultimaLiberacao;
    }

    function getAtivacao()
    {
        return $this->ativacao;
    }

    function setSerie($serie)
    {
        $this->serie = $serie;
    }

    function setVersaoNfe($versaoNfe)
    {
        $this->versaoNfe = $versaoNfe;
    }

    function setVersaoCs($versaoCs)
    {
        $this->versaoCs = $versaoCs;
    }

    function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function setUltimaLiberacao($ultimaLiberacao)
    {
        $this->ultimaLiberacao = $ultimaLiberacao;
    }

    function setAtivacao($ativacao)
    {
        $this->ativacao = $ativacao;
    }

    function getTodos()
    {
        return $this->todos;
    }

    function getInativos()
    {
        return $this->inativos;
    }

    function getAtivos()
    {
        return $this->ativos;
    }

    function setTodos($todos)
    {
        $this->todos = $todos;
    }

    function setInativos($inativos)
    {
        $this->inativos = $inativos;
    }

    function setAtivos($ativos)
    {
        $this->ativos = $ativos;
    }

}
