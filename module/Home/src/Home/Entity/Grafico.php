<?php

namespace Home\Entity;

/**
 * @author Leandro Machado
 */
class Grafico
{

    public $id;
    public $titulo;
    public $tipo;
    public $grid;
    
    function getGrid()
    {
        return $this->grid;
    }

    function setGrid($grid)
    {
        $this->grid = $grid;
    }
    
    function getId()
    {
        return $this->id;
    }

    function getTitulo()
    {
        return $this->titulo;
    }

    function getTipo()
    {
        return $this->tipo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    function setTipo($tipo)
    {
        $this->tipo = $tipo;
    }

    
    public function exchangeArray(array $data)
    {
        foreach ($data as $atributo => $valor) {
            if (property_exists($this, $atributo)) {
                $this->{'set' . ucfirst($atributo)}($valor);
            }
        }
    }

}
