<?php

namespace Home\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use ZfcBase\Mapper\AbstractDbMapper;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Acessos extends AbstractDbMapper
{

    /**
     * @return obj $retorno dados selecionados
     */
    public function get($parametros)
    {
        $where = new Where;

        if ($parametros['where']['valor'] !== 'visao.geral') {
            $where->equalTo($parametros['where']['coluna'],
                    $parametros['where']['valor']);
        } else {
            $where->in('cfisislog',
                    ['app.csgestor.com.br', 'app.csb2b.com.br', 'rigelnatural.com.br',
                'forcadevendas.csgestor.com.br']);
        }
        $where->notEqualTo('cfiniplog', '191.255.244.170');

        if (isset($parametros["between"])) {
            $datas = $this->traduzirParametroData($parametros["between"]);

            $de = $datas['de'];
            $ate = $datas['ate'];
            $where->between('cfidtalog', "'$de'", "'$ate'");
        }

        $select = $this->getSelect()->from("mxlogcfi");
        $select->columns(["dte" => new Expression("to_char(cfidtalog, 'YYYY-MM-DD')"),
                    'contador' => new Expression("count(1)")])
                ->where($where)
                ->group(['dte'])
                ->order("dte ASC");

        return $this->select($select);
    }

    public function traduzirParametroData($datas)
    {
        $retorno = [];
        $de = explode('/', $datas['de']);
        $ate = explode('/', $datas['ate']);

        $retorno['de'] = $de[2] . '-' . $de[1] . '-' . $de[0];
        $retorno['ate'] = $ate[2] . '-' . $ate[1] . '-' . $ate[0];
        return $retorno;
    }

    /**
     * @return obj $retorno dados selecionados
     */
    public function grid($parametros)
    {
        $this->setEntityPrototype(new \Home\Entity\AcessosGrid());
        $this->setHydrator(new \Home\Mapper\Hydrator\AcessosGrid());

        $where = new Where;
        if (isset($parametros['pesquisa']) && is_string($parametros['pesquisa'])) {
            $parametros = ['pesquisa' => [$parametros["pesquisa"]], 'between' => $parametros["between"]];
        }
        $where->in('cfisislog', $parametros['pesquisa']);

        $where->notEqualTo('cfiniplog', '191.255.244.170');

        if (isset($parametros["between"])) {
            $datas = $this->traduzirParametroData($parametros["between"]);

            $de = $datas['de'];
            $ate = $datas['ate'];
            $where->between('cfidtalog', "'$de'", "'$ate'");
        }

        $select = $this->getSelect()->from("mxlogcfi");
        $select->columns(["colocacao" => new Expression("row_number() OVER (order by count(1) DESC) "),
            'cfiapelog', 'qtd' => new Expression("count(1)")]);
        $select->where($where);

        $select->group("cfiapelog");
        $select->order("qtd DESC");

        return $this->select($select);
    }

    /**
     * Selecionando produtos
     * @param Where $where
     * @return obj $retorno dados selecionados
     */
    public function selecionar(Where $where, $config)
    {
        $coluns = array(
            'usuario' => new Expression("cfiusulgn"),
            'cfiniplog',
            'cfisislog',
            'cfiapelog',
            'cfidtalg2' => new Expression("to_char(cfidtalog, 'dd-mm-yyyy HH24:MI:SS')"),
                //'cfidtalg2' => new Expression("cfidtalog"),
        );

        $select = $this->getSelect()->from("mxlogcfi");
        $select->columns($coluns);
        $select->where($where);

        $select->where->notEqualTo('cfiniplog', '191.255.244.170');
        $select->where->addPredicates("cfisislog NOT SIMILAR TO '%(janley|vinicius|leandro)%'");

        $select->join('mxlgncfi', 'cficodlgn = cfilgnlog', ['cfiusulgn'], 'left');

        if (isset($config['order']) && !empty($config['order'])) {
            if ($config['order']['coluna'] === 'cfidtalg2') {
                $select->order('cfidtalog ' . $config['order']['valor']);
            } else {
                $select->order($config['order']['coluna'] . ' ' . $config['order']['valor']);
            }
        }
        if (isset($config['offset']) && !empty($config['offset'])) {
            $select->offset($config['offset']);
        }
        if (isset($config['limit']) && !empty($config['limit'])) {
            $select->limit($config['limit']);
        }
        //var_dump($select->getSqlString());die();
        return $this->select($select);
    }

}
