<?php

namespace Home\Mapper;

use ZfcBase\Mapper\AbstractDbMapper;
use Zend\Db\Sql\Where;
//use Home\Entity\Clientes as EntidadeClientes;
use Zend\Db\Sql\Expression;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Clientes extends AbstractDbMapper
{

    /**
     * Selecionando produtos
     * @param Where $where
     * @return obj $retorno dados selecionados 
     */
    public function selecionar(Where $where, $config)
    {
        $selectCont = $this->getSelect()->from("mxvernfe");
        $selectCont->columns(array("total" => new Expression('count(*)')));
        $selectCont->where($where);

        $coluns = array(
            'cfiserver',
            'cfinumver',
            'cfincsver',
            'cficgcver',
            'cfinclver',
            'cfilibver',
            'cfidliver' => new Expression("to_char(cfidliver, 'dd-mm-yyyy')"),
            'totalTable' => new Expression('?', array($selectCont)),
        );

        $select = $this->getSelect()->from("mxvernfe");
        $select->columns($coluns);
        $select->where($where);

        if (isset($config['order']) && !empty($config['order'])) {
            $select->order($config['order']['coluna'] . ' ' . $config['order']['valor']);
        }
        if (isset($config['offset']) && !empty($config['offset'])) {
            $select->offset($config['offset']);
        }
        if (isset($config['limit']) && !empty($config['limit'])) {
            $select->limit($config['limit']);
        }

        return $this->select($select);
    }

    public function contarClientes()
    {
        $selectTodos = $this->getSelect()->from("mxvernfe");
        $selectTodos->columns(array("todos" => new Expression('count(cfiserver)')));

        $selectInativos = $this->getSelect()->from("mxvernfe");
        $selectInativos->columns(array("inativos" => new Expression('count(cfiserver)')));
        $selectInativos->where->equalTo('cfilibver', 'N');

        $selectAtivos = $this->getSelect()->from("mxvernfe");
        $selectAtivos->columns(array("ativos" => new Expression('count(cfiserver)')));
        $selectAtivos->where->equalTo('cfilibver', 'S');


        $colunas = array(
            'todos' => new Expression('?', array($selectTodos)),
            'inativos' => new Expression('?', array($selectInativos)),
            'ativos' => new Expression('?', array($selectAtivos))
        );

        $select = $this->getSelect()->from("mxvernfe");
        $select->columns($colunas);
        $select->group(['inativos', 'ativos', 'todos']);

        return $this->select($select);
    }

}
