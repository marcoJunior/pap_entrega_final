<?php

namespace Home\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ControlShopV2 extends APIGrid
{

    public $tableName = 'mxvernfe';
    public $mapperName = 'Home\Mapper\Hydrator\ControlShopV2';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function selecionarId($id)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('cfiserver', $id);

        return $this->select($select->where($where))->current();
    }

    public function contarClientes()
    {
        $this->inicializar($this->tableName,
                'Home\Mapper\Hydrator\ControlShopV2');

        $selectTodos = $this->getSelect();
        $selectTodos->columns(array("todos" => new Expression('count(cfiserver)')));

        $selectInativos = $this->getSelect();
        $selectInativos->columns(array("inativos" => new Expression('count(cfiserver)')));
        $selectInativos->where->equalTo('cfilibver', 'N');

        $selectAtivos = $this->getSelect();
        $selectAtivos->columns(array("ativos" => new Expression('count(cfiserver)')));
        $selectAtivos->where->equalTo('cfilibver', 'S');


        $colunas = array(
            'todos' => new Expression('?', array($selectTodos)),
            'inativos' => new Expression('?', array($selectInativos)),
            'ativos' => new Expression('?', array($selectAtivos))
        );

        $select = $this->getSelect();
        $select->columns($colunas);
        $select->group(['inativos', 'ativos', 'todos']);

        return $this->select($select)->current();
    }

    public function getColunas()
    {

        return [
            'cfiserver',
            'cfinumver',
            'cfincsver',
            'cficgcver',
            'cfinclver',
            'cfilibver',
            'cfidliver' => new Expression("to_char(cfidliver, 'dd/mm/yyyy')")
        ];
    }

}
