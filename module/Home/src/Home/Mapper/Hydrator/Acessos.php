<?php

namespace Home\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Acessos extends Hydrator
{

    protected function getEntity()
    {
        return 'Home\Entity\Acessos';
    }

    public function getMap()
    {
        return [
            'data' => 'dte',
            'acessos' => 'contador',
            'ip' => 'cfiniplog',
            'sistema' => 'cfisislog',
            'apelido' => 'cfiapelog',
            'dataDoLog' => 'cfidtalg2',
            'usuario' => 'cfiusulgn'
        ];
    }

}
