<?php

namespace Home\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class AcessosGrid extends Hydrator
{

    protected function getEntity()
    {
        return 'Home\Entity\AcessosGrid';
    }

    public function getMap()
    {
        return [
            'rank' => 'colocacao',
            'apelido' => 'cfiapelog',
            'acessos' => 'qtd',
        ];
    }

}
