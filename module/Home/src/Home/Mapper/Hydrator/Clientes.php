<?php

namespace Home\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Leandro Machado <leandro@maxscalla.com.br>
 */
class Clientes extends Hydrator
{

    protected function getEntity()
    {
        return 'Home\Entity\Clientes';
    }

    public function getMap()
    {
        return [
            'serie' => 'cfiserver',
            'versaonfe' => 'cfinumver',
            'versaocs' => 'cfincsver',
            'cnpj' => 'cficgcver',
            'cliente' => 'cfinclver',
            'ultimaliberacao' => 'cfidliver',
            'ativacao' => 'cfilibver'
        ];
    }

    public function getTemporary()
    {
        return [
            'ativos',
            'inativos',
            'todos'
        ];
    }

}
