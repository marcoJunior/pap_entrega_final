<?php

namespace Home\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ControlShopV2 extends Hydrator
{

    protected function getEntity()
    {
        return 'Home\Entity\ControlShopV2';
    }

    public function getMap()
    {
        $arrayMap = [
            'serie' => 'cfiserver',
            'versaoNfe' => 'cfinumver',
            'versaoCs' => 'cfincsver',
            'cnpj' => 'cficgcver',
            'cliente' => 'cfinclver',
            'ultimaLiberacao' => 'cfidliver',
            'ativacao' => 'cfilibver'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'todos',
            'inativos',
            'ativos'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new ControlShopV2();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
