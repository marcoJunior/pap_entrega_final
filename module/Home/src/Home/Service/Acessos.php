<?php

namespace Home\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface,
    Zend\ServiceManager\ServiceManager,
    APISql\Service\ConvertObject,
    Charts\Entity\Options,
    Home\Entity\Grafico;

class Acessos implements DashboardInterface //ServiceManagerAwareInterface,
{

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @return getServiceManagerf
     */
    public function getInstance($instance)
    {
        return $this->getServiceManager()->get($instance);
    }

    /**
     * @return array $retorno Montagem de gráfico
     */
    public function set($parametros)
    {
        $retorno = [];
        $retorno['dados'] = $this->get($parametros);
        $retorno['options'] = $this->setOptions();
        $retorno['grafico'] = $this->setGrafico();

        $grid = ['pesquisa' => $parametros['where']['valor']];
        if (isset($parametros["between"])) {
            $grid['between'] = $parametros["between"];
        }
        if ($grid['pesquisa'] === 'visao.geral') {
            $grid = ['pesquisa' => ['app.csgestor.com.br', 'app.csb2b.com.br',
                    'rigelnatural.com.br', 'forcadevendas.csgestor.com.br'],
            ];
            if (isset($parametros["between"])) {
                $grid['between'] = $parametros["between"];
            }
        }

        $retorno['grid'] = $this->setGrid($grid);
        return $retorno;
    }

    /**
     * @return array $retorno Resultado do banco para o gráfico
     */
    public function get($parametros)
    {
        $converter = new ConvertObject;
        return $this->setArray($converter->convertObject($this->getInstance(\Home\Mapper\Acessos::class)->get($parametros)));
    }

    /**
     * @return array Options
     */
    public function setOptions()
    {
        $options = new Options;

        $options->setWidth('100%');
        $options->setLegend(['position' => 'none']);
        $options->setAxes(['y' => ['distance' => ['label' => 'Acessos'], 'brightness' => [
                    'side' => 'right', 'label' => 'apparent magnitude']]]);
        $options->setBar(['groupWidth' => '90%']);
        $options->setSeries([0 => ['axis' => 'distance'], 1 => ['axis' => 'brightness']]);

        return (array) $options;
    }

    /**
     * @return array Grafico
     */
    public function setGrafico()
    {
        $parametros = $_POST;

        $grafico = new Grafico;
        $grafico->setId('chart-' . explode('.', $parametros['where']['valor'])[1]);
        $grafico->setGrid('grid-' . explode('.', $parametros['where']['valor'])[1]);
        $grafico->setTipo('bar');
        $grafico->setTitulo('Acessos ao sistema csb2b');

        return (array) $grafico;
    }

    /**
     * @param array $resultado Resultado da query
     * @return array
     */
    public function setArray($resultado)
    {
        $retorno = [];
        $retorno[] = ['Dia', 'Acessos', ['role' => 'style']];

        if (!empty($resultado)) {
            foreach ($resultado as $valores) {
                $data = explode('-', $valores['data']);
                $retorno[] = [$data[2] . '/' . $data[1] . '/' . $data[0], (int) $valores['acessos'],
                    '#039be5'];
            }
        }

        return $retorno;
    }

    /**
     * Seta grid do gráficos
     * @param array $parametros
     * @return string $html
     */
    public function setGrid($parametros)
    {

        $converter = new ConvertObject;
        $resultado = $converter->convertObject($this->getInstance(\Home\Mapper\Acessos::class)->grid($parametros));

        $html = '<div class="table-scrollable table-scrollable-borderless">
                    <table class="table table-hover table-light">
                        <thead>
                            <tr class="uppercase">
                                <th> Rank </th>
                                <th> Empresa </th>
                                <th> Acessos </th>
                            </tr>
                        </thead>
                        <tbody class="ScrollTab">';
        if (!empty($resultado)) {
            foreach ($resultado as $valores) {
                $html .= '<tr>
                    <td> ' . $valores['rank'] . '° </td>
                    <td> ' . ucfirst($valores['apelido']) . ' </td>
                    <td> ' . $valores['acessos'] . ' </td>
                </tr>';
            }
        }

        $html .= '</tbody></table></div>';

        return $html;
    }

    /**
     * @param array $dados Resultado do setArray
     * @return string $html
     */
    public function setBlocos()
    {

    }

    public function selecionarGrid($config)
    {
        $configPadrao = $this->getConfig($config['gridAjax']['tela']);
        $mapperGrid = $this->getServiceGrid();

        $where = $mapperGrid->montaWhere($config['gridAjax']['parametro']['where'], 'Home\Mapper\Hydrator\Acessos',
            $this->getConfig('')['gridPhp']['colunas']);
        $select = $mapperGrid->traduzirParametros($config['gridAjax']['parametro']['select'],
            'Home\Mapper\Hydrator\Acessos', $this->getConfig('')['gridPhp']['colunas']);
        $mapper = $this->getInstance(\Home\Mapper\Acessos::class);

        $retorno = $mapper->selecionar($where, $select);

        $convert = new ConvertObject();
        $array = $convert->convertObject($retorno);

        $configPadrao['gridPhp']['data'] = $array;
        $configPadrao['gridPhp']['contData'] = []; //$this->getServiceHelperSql()->contaLinhasTabelaBanco('mxlgncfi', $mapperGrid->getMapHydrator('Home\Mapper\Hydrator\Acessos'), $this->getConfig('')['gridPhp']['colunas'], $where);

        return $configPadrao;
    }

    public function getConfig()
    {
        $colunas = [
            'dataDoLog' => ['exibir' => 'true', 'tipo' => 'int-data', 'titulo' => 'Data',
                'mask' => '##/##/####'],
            'ip' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'IP', 'mask' => '#####################'],
            'usuario' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Usuario',
                'mask' => '#####################'],
            'sistema' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Sistema',
                'mask' => '#####################'],
            'apelido' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Apelido',
                'mask' => '#####################'],
        ];

        return $config = [
            'gridPhp' => [
                'titulo' => 'Logs',
                'colunas' => $colunas,
                'data' => '',
                'configParam' => [
                ],
            ],
        ];
    }

    /**
     * @return Grid\Mapper\Grid
     */
    public function getServiceGrid()
    {
        return $this->getServiceManager()->get('Grid\Service\Grid');
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

}
