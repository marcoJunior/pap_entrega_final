<?php

namespace Home\Service;

use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\ServiceManager\ServiceManager;
use APISql\Service\ConvertObject;

//use Home\Entity\Clientes as Entidade;

class Clientes //implements ServiceManagerAwareInterface
{

    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager = $serviceManager;
        return $this;
    }

    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * @return Home\Mapper\Clientes
     */
    public function getMapperClientes()
    {
        return $this->getServiceManager()->get('MapperClientes');
    }

    /**
     * @return Grid\Mapper\Grid
     */
    public function getServiceGrid()
    {
        return $this->getServiceManager()->get('Grid\Service\Grid');
    }

    /**
     * @return APISql\Service\HelperSql
     */
    public function getServiceHelperSql()
    {
        return $this->getServiceManager()->get('APISql\Service\HelperSql');
    }

    public function selecionarGrid($config)
    {

        $configPadrao = $this->getConfig($config['gridAjax']['tela']);
        $mapperGrid = $this->getServiceGrid();

        $where = $mapperGrid->montaWhere($config['gridAjax']['parametro']['where'], 'Home\Mapper\Hydrator\Clientes', $this->getConfig('')['gridPhp']['colunas']);
        $select = $mapperGrid->traduzirParametros($config['gridAjax']['parametro']['select'], 'Home\Mapper\Hydrator\Clientes', $this->getConfig('')['gridPhp']['colunas']);
        $mapperClientes = $this->getMapperClientes();

        $retorno = $mapperClientes->selecionar($where, $select);

        $convert = new ConvertObject();
        $array = $convert->convertObject($retorno);

        $configPadrao['gridPhp']['data'] = $array;
        //$configPadrao['gridPhp']['contData'] = $this->getServiceHelperSql()->contaLinhasTabelaBanco('mxvernfe', $mapperGrid->getMapHydrator('Home\Mapper\Hydrator\Clientes'), $this->getConfig('')['gridPhp']['colunas'], $where);
        $configPadrao['gridPhp']['contData']['clientes'] = $this->contarClientes();

        return $configPadrao;
    }

    public function contarClientes()
    {
        $convert = new ConvertObject();
        $mapperClientes = $this->getMapperClientes();

        $array = $mapperClientes->contarClientes();
        $retorno = $convert->convertObject($array)[0];

        $arrayRetorno = ['inativos' => $retorno['inativos'], 'ativos' => $retorno['ativos'], 'todos' => $retorno['todos']];

        return $arrayRetorno;
    }

    public function getConfig()
    {

        $colunas = [
            'serie' => ['exibir' => 'true', 'tipo' => 'int', 'titulo' => 'Série', 'mask' => '#####################'],
            'versaonfe' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Versão NFE', 'mask' => '#####################'],
            'versaocs' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Versão CS', 'mask' => '#####################'],
            'cnpj' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'CNPJ', 'mask' => '#####################'],
            'cliente' => ['exibir' => 'true', 'tipo' => 'string', 'titulo' => 'Cliente', 'mask' => '#####################'],
            'ultimaliberacao' => ['exibir' => 'true', 'tipo' => 'int-data', 'titulo' => 'Última liberação', 'mask' => '##/##/####'],
        ];

        return $config = [
            'gridPhp' => [
                'titulo' => 'Home',
                'colunas' => $colunas,
                'data' => '',
                'configParam' => [
                ],
            ],
        ];
    }

}
