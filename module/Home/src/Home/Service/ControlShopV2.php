<?php

namespace Home\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;

//use Home\Entity\ControlShopV2 as ControlShopV2Entity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class ControlShopV2 extends APIGrid
{

    public function selecionar(Parameters $get)
    {

        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperControlShopV2()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperControlShopV2()->selecionarId($id);
    }

    public function contarClientes()
    {
        return $this->mapperControlShopV2()->contarClientes();
    }

    /**
     * @return \Home\Mapper\ControlShopV2
     */
    public function mapperControlShopV2()
    {
        return $this->getServiceManager()->get(\Home\Mapper\ControlShopV2::class);
    }

}
