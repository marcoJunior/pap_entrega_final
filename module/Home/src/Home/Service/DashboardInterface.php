<?php

namespace Home\Service;

/**
 * DashboardInterface
 *
 * Implement to inject Dashboard into Service
 */
interface DashboardInterface
{

    /**
     * @param str $instance Nome da mapper ou service
     * @return ServiceManager $this 
     */
    public function getInstance($instance);

    /**
     * @return array 
     */
    public function set($parametros);

    /**
     * @return array $retorno Resultado do banco para o gráfico
     */
    public function get($parametros);

    /**
     * @return array OptionsEntity
     */
    public function setOptions();

    /**
     * @return array GraficoEntity
     */
    public function setGrafico();

    /**
     * @param array $resultado Resultado da query
     * @return array
     */
    public function setArray($resultado);

    /**
     * @param array $dados Resultado do setArray
     * @return string $html 
     */
    public function setGrid($dados);

    /**
     * Retorna resultado da query a partir de regra
     * @return array 
     */
    public function setBlocos();
}
