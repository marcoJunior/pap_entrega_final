<?php

namespace Home\View;

use Zend\View\Model\ViewModel;
use Zend\View\Renderer\PhpRenderer;

class Dashboard extends ViewModel
{

    protected $template = 'home/get-list';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [
        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
            'https://www.google.com/jsapi',
            'https://www.gstatic.com/charts/loader.js',
            "https://www.google.com/jsapi?autoload={'modules':[{'name':'visualization','version':'1.1','packages':['bar', 'corechart']}]}",
        ];
    }

}
