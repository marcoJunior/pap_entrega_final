<?php

namespace Planos;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Plano::class => function ($sm) {
                    $service = new Service\Plano();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'Planos\Mapper\Plano' =>
                function ($sm) {
                    $mapper = new Mapper\Plano();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Plano())
                        ->setHydrator(new Mapper\Hydrator\Plano());
                    return $mapper;
                },
            ),
        );
    }

}
