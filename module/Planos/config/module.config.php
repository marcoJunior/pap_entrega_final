<?php

return array(
    'controllers' => array(
        'factories' => [
            'planos' => function ($sl) {
                $controller = new \Planos\Controller\PlanoController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'planos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/planos[/:id][/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'planos',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'planos/grid' => __DIR__ . '/../view/planos/grid.phtml',
            'planos/formulario' => __DIR__ . '/../view/planos/formulario.phtml',
            'planos/modal' => __DIR__ . '/../view/planos/modal.phtml',
        ),
        'template_path_stack' => array(
            'planos' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
