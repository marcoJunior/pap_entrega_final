<?php

namespace Planos\Controller;

use Planos\View\Plano;
use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use APIGrid\Controller\APIGridController;

class PlanoController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewClientes = new Plano($phpRenderer);
            $viewClientes->setVariable('titulo', 'Planos');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        $service = $this->servicePlano();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->servicePlano();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->servicePlano();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->servicePlano();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->servicePlano();
        return new JsonModel((array) $service->excluir($id));
    }

    public function selecionarPorIdClienteAction()
    {
        $get = $this->getRequest()->getQuery();
        $IdCliente = $get->get('idCliente', null);
        $service = $this->servicePlano();
        return new JsonModel((array) $service->selecionarPlanosPorCliente($IdCliente));
    }

    /**
     * @return \Planos\Service\Plano
     */
    public function servicePlano()
    {
        return $this->getService()->get(\Planos\Service\Plano::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
