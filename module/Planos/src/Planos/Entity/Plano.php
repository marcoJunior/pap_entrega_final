<?php

namespace Planos\Entity;

use APIHelper\Entity\AbstractEntity;

class Plano extends AbstractEntity
{

    protected $id;
    protected $descricao;
    protected $ativo;
    protected $sistema;
    protected $idSistema;

    function getId()
    {
        return $this->id;
    }

    function getAtivo()
    {
        if (gettype($this->ativo) == 'string') {
            $this->ativo = $this->ativo == 'true' ? 1 : 0;
        }

        return $this->ativo;
    }

    function getIdSistema()
    {
        return $this->idSistema;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function setIdSistema($idSistema)
    {
        $this->idSistema = (int) $idSistema;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

}
