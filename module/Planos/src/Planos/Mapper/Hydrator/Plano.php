<?php

namespace Planos\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Plano extends Hydrator
{

    protected function getEntity()
    {
        return 'Planos\Entity\Plano';
    }

    public function getMap()
    {
        return [];
    }

    protected function getTemporary()
    {
        return [
            'id',
            'sistema',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Plano();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
