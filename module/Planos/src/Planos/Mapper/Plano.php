<?php

namespace Planos\Mapper;

use Zend\Db\Sql\Select;
use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use Planos\Entity\Plano as PlanoEntity;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Plano extends APIGrid
{

    public $tableName = 'plano';
    public $mapperName = 'Planos\Mapper\Hydrator\Plano';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $this->setJoin(new APIGridJoinEntity(
            'sistema', 'sistema.id = plano.id_sistema', ['sistema' => 'nome'], 'left outer'
        ));

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getColunas()
    {
        return [
            'id',
            'descricao',
            'ativo',
            'id_sistema',
        ];
    }

    public function adicao(PlanoEntity $entity)
    {
        return $this->insert($entity)->getAffectedRows();
    }

    public function alterar(PlanoEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

    function selecionarPlanosPorCliente($idCliente)
    {
        //SELECT id, descricao FROM plano WHERE id IN (SELECT id_plano FROM csgestor_admin.contrato WHERE id_cliente = 639)

        $select = $this->getSelect()
            ->columns(['id', 'descricao']);

        $selectInPlanos = new Select();
        $selectInPlanos->columns(['id_plano' => new Expression(' DISTINCT id_plano ')]);
        $selectInPlanos->from('contrato');

        $whereInPlanos = new Where();
        $whereInPlanos->equalTo('id_cliente', $idCliente);

        $where = new Where();
        $where->in('id', $selectInPlanos->where($whereInPlanos));

        return ConvertObject::convertObject(
                $this->select($select->where($where))
        );
    }

}
