<?php

namespace Planos\Service;

use Zend\Stdlib\Parameters;
use Planos\Entity\Plano as PlanoEntity;
use APIFiltro\Entity\Filtros as FiltrosEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Plano extends \APIGrid\Service\APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperPlano()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperPlano();
        $entidade = new PlanoEntity($formulario);

        return $mapper->adicao($entidade);
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $mapper = $this->mapperPlano();
        $entidade = new PlanoEntity($formulario);

        return $mapper->alterar($entidade);
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        $mapper = $this->mapperPlano();
        return $mapper->excluir($id);
    }

    public function selecionarPlanosPorCliente($idClienteMaxscalla)
    {
        $mapper = $this->mapperPlano();
        return $mapper->selecionarPlanosPorCliente($idClienteMaxscalla);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return Planos\Mapper\Plano
     */
    public function mapperPlano()
    {
        return $this->getServiceManager()->get('Planos\Mapper\Plano');
    }

}
