<?php

namespace Produtos;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Produtos::class => function ($sm) {
                    $service = new Service\Produtos();
                    return $service->setServiceManager($sm);
                },
                'Produtos\Mapper\Produtos' =>
                function ($sm) {
                    $mapper = new Mapper\Produtos();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Produtos())
                        ->setHydrator(new Mapper\Hydrator\Produtos());
                    return $mapper;
                },
            ),
        );
    }

}
