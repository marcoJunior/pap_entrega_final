<?php

return array(
    'controllers' => array(
        'factories' => array(
            'produtos' => function ($sl) {
                $controller = new \Produtos\Controller\ProdutosController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ),
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'produtos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/produto[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'produtos',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
//            'grid' => __DIR__ . '/../view/clientes/grid.phtml',
//            'formulario' => __DIR__ . '/../view/clientes/formulario.phtml',
        ),
        'template_path_stack' => array(
            'produtos' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
