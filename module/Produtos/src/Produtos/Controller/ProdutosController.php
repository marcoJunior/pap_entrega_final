<?php

namespace Produtos\Controller;

use Zend\Stdlib\Parameters;
use Application\View\Application;
use Zend\View\Model\JsonModel;
use Produtos\View\Produtos;

class ProdutosController extends \APIGrid\Controller\APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewCliente = new Produtos($phpRenderer);
            $viewCliente->setVariable('titulo', 'Produtos');
            $viewCliente->grid();
            $viewCliente->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewCliente);

            return $app;
        }

        $service = $this->serviceProdutos();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceProdutos();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceProdutos();
        $get = new Parameters($data);

        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceProdutos();
        $get = new Parameters($data);

        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceProdutos();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \Produtos\Service\Produtos
     */
    public function serviceProdutos()
    {
        return $this->getService()->get(\Produtos\Service\Produtos::class);
    }

    /**
     * @return \Produtos\Service\ConexaoCliente
     */
    public function serviceConexaoCliente()
    {
        return $this->getService()->get(\Produtos\Service\ConexaoCliente::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
