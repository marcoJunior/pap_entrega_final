<?php

namespace Produtos\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Produtos extends Hydrator
{

    protected function getEntity()
    {
        return 'Produtos\Entity\Produtos';
    }

    public function getMap()
    {
        $arrayMap = [
            // 'razaoSocial' => 'razao_social'
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Produtos();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
