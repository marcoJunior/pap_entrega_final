<?php

namespace Produtos\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use Produtos\Entity\Produtos as ClienteEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Produtos extends APIGrid {

    public $tableName = 'produtos';
    public $mapperName = 'Produtos\Mapper\Hydrator\Produtos';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros) {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

//        foreach ($filtros->toArray() as $index => $valor) {
//            $valorDescricao = str_replace('_', '',
//                    str_replace('_de', '', ucfirst($index)));
//            $getValor = 'get' . $valorDescricao;
//            $setWhere = 'setWhere' . $valorDescricao;
//            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere}($filtros);
//            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
//                $this->{$setWhere . "s"}($filtros);
//            }
//        }

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function selecionarId($id) {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('serie', $id);

        return $this->select($select->where($where))->current();
    }

    public function getColunas() {
        return [
            'id',
            'nome',
            'descricao',
            'preco',
            'imagem'
        ];
    }

    public function validaExistencia(ClienteEntity $entity) {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        if (!!$entity->getDocumento()) {
            $where->equalTo('documento', $entity->getDocumento());
        } else {
            $where->equalTo('fantasia', $entity->getFantasia());
        }

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaExistenciaPorId($id) {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(ClienteEntity $entity) {
        return $this->insert($entity)->getGeneratedValue();
    }

    public function alterar(ClienteEntity $entity) {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id) {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
