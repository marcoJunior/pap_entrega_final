<?php

namespace Produtos\Service;

use Zend\Stdlib\Parameters;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Produtos\Entity\Produtos as ClienteEntity;
use Produtos\Entity\Produtos as ClienteCompletoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Produtos extends \APIGrid\Service\APIGrid {

    public function selecionar(Parameters $get) {

        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperCliente()
                ->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id) {
        return $this->mapperCliente()->selecionarId($id);
    }

    public function adicao(Parameters $post) {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $produto = new ClienteEntity($formulario);

//        $validacao = $this->validaExistencia($cliente, $contato, $endereco);
//        if (!isset($validacao['mensagem'])) {
        return $this->mapperCliente()->adicao($produto);
//        }
//        return $validacao;
    }

    public function validaExistencia($cliente, $contato, $endereco) {

        if ($this->mapperCliente()->validaExistencia($cliente)) {
            return ['mensagem' => 'Já existe um cadastro para está empresa!'];
        }

        if ($this->mapperContatoCliente()->validaExistencia($contato)) {
            return ['mensagem' => 'Já existe um cadastro de CONTATO para está empresa!'];
        }

        if ($this->mapperEnderecoCliente()->validaExistencia($endereco)) {
            return ['mensagem' => 'Já existe um cadastro de ENDEREÇO para está empresa!'];
        }
    }

    public function alterar(Parameters $post) {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $produto = new ClienteEntity($formulario);

        return $this->mapperCliente()->alterar($produto);
    }

    public function excluir($id) {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        $retorno[0] = $this->mapperEnderecoCliente()->excluir($id);
        $retorno[1] = $this->mapperContatoCliente()->excluir($id);
        $retorno[2] = $this->mapperCliente()->excluir($id);

        return $retorno;
    }

    public function validarFormulario($post) {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return \Produtos\Mapper\Produtos
     */
    public function mapperCliente() {
        return $this->getServiceManager()->get('Produtos\Mapper\Produtos');
    }

    /**
     * @return Produtos\Mapper\EnderecoCliente
     */
    public function mapperEnderecoCliente() {
        return $this->getServiceManager()->get('Produtos\Mapper\EnderecoCliente');
    }

    /**
     * @return Produtos\Mapper\ContatoCliente
     */
    public function mapperContatoCliente() {
        return $this->getServiceManager()->get('Produtos\Mapper\ContatoCliente');
    }

}
