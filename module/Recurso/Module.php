<?php

namespace Recurso;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\RecursoContratado::class => function ($sm) {
                    $service = new Service\RecursoContratado();
                    return $service->setServiceManager($sm);
                },
                Service\Recurso::class => function ($sm) {
                    $service = new Service\Recurso();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'Recurso\Mapper\Recurso' =>
                function ($sm) {
                    $mapper = new Mapper\Recurso();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Recurso())
                        ->setHydrator(new Mapper\Hydrator\Recurso());
                    return $mapper;
                },
                'Recurso\Mapper\RecursoContratado' =>
                function ($sm) {
                    $mapper = new Mapper\RecursoContratado();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\RecursoContratado())
                        ->setHydrator(new Mapper\Hydrator\RecursoContratado());
                    return $mapper;
                },
            ),
        );
    }

}
