<?php

return array(
    'controllers' => array(
        'factories' => array(
            'recursos' => function ($sl) {
                $controller = new \Recurso\Controller\RecursoController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            'recursos-contratados' => function ($sl) {
                $controller = new \Recurso\Controller\RecursoContratadoController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ),
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'recursos' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/recursos[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'recursos',
                    )
                ),
            ),
            'recursos/contratados' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/recursos-contratados[/:id][/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'recursos-contratados',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'recursos/grid' => __DIR__ . '/../view/recursos/grid.phtml',
            'recursos/formulario' => __DIR__ . '/../view/recursos/formulario.phtml',
            'recursos/modal' => __DIR__ . '/../view/recursos/modal.phtml',
        ),
        'template_path_stack' => array(
            'recursos' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
