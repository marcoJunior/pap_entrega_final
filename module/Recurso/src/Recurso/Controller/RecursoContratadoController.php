<?php

namespace Recurso\Controller;

use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;
use APIGrid\Controller\APIGridController;

class RecursoContratadoController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();
        $service = $this->serviceRecursoContratado();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceRecursoContratado();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceRecursoContratado();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceRecursoContratado();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceRecursoContratado();
        return new JsonModel((array) $service->excluir($id));
    }

    public function getClienteAction()
    {
        $service = $this->serviceRecursoContratado();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionarPorCliente($post['cliente']));
    }

    public function getPorClienteAgrupadoAction()
    {
        $service = $this->serviceRecursoContratado();
        $post = $this->getRequest()->getPost();

        return new JsonModel($service->selecionarPorClienteAgrupado($post['cliente']));
    }

    public function calcularTotaisAction()
    {
        $service = $this->serviceRecursoContratado();
        $post = $this->getRequest()->getPost();
        $return = [
            'qtdTotalRecursos' => $service->qtdTotalRecursos($post),
            'valorTotalPlano' => $service->valorTotalPlano($post)
        ];

        return new JsonModel((array) $return);
    }

    public function calcularTotaisEmpresaAction()
    {
        $service = $this->serviceRecursoContratado();
        $post = $this->getRequest()->getPost();

        $return = [
//            'qtdTotalRecursos' => $service->qtdTotalRecursosEmpresa($post),
            'valorTotalPlano' => $service->valorTotalPlanoEmpresa($post)
        ];

        return new JsonModel((array) $return);
    }

    /**
     * @return \Recurso\Service\RecursoContratado
     */
    public function serviceRecursoContratado()
    {
        return $this->getService()->get(\Recurso\Service\RecursoContratado::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
