<?php

namespace Recurso\Entity;

use APIHelper\Entity\AbstractEntity;

class Recurso extends AbstractEntity
{

    protected $id;
    protected $submodulo;
    protected $descricao;
    protected $chave;
    protected $valor;

    function getId()
    {
        return $this->id;
    }

    function getSubmodulo()
    {
        return $this->submodulo;
    }

    function getDescricao()
    {
        return $this->descricao;
    }

    function getChave()
    {
        return $this->chave;
    }

    function getValor()
    {
        return $this->valor;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setSubmodulo($submodulo)
    {
        $this->submodulo = $submodulo;
    }

    function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

    function setChave($chave)
    {
        $this->chave = $chave;
    }

    function setValor($valor)
    {
        $this->valor = $valor;
    }

}
