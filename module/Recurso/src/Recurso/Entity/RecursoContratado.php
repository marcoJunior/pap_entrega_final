<?php

namespace Recurso\Entity;

use APIHelper\Entity\AbstractEntity;

class RecursoContratado extends AbstractEntity
{

    protected $id;
    protected $data;
    protected $quantidade;
    protected $idPlano;
    protected $idRecurso;
    protected $idContrato;
    protected $recurso;
    protected $plano;
    protected $cliente;
    protected $preco;
    protected $sistema;
    protected $submodulo;

    function getId()
    {
        return $this->id;
    }

    function getData()
    {
        return $this->data;
    }

    function getQuantidade()
    {
        return $this->quantidade;
    }

    function getIdPlano()
    {
        return $this->idPlano;
    }

    function getIdRecurso()
    {
        return $this->idRecurso;
    }

    function getIdContrato()
    {
        return $this->idContrato;
    }

    function getRecurso()
    {
        return $this->recurso;
    }

    function getPlano()
    {
        return $this->plano;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function getPreco()
    {
        return $this->preco;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function getSubmodulo()
    {
        return $this->submodulo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    function setIdPlano($idPlano)
    {
        $this->idPlano = $idPlano;
    }

    function setIdRecurso($idRecurso)
    {
        $this->idRecurso = $idRecurso;
    }

    function setIdContrato($idContrato)
    {
        $this->idContrato = (int) $idContrato;
    }

    function setRecurso($recurso)
    {
        $this->recurso = $recurso;
    }

    function setPlano($plano)
    {
        $this->plano = $plano;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function setPreco($preco)
    {
        $this->preco = $preco;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function setSubmodulo($submodulo)
    {
        $this->submodulo = $submodulo;
    }

}
