<?php

namespace Recurso\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Recurso extends Hydrator
{

    protected function getEntity()
    {
        return 'Recurso\Entity\Recurso';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Recurso();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
