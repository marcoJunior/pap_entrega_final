<?php

namespace Recurso\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class RecursoContratado extends Hydrator
{

    protected function getEntity()
    {
        return 'Recurso\Entity\RecursoContratado';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id',
            'recurso',
            'plano',
            'cliente',
            'preco',
            'sistema',
            'submodulo',
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new RecursoContratado();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
