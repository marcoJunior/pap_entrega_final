<?php

namespace Recurso\Mapper;

use Zend\Db\Sql\Where;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Recurso\Entity\Recurso as RecursoEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Recurso extends APIGrid
{

    public $tableName = 'recurso';
    public $mapperName = 'Recurso\Mapper\Hydrator\Recurso';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function selecionarId($id)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        return $this->select($select->where($where))->current();
    }

    public function getColunas()
    {
        return [
            'id',
            'submodulo',
            'descricao',
            'chave',
            'valor',
        ];
    }

    public function validaExistencia(RecursoEntity $entity)
    {
        $select = $this->getSelect()
                ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('chave', $entity->getChave());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(RecursoEntity $entity)
    {
        return $this->insert($entity)->getGeneratedValue();
    }

    public function alterar(RecursoEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
