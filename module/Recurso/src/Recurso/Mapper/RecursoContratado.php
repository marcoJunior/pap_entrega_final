<?php

namespace Recurso\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Recurso\Entity\RecursoContratado as RecursoEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class RecursoContratado extends APIGrid
{

    public $tableName = 'recurso_vinculados';
    public $mapperName = 'Recurso\Mapper\Hydrator\RecursoContratado';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros, $filtroCliente = false)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $this->setJoin(new APIGridJoinEntity('recurso', 'recurso_vinculados.id_recurso = recurso.id',
            [
            'recurso' => new Expression("recurso.descricao"),
            'submodulo' => new Expression("recurso.submodulo"),
            'preco' => new Expression("recurso.valor")
            ]
        ));

        $this->setJoin(new APIGridJoinEntity('plano', 'recurso_vinculados.id_plano = plano.id',
            ['plano' => new Expression("plano.descricao")], ' LEFT OUTER '
        ));

        $this->setJoin(new APIGridJoinEntity('cliente', 'recurso_vinculados.id_contrato = cliente.id',
            ['cliente' => new Expression("fantasia")], ' LEFT OUTER '
        ));

        if ($filtros->getCodigo() == null) {
            $filtros->setCodigo('99999999');
        }

        if ($filtroCliente) {
            $where = new Where();
            $where->isNull('id_contrato');
            $this->setWhere($where);
        }

        foreach ($filtros->toArray() as $index => $valor) {
            $valorDescricao = str_replace('_', '', str_replace('_de', '', ucfirst($index)));
            $getValor = 'get' . $valorDescricao;
            $setWhere = 'setWhere' . $valorDescricao;
            if (method_exists($this, $setWhere) && $filtros->{$getValor}() != Null) {
                $this->{$setWhere}($filtros);
            } else if (method_exists($this, $setWhere . "s") && $filtros->{$getValor}() != Null) {
                $this->{$setWhere . "s"}($filtros);
            }
        }

//        $this->getResultadoDb($postEntity);
//        echo $this->getSqlString();
//        die();

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereCodigo(FiltrosEntity $filtros)
    {
        if ($filtros->getPlano() !== null) {
            $filtros->setCodigo(
                (int) $filtros->getPlano()
            );
        }

        if ($filtros->getCodigo() !== "") {
            $where = new Where();
            if ($filtros->getCodigo() !== null) {
                if (is_array($filtros->getCodigo())) {
                    $where->in('id_plano', $filtros->getCodigo());
                } else {
                    $where->equalTo('id_plano', (int) $filtros->getCodigo());
                }
            }
            $this->setWhere($where);
            return $where;
        }
    }

    /**
     * Where e Join necessario para pesquisa e parametrização por Código de serie do cliente
     * @param FiltrosEntity $filtros
     * @return Where
     */
    function setWhereClientes(FiltrosEntity $filtros)
    {
        $where = new Where();
        $where->or->equalTo('id_contrato', (int) $filtros->getClientes());

        if ($filtros->getPlano() !== null) {
            $where->equalTo('id_plano', (int) $filtros->getPlano());
        }

        $this->setWhere($where, 'OR');
        return $where;
    }

    public function selecionarId($id)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        return $this->select($select->where($where))->current();
    }

    public function selecionarPorCliente($cliente, $planos)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $select->join('recurso', 'recurso_vinculados.id_recurso = recurso.id',
            ['recurso' => new Expression("recurso.chave"),
            'preco' => new Expression("recurso.valor")]
        );

        $select->join('plano', 'recurso_vinculados.id_plano = plano.id', ['plano' => new Expression("plano.descricao")],
            ' LEFT OUTER '
        );

        $select->join('sistema', 'sistema.id = plano.id_sistema', ['sistema' => new Expression("sistema.chave")]
        );

        $select->join('cliente', 'recurso_vinculados.id_contrato = cliente.id',
            ['cliente' => new Expression("fantasia")], ' LEFT OUTER '
        );

        $where = new Where();

        if (is_array($planos)) {
            $where->in('id_plano', (array) $planos);
        } else {
            $where->equalTo('id_plano', (int) $planos);
        }

        $where->or->equalTo('id_contrato', $cliente);

        return ConvertObject::convertObject($this->select($select->where($where)));
    }

    public function selecionarPorClienteAgrupado($cliente, $planos)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns(
            [
                'id',
                'data' => new Expression(" DATE_FORMAT(data, '%d/%m/%Y') "),
                'quantidade' => new Expression('SUM( quantidade ) '),
                'id_plano',
                'id_contrato',
            ]
        );

        $select->join('recurso', 'recurso_vinculados.id_recurso = recurso.id',
            ['recurso' => new Expression("recurso.chave"),
            'preco' => new Expression("recurso.valor")]
        );

        $select->join('plano', 'recurso_vinculados.id_plano = plano.id', ['plano' => new Expression("plano.descricao")],
            ' LEFT OUTER '
        );

        $select->join('sistema', 'sistema.id = plano.id_sistema', ['sistema' => new Expression("sistema.chave")]
        );

        $select->join('cliente', 'recurso_vinculados.id_contrato = cliente.id',
            ['cliente' => new Expression("fantasia")], ' LEFT OUTER '
        );

        $where = new Where();

        if (is_array($planos)) {
            $where->in('id_plano', (array) $planos);
        } else {
            $where->equalTo('id_plano', (int) $planos);
        }

        $where->or->equalTo('id_contrato', $cliente);

        return ConvertObject::convertObject($this->select($select->where($where)->group([
                        'plano.id', 'recurso.id'])));
    }

    public function getColunas()
    {
        return [
            'id',
            'data' => new Expression(" DATE_FORMAT(data, '%d/%m/%Y') "),
            'quantidade',
            'id_plano',
            'id_contrato',
        ];
    }

    public function qtdTotalSistema(RecursoEntity $entity)
    {
        $select = $this->getSelect()
            ->columns(['count(1)']);

        $where = new Where();
        $where->equalTo('id_contrato', $entity->getId());

        return ConvertObject::convertObject($this->select($select->where($where)));
    }

    public function qtdTotalRecursos(RecursoEntity $entity, $filtroCliente = false)
    {
        $select = $this->getSelect()
            ->columns(['recurso' => new Expression('count(1)')]);

        $where = new Where();
        if ($entity->getPlano() !== null) {
            if (is_array($entity->getPlano())) {
                $where->in('id_plano', $entity->getPlano());
            } else {
                $where->equalTo('id_plano', $entity->getPlano());
            }
        }

        if ($filtroCliente) {
            $where->isNull('id_contrato');
        }

        if ($entity->getIdContrato() !== null) {
            $where->or->equalTo('id_contrato', $entity->getIdContrato());
        }

        return $this->select($select->where($where))->current()->getRecurso();
    }

    public function valorTotalPlano(RecursoEntity $entity, $filtroCliente = false)
    {
        $select = $this->getSelect()
            ->columns([new Expression('distinct id_recurso'), 'preco' => new Expression('sum((recurso_vinculados.quantidade * recurso.valor))')]);

        $select->join(
            'recurso', 'recurso_vinculados.id_recurso = recurso.id', ['recurso' => new Expression("recurso.descricao")]
        );

        $where = new Where();
        if ($entity->getPlano() !== null) {
            if (is_array($entity->getPlano())) {
                $where->in('id_plano', $entity->getPlano());
            } else {
                $where->equalTo('id_plano', $entity->getPlano());
            }
        }

        if ($entity->getIdPlano() !== null) {
            $where->equalTo('id_plano', (int) $entity->getIdPlano());
        }

        if ($filtroCliente) {
            $where->isNull('id_contrato');
        }

        if ($entity->getIdContrato() !== null) {
            $where->or->equalTo('id_contrato', $entity->getIdContrato());
        }

        return number_format(
            $this->select($select->where($where))->current()->getPreco()
            , 2, ',', '.');
    }

    public function validaExistencia(RecursoEntity $entity)
    {
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('chave', $entity->getChave());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaPermissaoAdicaoSubmodulo(RecursoEntity $entity)
    {
        $this->inicializar($this->tableName, $this->mapperName);
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $select->join('recurso', 'recurso_vinculados.id_recurso = recurso.id',
            ['recurso' => new Expression("recurso.chave"),
            'submodulo' => new Expression("recurso.submodulo"),
            'preco' => new Expression("recurso.valor")]
        );

        $select->join('plano', 'recurso_vinculados.id_plano = plano.id', ['plano' => new Expression("plano.descricao")],
            ' LEFT OUTER '
        );

        $select->join('sistema', 'sistema.id = plano.id_sistema', ['sistema' => new Expression("sistema.chave")]
        );

        $select->join('cliente', 'recurso_vinculados.id_contrato = cliente.id',
            ['cliente' => new Expression("fantasia")], ' LEFT OUTER '
        );

        $where = new Where();
        $where->equalTo('id_plano', (int) $entity->getIdPlano());
        $where->equalTo('recurso.submodulo', (int) '1');

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(RecursoEntity $entity)
    {
        if ($entity->getSubmodulo() != '0') {
            if ($this->validaPermissaoAdicaoSubmodulo($entity)) {
                return ['mensagem' => 'Já existe um recurso de submodulo para este plano!'];
            }
        }

        return $this->insert($entity)->getGeneratedValue();
    }

    public function alterar(RecursoEntity $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

}
