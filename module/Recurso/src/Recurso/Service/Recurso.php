<?php

namespace Recurso\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Recurso\Entity\Recurso as RecursoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Recurso extends APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperRecurso()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperRecurso()->selecionarId($id);
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $recurso = new RecursoEntity($formulario);
        $validacao = $this->validaExistencia($recurso);

        if (!isset($validacao['mensagem'])) {
            $retorno = $this->mapperRecurso()->adicao($recurso);
            return $retorno;
        }

        return $validacao;
    }

    public function validaExistencia($recurso)
    {
        if ($this->mapperRecurso()->validaExistencia($recurso)) {
            return ['mensagem' => 'Já existe um cadastro para este Recurso!'];
        }
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $recurso = new RecursoEntity($formulario);
        return $this->mapperRecurso()->alterar($recurso);
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $this->mapperRecurso()->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return Recursos\Mapper\Recurso
     */
    public function mapperRecurso()
    {
        return $this->getServiceManager()->get('Recurso\Mapper\Recurso');
    }

}
