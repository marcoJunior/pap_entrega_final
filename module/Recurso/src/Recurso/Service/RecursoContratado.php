<?php

namespace Recurso\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Recurso\Entity\RecursoContratado as RecursoEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class RecursoContratado extends APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        if (isset($get->get('paramData', null)['codigo'])) {
            $filtros->setCodigo($get->get('paramData')['codigo']);
        }

        if (isset($get->get('paramData', null)['cliente'])) {
            $filtros->setClientes($get->get('paramData')['cliente']);
        }

        if (isset($get->get('paramData', null)['plano']) && $get->get('paramData',
                        null)['plano'] != null) {
            $filtros->setPlano($get->get('paramData')['plano']);
        }

        $plano = $this->serviceConexaoClienteMaxScalla()->selecionarId(
                $filtros->getClientes()
        );

        if ($plano && !$filtros->getCodigo()) {
            $filtros->setCodigo($plano->getPlano());
        }

        $retorno = $this->mapperRecursoContratado()->selecionar($postTratado,
                $filtros, $get->get('filtroCliente', false));
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperRecursoContratado()->selecionarId($id);
    }

    public function selecionarPorCliente($cliente)
    {
        $plano = $this->serviceConexaoClienteMaxScalla()->selecionarId(
                $cliente
        );
        return $this->mapperRecursoContratado()->selecionarPorCliente($cliente,
                        $plano->getPlano());
    }

    public function selecionarPorClienteAgrupado($cliente)
    {
        $plano = $this->serviceConexaoClienteMaxScalla()->selecionarId(
                $cliente
        );
        return $this->mapperRecursoContratado()->selecionarPorClienteAgrupado($cliente,
                        $plano->getPlano());
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $recurso = new RecursoEntity($formulario);
        $data = new \DateTime();
        $recurso->setData($data->format('Y-m-d'));
        $retorno = $this->mapperRecursoContratado()->adicao($recurso);
        return $retorno;
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $recurso = new RecursoEntity($formulario);
        return $this->mapperRecursoContratado()->alterar($recurso);
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $this->mapperRecursoContratado()->excluir($id);
    }

    public function qtdTotalSistema(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        $mapper = $this->mapperRecursoContratado();
        $entidade = new RecursoEntity($formulario);

        return $mapper->qtdTotalSistema($entidade);
    }

    public function qtdTotalRecursos(Parameters $post)
    {
        $entidade = new RecursoEntity();
        $entidade->setPlano($post->get('formulario')['id']);

        $mapper = $this->mapperRecursoContratado();
        return $mapper->qtdTotalRecursos($entidade, true);
    }

    public function valorTotalPlano(Parameters $post)
    {
        $entidade = new RecursoEntity();
        $entidade->setPlano($post->get('formulario')['id']);

        $mapper = $this->mapperRecursoContratado();
        return $mapper->valorTotalPlano($entidade, true);
    }

    public function qtdTotalRecursosEmpresa(Parameters $post)
    {
        $entidade = new RecursoEntity();
        $entidade->setIdContrato($post->get('formulario')['id']);

        $plano = $this->serviceConexaoClienteMaxScalla()->selecionarId(
                $entidade->getIdContrato()
        );

        if ($plano) {
            $entidade->setPlano($plano->getPlano());
        }

        $mapper = $this->mapperRecursoContratado();
        return $mapper->qtdTotalRecursos($entidade);
    }

    public function valorTotalPlanoEmpresa(Parameters $post)
    {
        $entidade = new RecursoEntity();
        $entidade->setIdContrato($post->get('formulario')['id']);

        if (isset($post->get('formulario')['idPlano']) && $post->get('formulario')['idPlano'] != null) {
            $entidade->setIdPlano($post->get('formulario')['idPlano']);
        }

        if ($entidade->getIdPlano() == null) {
            $plano = $this->serviceConexaoClienteMaxScalla()->selecionarId(
                    $entidade->getIdContrato()
            );

            if ($plano) {
                $entidade->setPlano($plano->getPlano());
            }
        }

        $mapper = $this->mapperRecursoContratado();
        return $mapper->valorTotalPlano($entidade);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return Recursos\Mapper\RecursoContratado
     */
    public function mapperRecursoContratado()
    {
        return $this->getServiceManager()->get(\Recurso\Mapper\RecursoContratado::class);
    }

    /**
     * @return ConexaoClienteMaxScalla\Service\ConexaoClienteMaxScalla
     */
    public function serviceConexaoClienteMaxScalla()
    {
        return $this->getServiceManager()->get(\ConexaoClienteMaxScalla\Service\ConexaoClienteMaxScalla::class);
    }

}
