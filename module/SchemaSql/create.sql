-- Tabela de usuário dos funcionários da Maxscalla
CREATE SEQUENCE mxusucfi_sq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1


CREATE TABLE mxusucfi
(
    cficodusu numeric(15,0) NOT NULL DEFAULT nextval('mxusucfi_sq'::regclass), -- Código
    cfiatvusu numeric(1,0) NOT NULL DEFAULT 0, -- Ativação do usuário
    cfinomusu character(40) NOT NULL DEFAULT ''::bpchar, -- Nome completo do usuário
    cfiusuusu character(20) NOT NULL DEFAULT ''::bpchar, -- Usuário para fazer login
    cfiemlusu character(40) NOT NULL DEFAULT ''::bpchar, -- Email 
    cfisenusu character(40) NOT NULL DEFAULT ''::bpchar, -- Senha
    cfitipusu character(1) NOT NULL DEFAULT 'U', -- Tipo do login
    CONSTRAINT mxusucfi_cficodusu_key UNIQUE (cficodusu)
)
WITH (
  OIDS=FALSE
);
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Criação de coluna na mxvldcfi
ALTER TABLE mxvldcfi ADD COLUMN cfiqclvld INTEGER
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
-- Tabela dos tipos de pagamentos onlines de acordo com as empresas
CREATE SEQUENCE mxpoecfi_sq
INCREMENT 1
MINVALUE 1
MAXVALUE 9223372036854775807
START 1
CACHE 1


CREATE TABLE mxpoecfi
(
    cficodpoe numeric(15,0) NOT NULL DEFAULT 0, -- Código da empresa
    cficiepoe numeric(1,0) NOT NULL DEFAULT 0,  -- Cielo (ativa ou não)
    cfipagpoe numeric(1,0) NOT NULL DEFAULT 0,  -- Pagseguro (ativa ou não)
    cfipaypoe numeric(1,0) NOT NULL DEFAULT 0,  -- Paypal (ativa ou não)
    cfimoipoe numeric(1,0) NOT NULL DEFAULT 0,  -- MOIP (ativa ou não)
    CONSTRAINT mxpoecfi_cficodpoe_key UNIQUE (cficodpoe)
)
WITH (
  OIDS=FALSE
);
-->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

ALTER TABLE MXCELCFI ADD COLUMN cficodcel bigserial primary key;
ALTER TABLE MXCELCFI ADD COLUMN cfivercel character varying(15) NOT NULL DEFAULT ''::character varying;
--CÓDIGOS DE SERIE DO FORÇA DE VENDAS------------------------------------------
CREATE TABLE mxcelcfi (
    cficodcel bigserial primary key,
    cfivercel character varying(15) NOT NULL DEFAULT ''::character varying,
    cfisercel character varying(20) NOT NULL DEFAULT ''::character varying,
    cfisiscel character varying(20) NOT NULL DEFAULT ''::character varying,
    cficidcel character varying(100) NOT NULL DEFAULT ''::character varying,
    cfirepcel integer NOT NULL DEFAULT 0,
    cfiaticel character varying(20) NOT NULL DEFAULT ''::character varying
);


-- Table: mxcamcfi

-- DROP TABLE mxcamcfi;

CREATE TABLE mxcamcfi
(
  cficodcam character varying(10),
  cficlicam character varying(40),
  cfisercam character varying(10),
  cfifatcam integer,
  cfinipcam character varying(26),
  cfindbcam character varying(20),
  cfitipcam character varying(10),
  cfisiscam character varying(20) NOT NULL DEFAULT ''::character varying,
  cfiqalcam integer NOT NULL DEFAULT 0,
  cfiqaacam integer NOT NULL DEFAULT 0,
  cfidtacam varchar(60) NOT NULL DEFAULT '',
  cfivalcam integer NOT NULL DEFAULT 0,
  cfivercam character varying(10) NOT NULL DEFAULT ''::character varying
)
WITH (
  OIDS=FALSE
);
