--DB ADM

create table plano
  (
   id serial not null primary key,
   qtd_espaco numeric(20) not null default 50,
   qtd_cliente numeric(20) not null default 1,
   qtd_funcionario numeric(20) not null default 1,
   qtd_conexoes numeric(20) not null default 1,
   preco numeric(20,2) not null default 0,
   ativo numeric(3) not null default 0,
   id_sistema integer references sistema(id),
  );

create table sistema
 (
  id serial not null primary key,
  nome varchar(60) not null default '',
  preco numeric(20,2) not null default 0.0,
  ativo numeric(3) not null default 0
 );

create table administradora
 (
  id serial not null primary key,
  descricao varchar(60) not null default '',
  alias varchar(40) not null default ''
 );

create table cliente
  (
   id serial not null primary key,
   razao_social varchar(140) NOT NULL,
   fantasia varchar(60) default '',
   documento varchar(30) NOT NULL,
   telefone varchar(30) NOT NULL default '',
   email varchar(120) default '',
   logo varchar(120) default '',
   apelido varchar(60) NOT NULL default '',
   dominio varchar(260) default '',
   sub_dominio varchar(260) default '',
   cep character varying(23) NOT NULL default '',
   rua character varying(60) NOT NULL default '',
   numero character varying(6) NOT NULL default '',
   complemento character varying(60) default '',
   bairro character varying(60) NOT NULL default '',
   cidade character varying(60) NOT NULL default '',
   estado character varying(15) NOT NULL default '',
   ativo boolean not null default false
  );

 create table conexao
  (
   id serial not null primary key,
   ip varchar(120) not null default '',
   porta varchar(6) not null default '',
   usuario_banco varchar(45) not null default '',
   senha_banco varchar(45) not null default '',
   banco varchar(45) not null default '',
   ativo numeric(3) not null default 0,
   id_plano integer references plano(id),
   id_cliente_max integer references cliente(id)
  );

 create table funcao
  (
   id serial not null primary key,
   descricao varchar(60) not null default ''
  );

create table usuario
(
    id serial not null primary key,
    usuario character varying(60) NOT NULL,
    senha character varying(60) NOT NULL,
    nome_completo character varying(120),
    telefone character varying(60),
    email character varying(60),
    cep character varying(23) NOT NULL,
    rua character varying(60) NOT NULL,
    numero character varying(6) NOT NULL,
    complemento character varying(60),
    bairro character varying(60) NOT NULL,
    cidade character varying(60) NOT NULL,
    estado character varying(15) NOT NULL,
    pais character varying(25) DEFAULT 'BR',
    token_login character varying(35),
    id_funcao integer references funcao(id),
    id_conexao integer NOT NULL references conexao(id)
);

 create table api_pagamento
  (
   id serial not null primary key,
   ativo numeric(2) not null default 0,
   token varchar(60) not null default '',
   chave varchar(60) not null default '',
   ambiente numeric(2) not null default 0,
   parcelamento numeric(2) not null default 0,
   auto_captura numeric(1) default 0,
   forma_pagamento varchar(60) not null default '',
   parcelas numeric(2) not null default 0,
   id_cliente_max integer references cliente(id),
   id_administradora integer references administradora(id)
  );

 create table solicitacao
  (
   id serial not null primary key,
   pedido numeric(19,0) not null default 0,
   valor numeric(20,4) not null default 0,
   data_entrada date not null default current_date,
   observacao text default '',
   status char(1) default 'A',
   status_banco_loja numeric(1) default 0,
   token varchar(60) not null default '',
   validade date not null default current_date,
   transacao varchar(65) not null default '',
   status_transacao varchar(3) default '',
   data_transacao date not null default current_date,
   id_cliente_max integer references cliente(id),
   id_api_pagamento integer references api_pagamento(id)
  );

 create table bandeira
  (
   id serial not null primary key,
   visa boolean not null default false,
   mastercard boolean not null default false,
   amex boolean not null default false,
   diners boolean not null default false,
   elo boolean not null default false,
   aura boolean not null default false,
   hipercard boolean not null default false,
   hiper boolean not null default false,
   boleto boolean not null default false,
   id_cliente_max integer references cliente(id)
  );
----------------------------------------------------------------------------------------------
INSERT INTO public.administradora(descricao, alias) VALUES ('Cielo', 'cielo');
INSERT INTO public.administradora(descricao, alias) VALUES ('PagSeguro', 'pagseguro');
INSERT INTO public.administradora(descricao, alias) VALUES ('Moip', 'moip');

INSERT INTO public.api_pagamento( ativo, token, chave, ambiente, parcelamento, auto_captura, forma_pagamento, parcelas, id_cliente_max, id_administradora)
    VALUES (0, 'GF6D4FG6D45GFD645G', 'FSD654FSD', 0, 1, 0, '', 12, 0, 1);

INSERT INTO public.solicitacao(pedido, valor, data_entrada, observacao, status, status_banco_loja, token, validade, transacao, status_transacao, data_transacao, id_cliente_max, id_gateway_pagamento)
    VALUES (101547, 4522.89, '2017-03-10', 'Testo de observação para teste', 'A', 0, 'G45FDG46F45', '2017-03-20', 'B1HBG561WE929FCVWER3', '1', '2017-03-15', 0, 1);

INSERT INTO plano(qtd_espaco, qtd_cliente, qtd_funcionario, qtd_conexoes, preco, ativo)
    VALUES (50, 1, 1, 1, 39.90, 1);

INSERT INTO cliente(razao_social, fantasia, documento, telefone, email, apelido, dominio, sub_dominio,
cep,rua,numero,complemento,bairro,cidade,estado,id_plano)
    VALUES ('Empresa teste junior .Ltda', 'MarcoJunior', '441.270.508-02', '+55 (11) 94550-8188', 
'marco_r_c_junior@hotmail.com','MarcoJu', '', '','09280-030','Rua Guatemala','171','Apt 1','Pq.das Nações','Santo André','SP', 1);

INSERT INTO sistema(nome, preco, ativo)
    VALUES ('projetox', 199.99, 1);

INSERT INTO conexao(ip, porta, usuario_banco, senha_banco, banco, ativo, id_sistema, id_cliente_max)
    VALUES ('csgestor.com.br', '5432', 'csgestor_max', 'amdsdl7586', 'csgestor_projetox_cliente', 1, 1, 1);

INSERT INTO usuario(usuario,senha,nome_completo,telefone,email,cep,rua,numero,complemento,bairro,cidade,estado,pais,id_funcao,id_conexao)
    VALUES ('MarcoJunior', 'm1r2c3j4','Marco Roberto Crema Junior','(11)94550-8188','marco_r_c_junior@hotmail.com','09280030','rua guatemala','171','apt1','Parque das Nações','Santo André','SP','BR', 1, 1);

INSERT INTO funcao (descricao) VALUES ('Admin');
INSERT INTO funcao (descricao) VALUES ('Vendedor');
INSERT INTO funcao (descricao) VALUES ('Contador');

select * from usuario usu_con
inner join conexao con on usu_con.id_conexao = con.id
inner join sistema sis on con.id_sistema = sis.id
inner join cliente cli on con.id_cliente_max = cli.id
inner join plano pla on cli.id_plano = pla.id
where usu_con.nome = 'MarcoJunior' and usu_con.senha = 'm1r2c3j4'
