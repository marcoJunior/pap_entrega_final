<?php

namespace Sistemas;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Sistema::class => function ($sm) {
                    $service = new Service\Sistema();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'Sistemas\Mapper\Sistema' =>
                function ($sm) {
                    $mapper = new Mapper\Sistema();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\Sistema())
                        ->setHydrator(new Mapper\Hydrator\Sistema());
                    return $mapper;
                },
            ),
        );
    }

}
