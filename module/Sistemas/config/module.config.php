<?php

return array(
    'controllers' => array(
        'factories' => [
            'sistemas' => function ($sl) {
                $controller = new \Sistemas\Controller\SistemaController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'sistemas' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/sistemas[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'sistemas',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            'sistema/grid' => __DIR__ . '/../view/sistema/grid.phtml',
            'sistema/formulario' => __DIR__ . '/../view/sistema/formulario.phtml',
            'sistema/modal' => __DIR__ . '/../view/sistema/modal.phtml',
        ),
        'template_path_stack' => array(
            'sistemas' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
