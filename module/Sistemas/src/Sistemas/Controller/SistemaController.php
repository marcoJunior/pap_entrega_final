<?php

namespace Sistemas\Controller;

use APIGrid\Controller\APIGridController;
use Application\View\Application;
use Sistemas\View\Sistema;
use Zend\Stdlib\Parameters;
use Zend\View\Model\JsonModel;

class SistemaController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewClientes = new Sistema($phpRenderer);
            $viewClientes->setVariable('titulo', 'Sistemas');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        $service = $this->serviceSistema();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $service = $this->serviceSistema();
        return new JsonModel($service->selecionarId($id)->toArray());
    }

    public function create($data)
    {
        $service = $this->serviceSistema();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceSistema();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceSistema();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \Sistemas\Service\Sistema
     */
    public function serviceSistema()
    {
        return $this->getService()->get(\Sistemas\Service\Sistema::class);
    }

    /**
     * @return \APIFiltro\Service\Init
     */
    public function serviceFiltro()
    {
        return $this->getService()->get(\APIFiltro\Service\Init::class);
    }

}
