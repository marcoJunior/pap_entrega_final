<?php

namespace Sistemas\Entity;

use APIHelper\Entity\AbstractEntity;

class Sistema extends AbstractEntity
{

    protected $id;
    protected $nome;
    protected $chave;
    protected $ativo;

    function getId()
    {
        return $this->id;
    }

    function getNome()
    {
        return $this->nome;
    }

    function getAtivo()
    {
        if (gettype($this->ativo) == 'string') {
            $this->ativo = $this->ativo == 'true' ? 1 : 0;
        }

        return $this->ativo;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setNome($nome)
    {
        $this->nome = $nome;
    }

    function setAtivo($ativo)
    {
        $this->ativo = $ativo;
    }

    function getChave()
    {
        return $this->chave;
    }

    function setChave($chave)
    {
        $this->chave = $chave;
    }

}
