<?php

namespace Sistemas\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Sistema extends Hydrator
{

    protected function getEntity()
    {
        return 'Sistemas\Entity\Sistema';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Sistema();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
