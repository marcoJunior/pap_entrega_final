<?php

namespace Sistemas\Service;

use Zend\Stdlib\Parameters;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Sistemas\Entity\Sistema as SistemaEntity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Sistema extends APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperSistema()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id)
    {
        return $this->mapperSistema()->selecionarId($id);
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $sistema = new SistemaEntity($formulario);
        $validacao = $this->validaExistencia($sistema);

        if (!isset($validacao['mensagem'])) {
            $retorno = $this->mapperSistema()->adicao($sistema);
            return $retorno;
        }

        return $validacao;
    }

    public function validaExistencia($sistema)
    {
        if ($this->mapperSistema()->validaExistencia($sistema)) {
            return ['mensagem' => 'Já existe um cadastro para este Sistema!'];
        }
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $sistema = new SistemaEntity($formulario);
        return $this->mapperSistema()->alterar($sistema);
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $this->mapperSistema()->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return Sistemas\Mapper\Sistema
     */
    public function mapperSistema()
    {
        return $this->getServiceManager()->get('Sistemas\Mapper\Sistema');
    }

}
