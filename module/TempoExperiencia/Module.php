<?php

namespace TempoExperiencia;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\TempoExperienciaCliente::class => function ($sm) {
                    $service = new Service\TempoExperienciaCliente();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'TempoExperiencia\Mapper\TempoExperienciaCliente' =>
                function ($sm) {
                    $mapper = new Mapper\TempoExperienciaCliente();
                    $dbConfig = $sm->get('dbMysql');
                    $mapper->setDbAdapter($dbConfig)
                        ->setEntityPrototype(new Entity\TempoExperienciaCliente())
                        ->setHydrator(new Mapper\Hydrator\TempoExperienciaCliente());
                    return $mapper;
                },
            ),
        );
    }

}
