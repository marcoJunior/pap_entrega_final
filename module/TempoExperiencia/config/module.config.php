<?php

return array(
    'controllers' => array(
        'factories' => [
            'tempo_experiencia' => function ($sl) {
                $controller = new \TempoExperiencia\Controller\TempoExperienciaClienteController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ),
    'router' => array(
        'routes' => array(
            /**
             * SERVIÇOS
             */
            'clientes/tempo_experiencia' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/clientes/tempo_experiencia[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'tempo_experiencia',
                    )
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_map' => array(
            /////////TEMPO DE EXPERIENCIA
            'tempo_experiencia/grid' => __DIR__ . '/../view/tempo_experiencia/grid.phtml',
            'tempo_experiencia/formulario' => __DIR__ . '/../view/tempo_experiencia/formulario.phtml',
            'tempo_experiencia/modal' => __DIR__ . '/../view/tempo_experiencia/modal.phtml',
        ),
        'template_path_stack' => array(
            'tempo_experiencia' => __DIR__ . '/../view',
        ),
    ),
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
