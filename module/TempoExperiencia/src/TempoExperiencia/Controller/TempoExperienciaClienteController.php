<?php

namespace TempoExperiencia\Controller;

use Zend\Stdlib\Parameters;
use TempoExperiencia\Exception;
use Zend\View\Model\JsonModel;
use Application\View\Application;
use APIGrid\Controller\APIGridController;
use TempoExperiencia\View\TempoExperiencia;
use Zend\Db\Adapter\Exception\InvalidQueryException;

class TempoExperienciaClienteController extends APIGridController
{

    public function getList()
    {
        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewClientes = new TempoExperiencia($phpRenderer);
            $viewClientes->setVariable('titulo', 'Tempo de experiência');
            $viewClientes->grid();
            $viewClientes->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewClientes);

            return $app;
        }

        $service = $this->serviceTempoExperienciaCliente();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $retorno = ['mensagem' => 'Não foi possivel encontrar validade para este código de série!'];

        $get = $this->getRequest()->getQuery();
        $sistema = (int) $get->get('sistema', 1);
        $service = $this->serviceTempoExperienciaCliente();


        try {
            return new JsonModel($service->selecionarId($id, $sistema));
        } catch (Exception\UnexpectedValueException $e) {
            $this->getResponse()->setStatusCode(404);
            return new JsonModel([
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
            ]);
        } catch (InvalidQueryException $exc) {
            $exc->getTraceAsString();
        }
    }

    public function create($data)
    {
        $service = $this->serviceTempoExperienciaCliente();
        $get = new Parameters($data);
        return new JsonModel((array) $service->adicao($get));
    }

    public function update($id, $data)
    {
        $service = $this->serviceTempoExperienciaCliente();
        $get = new Parameters($data);
        return new JsonModel((array) $service->alterar($get));
    }

    public function delete($id)
    {
        $service = $this->serviceTempoExperienciaCliente();
        return new JsonModel((array) $service->excluir($id));
    }

    /**
     * @return \TempoExperiencia\Service\TempoExperienciaCliente
     */
    public function serviceTempoExperienciaCliente()
    {
        return $this->getService()->get(\TempoExperiencia\Service\TempoExperienciaCliente::class);
    }

}
