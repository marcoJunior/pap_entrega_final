<?php

namespace TempoExperiencia\Entity;

use APIHelper\Entity\AbstractEntity;

class TempoExperiencia extends AbstractEntity
{

    protected $id;
    protected $data;
    protected $diasValidade;
    protected $idContrato;

    function getId()
    {
        return $this->id;
    }

    function getData()
    {
        return $this->data;
    }

    function getDiasValidade()
    {
        return $this->diasValidade;
    }

    function setDiasValidade($diasValidade)
    {
        $this->diasValidade = $diasValidade;
    }

    function setId($id)
    {
        $this->id = (int) $id;
    }

    function setData($data)
    {
        $new = new \DateTime(str_replace('/', '-', $data));
        $this->data = $new->format('Y-m-d');
    }

    function getIdContrato()
    {
        return $this->idContrato;
    }

    function setIdContrato($idContrato)
    {
        $this->idContrato = (int) $idContrato;
    }

}
