<?php

namespace TempoExperiencia\Entity;

use APIHelper\Entity\AbstractEntity;

class TempoExperienciaCliente extends AbstractEntity
{

    protected $id;
    protected $data;
    protected $dataValidade;
    protected $diasValidade;
    protected $diasVencimento;
    protected $cliente;
    protected $sistema;
    protected $idContrato;
    protected $plano;
    protected $idCliente;

    function getId()
    {
        return $this->id;
    }

    function getData()
    {
        return $this->data;
    }

    function getDataValidade()
    {
        return $this->dataValidade;
    }

    function getDiasValidade()
    {
        return $this->diasValidade;
    }

    function getDiasVencimento()
    {
        return $this->diasVencimento;
    }

    function getCliente()
    {
        return $this->cliente;
    }

    function getSistema()
    {
        return $this->sistema;
    }

    function setId($id)
    {
        $this->id = (int) $id;
    }

    function setData($data)
    {
        $this->data = $data;
    }

    function setDataValidade($dataValidade)
    {
        $this->dataValidade = $dataValidade;
    }

    function setDiasValidade($diasValidade)
    {
        $this->diasValidade = $diasValidade;
    }

    function setDiasVencimento($diasVencimento)
    {
        $this->diasVencimento = $diasVencimento;
    }

    function setCliente($cliente)
    {
        $this->cliente = $cliente;
    }

    function setSistema($sistema)
    {
        $this->sistema = $sistema;
    }

    function getIdCliente()
    {
        return $this->idCliente;
    }

    function setIdCliente($idCliente)
    {
        $this->idCliente = $idCliente;
    }

    function getPlano()
    {
        return $this->plano;
    }

    function setPlano($plano)
    {
        $this->plano = $plano;
    }

    function getIdContrato()
    {
        return $this->idContrato;
    }

    function setIdContrato($idContrato)
    {
        $this->idContrato = $idContrato;
    }

}
