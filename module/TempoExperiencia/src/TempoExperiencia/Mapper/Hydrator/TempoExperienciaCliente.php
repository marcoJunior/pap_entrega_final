<?php

namespace TempoExperiencia\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class TempoExperienciaCliente extends Hydrator
{

    protected function getEntity()
    {
        return 'TempoExperiencia\Entity\TempoExperienciaCliente';
    }

    public function getMap()
    {
        $arrayMap = [
        ];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new TempoExperienciaCliente();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
