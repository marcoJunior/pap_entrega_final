<?php

namespace TempoExperiencia\Mapper;

use Zend\Db\Sql\Where;
use Zend\Db\Sql\Expression;
use APIGrid\Mapper\APIGrid;
use APISql\Service\ConvertObject;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIGrid\Entity\Action\APIGridJoin as APIGridJoinEntity;
use TempoExperiencia\Entity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class TempoExperienciaCliente extends APIGrid
{

    public $tableName = 'cliente_experiencia';
    public $mapperName = 'TempoExperiencia\Mapper\Hydrator\TempoExperienciaCliente';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    public function selecionarId($id, $sistema = 1)
    {
//        var_dump($id);die();
        $this->inicializar($this->tableName, $this->mapperName);

        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('serie', $id);

        $retorno = $this->select($select->where($where))->current();

        if (count($retorno) <= 0) {
            $retorno = null;
        }

        return $retorno;
    }

    public function getColunas()
    {
        return [
            'id',
            'data' => new Expression(" DATE_FORMAT(data, '%d/%m/%Y') "),
            'dataValidade' => new Expression("DATE_FORMAT(DATE_ADD(data, INTERVAL dias_validade DAY), '%d/%m/%Y')"),
            'dias_validade',
            'diasVencimento' => new Expression(" DATEDIFF(DATE_ADD(data, INTERVAL dias_validade DAY), NOW()) "),
            'cliente',
            'plano',
            'sistema',
            'serie',
            'id_cliente',
            'id_contrato',
        ];
    }

    public function validaExistencia(Entity\TempoExperiencia $entity)
    {
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id_contrato', $entity->getIdContrato());

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function validaExistenciaPorId($id)
    {
        $select = $this->getSelect()
            ->columns($this->getColunas());

        $where = new Where();
        $where->equalTo('id', $id);

        $dbVerificaExistencia = ConvertObject::convertObject($this->select($select->where($where)));

        if (count($dbVerificaExistencia) > 0) {
            return true;
        }
        return false;
    }

    public function adicao(Entity\TempoExperiencia $entity)
    {
        $this->inicializaTempoExperiencia();
        return $this->insert($entity)->getGeneratedValue();
    }

    public function alterar(Entity\TempoExperiencia $entity)
    {
        $this->inicializaTempoExperiencia();
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    public function excluir($id)
    {
        $this->inicializaTempoExperiencia();
        $where = new Where();
        $where->equalTo('id', $id);

        return $this->delete($where)->getAffectedRows();
    }

    public function inicializaTempoExperiencia()
    {
        $this->inicializar('tempo_experiencia', 'TempoExperiencia\Mapper\Hydrator\TempoExperiencia');

        $this->setEntityPrototype(new Entity\TempoExperiencia);
        $this->setHydrator(new Hydrator\TempoExperiencia);
    }

}
