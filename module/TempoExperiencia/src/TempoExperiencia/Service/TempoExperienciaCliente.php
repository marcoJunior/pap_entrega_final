<?php

namespace TempoExperiencia\Service;

use Zend\Stdlib\Parameters;
use TempoExperiencia\Exception;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use TempoExperiencia\Entity;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class TempoExperienciaCliente extends APIGrid
{

    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperTempoExperienciaCliente()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    public function selecionarId($id, $sistema)
    {
        $tempoExperiencia = $this->mapperTempoExperienciaCliente()->selecionarId($id, $sistema);
        if (!$tempoExperiencia) {
            throw new Exception\UnexpectedValueException('Tempo de experiencia não encontrado.');
        }
        return $tempoExperiencia->toArray();
    }

    public function adicao(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $tempoExperiencia = new Entity\TempoExperiencia($formulario);

        if (isset($this->validaExistencia($tempoExperiencia)['mensagem'])) {
            return ['mensagem' => 'Já existe um cadastro para está empresa!'];
        }

        return $this->mapperTempoExperienciaCliente()->adicao($tempoExperiencia);
    }

    public function validaExistencia($cliente)
    {
        if ($this->mapperTempoExperienciaCliente()->validaExistencia($cliente)) {
            return ['mensagem' => 'Já existe um cadastro para está empresa!'];
        }
    }

    public function alterar(Parameters $post)
    {
        $formulario = $this->validarFormulario($post);

        if (isset($formulario['mensagem'])) {
            return $formulario;
        }

        $tempoExperiencia = new Entity\TempoExperiencia($formulario);

        return $this->mapperTempoExperienciaCliente()->alterar($tempoExperiencia);
    }

    public function excluir($id)
    {
        if ($id === null) {
            return ['mensagem' => 'Não foi possivel excluir o registro!'];
        }

        return $this->mapperTempoExperienciaCliente()->excluir($id);
    }

    public function validarFormulario($post)
    {
        if (empty($post)) {
            return ['mensagem' => 'Houve um erro!'];
        }

        $formulario = $post->get('formulario', null);

        if ($formulario === null) {
            return ['mensagem' => 'Não foi fornecido todas informações necessarias!'];
        }

        return $formulario;
    }

    /**
     * @return TempoExperiencia\Mapper\TempoExperienciaCliente
     */
    public function mapperTempoExperienciaCliente()
    {
        return $this->getServiceManager()->get(\TempoExperiencia\Mapper\TempoExperienciaCliente::class);
    }

}
