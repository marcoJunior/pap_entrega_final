<?php

namespace TempoExperiencia\View;

use Zend\View\Renderer\PhpRenderer;
use Zend\View\Model\ViewModel;

class TempoExperiencia extends ViewModel
{

    protected $template = 'tempo_experiencia/get-list';

    public function __construct(PhpRenderer $render, $variables = null, $options = null)
    {
        parent::__construct($variables, $options);

        $this->setCss($render);
        $this->setJs($render);
    }

    public function setCss($render)
    {
        foreach ($this->getCss($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $css = $arquivo . $this->getVariable('v');
            } else {
                $css = $render->basePath($arquivo . $this->getVariable('v'));
            }

            $render->headLink()->appendStylesheet($css);
        }
    }

    public function getCss($render)
    {
        return [
        ];
    }

    public function setJs($render)
    {
        foreach ($this->getJs($render) as $arquivo) {
            if (count(explode("http", $arquivo)) > 1) {
                $js = $arquivo . $this->getVariable('v');
            } else {
                $js = $render->basePath($arquivo . $this->getVariable('v'));
            }
            $render->headScript()->appendFile($js);
        }
    }

    public function getJs($render)
    {
        return [
            '../APIGridGitLab/public/js/api_grid_v2.js',
            '../app/manutencao/formulario.js',
            '../app/manutencao/tempo_experiencia/index.js',
            '../app/manutencao/tempo_experiencia/indexEmpresas.js',
            '../app/manutencao/tempo_experiencia/indexSistema.js',
        ];
        ;
    }

    public function grid()
    {
        $view = new ViewModel();
        $view->setTemplate("tempo_experiencia/grid");
        $view->setCaptureTo("grid");

        $this->addChild($view);
    }

    public function formulario()
    {
        $view = new ViewModel();
        $view->setTemplate("tempo_experiencia/formulario");
        $view->setCaptureTo("formulario");

        $this->addChild($view);
    }

    public function modal()
    {
        $view = new ViewModel();
        $view->setTemplate("tempo_experiencia/modal");
        $view->setCaptureTo("modal");

        $this->addChild($view);
    }

}
