<?php

namespace Usuarios;

use Zend\Stdlib\Hydrator\ClassMethods;
use Autenticacao\Entity\Usuario;
use Usuarios\Entity;

class Module
{

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                Service\Usuarios::class => function ($sm) {
                    $service = new Service\Usuarios();
                    return $service->setServiceManager($sm);
                },
                Service\TiposPermissoes::class => function ($sm) {
                    $service = new Service\TiposPermissoes();
                    return $service->setServiceManager($sm);
                },
                Service\Permissoes::class => function ($sm) {
                    $service = new Service\Permissoes();
                    return $service->setServiceManager($sm);
                },
                //////////////////
                'Usuarios\Mapper\Usuarios' => function ($sm) {
                    $mapper = new Mapper\Usuarios();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Usuario())
                        ->setHydrator(new ClassMethods());

                    return $mapper;
                },
                \Usuarios\Mapper\TiposPermissoes::class => function ($sm) {
                    $mapper = new Mapper\TiposPermissoes();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Entity\TiposPermissoes())
                        ->setHydrator(new ClassMethods());

                    return $mapper;
                },
                \Usuarios\Mapper\Permissoes::class => function ($sm) {
                    $mapper = new Mapper\Permissoes();
                    $mapper->setDbAdapter($sm->get('adapter'))
                        ->setEntityPrototype(new Entity\Permissoes())
                        ->setHydrator(new Mapper\Hydrator\Permissoes());

                    return $mapper;
                },
            ),
        );
    }
}
