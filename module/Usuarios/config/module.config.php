<?php

namespace Usuarios;

return [
    'controllers' => [
        'factories' => [
            Controller\UsuariosController::class => function ($sl) {
                $controller = new Controller\UsuariosController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            Controller\TiposPermissoesController::class => function ($sl) {
                $controller = new Controller\TiposPermissoesController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
            Controller\PermissoesController::class => function ($sl) {
                $controller = new Controller\PermissoesController();
                $controller->setService($sl->getServiceLocator());
                return $controller;
            },
        ],
    ],
    'router' => [
        'routes' => [
            'usuarios' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/usuarios[/:id]',
                    'defaults' => [
                        'controller' => Controller\UsuariosController::class,
                    ],
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ]
                ]
            ],
            'usuarios/tipos' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/usuarios/tipos[/:id]',
                    'defaults' => [
                        'controller' => Controller\TiposPermissoesController::class,
                    ],
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ]
                ]
            ],
            'permissoes' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/usuarios/tipos[/:id]/permissoes',
                    'defaults' => [
                        'controller' => Controller\PermissoesController::class,
                    ],
                    'constraints' => [
                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ]
                ]
            ],
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ]
    ]
];
