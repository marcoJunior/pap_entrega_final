<?php

namespace Usuarios\Controller;

use Application\Exception\Exception;
use APIGrid\Controller\APIGridController;
use Application\View\Application;
use Usuarios\View\TiposPermissoes;
use Usuarios\Entity\PesquisaUsuario;
use Zend\View\Model\JsonModel;

class TiposPermissoesController extends APIGridController
{

    public function getList()
    {

        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewUsuarios = new TiposPermissoes($phpRenderer);
            $viewUsuarios->setVariable('titulo', 'Tipos de usuarios');
            $viewUsuarios->grid();
            $viewUsuarios->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewUsuarios);

            return $app;
        }

        $service = $this->serviceTiposPermissoes();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        try {
            $retorno = $this->serviceTiposPermissoes()->selecionarPorId($id);
            $this->response->setStatusCode(200);
        } catch (Exception $err) {
            $retorno = ['mensagem' => $err->getMessage()];
            $this->response->setStatusCode($err->getCode());
        }
        return new JsonModel((array) $retorno);
    }

    public function delete($id)
    {
        try {
            $retorno = $this->serviceTiposPermissoes()->deletarPorId($id);
            $this->response->setStatusCode(204);
        } catch (Exception $err) {
            $retorno = ['mensagem' => $err->getMessage()];
            $this->response->setStatusCode($err->getCode());
        }
        return new JsonModel((array) $retorno);
    }

    public function update($id, $data)
    {
        try {
            $retorno = $this->serviceTiposPermissoes()->editarPorId($id, $data['formulario']);
            $this->response->setStatusCode(200);
        } catch (Exception $err) {
            $retorno = ['mensagem' => $err->getMessage()];
            $this->response->setStatusCode($err->getCode());
        }
        return new JsonModel($retorno);
    }

    public function create($data)
    {
        try {
            $retorno = $this->serviceTiposPermissoes()->inserir($data['formulario']);
            $this->response->setStatusCode(201);
        } catch (Exception $err) {
            $retorno = ['mensagem' => $err->getMessage()];
            $this->response->setStatusCode($err->getCode());
        }
        return new JsonModel((array) $retorno);
    }

    /**
     * @return \Usuarios\Service\TiposPermissoes
     */
    private function serviceTiposPermissoes()
    {
        return $this->getService()->get(\Usuarios\Service\TiposPermissoes::class);
    }
}
