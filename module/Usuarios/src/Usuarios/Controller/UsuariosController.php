<?php

namespace Usuarios\Controller;

use Application\Exception\Exception;
use APIGrid\Controller\APIGridController;
use Application\View\Application;
use Usuarios\View\Usuarios;
use Usuarios\Entity\PesquisaUsuario;
use Zend\View\Model\JsonModel;

class UsuariosController extends APIGridController
{

    public function getList()
    {

        $get = $this->getRequest()->getQuery();

        if ($get->get('draw', null) == null) {
            $phpRenderer = $this->getService()->get('ViewRenderer');
            $viewUsuarios = new Usuarios($phpRenderer);
            $viewUsuarios->setVariable('titulo', 'Usuarios');
            $viewUsuarios->grid();
            $viewUsuarios->formulario();

            $app = new Application($phpRenderer);
            $app->addChild($viewUsuarios);

            return $app;
        }

        $service = $this->serviceUsuarios();
        return new JsonModel($service->selecionar($get));
    }

    public function get($id)
    {
        $json = new JsonModel();
        try {
            $usuario = $this->serviceUsuarios()->selecionarPorId($id);
            $json->setVariables($usuario->toArray());
        } catch (Exception $err) {
            $json->setVariables(['mensagem' => $err->getMessage()]);
            $this->response->setStatusCode($err->getCode());
        }
        return $json;
    }

    public function delete($id)
    {
        $json = new JsonModel();
        try {
            $usuario = $this->serviceUsuarios()->deletarPorId($id);
            $this->response->setStatusCode(204);
        } catch (Exception $err) {
            $json->setVariables(['mensagem' => $err->getMessage()]);
            $this->response->setStatusCode($err->getCode());
        }
        return $json;
    }

    public function update($id, $data)
    {

        $json = new JsonModel();
        try {
            $usuario = $this->serviceUsuarios()->editarPorId($id, $data['formulario']);
            $json->setVariables($usuario->toArray());
        } catch (Exception $err) {
            $json->setVariables(['mensagem' => $err->getMessage()]);
            $this->response->setStatusCode($err->getCode());
        }
        return $json;
    }

    public function create($data)
    {
        $json = new JsonModel();
        try {
            $this->serviceUsuarios()->inserir($data);
            $this->response->setStatusCode(201);
        } catch (Exception $err) {
            $json->setVariables(['mensagem' => $err->getMessage()]);
            $this->response->setStatusCode($err->getCode());
        }
        return $json;
    }

    /**
     * @return \Usuarios\Service\Usuarios
     */
    private function serviceUsuarios()
    {
        return $this->getService()->get(\Usuarios\Service\Usuarios::class);
    }

    private function serviceCount()
    {
        return $this->getService()->get(\Application\Service\Count::class);
    }

}
