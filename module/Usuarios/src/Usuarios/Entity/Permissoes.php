<?php

namespace Usuarios\Entity;

use APIHelper\Entity\AbstractEntity;

class Permissoes extends AbstractEntity
{

    protected $id;
    protected $url;
    protected $get;
    protected $put;
    protected $post;
    protected $delete;
    protected $idTipo;
    protected $privilegios;

    public function getId()
    {
        return $this->id;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getGet()
    {
        return $this->get;
    }

    public function getPut()
    {
        return $this->put;
    }

    public function getPost()
    {
        return $this->post;
    }

    public function getDelete()
    {
        return $this->delete;
    }

    public function getIdTipo()
    {
        return $this->idTipo;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }

    public function setGet($get)
    {
        $this->get = (int) $get;
    }

    public function setPut($put)
    {
        $this->put = (int) $put;
    }

    public function setPost($post)
    {
        $this->post = (int) $post;
    }

    public function setDelete($delete)
    {
        $this->delete = (int) $delete;
    }

    public function setIdTipo($idTipo)
    {
        $this->idTipo = $idTipo;
    }

    public function setPrivilegios($privilegios)
    {
        $this->privilegios = $privilegios;
    }

    public function getPrivilegios()
    {
        $privileges = [];

        if ($this->getGet())
            array_push($privileges, 'get');

        if ($this->getPut())
            array_push($privileges, 'put');

        if ($this->getPost())
            array_push($privileges, 'post');

        if ($this->getDelete())
            array_push($privileges, 'delete');

        return $privileges;
    }

}
