<?php

namespace Usuarios\Entity;

use Application\Entity\Pesquisa;

class PesquisaUsuario extends Pesquisa
{
    public function __construct($array)
    {
        $this->ordenacao = 'id asc';
        $this->colunas = ['id', 'usuario', 'nome_completo'];
        $this->quantidade = 10;
        $this->pagina = 0;
        parent::__construct($array);
    }
}
