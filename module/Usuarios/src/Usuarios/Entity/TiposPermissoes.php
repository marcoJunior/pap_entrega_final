<?php

namespace Usuarios\Entity;

use APIHelper\Entity\AbstractEntity;

class TiposPermissoes extends AbstractEntity
{

    protected $id;
    protected $descricao;

    public function getId()
    {
        return $this->id;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setDescricao($descricao)
    {
        $this->descricao = $descricao;
    }

}
