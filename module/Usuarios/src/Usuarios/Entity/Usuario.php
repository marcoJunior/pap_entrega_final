<?php

namespace Usuarios\Entity;

use APIHelper\Entity\AbstractEntity;

class Usuario extends AbstractEntity
{

    protected $id;
    protected $usuario;
    protected $senha;
    protected $nomeCompleto;

    function getId()
    {
        return $this->id;
    }

    function getUsuario()
    {
        return $this->usuario;
    }

    function getSenha()
    {
        return $this->senha;
    }

    function getNomeCompleto()
    {
        return $this->nomeCompleto;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setUsuario($usuario)
    {
        $this->usuario = $usuario;
    }

    function setSenha($senha)
    {
        $this->senha = $senha;
    }

    function setNomeCompleto($nomeCompleto)
    {
        $this->nomeCompleto = $nomeCompleto;
    }

}
