<?php

namespace Usuarios\Mapper\Hydrator;

use APISql\Mapper\Hydrator\Hydrator;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class Permissoes extends Hydrator
{

    protected function getEntity()
    {
        return 'Usuarios\Entity\Permissoes';
    }

    public function getMap()
    {
        $arrayMap = [];

        return $arrayMap;
    }

    protected function getTemporary()
    {
        return [
            'id',
            'privilegios'
        ];
    }

    public static function getColuna($coluna)
    {
        $mapa = new Permissoes();
        return isset($mapa->getMap()[$coluna]) ? $mapa->getMap()[$coluna] : '';
    }

}
