<?php

namespace Usuarios\Mapper;

use APIGrid\Mapper\APIGrid;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Usuarios\Entity;
use Zend\Db\Sql\Where;

/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */
class TiposPermissoes extends APIGrid
{

    /**
     * @param  Pesquisa $pesquisa
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public $tableName = 'tipo';
    public $mapperName = 'Usuarios\Mapper\Hydrator\TiposPermissoes';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function selecionarObjeto()
    {
        $select = $this->getSelect();
        return $this->select($select);
    }

    /**
     * @param  int $id
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function selecionarPorId($id)
    {
        $select = $this->getSelect()->limit(1);
        $select->where->equalTo('id', $id);
        return $this->select($select);
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function editarPorId(Entity\TiposPermissoes $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where)->getAffectedRows();
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function deletarPorId($id)
    {
        return $this->delete(['id' => $id])->getAffectedRows();
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function inserir(Entity\TiposPermissoes $tipoPermissao)
    {
        return $this->insert($tipoPermissao)->getGeneratedValue();
    }

}
