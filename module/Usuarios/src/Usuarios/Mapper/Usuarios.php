<?php

namespace Usuarios\Mapper;

use Autenticacao\Entity;
use Autenticacao\Mapper\Usuarios as UsuariosAutenticacao;
use Application\Entity\Pesquisa;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use APIGrid\Mapper\APIGrid;
use APIGrid\Entity\Action\APIGrid as APIGridEntityAction;
use Zend\Db\Sql\Where;

/**
 * @author Vinicius Meira <vinicius@maxscalla.com.br>
 */
class Usuarios extends APIGrid
{

    /**
     * @param  Pesquisa $pesquisa
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public $tableName = 'usuario';
    public $mapperName = 'Usuarios\Mapper\Hydrator\Usuarios';

    public function selecionar(APIGridEntityAction $postEntity, FiltrosEntity $filtros)
    {

        $this->inicializar($this->tableName, $this->mapperName);
        $this->setColunas($this->getColunas());
        $this->setColunasTotalizador($this->getColunas());
        $this->setLimitOffset(true);

        $where = new Where();
        $where->notIn('usuario', ['admin', 'api']);
        $this->setWhere($where);

        try {
            return $this->getResultadoDb($postEntity);
        } catch (Exception $exc) {
            return false;
        }
    }

    /**
     * @param  int $id
     * @return \Zend\Db\ResultSet\HydratingResultSet
     */
    public function selecionarPorId($id)
    {
        $select = $this->getSelect()->limit(1);
        $select->where->equalTo('id', $id);
        return $this->select($select);
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function editarPorId(Entity\Usuario $entity)
    {
        $where = new Where();
        $where->equalTo('id', $entity->getId());

        return $this->update($entity, $where);
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function deletarPorId($id)
    {
        return $this->delete(['id' => $id]);
    }

    /**
     * @param  int $id
     * @return ResultInterface
     */
    public function inserir(Entity\Usuario $usuario)
    {
        $usuario->setIdConexao(1);
        $usuario->setIdTipo(0);
        return $this->insert($usuario);
    }

}
