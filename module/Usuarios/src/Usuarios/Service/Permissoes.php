<?php

namespace Usuarios\Service;

use APISql\Service\ConvertObject;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Application\Entity\Pesquisa;
use Application\Exception\Exception;
use Usuarios\Entity;
use Zend\Stdlib\Parameters;

class Permissoes extends APIGrid
{

    /**
     * Busca usuarios no banco
     * @param  Pesquisa $pesquisa
     * @return array
     */
    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperPermissoes()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    /**
     * Busca usuarios no banco e converte em array
     * @param  Pesquisa $pesquisa
     * @return \Zend\Db\ResultSet\HydratingResultSet|Entity\Usuario[]
     */
    public function selecionarArray(Pesquisa $pesquisa)
    {
        $usuarios = $this->selecionar($pesquisa);
        return ConvertObject::resultSetToArray($usuarios);
    }

    /**
     * Busca o id do usuário
     * @param  int  $id
     * @return Entity\Usuario
     */
    public function selecionarPorTipo($idTipo)
    {
        $permissoes = $this->mapperPermissoes()->selecionarPorId($idTipo);
        if (!$permissoes) {
            throw new Exception("Tipo de usuário #$idTipo não econtrado", 404);
        }
        return $permissoes;
    }
    /**
     * Busca o id do usuário
     * @param  int  $id
     * @return Entity\Usuario
     */
    public function selecionarPorId($id)
    {
        $permissoes = $this->mapperPermissoes()->selecionarPorId($id);
        if (!$permissoes) {
            throw new Exception("Usuário #$id não econtrado", 404);
        }
        return ConvertObject::convertObject($permissoes);
    }

    /**
     * Verifica se o usuário existe e apaga do banco
     * @param  int  $id
     * @throws Exception
     */
    public function deletarPorId($id)
    {
        return $this->mapperPermissoes()->deletarPorId($id);
    }

    /**
     * Verifica se o usuário existe e atualiza os dados no banco
     * @param  int      $id
     * @param  array    $data   Dados enviados pelo cliente
     * @return Entity\Usuario
     * @throws Exception
     */
    public function editarPorId($id, array $data)
    {
        $tipoPermissao = new Entity\TiposPermissoes($data);

        $retorno = [];
        $retorno['tipoPermissao'] = $this->mapperTipoPermissoes()->editarPorId($tipoPermissao);

        foreach ($data['permissoes'] as $key => $value) {
            $permissao = new Entity\Permissoes($value);
            $retorno[$key] = $this->mapperPermissoes()->editarPorId($permissao);
        }

        return $retorno;
    }

    /**
     * Verifica se o usuário existe e atualiza os dados no banco
     * @param  int      $id
     * @param  array    $data   Dados enviados pelo cliente
     * @return Usuario
     * @throws Exception
     */
    public function inserir($data)
    {
        $tipoPermissao = new Entity\TiposPermissoes();
        $tipoPermissao->setDescricao($data['descricao']);

        $retorno = [];

        $retorno['idTipo'] = $this->mapperPermissoes()->inserir($tipoPermissao);

        foreach ($data['permissoes'] as $key => $value) {
            $permissao = new Entity\Permissoes($value);
            $permissao->setIdTipo($retorno['idTipo']);
            $retorno[$key] = $this->mapperPermissoes()->inserir($permissao);
        }

        return $retorno;
    }

    /**
     * @return \Usuarios\Mapper\Permissoes
     */
    protected function mapperPermissoes()
    {
        return $this->getServiceManager()->get(\Usuarios\Mapper\Permissoes::class);
    }
    /**
     * @return \Usuarios\Mapper\Permissoes
     */
    protected function mapperTipoPermissoes()
    {
        return $this->getServiceManager()->get(\Usuarios\Mapper\TiposPermissoes::class);
    }
}
