<?php

namespace Usuarios\Service;

use Application\Entity\Pesquisa;
use Application\Service\ServiceCrud;
use Application\Exception\Exception;
use Autenticacao\Entity;
use Autenticacao\Service\Usuarios as AutenticacaoService;
use APISql\Service\ConvertObject;
use APIGrid\Service\APIGrid;
use APIFiltro\Entity\Filtros as FiltrosEntity;
use Zend\Stdlib\Parameters;

class Usuarios extends APIGrid
{

    /**
     * Busca usuarios no banco
     * @param  Pesquisa $pesquisa
     * @return array
     */
    public function selecionar(Parameters $get)
    {
        $postTratado = $this->getPostTratado($get);
        $filtros = FiltrosEntity::get()->exchangeArray((array) $get->get("filtros"));
        $filtros->exchangeArray((array) $get);

        if (!isset($get)) {
            $filtros->setCodigo(null);
        }

        $retorno = $this->mapperUsuarios()->selecionar($postTratado, $filtros);
        $retorno->setDraw((int) $get->get('draw', 1));
        return $retorno->toArray();
    }

    /**
     * Busca usuarios no banco e converte em array
     * @param  Pesquisa $pesquisa
     * @return \Zend\Db\ResultSet\HydratingResultSet|Entity\Usuario[]
     */
    public function selecionarArray(Pesquisa $pesquisa)
    {
        $usuarios = $this->selecionar($pesquisa);
        return ConvertObject::resultSetToArray($usuarios);
    }

    /**
     * Busca o id do usuário
     * @param  int  $id
     * @return Entity\Usuario
     */
    public function selecionarPorId($id)
    {
        $usuario = $this->mapperUsuarios()->selecionarPorId($id)->current();
        if (!$usuario)
            throw new Exception("Usuário #$id não econtrado", 404);
        return $usuario;
    }

    /**
     * Verifica se o usuário existe e apaga do banco
     * @param  int  $id
     * @throws Exception
     */
    public function deletarPorId($id)
    {
        $this->selecionarPorId($id);
        $this->mapperUsuarios()->deletarPorId($id);
    }

    /**
     * Verifica se o usuário existe e atualiza os dados no banco
     * @param  int      $id
     * @param  array    $data   Dados enviados pelo cliente
     * @return Entity\Usuario
     * @throws Exception
     */
    public function editarPorId($id, array $data)
    {
        $this->selecionarPorId($id);
        $usuario = new Entity\Usuario($data);
        $usuario->setId($id);
        $result = $this->mapperUsuarios()->editarPorId($usuario);
        if (!$result->getAffectedRows())
            throw new Exception("Não foi possivel editar Usuário $id", 400);
        return $usuario;
    }

    /**
     * Verifica se o usuário existe e atualiza os dados no banco
     * @param  int      $id
     * @param  array    $data   Dados enviados pelo cliente
     * @return Usuario
     * @throws Exception
     */
    public function inserir(array $data)
    {
        $usuario = new Entity\Usuario($data);
        $this->mapperUsuarios()->inserir($usuario);
    }

    /**
     * @return \Usuarios\Mapper\Usuarios
     */
    protected function mapperUsuarios()
    {
        return $this->getServiceManager()->get(\Usuarios\Mapper\Usuarios::class);
    }
}
