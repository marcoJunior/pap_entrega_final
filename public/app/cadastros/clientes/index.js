var serviceGridClientes = new serviceGrid();
var serviceGridEmpresas = new serviceGrid();

function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function calculaDiaVencimento() {
    var dataVencimento = moment().subtract(-$('#dias').val(), 'days').format('DD/MM/YYYY');
    $('#dataValidade').val(dataVencimento)
            .text(dataVencimento);
}

var moduleClientes = function () {
    this.linhaSelecionada = [];

    this.colunasClientes = [
        {'descricao': 'Código', 'tipo': 'number', 'data': 'codigo'},
        {'descricao': 'E-mail', 'tipo': 'text', 'data': 'email', 'width': '25%'},
        {'descricao': 'Empresa', 'tipo': 'text', 'data': 'empresa', 'width': '25%'},
        {'descricao': 'Nível', 'tipo': 'text', 'data': 'nivel'},
        {'descricao': 'Sistema', 'tipo': 'text', 'data': 'sistema'},
        {'data': 'Ativacao'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'},
    ];

    this.statusSelecionado = 1;
    this.status = {
        1: 'Consulta/Edição',
        2: 'Adição'
    };

    this.iniciaFormulario = function () {

        intervaloData();

        if ($('#data').val() == "undefined") {
            $('#data').val(moment().format('DD/MM/YYYY'));
        }
        $('#dias').on('change', function () {
            calculaDiaVencimento()
        });
        $('#dias').keyup(function () {
            calculaDiaVencimento()
        });
        $('#dias').keydown(function () {
            calculaDiaVencimento()
        });
        $('#dias').on('click', function () {
            VanillaToasts.create({
                title: 'Atenção !',
                text: 'Dia de vencimento sera calculado há partir do dia atual para gerar uma nova validade !',
                type: 'info', // success, info, warning, error   / optional parameter​
                timeout: 10000, // hide after 5000ms, // optional parameter
            });
            $('#data').val(moment().format('DD/MM/YYYY'));
        });
        var trElement;
        $('#datatable tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $('#datatable tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

    };

    this.iniciarGridClientes = function () {

        var colunasEspeClientes = [
            {
                'targets': [3],
                'render': function (data, type, row) {
                    data = data.toUpperCase().trim();
                    var colunas = {
                        'A': ['ADMIN', 'btn-success'],
                        'U': ['USUARIO', 'btn-info'],
                        'C': ['CLIENTE', 'btn-warning'],
                    };
                    try {
                        return '<center><a class="btn ' + colunas[data][1] + ' btn-xs">' +
                                colunas[data][0] + '</a></center>';
                    } catch (ex) {
                        return '<center><a class="btn btn-xs">' +
                                'Não definido</a></center>';
                    }

                }
            },
            {
                'targets': [4],
                'orderable': false,
                "render": function (data, type, row) {
                    var sistema = '<a href="https://' + row.sistema + '" target="_blanck">' + row.sistema + '</a>';
                    return '<center>' + sistema + '</center>';
                }
            },
            {
                'targets': [5],
                'orderable': false,
                "render": function (data, type, row) {
                    var checked = row.ativo == 1 ? true : false;
                    var btnCheckbox = 'Não definido';
                    if (checked) {
                        btnCheckbox = '<i class="fa fa-check" style="font-size:18px; color:#449D44;"></i>';
                    } else {
                        btnCheckbox = '<i class="fa fa-close" style="font-size:18px; color:#BF0B0B;"></i>';
                    }

                    return '<center>' + btnCheckbox + '</center>';
                }
            },
            {
                'targets': [6],
                'orderable': false,
                "render": function (data, type, row) {
                    if (typeof initClientes.linhaSelecionada == "undefined") {
                        initClientes.linhaSelecionada = {};
                    }
                    initClientes.linhaSelecionada[(row.serie + '-' + row.modulo)] = row;
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initClientes.clickBotaoConsultar('
                            + '\'' + (row.serie + '-' + row.modulo) + '\'' +
                            ');" class="btn btn-info btn-xs BtnDetalhes">' +
                            '<i class="fa fa-search-plus"></i></button>';

                    return '<center>' + btnConsultar + '</center>';
                }
            },
            {
                'targets': [7],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row.email + "'";
                    var sistema = "'" + row.sistema + "'";
                    var btnExcluir = '<button type="button" ' +
                            ' onclick="initClientes.confirmarExclusaoRegistro('
                            + row.codigo + ',' + fantasia + ',' + sistema +
                            ');" class="btn btn-danger btn-xs BtnExcluir">' +
                            '<i class="fa fa-trash-o"></i></button>';

                    return '<center>' + btnExcluir + '</center>';
                }
            }

        ];

        serviceGridClientes.carregarGrid(
                '#datatable',
                location.pathname + '/selecionar',
                this.colunasClientes,
                colunasEspeClientes,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatable_Filtros')
                    });
                }
        );
    };

    this.transicaoLayout = function (formVisivel) {
        if (formVisivel) {
            $('.Grid').hide(600);
            $('.Formulario').show(600);
        } else {
            $('.Formulario').hide(600);
            $('.Grid').show(600);
        }
    };

    this.adicionarRegistro = function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $.each(this.colunasClientes, function (index, value) {
            $('[name=' + value.data + ']').val(null);
        });
        $('#data').val(moment().format('DD/MM/YYYY'));
    };

    this.clickBotaoConsultar = function (codigo) {
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(initClientes.linhaSelecionada[codigo], function (index, value) {
            if (index == "empresa") {
                $('[name=' + index.trim() + ']').val($('[name=serie]').val() + ' - ' + value.toUpperCase());
            } else if (index == "sistema") {
                if (value.trim().indexOf('csgestor') > -1) {
                    $('[name=' + index.trim() + ']').val('csgestor');
                } else if (value.trim().indexOf('csb2b') > -1) {
                    $('[name=' + index.trim() + ']').val('app.csb2b');
                }
            } else {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });
    };

    this.setLabelStatus = function (codigo) {
        this.statusSelecionado = codigo;
        $('#statusForm').text(this.status[codigo]);
    };

    this.confirmarAlteracaoDeRegistro = function () {
        var codigo = $('#serie').val();
        var fantasia = $('#empresa').val().split(' - ')[1];

        var form = $('#clientesWeb');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = initClientes.status[initClientes.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            initClientes.alterarRegistro(codigo, fantasia);
        });

    };

    this.alterarRegistro = function (codigo, fantasia) {
        var form = $('#clientesWeb');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var url = initClientes.statusSelecionado == 1 ? '/alterar' : '/adicao';

        $.ajax({
            type: "POST",
            url: location.pathname + url,
            timeout: 1000,
            data: {
                'formulario': valFormulario
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridClientes.getDatarTable().ajax.reload();
                load.esconde();
                initClientes.transicaoLayout(false);

                if (data[0] !== "Cadastro já existente !") {
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro alterado com sucesso !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: data[0],
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

    this.confirmarExclusaoRegistro = function (codigo, fantasia, sistema) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            initClientes.excluirRegistro(codigo);
        });

    };

    this.excluirRegistro = function (codigo) {
        $.ajax({
            type: "POST",
            url: location.pathname + '/excluir',
            data: {
                'codigo': codigo,
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridClientes.getDatarTable().ajax.reload();
                load.esconde();
                VanillaToasts.create({
                    title: 'Sucesso !',
                    text: 'Registro excluido !',
                    type: 'success', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });

            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

    this.carregarGridEmpresas = function () {
        var colunasEmpresas = [
            {'descricao': 'Código serie', 'tipo': 'number', 'data': 'serie'},
            {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'empresa', 'width': '25%'},
            {'descricao': 'IP', 'tipo': 'text', 'data': 'ip'},
            {'descricao': 'Banco de dados', 'tipo': 'text', 'data': 'banco'},
            {'descricao': 'Porta', 'tipo': 'text', 'data': 'porta'},
            {'descricao': 'Dominio', 'tipo': 'text', 'data': 'dominio'},
                    // {'data': 'Selecionar'},
        ];

        var colunasEspeEmpresas = [
            {
                'targets': [6],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row['empresa'] + "'";
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initClientes.selecionarEmpresa('
                            + row.serie + ',' + fantasia +
                            ');" class="btn btn-info btn-xs BtnDetalhes">' +
                            '<i class="fa fa-arrow-right"></i></button>';

                    return '<center>' + btnConsultar + '</center>';

                }
            }

        ];

        serviceGridEmpresas.carregarGrid(
                '#datatableEmpresas',
                '/cadastros/empresas/selecionar',
                colunasEmpresas,
                null, //colunasEspeEmpresas,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatableEmpresas_Filtros')
                    });
                }
        );
    };

    this.selecionarEmpresa = function (serie, fantasia) {
        $('#serie').val(serie);
        $('#fantasia').val(serie + ' - ' + fantasia);
        $('.bs-example-modal-lg-g').click();
    };

}

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

function selectDate() {
    serviceGridClientes.getDatarTable().ajax.reload();
    selecionaTable($('#datatable tr'));
}

function selecionaTable(local) {
    if ($('#grid').is(":visible")) {
        local.eq(0).click();
        local.eq(0).attr("tabindex", -1).focus();
    }
}

$(document).ready(function () {
    initClientes.iniciarGridClientes();
    initClientes.iniciaFormulario();
    initClientes.carregarGridEmpresas();

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('#adicionarTicket').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

    $(window).resize(function () {
        serviceGridClientes.getDatarTable().ajax.reload();
        serviceGridEmpresas.getDatarTable().ajax.reload();
    });

});

var initClientes = new moduleClientes();
