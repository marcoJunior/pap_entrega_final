var serviceGridEmpresas = new serviceGrid();

function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

var moduleEmpresas = function () {

    this.linhaSelecionada = [];

    this.colunasEmpresas = [
        {'descricao': 'Código serie', 'tipo': 'number', 'data': 'serie'},
        {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'empresa', 'width': '25%'},
        {'descricao': 'IP', 'tipo': 'text', 'data': 'ip'},
        {'descricao': 'Banco de dados', 'tipo': 'text', 'data': 'banco'},
        {'descricao': 'Porta', 'tipo': 'text', 'data': 'porta'},
        {'descricao': 'Dominio', 'tipo': 'text', 'data': 'dominio'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'},
    ];

    this.statusSelecionado = 1;
    this.status = {
        1: 'Consulta/Edição',
        2: 'Adição'
    };

    this.iniciaFormulario = function () {

        intervaloData();

        $(".GerarTabelas").click(function () {
            initEmpresas.gerarTabelas();
        });

        $(".TestarConexao").click(function () {
            initEmpresas.testarConexao();
        });

        var trElement;
        $('#datatable tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $('#datatable tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

    };

    this.testarConexao = function () {
        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: initEmpresas.getDados(),
                acao: 'testarConexao'
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                if ('teste' in data && data.teste) {
                    $("body").data("ct", true);
                    toastr.success('Conexão verificada com sucesso.', 'Informação!');
                    return false;
                } else if ('teste' in data && !data.teste) {
                    $("body").data("ct", false);
                    toastr.error('Conexão falhou.');
                    return false;
                }

            },
            error: function (retorno) {
                load.esconde();
                if (retorno.responseText.search("pg_connect") > 0) {
                    toastr.error('Conexão falhou.');
                }
            },
            complete: function () {
                load.esconde();
            }
        });
    }

    this.gerarTabelas = function () {

        $.ajax({
            type: "POST",
            url: '/gerar/tabelas',
            data: {
                dados: initEmpresas.getDados()
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                load.esconde();

                if (data[0] === 'erro') {
                    toastr.error('Conexão falhou.');
                    return false;
                }

                var html = '';
                var contador = 1;
                $.each(data, function (tabela, desctabela) {
                    html += '<tr><th scope="row">' + contador + '</th><td>' + tabela + '</td><td>' + desctabela + '</td><td>Tabela gerada com sucesso!</td></tr>';
                    contador++;
                });

                $("#tabelasCriadas tbody").html(html);
                $("#tabelasCriadas").modal("toggle");
            }
        });
    };

    this.getDados = function () {
        var serialize = $("#empresas").serializeArray();
        var json = 'csb2b: ' + $(".csb2b").val() + ', csgestor: ' + $(".csgestor").val() + '';
        serialize[serialize.length] = {name: 'projetos', value: json};
        return serialize;
    };

    this.iniciarGridEmpresas = function () {

        var colunasEspeEmpresas = [
            {
                'targets': [6],
                'orderable': false,
                "render": function (data, type, row) {
                    if (typeof initEmpresas.linhaSelecionada == "undefined") {
                        initEmpresas.linhaSelecionada = {};
                    }
                    initEmpresas.linhaSelecionada[(row.serie + '-' + row.empresa)] = row;

                    var fantasia = "'" + row.empresa + "'";
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initEmpresas.clickBotaoConsultar('
                            + row.serie + ',' + fantasia +
                            ');" class="btn btn-info btn-xs BtnDetalhes">' +
                            '<i class="fa fa-search-plus"></i></button>';

                    return '<center>' + btnConsultar + '</center>';

                }
            },
            {
                'targets': [7],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row['empresa'] + "'";
                    var btnExcluir = '<button type="button" ' +
                            ' onclick="initEmpresas.confirmarExclusaoRegistro('
                            + row['serie'] + ',' + fantasia +
                            ');" class="btn btn-danger btn-xs BtnExcluir">' +
                            '<i class="fa fa-trash-o"></i></button>';

                    return '<center>' + btnExcluir + '</center>';
                }
            }

        ];

        serviceGridEmpresas.carregarGrid(
                '#datatable',
                location.pathname + '/selecionar',
                this.colunasEmpresas,
                colunasEspeEmpresas,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatable_Filtros')
                    });
                }
        );
    };

    this.transicaoLayout = function (formVisivel) {
        if (formVisivel) {
            $('.Grid').hide(600);
            $('.Formulario').show(600);
        } else {
            $('.Formulario').hide(600);
            $('.Grid').show(600);
        }
    };

    this.adicionarRegistro = function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $.each(this.colunasEmpresas, function (index, value) {
            $('[name=' + value.data + ']').val(null);
        });

        var limpaFormulario = [
            'login', 'subdominio', 'qtdLogins',
            'iptintometrico', 'portatintometrico', 'bancotintometrico',
            'csgestor', 'csb2b',
        ];

        $.each(limpaFormulario, function (index, value) {
            $('[name=' + value + ']').val(null);
        });

        $.each(['qtdClientes', 'qtdMb'], function (index, value) {
            $('[name=' + value + ']').val(150);
        });

        $.each(['cielo', 'pagseguro', 'moip'], function (index, value) {
            $('[name=' + value + ']').val(0);
        });

    };

    this.clickBotaoConsultar = function (codigo, fantasia) {
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(initEmpresas.linhaSelecionada[codigo + "-" + fantasia], function (index, value) {
            $('[name=' + index.trim() + ']').val(value.trim());

            if (index === "projetos") {
                $.each(value.split(','), function (i, val) {
                    $("." + val.split(':')[0].trim()).val(val.split(':')[1].trim());
                });
            }

        });
    };

    this.setLabelStatus = function (codigo) {
        this.statusSelecionado = codigo;
        $('#statusForm').text(this.status[codigo]);
    };

    this.confirmarAlteracaoDeRegistro = function () {
        var codigo = $('#serie').val();
        var fantasia = $('#empresa').val();

        var form = $('#empresas');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        var json = 'csb2b: ' + $(".csb2b").val() + ', csgestor: ' + $(".csgestor").val() + '';
        valFormulario['projetos'] = json;

        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = initEmpresas.status[initEmpresas.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            initEmpresas.alterarRegistro(valFormulario);
        });

    };

    this.alterarRegistro = function (formulario) {
        var url = initEmpresas.statusSelecionado == 1 ? '/alterar' : '/adicao';

        formularioEmpresas.statusSelecionado = initEmpresas.statusSelecionado;
        formularioEmpresas.alterarRegistro(formulario, '/clientes');

        $.ajax({
            type: "POST",
            url: location.pathname + url,
            data: {
                'formulario': formulario
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridEmpresas.getDatarTable().ajax.reload();
                load.esconde();
                initEmpresas.transicaoLayout(false);

                if (data[0] !== "Cadastro já existente !" && data[0] !== 0) {
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro alterado com sucesso !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    var erro = data[0];
                    if (data[0] === 0) {
                        erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                    }
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: erro,
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

    this.confirmarExclusaoRegistro = function (codigo, fantasia) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            initEmpresas.excluirRegistro(codigo, fantasia);
        });

    };

    this.excluirRegistro = function (codigo, fantasia) {
        $.ajax({
            type: "POST",
            url: location.pathname + '/excluir',
            data: {
                'codigo': codigo,
                'empresa': fantasia
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridEmpresas.getDatarTable().ajax.reload();
                load.esconde();
                VanillaToasts.create({
                    title: 'Sucesso !',
                    text: 'Registro excluido !',
                    type: 'success', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });

            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

}

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

function selectDate() {
    serviceGridEmpresas.getDatarTable().ajax.reload();
    selecionaTable($('#datatable tr'));
}

function selecionaTable(local) {
    if ($('#grid').is(":visible")) {
        local.eq(0).click();
        local.eq(0).attr("tabindex", -1).focus();
    }
}

$(document).ready(function () {
    initEmpresas.iniciarGridEmpresas();
    initEmpresas.iniciaFormulario();

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('#adicionarTicket').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

});

var initEmpresas = new moduleEmpresas();
