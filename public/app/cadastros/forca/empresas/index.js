var serviceGridForcaVendas = new serviceGrid();
var serviceGridEmpresas = new serviceGrid();

function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function calculaDiaVencimento() {
    var dataVencimento = moment().subtract(-$('#dias').val(), 'days').format('DD/MM/YYYY');
    $('#data').val(dataVencimento)
            .text(dataVencimento);
}

var moduleForcaVendas = function () {
    this.linhaSelecionada = [];

    this.colunasModuloValidade = [
        {'descricao': 'Código serie', 'tipo': 'number', 'data': 'serie'},
        {'tipo': 'text', 'data': 'fantasia', 'width': '25%'},
        {'descricao': 'IP', 'tipo': 'number', 'data': 'ip'},
        {'descricao': 'Banco', 'tipo': 'text', 'data': 'banco'},
        {'tipo': 'number', 'data': 'qtdAdiquirida'},
        {'tipo': 'number', 'data': 'qtdUso'},
        {'tipo': 'date', 'data': 'dataValidade'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'},
    ];

    this.statusSelecionado = 1;
    this.status = {
        1: 'Consulta/Edição',
        2: 'Adição'
    };

    this.iniciaFormulario = function () {

        new Formulario().initDropZoneLogo('#file-dropzone', '/dropzone/upload/forca-apk', '.apk', 'ForcaVendas.apk');

        intervaloData();

        if ($('#dataValidade').val() == "undefined") {
            $('#dataValidade').val(moment().format('DD/MM/YYYY'));
        }
        $('#dias').on('change', function () {
            calculaDiaVencimento();
        });
        $('#dias').keyup(function () {
            calculaDiaVencimento();
        });
        $('#dias').keydown(function () {
            calculaDiaVencimento();
        });
        $('#dias').on('click', function () {

            if ($('#vanillatoasts-container').html() === '') {

                VanillaToasts.create({
                    title: 'Atenção !',
                    text: 'Dia de vencimento sera calculado há partir do dia atual para gerar uma nova validade !',
                    type: 'info', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
            }
            $('#dataValidade').val(moment().format('DD/MM/YYYY'));
        });
        var trElement;
        $('#datatable tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $('#datatable tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

    };

    this.iniciarGridModuloValidade = function () {

        var colunasEspeModuloValidade = [
            {
                'targets': [7],
                'orderable': false,
                "render": function (data, type, row) {
                    if (typeof initForcaVendas.linhaSelecionada == "undefined") {
                        initForcaVendas.linhaSelecionada = {};
                    }
                    initForcaVendas.linhaSelecionada[(row.serie + '-' + row.modulo)] = row;
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initForcaVendas.clickBotaoConsultar('
                            + '\'' + (row.serie + '-' + row.modulo) + '\'' +
                            ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                            '<i class="fa fa-search"></i></button>';

                    return '<center>' + btnConsultar + '</center>';
                }
            },
            {
                'targets': [8],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row.fantasia + "'";
                    var sistema = "'" + row.sistema + "'";
                    var btnExcluir = '<button type="button" ' +
                            ' onclick="initForcaVendas.confirmarExclusaoRegistro('
                            + row.serie + ',' + fantasia + ',' + sistema +
                            ');" class="btn bg-red-soft btn-xs BtnExcluir">' +
                            '<i class="fa fa-trash"></i></button>';

                    return '<center>' + btnExcluir + '</center>';
                }
            }

        ];

        serviceGridForcaVendas.carregarGrid(
                '#datatable',
                location.pathname + 'Selecionar',
                this.colunasModuloValidade,
                colunasEspeModuloValidade,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatable_Filtros')
                    });
                }
        );
    };

    this.transicaoLayout = function (formVisivel) {
        if (formVisivel) {
            $('.Grid').hide(600);
            $('.Formulario').show(600);
        } else {
            $('.Formulario').hide(600);
            $('.Grid').show(600);
        }
    };

    this.adicionarRegistro = function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        var form = $('#forcaCadastro');
        form[0].reset();

        $('#data').val(moment().format('DD/MM/YYYY'));

        CodigoEmpresaForca = 0;
        QtdLicencas = $('[name=qtdAdiquirida]').val();
        serviceGridSeries.getDatarTable().ajax.reload();

    };

    this.uploadApk = function () {
        $('#file-dropzone').html('');
        $('.UploadLogo').modal();
    };

    this.clickBotaoConsultar = function (codigo) {

        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(initForcaVendas.linhaSelecionada[codigo], function (index, value) {
            if (index == "fantasia") {
                $('[name=' + index.trim() + ']').val($('[name=serie]').val() + ' - ' + value);
            } else {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });
        setTimeout(function () {
            initSerie.clickBotaoConsultar(codigo);
        }, 100);
    };

    this.setLabelStatus = function (codigo) {
        this.statusSelecionado = codigo;
        $('#statusForm').text(this.status[codigo]);
    };

    this.confirmarAlteracaoDeRegistro = function () {
        var codigo = $('#serie').val();
        var fantasia = $('#fantasia').val().split(' - ')[1];

        var form = $('#forcaCadastro');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (value == "" && index != "dias" && index != "dataValidade") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = initForcaVendas.status[initForcaVendas.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            initForcaVendas.alterarRegistro(codigo, fantasia);
        });

    };

    this.alterarRegistro = function (codigo, fantasia) {
        var form = $('#forcaCadastro');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var url = initForcaVendas.statusSelecionado == 1 ? 'Alterar' : 'Adicao';
        valFormulario['fantasia'] = fantasia;
        if (valFormulario['dataValidade'] !== "") {
            valFormulario['dataValidade'] = moment(valFormulario['dataValidade'], 'DD/MM/YYYY').format('YYYY-MM-DD');
        }

        if ($('#dataValidade').val() === 'Contratado') {
            valFormulario['dataValidade'] = '';
        }

        this.atualizaCliente(url, valFormulario, 'localweb2');
        this.atualizaCliente(url, valFormulario, 'cliente');
    };



    this.atualizaCliente = function (url, formulario, tipo) {

        $.ajax({
            type: "POST",
            url: location.pathname + url,
            data: {
                'formulario': formulario,
                'tipo': tipo,
                'login': {
                    'ip': initForcaVendas.linhaSelecionada[CodigoEmpresaForca + '-undefined'].ip,
                    'banco': initForcaVendas.linhaSelecionada[CodigoEmpresaForca + '-undefined'].banco,
                    'porta': initForcaVendas.linhaSelecionada[CodigoEmpresaForca + '-undefined'].porta
                },
                'codigo': CodigoEmpresaForca,
                'licencas': QtdLicencas,
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridForcaVendas.getDatarTable().ajax.reload();
                load.esconde();
                initForcaVendas.transicaoLayout(false);

                if (data[0] !== "Não foi possivel executar a atualização !") {
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro alterado com sucesso !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: data[0],
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

    this.confirmarExclusaoRegistro = function (codigo, fantasia, modulo) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            initForcaVendas.excluirRegistro(codigo, modulo);
        });

    };

    this.excluirRegistro = function (codigo, modulo) {
        $.ajax({
            type: "POST",
            url: location.pathname + 'Excluir',
            data: {
                'codigo': codigo,
                'modulo': modulo
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridForcaVendas.getDatarTable().ajax.reload();
                load.esconde();
                VanillaToasts.create({
                    title: 'Sucesso !',
                    text: 'Registro excluido !',
                    type: 'success', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });

            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });
    };

    this.setAtualiza = function (tipo) {

        var form = $('#forcaCadastro');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        $.ajax({
            type: "POST",
            url: location.pathname + 'Atualiza',
            data: {
                'tipo': tipo,
                'formulario': valFormulario
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {

                load.esconde();
                if (data[0] == true) {
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Operação de atualização concluida !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: data[0],
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }

                serviceGridSeries.getDatarTable().ajax.reload();
            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel efetuar a atualização ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        });

    };

    this.carregarGridEmpresas = function () {
        var colunasEmpresas = [
            {'descricao': 'Código serie', 'tipo': 'number', 'data': 'serie'},
            {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'empresa', 'width': '25%'},
            {'descricao': 'IP', 'tipo': 'text', 'data': 'ip'},
            {'descricao': 'Banco de dados', 'tipo': 'text', 'data': 'banco'},
            {'descricao': 'Porta', 'tipo': 'text', 'data': 'porta'},
            {'descricao': 'Dominio', 'tipo': 'text', 'data': 'dominio'},
            {'data': 'Selecionar'},
        ];

        var colunasEspeEmpresas = [
            {
                'targets': [6],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row['empresa'] + "'";
                    var ip = "'" + row['ip'] + "'";
                    var banco = "'" + row['banco'] + "'";
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initForcaVendas.selecionarEmpresa('
                            + row.serie + ',' + fantasia + ','
                            + ip + ',' + banco +
                            ');" class="btn btn-info btn-xs BtnDetalhes">' +
                            '<i class="fa fa-arrow-right"></i></button>';

                    return '<center>' + btnConsultar + '</center>';

                }
            }

        ];

        serviceGridEmpresas.carregarGrid(
                '#datatableEmpresas',
                '/cadastros/empresas/selecionar',
                colunasEmpresas,
                colunasEspeEmpresas,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatableEmpresas_Filtros')
                    });
                }
        );
    };

    this.selecionarEmpresa = function (serie, fantasia, ip, banco) {
        $('#serie').val(serie);
        $('#fantasia').val(serie + ' - ' + fantasia);
        $('[name=ip]').val(ip);
        $('[name=banco]').val(banco);
        $('.bs-example-modal-lg-g').click();
    };

}

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

function selectDate() {
    serviceGridForcaVendas.getDatarTable().ajax.reload();
    selecionaTable($('#datatable tr'));
}

function selecionaTable(local) {
    if ($('#grid').is(":visible")) {
        local.eq(0).click();
        local.eq(0).attr("tabindex", -1).focus();
    }
}

$(document).ready(function () {
    initForcaVendas.iniciarGridModuloValidade();
    initForcaVendas.iniciaFormulario();
    initForcaVendas.carregarGridEmpresas();

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('#adicionarTicket').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

    $(window).resize(function () {
        serviceGridForcaVendas.getDatarTable().ajax.reload();
        serviceGridEmpresas.getDatarTable().ajax.reload();
    });

});

var initForcaVendas = new moduleForcaVendas();
