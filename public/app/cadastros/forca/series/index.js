var serviceGridSeries = new serviceGrid();
var CodigoEmpresaForca = 0;
var QtdLicencas = 0;

function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function calculaDiaVencimento() {
    var dataVencimento = moment().subtract(-$('#dias').val(), 'days').format('DD/MM/YYYY');
    $('#dataValidade').val(dataVencimento)
            .text(dataVencimento);
}

var moduleSerie = function () {
    this.linhaSelecionada = [];

    this.colunasModuloValidade = [
        {'descricao': 'Codigo', 'tipo': 'number', 'data': 'codigo'},
        {'descricao': 'Aparelho', 'tipo': 'number', 'data': 'aparelho'},
        {'descricao': 'Representante', 'tipo': 'number', 'data': 'representante'},
        {'descricao': 'Numero de série', 'tipo': 'number', 'data': 'numForca'},
        {'data': 'Limpar'},
        {'data': 'Excluir'},
    ];

    this.statusSelecionado = 1;
    this.status = {
        1: 'Consulta/Edição',
        2: 'Adição'
    };

    this.iniciaFormulario = function () {

        intervaloData();

        $('#datatableSerie tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $('#datatableSerie tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

    };

    this.iniciarGridSerieForca = function () {

        var colunasEspeForca = [
            {
                'targets': [4],
                'orderable': false,
                "render": function (data, type, row) {
                    var btnLimpar = '<button type="button" ' +
                            ' onclick="initSerie.confirmarLimpesaRegistro('
                            + row.codigo + ',' + row.representante +
                            ');" class="btn btn-warning btn-xs BtnLimpar">' +
                            '<i class="fa fa-refresh"></i></button>';

                    return '<center>' + btnLimpar + '</center>';
                }
            },
            {
                'targets': [5],
                'orderable': false,
                "render": function (data, type, row) {
                    var btnExcluir = '<button type="button" ' +
                            ' onclick="initSerie.confirmarExclusaoRegistro('
                            + row.codigo + ',' + row.representante +
                            ');" class="btn bg-red-soft btn-xs BtnExcluir">' +
                            '<i class="fa fa-close"></i></button>';

                    return '<center>' + btnExcluir + '</center>';
                }
            }

        ];

        serviceGridSeries.carregarGrid(
                '#datatableSerie',
                '/cadastros/forca/empresasSerie',
                this.colunasModuloValidade,
                colunasEspeForca,
                null,
                function (resultado) {

                },
                function () {
                    return                 {
                        'login': {
                            'ip': jQuery('[name=ip]').val(),
                            'banco': jQuery('[name=banco]').val(),
                            'porta': jQuery('[name=porta]').val()
                        },
                        'codigo': CodigoEmpresaForca,
                        'licencas': QtdLicencas,

                    };
                }
        );
    };

    this.transicaoLayout = function (formVisivel) {
        if (formVisivel) {
            $('.Grid').hide(600);
            $('.Formulario').show(600);
        } else {
            $('.Formulario').hide(600);
            $('.Grid').show(600);
        }
    };

    this.adicionarRegistro = function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $.each(this.colunasModuloValidade, function (index, value) {
            $('[name=' + value.data + ']').val(null);
        });

    };

    this.clickBotaoConsultar = function (codigo) {

        if (typeof serviceGridSeries.getDatarTable() === "undefined") {
            initSerie.iniciarGridSerieForca();
        } else {
            serviceGridSeries.getDatarTable().ajax.reload();
        }

        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(initSerie.linhaSelecionada[codigo], function (index, value) {
            if (index == "fantasia") {
                $('[name=' + index.trim() + ']').val($('[name=serie]').val() + ' - ' + value);
            } else if (index == "dataValidade") {
                $('[name=' + index.trim() + ']').text(value.trim());
            } else {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });
        CodigoEmpresaForca = $('[name=serie]').val();
        QtdLicencas = $('[name=qtdAdiquirida]').val();


//        initForcaVendas.setAtualiza('Provedor');

    };

    this.setLabelStatus = function (codigo) {
        this.statusSelecionado = codigo;
        $('#statusForm').text(this.status[codigo]);
    };

    this.confirmarAlteracaoDeRegistro = function () {
        var codigo = $('#serie').val();
        var fantasia = $('#fantasia').val().split(' - ')[1];

        var form = $('#modulosValidade');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = initSerie.status[initSerie.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            initSerie.alterarRegistro(codigo, fantasia);
        });

    };

    this.alterarRegistro = function (codigo, fantasia) {
        var form = $('#modulosValidade');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var url = initSerie.statusSelecionado == 1 ? '/alterar' : 'Adicao';

        $.ajax({
            type: "POST",
            url: location.pathname + url,
            data: {
                'formulario': valFormulario
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridSeries.getDatarTable().ajax.reload();
                initSerie.transicaoLayout(false);

                if (data[0] !== "Cadastro já existente !") {
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro alterado com sucesso !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: data[0],
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
            },
            error: function (e) {
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                console.log(e);
            }
        }).always(function () {
            load.esconde('html');
        });

    };

    this.confirmarLimpesaRegistro = function (codigo, codRep) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-warning">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de limpesa !',
            text: 'Deseja mesmo LIMPAR cadastro (<b>' + codigo +
                    ' - Representate: ' + codRep + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'warning', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-warning').on('click', function () {
            initSerie.limparRegistro(codigo, codRep);
        });

    };

    this.limparRegistro = function (codigo, codRep) {
        $.ajax({
            type: "POST",
            url: location.pathname + 'CodigoSerieLimpar',
            data: {
                'codigo': codigo,
                'codRep': codRep,
                'login': {
                    'ip': jQuery('[name=ip]').val(),
                    'banco': jQuery('[name=banco]').val(),
                    'porta': jQuery('[name=porta]').val()
                },
            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            serviceGridSeries.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro limpo !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel limpar o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });
    };

    this.confirmarExclusaoRegistro = function (codigo, codRep) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo EXCLUIR cadastro (<b>' + codigo +
                    ' - Representate: ' + codRep + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            initSerie.excluirRegistro(codigo, codRep);
        });

    };

    this.excluirRegistro = function (codigo, codRep) {
        $.ajax({
            type: "POST",
            url: location.pathname + 'CodigoSerieExcluir',
            data: {
                'codigo': codigo,
                'codRep': codRep,
                'login': {
                    'ip': jQuery('[name=ip]').val(),
                    'banco': jQuery('[name=banco]').val(),
                    'porta': jQuery('[name=porta]').val()
                },

            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            serviceGridSeries.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro excluido !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });

    };

    this.gerarCodigo = function (serie) {
        $.ajax({
            type: "POST",
            url: location.pathname + 'CodigoSerieGerar',
            data: {
                'serie': serie,
                'login': {
                    'ip': jQuery('[name=ip]').val(),
                    'banco': jQuery('[name=banco]').val(),
                    'porta': jQuery('[name=porta]').val()
                },
            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            serviceGridSeries.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro criado !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel criar o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });
    };

    this.carregarGridEmpresas = function () {
        var colunasEmpresas = [
            {'descricao': 'Código serie', 'tipo': 'number', 'data': 'serie'},
            {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'empresa', 'width': '25%'},
            {'descricao': 'IP', 'tipo': 'text', 'data': 'ip'},
            {'descricao': 'Banco de dados', 'tipo': 'text', 'data': 'banco'},
            {'descricao': 'Porta', 'tipo': 'text', 'data': 'porta'},
            {'descricao': 'Dominio', 'tipo': 'text', 'data': 'dominio'},
            {'data': 'Selecionar'},
        ];

        var colunasEspeEmpresas = [
            {
                'targets': [6],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row['empresa'] + "'";
                    var btnConsultar = '<button type="button" ' +
                            ' onclick="initSerie.selecionarEmpresa('
                            + row.serie + ',' + fantasia +
                            ');" class="btn btn-info btn-xs BtnDetalhes">' +
                            '<i class="fa fa-arrow-right"></i></button>';

                    return '<center>' + btnConsultar + '</center>';

                }
            }

        ];

        serviceGridEmpresas.carregarGrid(
                '#datatableSerieEmpresas',
                '/cadastros/empresas/selecionar',
                colunasEmpresas,
                colunasEspeEmpresas,
                null,
                function (d) {
                    return $.extend({}, d, {
                        'filtros': getFiltros('.datatableSerieEmpresas_Filtros')
                    });
                }
        );
    };

    this.selecionarEmpresa = function (serie, fantasia) {
        $('#serie').val(serie);
        $('#fantasia').val(serie + ' - ' + fantasia);
        $('.bs-example-modal-lg-g').click();
    };

}

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

// function selectDate() {
// serviceGridSeries.getDatarTable().ajax.reload();
// selecionaTable($('#datatableSerie tr'));
// }

function selecionaTable(local) {
    if ($('#grid').is(":visible")) {
        local.eq(0).click();
        local.eq(0).attr("tabindex", -1).focus();
    }
}

$(document).ready(function () {
    initSerie.iniciaFormulario();

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('#adicionarTicket').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

    $(window).resize(function () {
        serviceGridSeries.getDatarTable().ajax.reload();
    });

});

var initSerie = new moduleSerie();
