/* @flow */

import qs from 'qs';
import { obrigatorio } from './utils';
import { TipoOpcaoEnum } from './types';

import type { Filtro as FiltroType, TipoOpcao, ValoresFormulario } from './types';

export default class Filtro {
    form: HTMLFormElement;
    quantidade: HTMLSelectElement;
    coluna: HTMLSelectElement;
    opcao: HTMLSelectElement;
    valor: HTMLInputElement;

    constructor(form: HTMLFormElement = obrigatorio()) {
        if (
            form.quantidade instanceof HTMLSelectElement &&
            form.coluna instanceof HTMLSelectElement &&
            form.opcao instanceof HTMLSelectElement &&
            form.valor instanceof HTMLInputElement
        ) {
            this.form = form;
            this.quantidade = form.quantidade;
            this.coluna = form.coluna;
            this.opcao = form.opcao;
            this.valor = form.valor;
            this.setEventoAlteraOpcao(this.coluna);
        }
    }

    getFiltro(): FiltroType | null {
        const valor = this.valor.value;
        if (!valor) return null;
        const opcao = this.opcao.value;
        const coluna = this.coluna.value;
        return { valor, opcao, coluna };
    }

    getQuantidade(): number {
        return Number(this.quantidade.value) || 10;
    }

    setEventoAlteraOpcao(select: HTMLSelectElement) {
        select.onchange = () => {
            const { dataset } = select.options[select.selectedIndex];
            const tipo = TipoOpcaoEnum[dataset.tipo];
            this.alterarOpcao(tipo);
            this.alterarTypeInput(tipo);
        };
    }

    /**
     * Altera os <option> do campo opcões de acordo como data-tipo do <select name="coluna">
     * @param  {String} tipo
     */
    alterarOpcao(tipo: TipoOpcao) {
        const options = Array.from(this.opcao.options);
        options.forEach((option) => {
            const display = option.dataset.tipo === tipo
                ? 'block'
                : 'none';

            option.style.display = display;
        });

        this.selecionarPrimeiroOptionValorVisivel();
    }

    /**
     * seleciona o primeiro <option> do <select name="opcao"> com display: block
     */
    selecionarPrimeiroOptionValorVisivel() {
        Array.from(this.opcao.options).every((option) => {
            if (option.style.display === 'block') {
                option.selected = true;
                return false;
            }
            return true;
        });
    }

    /**
     * Altera os <input> do campo valor de acordo como data-tipo do <select name="coluna">
     * @param  {String} tipo
     */
    alterarTypeInput(tipo: TipoOpcao) {
        if (tipo === 'number') {
            this.valor.type = 'number';
        } else if (tipo === 'string') {
            this.valor.type = 'search';
        }
    }

    /**
     * Seta os valores do objeto no formulario de acordo com as chaves
     * @param  {Object} obj
     */
    setValoresQuery() {
        const query: ValoresFormulario = qs.parse(location.search.substr(1));
        if (query.quantidade) {
            this.quantidade.value = String(query.quantidade);
        }
        if (query.filtro && query.filtro.valor) {
            this.valor.value = query.filtro.valor;
            this.coluna.value = query.filtro.coluna;
            this.opcao.value = query.filtro.opcao;
        }
    }
}
