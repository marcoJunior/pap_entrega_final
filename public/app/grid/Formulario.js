/* @flow */

import formSerialize from 'form-serialize';
import Inputmask from 'inputmask/dist/inputmask/inputmask';
import { notificarErro, xhr } from './utils';

import type { RespostaBuscaCep } from './types';

const buscarEnderecoPorCep = (cep: string): Promise<RespostaBuscaCep> =>
    xhr
    .get(`https://viacep.com.br/ws/${cep}/json/`)
    .then(response => response.data);

export default class Formulario {
    form: HTMLFormElement;
    portletGrid: HTMLElement;
    portletForm: HTMLElement;
    grid: Object;

    constructor(grid: Object) {
        const portletForm = grid.container.querySelector('.grid__formulario');
        const portletGrid = grid.container.querySelector('.grid__tabela');
        if (!portletForm || !portletGrid) {
            return;
        }
        const form = portletForm.querySelector('form:not(.grid__filtro)');
        if (!(form instanceof HTMLFormElement)) {
            return;
        }
        this.portletGrid = portletGrid;
        this.form = form;
        this.portletForm = portletForm;
        this.grid = grid;
    }

    /**
     * Seta os valores do objeto no formulario de acordo com as chaves
     * @param  {Object} obj
     */
    preencher(dados: Object) {
        Object.keys(dados).forEach((name) => {
            if (this.form[name] && typeof dados[name] !== 'undefined') {
                const input = this.form[name];
                const valor = dados[name];
                if (input.type === 'checkbox') {
                    input.checked = valor;
                } else if (input.type === 'checkbox' && input.value === valor) {
                    input.checked = true;
                } else {
                    input.value = valor;
                }
            }
        });
    }

    /**
     * Insere de acordo com os dados do formulario
     * @return {Promise}
     */
    inserir() {
        const dadosForm = this.getDadosForm();
        return xhr.post(this.grid.url, dadosForm, { load: true }).then(() => {
            this.esconder();
        }).catch((err) => {
            notificarErro(err);
            return Promise.reject(err);
        });
    }

    /**
     * Edita um usuario de acordo com o id e os dados do formulario
     * @return {Promise}
     */
    editar() {
        const dados = this.getDadosForm();
        const codigo = dados.id || dados.codigo;
        return xhr.put(`${this.grid.url}/${codigo}`, dados, { load: true }).then((obj) => {
            this.esconder();
            return obj;
        }).catch((err) => {
            notificarErro(err);
            return Promise.reject(err);
        });
    }

    reset() {
        this.form.reset();
    }

    /**
     * Monta um objeto de acoro com os dados do formulario
     * @return {Object}
     */
    getDadosForm(): Object {
        return formSerialize(this.form, { hash: true });
    }
    /**
     * Mostra o portlet de formulario e esconde o da grid
     */
    mostrar() {
        this.portletGrid.style.display = 'none';
        this.portletForm.style.display = 'block';
        const input = this.form.querySelector('[tabindex="0"]');
        if (input) {
            input.focus();
        }
    }

    /**
     * Esconde o portlet de formulario e mostra o da gri
     */
    esconder() {
        this.portletGrid.style.display = 'block';
        this.portletForm.style.display = 'none';
    }

    /**
     * Aplica a mascara de telefone ao input
     */
    setMask() {
        const telefone = this.form.elements.namedItem('telefone');
        const inputMask = new Inputmask('(99) 9999[9]-9999');
        inputMask.mask(telefone);
    }

    /**
     * Busca o endereço de acordo com o cep e seta no formulario
     * @return {Promise}
     */
    setEndereco(input: HTMLInputElement) {
        if (input.value.length < 8) {
            return Promise.resolve();
        }

        return buscarEnderecoPorCep(input.value).then((endereco) => {
            this.preencher({
                cep: endereco.cep,
                rua: endereco.logradouro,
                estado: endereco.uf,
                bairro: endereco.bairro,
                cidade: endereco.localidade,
            });
        });
    }

    /**
     * Seta o evento de buscar endereco
     */
    setBuscarEnderecoPorCep() {
        const cep = this.form.elements.namedItem('cep');
        if (cep instanceof HTMLInputElement) {
            cep.onchange = () => this.setEndereco(cep);

            const btn = document.getElementById('buscarCep');
            if (btn) {
                btn.onclick = () => this.setEndereco(cep);
            }
        }
    }
}
