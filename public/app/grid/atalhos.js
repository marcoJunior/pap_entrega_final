/* @flow */

import teclado from 'mousetrap';

const ATALHOS_PESQUISA = 'abcçdefghijklmnopqrstuvwxyz0123456789'.split('');

export default function setAtalhos(grid: Object) {
    teclado.bind('down', () => {
        grid.selecionarProxima();
    });

    teclado.bind('up', () => {
        grid.selecionarAnterior();
    });

    teclado.bind('right', () => {
        grid.paginacao.proximo();
    });

    teclado.bind('left', () => {
        grid.paginacao.anterior();
    });

    teclado.bind('enter', () => {
        const btns = grid.table.querySelectorAll('.selecionado .enter');
        Array.from(btns).forEach(btn => btn.click());
    });

    teclado.bind('space', () => {
        const btns = grid.table.querySelectorAll('.selecionado .space');
        Array.from(btns).forEach(btn => btn.click());
    });

    teclado.bind('del', () => {
        const btns = grid.table.querySelectorAll('.selecionado .del');
        Array.from(btns).forEach(btn => btn.click());
    });

    teclado.bind('escape', () => {
        const btns = grid.container.querySelectorAll('.esq');
        Array.from(btns).forEach(btn => btn.click());
    });

    teclado.bind(ATALHOS_PESQUISA, () => {
        const input = grid.filtro.form.elements.namedItem('valor');
        if (input instanceof HTMLInputElement) {
            input.value = '';
            input.focus();
        }
    });
}
