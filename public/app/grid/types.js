/* @flow */

export type Ordenacao = {
    coluna: string;
    ordem: string;
}

export type Count = {
    pagina: number,
    quantidadePaginas: number,
    totalRegistros: number,
}

export type Opcao = 'iniciando' | 'contendo' | '' | '<' | '>' | '<=' | '>=' | string;

export type Filtro = {
    valor: string,
    opcao: Opcao,
    coluna: string,
}

export type TipoOpcao = 'string' | 'number';

export type ValoresFormulario = {
    filtro: Filtro | null,
    ordenacao: string,
    pagina: number,
    quantidade: number,
    colunas: Array<string>,
}

export const TipoOpcaoEnum: {
    [string]: TipoOpcao,
} = {
    string: 'string',
    number: 'number',
};

export type Retorno<Object> = {
    count: Count,
    data: Array<Object>,
}

export type RespostaBuscaCep = {
    cep: string,
    logradouro: string, // rua
    complemento: string,
    bairro: string,
    localidade: string, // cidade
    uf: string,
    unidade: string,
    ibge: number,
    gia: number,
}

export type AxiosError = Error & {
    response: {
        data: Object & { mensagem: string },
        status: number,
    },
};
