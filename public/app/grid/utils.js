/* @flow */

import Notificacao from 'vanillatoasts';
import axios from 'axios';
import qs from 'qs';

import type { AxiosError } from './types';

/**
 * Metodo para usar como parametro default e deixa-lo obrigatorio
 * @throws TypeError
 */
export const obrigatorio = () => { throw new TypeError(); };

export const notificarErro = (err: AxiosError) => {
    const { status, data } = err.response;
    Notificacao.create({
        title: 'Houve um erro',
        text: status < 500 ? data.mensagem || data.message : '',
        type: 'error',
        timeout: 5000,
    });
};

const loadMask = document.getElementById('loading-mask');
const Loading = {
    hide() {
        if (loadMask) loadMask.style.display = 'none';
    },
    show() {
        if (loadMask) loadMask.style.display = 'block';
    },
};

export const xhr = axios.create();

let active = 0;

export const isActive = () => active > 0;

const fimDaRequisicaoComErro = (err) => {
    Loading.hide();
    active -= 1;
    return Promise.reject(err);
};

xhr.interceptors.request.use((config) => {
    if (
        typeof config.load !== 'undefined' &&
        (typeof config.load === 'undefined' ||
        config.load !== false)
    ) {
        Loading.show();
    }
    const headers = config.headers || {};
    Object.assign(headers, { 'X-Requested-With': 'XMLHttpRequest' });
    if (config.method === 'post') {
        config.data = qs.stringify(config.data);
        Object.assign(headers, { 'Content-Type': 'application/x-www-form-urlencoded' });
    }

    config.paramsSerializer = params => qs.stringify(params);

    active += 1;
    return config;
}, fimDaRequisicaoComErro);

xhr.interceptors.response.use((response) => {
    Loading.hide();
    active -= 1;
    return response;
}, fimDaRequisicaoComErro);
