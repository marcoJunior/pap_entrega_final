import $ from 'jquery';
import VanillaToasts from 'vanillatoasts'

        const Form = document.forms.namedItem('form-login');

function entrar() {
    return $.post('/entrar', {
        usuario: Form.usuario.value,
        senha: Form.senha.value,
        lembrar: Form.lembrar && Form.lembrar.checked ? '1' : '',
    });
}

window.debug = false;

function redirecionar() {
    if (window.debug === false) {
        location.pathname = '/dashboard';
    }
}

function alertar(err) {
    VanillaToasts.create({
        title: 'Atenção !',
        text: JSON.parse(err.responseText).mensagem,
        type: 'error',
        timeout: '3000',
    });
}

Form.addEventListener('submit', (e) => {
    e.preventDefault();
    entrar()
            .done((retorno) => location.pathname = retorno.url)//redirecionar())
            .fail(err => alertar(err));
    return false;
});
