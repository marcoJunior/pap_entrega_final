var serviceGridLogsAcessos = new serviceGrid();

var moduleLogsAcesso = function () {
    this.linhaSelecionada =  [];

    this.colunasModuloValidade = [
        {'descricao': 'Data', 'tipo': 'date', 'data': 'data'},
        {'descricao': 'IP', 'tipo': 'text', 'data': 'ip'},
        {'tipo': 'text', 'data': 'login'},
        {'descricao': 'Sistema', 'tipo': 'text', 'data': 'sistema'},
        {'descricao': 'Apelido', 'tipo': 'text', 'data': 'apelido'},
        // {'data': 'Detalhes'},
    ];

    this.statusSelecionado = 1;
    this.status = {
      1 : 'Consulta/Edição',
      2 : 'Adição'
    };

    this.iniciaFormulario = function () {

        intervaloData();

        var trElement;
        $('#datatable tbody').on('touchstart', function (e) {
            if (typeof trElement !== "undefined" && trElement === e) {
                $(this).find('.BtnDetalhes').click();
            }
            trElement = e;
        });

        $('#datatable tbody').on('dblclick', 'tr', function (e) {
            $(this).find('.BtnDetalhes').click();
        });

    };

    this.iniciarGridLogsAcessos = function () {

        var colunasEspeModuloValidade = [
            {
                'targets': [2],
                'render': function (data, type, row) {
                    data = data.toUpperCase().trim();
                    var colunas = {
                        '1': ['CSB2B', 'btn-success'],
                        '2': ['TERMINAL', 'btn-info'],
                        '3': ['CSGESTOR', 'btn-warning']
                    };
                    try {
                        return '<center><a class="btn ' + colunas[data][1] + ' btn-xs">' +
                            colunas[data][0] + '</a></center>';
                    } catch (ex) {
                        return '<center><a class="btn btn-xs">' +
                            'Não definido</a></center>';
                    }

                }
            },
            {
                'targets': [6],
                'render': function (data, type, row) {
                    try {
                        var btnAviso = "btn-info";
                        if(data < 10){
                            btnAviso = "btn-danger";
                       } else if(data < 20){
                           btnAviso = "btn-warning";
                       }else if(data > 30){
                             btnAviso = "btn-success";
                        }
                        return '<center><a class="btn ' + btnAviso + ' btn-xs">' + data + ' dias</a></center>';
                    } catch (ex) {
                        return '<center><a class="btn btn-xs">' +
                            'Não definido</a></center>';
                    }

                }
            },
            {
                'targets': [7],
                'orderable': false,
                "render": function (data, type, row) {
                  if(typeof initModuloValidade.linhaSelecionada == "undefined"){
                    initModuloValidade.linhaSelecionada = {};
                  }
                    initModuloValidade.linhaSelecionada[(row.serie + '-' + row.modulo)] = row;
                    var btnConsultar = '<button type="button" ' +
                        ' onclick="initModuloValidade.clickBotaoConsultar('
                        + '\'' + (row.serie + '-' + row.modulo) + '\''+
                        ');" class="btn btn-info btn-xs BtnDetalhes">' +
                        '<i class="fa fa-search-plus"></i></button>';

                    return '<center>' + btnConsultar + '</center>';
                }
            },
            {
                'targets': [8],
                'orderable': false,
                "render": function (data, type, row) {
                    var fantasia = "'" + row.fantasia + "'";
                    var btnExcluir = '<button type="button" ' +
                        ' onclick="initModuloValidade.confirmarExclusaoRegistro('
                        + row.serie + ',' + fantasia + ',' + row.modulo +
                        ');" class="btn btn-danger btn-xs BtnExcluir">' +
                        '<i class="fa fa-trash-o"></i></button>';

                    return '<center>' + btnExcluir + '</center>';
                }
            }

        ];

        serviceGridLogsAcessos.carregarGrid(
            '#datatable',
            location.pathname + '/selecionar',
            this.colunasModuloValidade,
            null,// colunasEspeModuloValidade,
            null,
            function (d) {
                return $.extend({}, d, {
                    'filtros': getFiltros('.datatable_Filtros')
                });
         }
        );
    };

    this.transicaoLayout = function(formVisivel) {
        if(formVisivel){
            $('.Grid').hide(600);
            $('.Formulario').show(600);
        } else {
            $('.Formulario').hide(600);
            $('.Grid').show(600);
        }
    };

    this.adicionarRegistro = function(){
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $.each(this.colunasModuloValidade, function (index, value) {
                $('[name=' + value.data + ']').val(null);
        });
        $('#data').val(moment().format('DD/MM/YYYY'));
    };

    this.clickBotaoConsultar = function(codigo) {
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(initModuloValidade.linhaSelecionada[codigo], function (index, value) {
          if(index == "fantasia"){
              $('[name=' + index.trim() + ']').val($('[name=serie]').val() + ' - ' + value);
          } else if(index == "dataValidade") {
              $('[name=' + index.trim() + ']'). text(value.trim());
          } else {
              $('[name=' + index.trim() + ']').val(value.trim());
          }
        });
    };

    this.setLabelStatus = function(codigo) {
        this.statusSelecionado = codigo;
      $('#statusForm').text(this.status[codigo]);
    };

    this.confirmarAlteracaoDeRegistro = function() {
        var codigo = $('#serie').val();
        var fantasia = $('#fantasia').val().split(' - ')[1];

        var form = $('#modulosValidade');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
                if(value == ""){
                    VanillaToasts.create({
                        title: 'Alerta de erro!',
                        text: 'Preencha todos os campos !',
                        type: 'error', // success, info, warning, error   / optional parameter​
                        timeout: 10000, // hide after 5000ms, // optional parameter
                      });
                      continuar = false;
                      return continuar;
                }
        });
        if(!continuar){
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
            '<button type="button" class="btn btn-danger">Cancelar</button>' +
            '<button type="button" class="btn btn-success">Confirmar</button>' +
        '</div>';

        var descricao = initModuloValidade.status[initModuloValidade.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
          });

          $(toast).find('.btn-success').on('click',function(){
              initModuloValidade.alterarRegistro(codigo, fantasia);
          });

    };

    this.alterarRegistro = function(codigo, fantasia) {
        var form = $('#modulosValidade');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var url = initModuloValidade.statusSelecionado == 1 ? '/alterar' :'/adicao';

        $.ajax({
            type: "POST",
            url: location.pathname + url,
            data: {
                'formulario': valFormulario
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridLogsAcessos.getDatarTable().ajax.reload();
                load.esconde();
                initModuloValidade.transicaoLayout(false);

                if(data[0] !== "Cadastro já existente !"){
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro alterado com sucesso !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function() {} // executed when toast is clicked / optional parameter
                    });
                } else {
                    VanillaToasts.create({
                        title: 'Alerta !',
                        text: data[0] ,
                        type: 'warning', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function() {} // executed when toast is clicked / optional parameter
                      });
                }
            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function() {} // executed when toast is clicked / optional parameter
                  });
                console.log(e);
            }
        });
    };

    this.confirmarExclusaoRegistro = function(codigo, fantasia, modulo) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
            '<button type="button" class="btn btn-default">Cancelar</button>' +
            '<button type="button" class="btn btn-danger">Confirmar</button>' +
        '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
          });

          $(toast).find('.btn-danger').on('click',function(){
              initModuloValidade.excluirRegistro(codigo, modulo);
          });

    };

    this.excluirRegistro = function(codigo, modulo) {
        $.ajax({
            type: "POST",
            url: location.pathname + '/excluir',
            data: {
                'codigo': codigo,
                'modulo': modulo
            },
            beforeSend: function () {
                load.mostra('html');
            },
            success: function (data) {
                serviceGridLogsAcessos.getDatarTable().ajax.reload();
                load.esconde();
                VanillaToasts.create({
                    title: 'Sucesso !',
                    text: 'Registro excluido !',
                    type: 'success', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function() {} // executed when toast is clicked / optional parameter
                  });

            },
            error: function (e) {
                load.esconde();
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function() {} // executed when toast is clicked / optional parameter
                  });
                console.log(e);
            }
        });
    };

}

$('body').on('click', '.dropdown-menu.hold-on-click *:not(.btn-success)', function (e) {
    e.preventDefault();
    var toElement = $(e.toElement);

    if (!toElement.is('.btn-success')) {
        e.stopPropagation();
    }

    if (toElement.is('[data-toggle=dropdown]')) {
        toElement.dropdown('toggle');
    }
});

function selectDate() {
    serviceGridLogsAcessos.getDatarTable().ajax.reload();
    selecionaTable($('#datatable tr'));
}

function selecionaTable(local) {
    if ($('#grid').is(":visible")) {
        local.eq(0).click();
        local.eq(0).attr("tabindex", -1).focus();
    }
}

$(document).ready(function () {
    initModuloValidade.iniciarGridLogsAcessos();
    initModuloValidade.iniciaFormulario();

    $(document).keyup(function (e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 27) {
            $('.BtnVoltar').click();
        }
        if (code == 107) { // && e.altKey) {
            $('#adicionarTicket').click();
        }
    });

    $(".collapse-link").on('click', function () {
        $(".right_col").css('min-height', '768px');
    });

    $(window).resize(function(){
        serviceGridLogsAcessos.getDatarTable().ajax.reload();
    });

});

var initModuloValidade = new moduleLogsAcesso();
