var codigoPlanoContratado;
var gridConexao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
//        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
//        {'descricao': 'Cliente', 'tipo': 'text', 'data': 'cliente'},
        {'tipo': 'text', 'data': 'sistema'},
        {'tipo': 'text', 'data': 'plano'},
        {'descricao': 'Apelido', 'tipo': 'text', 'data': 'apelido'},
//        {'descricao': 'Banco', 'tipo': 'text', 'data': 'banco'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],
    locationPathname: '/contrato',

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            formularioConexao.linhaSelecionada[row.id] = row;

            var btnConsultarSerie = '';
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioConexao.clickBotaoConsultar('
                    + row.id +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            if (row.sistemaChave === "forca_vendas_web") {
                btnConsultarSerie = '<button type="button" ' +
                        ' onclick="formularioConexao.clickBotaoConsultarSerie('
                        + row.id +
                        ');" class="btn bg-green btn-xs BtnDetalhesAndroid">' +
                        '<i class="font-white fa fa-android"></i></button>';
            }

            return '<center>' + btnConsultarSerie + "&nbsp;" + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioConexao.linhaSelecionada[row.id] = row;

            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioConexao.confirmarExclusaoRegistro('
                    + row.id + ',' + row.idConexao +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatableConexaoContratada', gridConexao.locationPathname,
                this.colunas, this.colunasEspeciais, null, null, function () {
                    return {
                        'cliente': $('#empresas').find('#idClienteFinal').val(),
                    };
                },
                function () {
                    setTimeout(function () {
                        formularioRecursosContratados.carregarTotais();
                    }, 100);
                });

        $('#datatableConexaoContratada tbody').on('click', 'tr', function () {
            codigoPlanoContratado = gridConexao.linhaSelecionada.id;
            idPlanoContratado = gridConexao.linhaSelecionada.idPlano;
            gridRecursosContratados.getDatarTable().ajax.reload();
            formularioConexao.clickBotaoConsultar(codigoPlanoContratado, false);
        });

    }

});

var formularioConexao = $.extend(new Formulario(), {
    iniciar: function () {

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        var idCliente = $('#plano').find('#idCliente').val();

        document.forms.plano.reset();
        $('#plano').find('#idCliente').val(idCliente);
        $('.GridConexaoContratada').hide();
        $('.FormConexaoContratada').show();

    },

    clickBotaoConsultarSerie: function (codigo) {
        gridForcaSeriesNovo.clienteSelecionado = formularioConexao.linhaSelecionada[codigo];
        if (typeof gridForcaSeriesNovo.getDatarTable() === "undefined") {
            gridForcaSeriesNovo.iniciar();
        } else {
            gridForcaSeriesNovo.getDatarTable().ajax.reload();
        }

        $('#modalForcaSerie').modal('show');
    },

    clickBotaoConsultar: function (codigo, aba) {

        aba = typeof aba !== "undefined" ? aba : true;
        if (aba) {
            $('.modal').modal('hide');
        }
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioConexao.linhaSelecionada[codigo], function (index, value) {

            $('#plano').find('[name=' + index.trim() + ']').val(value.trim());

            if (index.trim() === 'idCliente') {
                $('#plano').find('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioConexao.linhaSelecionada[codigo]['cliente']
                        );

            } else if (index.trim() === 'idPlano') {
                $('#plano').find('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioConexao.linhaSelecionada[codigo]['plano']
                        );

            }

        });

        if (aba) {
            $('.GridConexaoContratada').hide();
            $('.FormConexaoContratada').show();
//            $('#TabContratarPlanos').click();
        }
    },

    transicaoLayout3: function (param) {
//        this.transicaoLayout(param);
        if (!param) {
            $('.GridConexaoContratada').show();
            $('.FormConexaoContratada').hide();
        }
    },

    clickAtivacao: function (codigo) {
        formularioConexao.statusSelecionado = 1;
        formularioConexao.linhaSelecionada[codigo].ativo = $('#check-' + codigo).prop('checked');
        formularioConexao.alterarRegistro(formularioConexao.linhaSelecionada[codigo]);

    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#idCliente').val();
        var fantasia = $('#idPlano').val();

        var form = $('#plano');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('#plano').find('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioConexao.status[formularioConexao.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' / ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioConexao.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioConexao.statusSelecionado == 1 ? 'PUT' : 'POST';

        var url = gridConexao.locationPathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {
                    'formulario': formulario,
                    'alteracao': formularioConexao.statusSelecionado
                },
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();

                    if (typeof resultado.mensagem !== "undefined") {
                        VanillaToasts.create({
                            title: 'Erro na adição !',
                            text: resultado.mensagem,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                        return false;
                    }

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                    formularioConexao.setLabelStatus(1);
                }
        );
    },

    confirmarExclusaoRegistro: function (id, idConexao) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + ' - conexão: ' + idConexao + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioConexao.excluirRegistro(id, idConexao);
        });

    },

    excluirRegistro: function (id, idConexao) {
        this.requisicao(
                gridConexao.locationPathname + '/' + id,
                'DELETE',
                {
                    'id': id,
                    'idConexao': idConexao
                },
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});
