var idCliente = 0;
var formularioEmpresas = $.extend(new Formulario(), {
    iniciar: function () {
        this.initDropZoneLogo('#file-dropzone');
        this.initCep("#cep", "#buscarCep");
        this.initTelefone('#telefone');
        this.initCpfCnpj('#documento', "#buscarCnpj");

        $('#btnUpdateDb').on('click', function () {
            formularioEmpresas.atualizaDb();
        });


        $('#btnCriarDbPorTemplate').on('click', function () {
            formularioEmpresas.requisicao(
                    '/bancos_de_dado',
                    'POST',
                    {'idCliente': parseInt($('#idCliente').val())},
                    'Não foi possivel gerar o banco de dados!',
                    function (resultado) {
                        if (typeof resultado.mensagem !== "undefined") {
                            var msg = resultado.mensagem;
                            var msgTipo = 'error';
                            if (resultado.mensagem.indexOf('already exists') > -1) {
                                msg = 'Banco de dados já foi gerado!'
                                msgTipo = 'warning';

                                $('#ip').val('52.67.25.88');
                                $('#porta').val('5432');
                                $('#usuarioBanco').val('csgestor_max');
                                $('#senhaBanco').val('amdsdl7586');
                                $('#banco').val('simples_' + parseInt($('#idCliente').val()));

                            } else if (resultado.mensagem.indexOf('source database "template_simples" is being accessed by other users') > -1) {
                                msg = 'Banco de dados template está sendo alterado! <br> Tente novamente em instantes!'
                                msgTipo = 'info';
                            }


                            VanillaToasts.create({
                                title: 'Falha !',
                                text: msg,
                                type: msgTipo, // success, info, warning, error   / optional parameter​
                                timeout: 5000, // hide after 5000ms, // optional parameter
                                callback: function () {} // executed when toast is clicked / optional parameter
                            });

                        } else {

                            $('#ip').val('52.67.25.88');
                            $('#porta').val('5432');
                            $('#usuarioBanco').val('csgestor_max');
                            $('#senhaBanco').val('amdsdl7586');
                            $('#banco').val(resultado[0]);

                            VanillaToasts.create({
                                title: 'Sucesso !',
                                text: 'Banco de dados criado com sucesso!',
                                type: 'success', // success, info, warning, error   / optional parameter​
                                timeout: 5000, // hide after 5000ms, // optional parameter
                                callback: function () {} // executed when toast is clicked / optional parameter
                            });
                        }
                    }
            );

        });


        $('#TabEmpresas').on('click', function () {
            $('#tab1').show();
            $('#tab1').addClass('active');
            $('#tab2').hide();
            $('#tab2').removeClass('active');

            $('.datatable_Filtros').find('.btnAdicionar').show();
        });

        $('#TabPlanosContratado').on('click', function () {

            $('#tab1').hide();
            $('#tab1').removeClass('active');
            $('#tab2').show();
            $('#tab2').addClass('active');

            $('.datatable_Filtros').find('.btnAdicionar').hide();

            formularioRecursosContratados.carregarTotais();
        });

    },

    atualizaDb: function () {
//        $(this).parent('tr').click();
        load.mostra('html');
//            $('#loadUpdateBanco').show();

        var form = $('.FormularioSelecionarConexao');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');

        var id = parseInt(gridEmpresas.getDatarTable().data()[gridEmpresas.linhaSelecionadaIndex].id);
        formularioEmpresas.requisicao(
                '/bancos_de_dado/' + id,
                'PUT',
                {
                    'idCliente': id,
                    'formulario': valFormulario
                },
                'Não foi possivel atualizar o banco de dados!',
                function (resultado) {
                    setTimeout(function () {
                        load.esconde('html');
//                            $('#loadUpdateBanco').hide();
                        $('#modalDataBaseUpdate').modal();
                    }, 550);
                    $('#databaseUpdate > tbody').html('');

                    $.each(resultado, function (index, value) {
                        var linha = '<tr>';
                        if (typeof value['DESCRICAO'] !== "undefined") {
                            linha += '<td>';
                            linha += index;
                            linha += '</td>';

                            linha += '<td>';
                            linha += value['VERSAO'];
                            linha += '</td>';

                            linha += '<td>';
                            linha += value['DESCRICAO'];
                            linha += '</td>';

                            linha += '<td>';
                            linha += value['STATUS'];
                            linha += '</td>';

                        } else {
                            linha += '<td>';
                            linha += index;
                            linha += '</td>';

                            linha += '<td>';
                            linha += value['VERSAO'];
                            linha += '</td>';

                            linha += '<td colspan="2">';
                            linha += '<table class="table table-striped">';
                            linha += '<thead>';
                            linha += '<tr>';
                            linha += '<th value="indice">Indice</th>';
                            linha += '<th value="descricao">Descrição</th>';
                            linha += '<th value="status">Status</th>';
                            linha += '</tr>';
                            linha += '</thead>';

                            $.each(value, function (i, val) {
                                if (i !== 'VERSAO') {
                                    linha += '<tr>';
                                    linha += '<td>';
                                    linha += i;
                                    linha += '</td>';
                                    linha += '<td>';
                                    linha += val['DESCRICAO'];
                                    linha += '</td>';
                                    linha += '<td>';
                                    linha += val['STATUS'];
                                    linha += '</td>';
                                    linha += '</tr>';
                                }
                            });
                            linha += '</table>';
                            linha += '</td>';
                        }

                        linha += '</tr>';
                        $('#databaseUpdate > tbody').append(linha);

                    });

                    VanillaToasts.create({
                        title: 'Informação !',
                        text: 'Banco de dados atualizado!',
                        type: 'info', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $('#TabEmpresas').click();
        $('#logoEmpresa').attr('src', "/img/notfound.jpg");

        $('#TabPlanosContratado').hide();
        $('#TabContratarPlanos').hide();
        $('.panelsTotais').hide();
        $('#clienteSelecionadoGrid').hide();

        document.forms.empresas.reset();

    },
    transicaoLayout2: function (param) {
        $('.panelsTotais').hide();
        if (!param) {
            $('#TabPlanosContratado').click();
        }
    },

    clickBotaoConsultar: function (codigo, fantasia) {
        $('.modal').modal('hide');
        $('#TabEmpresas').click();
        this.setLabelStatus(1);
        this.transicaoLayout(true);
        $('.panelsTotais').show();


        if (typeof gridRecursosContratados.getDatarTable() === "undefined") {
            gridConexao.iniciar();
            gridRecursosContratados.iniciar();
            gridRecurso.iniciar($('#empresas').find('#idClienteFinal').val());
        } else {
            gridRecurso.getPlanos($('#empresas').find('#idClienteFinal').val());

            gridConexao.getDatarTable().ajax.reload();
            gridRecurso.getDatarTable().ajax.reload();
            gridRecursosContratados.getDatarTable().ajax.reload();
        }

        setTimeout(function () {
            formularioRecursosContratados.carregarTotais();
        }, 100);

    },

    clickAtivacao: function (codigo, fantasia) {

        $.each(formularioEmpresas.linhaSelecionada[codigo + "-" + fantasia], function (index, value) {
            if (typeof value === "object") {
                $.each(value, function (i, val) {
                    $('[name=' + i.trim() + ']').val(val.trim());
                });
            } else {

                $('[name=' + index.trim() + ']').val(value.trim());
                if (index.trim() === "logo") {
                    var logo = "/img/notfound.jpg";
                    if (value.trim() !== "") {
                        logo = "/imagens/admin/logo/cliente/" + value.trim();
                    }
                    $('#logoEmpresa').attr('src', logo);
                } else if (index.trim() === 'ativo') {
                    var check = $('[name=' + index.trim() + ']');
                    check.prop('checked', false);
                    if (!!value) {
                        check.prop('checked', true);
                    }
                }
            }
        });

        setTimeout(function () {
            var form = $('#empresas');
            var disabled = form.find(':input:disabled').removeAttr('disabled');
            var valFormulario = form.serializeObject();

            disabled.attr('disabled', 'disabled');
            valFormulario.ativo = $('#' + codigo).prop('checked');

            formularioEmpresas.alterarRegistro(valFormulario);
        }, 150);
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#documento').val();
        var fantasia = $('#fantasia').val();

        var form = $('#empresas');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioEmpresas.status[formularioEmpresas.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioEmpresas.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario, url) {
        var method = formularioEmpresas.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        url = typeof url !== "undefined" ? url : location.pathname;
        if (formulario.serie) {
            url = url + '/' + formulario.serie;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridEmpresas.getDatarTable().ajax.reload();
                    formularioEmpresas.transicaoLayout(false);
                    $('.panelsTotais').hide();

                    $('#TabPlanosContratado').show();
                    $('#TabContratarPlanos').show();
                    $('.panelsTotais').show();

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id, fantasia) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioEmpresas.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + '/' + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridEmpresas.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

    uploadLogo: function () {
        $('#file-dropzone').html('');
        $('.UploadLogo').modal();
    }

});

$(document).ready(function () {
    if (location.pathname !== '/cadastros/empresas') {
        gridEmpresas.iniciar();
        formularioEmpresas.iniciar();
    }
});
