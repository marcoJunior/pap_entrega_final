var gridEmpresas = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
//        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'tipo': 'number', 'data': 'serie'},
        {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'fantasia'},
        {'tipo': 'text', 'data': 'logo', 'width': '10%'},
        {'descricao': 'Razão social', 'tipo': 'text', 'data': 'razaoSocial'},
        {'descricao': 'Documento', 'tipo': 'text', 'data': 'documento'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

//        this.criarBotaoEspecial(0, true, function (data, type, row) {
//            var fantasia = "'" + row.fantasia + "'";
//            var checked = '';
//
//            if (parseInt(row.ativo) === 1) {
//                checked = 'checked="checked"';
//            }
//            var checkbox = '<input class="hidden-box" onchange="formularioEmpresas.clickAtivacao(' +
//                    +row.id + ',' + fantasia +
//                    ');" id="' + row.id +
//                    '" style="width: 25px!important;height: 15px!important;"' +
//                    ' type="checkbox" ' + checked + ' >';
//
//            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;' + checkbox + '</span>';
//        });

        this.criarBotaoEspecial(2, true, function (data, type, row) {
            var logo = "/img/notfoundRow.png";
            if (row.logo.trim() !== "") {
                logo = "/imagens/admin/logo/cliente/" + row.logo.trim();
            }
            return '<img style="max-width:100px;max-height:25px;" src="' + logo + '"/>';
        });

//        this.criarBotaoEspecial(5, false, function (data, type, row) {
//            formularioEmpresas.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;
//
//            var fantasia = "'" + row.fantasia + "'";
//            var btnConsultar = `<button type="button" onclick="formularioEmpresas.atualizaDb();" class="btn bg-yellow-saffron btn-xs">
//                                                <span class="fa-stack">
//                                                    <i class="fa fa-database font-white"></i>
//                                                    <i class="fa fa-refresh fa-stack-1x font-green-soft" style="left: 5px;bottom: 10px;"></i>
//                                                </span>
//                                            </button>`;
//
//            return '<center>' + btnConsultar + '</center>';
//        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            formularioEmpresas.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioEmpresas.clickBotaoConsultar('
                    + row.id + ',' + fantasia +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(6, false, function (data, type, row) {
            formularioEmpresas.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioEmpresas.confirmarExclusaoRegistro('
                    + row.id + ',' + fantasia +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);

        $('#datatable tbody').on('click', 'tr', function () {
            var row = gridEmpresas.getDatarTable().data()[gridEmpresas.linhaSelecionadaIndex];
            var codigo = row.id;
            var fantasia = row.fantasia;

            $('#clienteSelecionadoGrid').text("(" + fantasia + ")");
            $.each(row, function (index, value) {
                if (typeof value === "object") {
                    $.each(value, function (i, val) {
                        $('[name=' + i.trim() + ']').val(val.trim());
                    });
                } else {

                    $('[name=' + index.trim() + ']').val(value.trim());
                    if (index.trim() === "logo") {
                        var logo = "/img/notfound.jpg";
                        if (value.trim() !== "") {
                            logo = "/imagens/admin/logo/cliente/" + value.trim();
                        }
                        $('#logoEmpresa').attr('src', logo);
                    } else if (index.trim() === 'ativo') {
                        var check = $('[name=' + index.trim() + ']');
                        check.prop('checked', false);
                        if (!!value) {
                            check.prop('checked', true);
                        }
                    } else if (index.trim() === 'id') {
                        idCliente = value.trim();
                        $('[name=' + index.trim() + ']').val(value.trim());
                        setTimeout(function () {
                            $('[name=idCliente]').val(
                                    value.trim() + " - " + fantasia
                                    );
                        }, 150);

                    }
                }
            });

        });

    }

});
