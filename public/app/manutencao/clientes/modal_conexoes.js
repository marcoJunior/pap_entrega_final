var gridSelecionaConexao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Dominio', 'tipo': 'text', 'data': 'dominio'},
        {'descricao': 'Ip', 'tipo': 'text', 'data': 'ip'},
        {'descricao': 'Banco', 'tipo': 'text', 'data': 'banco'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'},
        {'data': 'Selecionar'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioSelecaoConexao.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioSelecaoConexao.clickBotaoConsultar('
                    + row.id + ',' + fantasia +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });


        this.criarBotaoEspecial(5, false, function (data, type, row) {
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioSelecaoConexao.confirmarExclusaoRegistro('
                    + row.id +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.criarBotaoEspecial(6, false, function (data, type, row) {
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridSelecionaConexao.selecionarConexao(\''
                    + row.id + "','" + row.ip +
                    '\');" class="btn btn-info btn-xs BtnDetalhesSelecionar">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatableConexao', '/conexao',
                this.colunas, this.colunasEspeciais);
    },

    selecionarConexao: function (id, ip) {

        var row = {};
        $.each(gridSelecionaConexao.getDatarTable().ajax.json().data, function (index, value) {
            if (value.id == id) {
                row = value;
            }
        });
        $.each(row, function (index, value) {
            if (index.trim() === 'id') {
                $('[name=idConexao]').val(value.trim() + " - " + ip);
            } else if (index.trim() !== 'idCliente') {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });

        $('.modal').modal('hide');
    }

});

var formularioSelecaoConexao = $.extend(new Formulario(), {
    iniciar: function () {

    },

    clickBotaoConsultar: function (codigo, fantasia) {
//        $('.modal').modal('hide');
//        $('#TabEmpresas').click();
        this.setLabelStatus(1);
        this.transicaoLayout(true, 'SelecionarConexao');

        var row = {};
        $.each(gridSelecionaConexao.getDatarTable().ajax.json().data, function (index, value) {
            if (value.id == codigo) {
                row = value;
            }
        });
        $.each(row, function (index, value) {
            if (index.trim() === 'id') {
                $('[name=idConexao]').val(value.trim());
            } else if (index.trim() !== 'idCliente') {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });


    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true, 'SelecionarConexao');

        document.forms.formularioSelecionarConexao.reset();
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#idCliente').val();
        var fantasia = $('#idPlano').val();

        var form = $('#plano');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('#plano').find('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioConexao.status[formularioConexao.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' / ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioConexao.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioConexao.statusSelecionado == 1 ? 'PUT' : 'POST';

        var url = gridConexao.locationPathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {
                    'formulario': formulario,
                    'alteracao': formularioConexao.statusSelecionado
                },
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();

                    if (typeof resultado.mensagem !== "undefined") {
                        VanillaToasts.create({
                            title: 'Erro na adição !',
                            text: resultado.mensagem,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                        return false;
                    }

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                    formularioConexao.setLabelStatus(1);
                }
        );
    },

    confirmarExclusaoRegistro: function (id) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>Conexão: ' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioSelecaoConexao.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                '/conexao/' + id,
                'DELETE',
                {},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();
                    gridSelecionaConexao.getDatarTable().ajax.reload();

                    if (typeof resultado.mensagem != "undefined") {
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: resultado.mensagem,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro excluido !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }
                }
        );
    },

});

$(document).ready(function () {
    gridSelecionaConexao.iniciar();
});