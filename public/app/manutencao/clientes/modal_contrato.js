var gridPlanos2 = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Sistema', 'tipo': 'text', 'data': 'sistema'},
        {'descricao': 'Plano', 'tipo': 'text', 'data': 'plano'},
        {'descricao': 'Preço', 'tipo': 'text', 'data': 'preco'},
    ],

    iniciar: function () {

        this.criarBotaoEspecial(3, true, function (data, type, row) {
            if (typeof row.preco !== "undefined") {
                return "R$ " + (row.preco).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            }
            return '';
        });

        this.carregarGrid(
                '#datatablePlanos2', '/contrato',
                this.colunas, this.colunasEspeciais, null, null, function () {
                    return {
                        'codigo': $('#idClienteFinal').val(),
                    };
                });
    }
});