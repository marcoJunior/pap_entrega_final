var QtdLicencas = 0;
var gridForcaSeriesNovo = $.extend(new serviceGrid(), {

    clienteSelecionado: {},

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Codigo', 'tipo': 'number', 'data': 'codigo'},
        {'descricao': 'Aparelho', 'tipo': 'number', 'data': 'aparelho'},
        {'descricao': 'Representante', 'tipo': 'number', 'data': 'representante'},
        {'descricao': 'Numero de série', 'tipo': 'number', 'data': 'numForca'},
        {'data': 'Limpar'},
        {'data': 'Excluir'}
    ],

    locationPathname: '/cadastros/forca/empresas',

    iniciar: function () {

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            var btnLimpar = '<button type="button" ' +
                    ' onclick="gridForcaSeriesNovo.confirmarLimpesaRegistro('
                    + row.codigo + ',' + row.representante +
                    ');" class="btn btn-warning btn-xs BtnLimpar">' +
                    '<i class="fa fa-refresh"></i></button>';

            return '<center>' + btnLimpar + '</center>';
        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            var btnExcluir = '<button type="button" ' +
                    ' onclick="gridForcaSeriesNovo.confirmarExclusaoRegistro('
                    + row.codigo + ',' + row.representante +
                    ');" class="btn bg-red-soft btn-xs BtnExcluir">' +
                    '<i class="fa fa-close"></i></button>';

            return '<center>' + btnExcluir + '</center>';
        });

        this.carregarGrid(
                '#datatableSerie', this.locationPathname + "Serie",
                this.colunas, this.colunasEspeciais,
                null, function () {

                },
                function () {
                    return {
                        'login': gridForcaSeriesNovo.clienteSelecionado,
                        'codigo': $('#serie').val(),
                        'licencas': 40,
                    };
                });
    },

    confirmarLimpesaRegistro: function (codigo, codRep) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-warning">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de limpesa !',
            text: 'Deseja mesmo LIMPAR cadastro (<b>' + codigo +
                    ' - Representate: ' + codRep + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'warning', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-warning').on('click', function () {
            gridForcaSeriesNovo.limparRegistro(codigo, codRep);
        });

    },

    limparRegistro: function (codigo, codRep) {
        $.ajax({
            type: "POST",
            url: this.locationPathname + 'CodigoSerieLimpar',
            data: {
                'codigo': codigo,
                'codRep': codRep,
                'login': gridForcaSeriesNovo.clienteSelecionado,
            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            gridForcaSeriesNovo.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro limpo !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel limpar o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });
    },

    confirmarExclusaoRegistro: function (codigo, codRep) {

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo EXCLUIR cadastro (<b>' + codigo +
                    ' - Representate: ' + codRep + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            gridForcaSeriesNovo.excluirRegistro(codigo, codRep);
        });

    },

    excluirRegistro: function (codigo, codRep) {
        $.ajax({
            type: "POST",
            url: this.locationPathname + 'CodigoSerieExcluir',
            data: {
                'codigo': codigo,
                'codRep': codRep,
                'login': gridForcaSeriesNovo.clienteSelecionado,

            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            gridForcaSeriesNovo.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro excluido !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel excluir o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });

    },

    gerarCodigo: function (serie, cliente) {
        $.ajax({
            type: "POST",
            url: this.locationPathname + 'CodigoSerieGerar',
            data: {
                'serie': serie,
                'login': gridForcaSeriesNovo.clienteSelecionado,
            },
            beforeSend: function () {
                load.mostra('#datatableSerie');
            }
        }).done(function (resultado) {
            gridForcaSeriesNovo.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro criado !',
                type: 'success', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });

        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel criar o registro ! <br>Tente novamente mais tarde !',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
            console.log(resultado);
            console.log("error");
        }).always(function () {
            load.esconde('#datatableSerie');
        });
    }


});