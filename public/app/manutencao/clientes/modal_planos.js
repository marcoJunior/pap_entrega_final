var gridPlanos = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'descricao'},
        {'descricao': 'Sistema', 'tipo': 'text', 'data': 'sistema'},
//        {'descricao': 'Preço', 'tipo': 'text', 'data': 'preco'},
        {'data': 'Detalhes'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {

            var nome = "'" + row.descricao + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridPlanos.selecionarPlano('
                    + row.id + ',' + nome + ',' + row.idSistema +
                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatablePlanos', '/planos',
                this.colunas, this.colunasEspeciais);
    },

    selecionarPlano: function (id, nome, idSistema) {
        $('[name=idPlano]').val(id + " - " + nome);
        $('[name=idSistema]').val(idSistema);
        $('.modal').modal('hide');
    }

});

$(document).ready(function () {
    gridPlanos.iniciar();
});