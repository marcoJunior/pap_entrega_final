var gridRecurso = $.extend(new serviceGrid(), {

    planos: [],
    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Submodulo', 'tipo': 'sim/nao', 'data': 'submodulo', 'options': {'1': 'SIM', '0': 'NÃO'}},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'descricao', 'width': '25%'},
        {'descricao': 'Chave', 'tipo': 'text', 'data': 'chave'},
        {'descricao': 'Valor', 'tipo': 'number', 'data': 'valor'},
        {'tipo': 'text', 'data': 'quantidade'},
        {'tipo': 'text', 'data': 'quantidade'},
        {'tipo': 'text', 'data': 'data'},
        {'data': 'Detalhes'}
    ],

    iniciar: function (idCliente) {

        this.criarBotaoEspecial(1, false, function (data, type, row) {
            var txSubmodulo = '<input type="number" readonly="readonly" style="display:none;" id="submodulo-' + row.id + '" value="' + row.submodulo +
                    '" />';
            return '<center>' + txSubmodulo + (row.submodulo == '1' ? 'SIM' : 'NÃO') + '</center>';
        });

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            return formataReal(row.valor);
        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            var txQuantidade = '<input type="number" value="1" max-value="999" id="qtd-' + row.id + '" class="selected-row-input" style="width: 75px;"/>';
            return '<center>' + txQuantidade + '</center>';

        });
        this.criarBotaoEspecial(6, false, function (data, type, row) {
            var selectPlano = '<select id="plano-' + row.id + '" class="form-control selected-row-input" style="max-height: 26px;padding: 1px;width: 100%;">';
            $.each(gridRecurso.planos, function (i, val) {
                selectPlano += '<option value="' + val.id + '">' + val.descricao + '</option>';
            });
            selectPlano += '</select> ';

            return selectPlano;

        });
        this.criarBotaoEspecial(7, false, function (data, type, row) {

            var txData = '<input readonly="readonly" class="notInput" id="data-' + row.id + ' " value="' + moment().format('DD/MM/YYYY') +
                    ' " />';
            return '<center>' + txData + '</center>';
        });


        this.criarBotaoEspecial(8, false, function (data, type, row) {
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridRecurso.adicionarRecursoSelecionado('
                    + row.id +
                    ');" class="btn btn-success btn-xs BtnDetalhes">' +
                    '<i class="fa fa-plus"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        gridRecurso.getPlanos(idCliente);

        this.carregarGrid(
                '#datatableRecurso2', '/recursos',
                this.colunas, this.colunasEspeciais);


    },

    getPlanos: function (idCliente) {

        $.ajax({
            method: 'GET',
            url: '/planos/0/selecionar-por-id-cliente?idCliente=' + idCliente,
        }).done(function (resultado) {
            gridRecurso.planos = resultado;
            gridRecurso.getDatarTable().ajax.reload();
        }).fail(function (resultado) {
            console.log(resultado);
        });

    },

    adicionarRecursoSelecionado: function (idRecurso) {

        var data = {
            formulario: {
                data: $('#data-' + idRecurso).val(),
                quantidade: $('#qtd-' + idRecurso).val(),
                idPlano: parseInt($('#idPlano').val()),
                idRecurso: idRecurso,
                idContrato: parseInt($('#id').val()),
                submodulo: $('#submodulo-' + idRecurso).val()
            }
        };

        $.ajax({
            method: 'POST',
            url: '/recursos-contratados',
            data: data,
            beforeSend: function () {
                load.mostra('html');
            }
        }).done(function (resultado) {
            if (typeof resultado.mensagem !== "undefined") {
                VanillaToasts.create({
                    title: 'Erro na adição !',
                    text: 'Não é posivel adicionar dois submodulos ao mesmo tempo e no mesmo plano!',
                    type: 'warning', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });

            } else {
                VanillaToasts.create({
                    title: 'Adição !',
                    text: 'Adição concluida com sucesso!',
                    type: 'success', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
            }
            gridRecurso.getDatarTable().ajax.reload();
            gridRecursosContratados.getDatarTable().ajax.reload();
            formularioRecursosContratados.carregarTotais();
        }).fail(function (resultado) {
            console.log(resultado);
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Não foi possivel adicionar este recurso no momento!<br>Tente novamente em instantes!',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
        }).always(function () {
            load.esconde('html');
        });


    }

});