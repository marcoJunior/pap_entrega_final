var gridConexao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Cliente', 'tipo': 'text', 'data': 'cliente'},
        {'tipo': 'text', 'data': 'sistema'},
        {'tipo': 'text', 'data': 'plano'},
        {'descricao': 'Apelido', 'tipo': 'text', 'data': 'apelido'},
        {'descricao': 'Banco', 'tipo': 'text', 'data': 'banco'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(0, true, function (data, type, row) {
            formularioConexao.linhaSelecionada[row.id] = row;

            var checked = '';

            if (parseInt(row.ativo) === 1) {
                checked = 'checked="checked"';
            }
            var checkbox = '<input class="hidden-box" onchange="formularioConexao.clickAtivacao(' +
                    +row.id +
                    ');" id="check-' + row.id +
                    '" style="width: 25px!important;height: 15px!important;"' +
                    ' type="checkbox" ' + checked + ' >';

            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;' + checkbox + '</span>';
        });

        this.criarBotaoEspecial(7, false, function (data, type, row) {
            formularioConexao.linhaSelecionada[row.id] = row;

            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioConexao.clickBotaoConsultar('
                    + row.id +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(8, false, function (data, type, row) {
            formularioConexao.linhaSelecionada[row.id] = row;

            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioConexao.confirmarExclusaoRegistro('
                    + row.id + ',' + row.idConexao +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

var formularioConexao = $.extend(new Formulario(), {
    iniciar: function () {

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.plano.reset();

    },

    clickBotaoConsultar: function (codigo, fantasia) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioConexao.linhaSelecionada[codigo], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            } else if (index.trim() === 'idCliente') {
                $('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioConexao.linhaSelecionada[codigo]['cliente']
                        );

            } else if (index.trim() === 'idPlano') {
                $('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioConexao.linhaSelecionada[codigo]['plano']
                        );

            }

        });
    },

    clickAtivacao: function (codigo) {
        formularioConexao.statusSelecionado = 1;

        formularioConexao.linhaSelecionada[codigo].ativo = $('#check-' + codigo).prop('checked');

        formularioConexao.alterarRegistro(formularioConexao.linhaSelecionada[codigo]);

    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#idCliente').val();
        var fantasia = $('#idPlano').val();

        var form = $('#plano');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioConexao.status[formularioConexao.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' / ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioConexao.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioConexao.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();
                    formularioConexao.transicaoLayout(false);

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id, idConexao) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + ' - conexão: ' + idConexao + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioConexao.excluirRegistro(id, idConexao);
        });

    },

    excluirRegistro: function (id, idConexao) {
        this.requisicao(
                location.pathname + '/' + id,
                'DELETE',
                {
                    'id': id,
                    'idConexao': idConexao
                },
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    gridConexao.iniciar();
    formularioConexao.iniciar();
});
