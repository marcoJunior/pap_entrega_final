var gridSelecionaConexao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Apelido', 'tipo': 'text', 'data': 'apelido'},
        {'descricao': 'Ip', 'tipo': 'text', 'data': 'ip'},
        {'descricao': 'Banco', 'tipo': 'text', 'data': 'banco'},
        {'data': 'Excluir'},
        {'data': 'Detalhes'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioSelecaoConexao.confirmarExclusaoRegistro('
                    + row.id +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridSelecionaConexao.selecionarConexao('
                    + row.id +
                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatableConexao', '/conexao',
                this.colunas, this.colunasEspeciais);
    },

    selecionarConexao: function (id) {
        var row = {};
        $.each(gridSelecionaConexao.getDatarTable().ajax.json().data, function (index, value) {
            if (value.id == id) {
                row = value;
            }
        });
        $.each(row, function (index, value) {
            if (index.trim() === 'id') {
                $('[name=idConexao]').val(value.trim());
            } else {
                $('[name=' + index.trim() + ']').val(value.trim());
            }
        });
        $('.modal').modal('hide');
    }

});

var formularioSelecaoConexao = $.extend(new Formulario(), {
    iniciar: function () {

    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>Conexão: ' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioSelecaoConexao.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                '/conexao/' + id,
                'DELETE',
                {},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridConexao.getDatarTable().ajax.reload();
                    gridSelecionaConexao.getDatarTable().ajax.reload();

                    if (typeof resultado.mensagem != "undefined") {
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: resultado.mensagem,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro excluido !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }
                }
        );
    },

});

$(document).ready(function () {
    gridSelecionaConexao.iniciar();
});