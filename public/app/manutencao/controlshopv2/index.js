var gridControlShopV2 = $.extend(new serviceGrid(), {

    colunasEspeciais: [],
    colunas: [
        {'descricao': 'Série', 'tipo': 'number', 'data': 'serie'},
        {'descricao': 'Cliente', 'tipo': 'text', 'data': 'cliente', 'width': '40%'},
        {'descricao': 'Versão NFE', 'tipo': 'text', 'data': 'versaoNfe'},
        {'descricao': 'Versão CS', 'tipo': 'text', 'data': 'versaoCs'},
        {'descricao': 'CNPJ', 'tipo': 'text', 'data': 'cnpj'},
        {'descricao': 'Ultima liberação', 'tipo': 'date', 'data': 'ultimaLiberacao'},
        {'descricao': 'Ativação', 'tipo': 'sim/nao', 'data': 'ativacao', 'options': {'S': 'Sim', 'N': 'Não'}},
    ],
    iniciar: function () {

        this.criarBotaoEspecial(6, true, function (data, type, row) {
            var descricao = row.ativacao === 'S' ? 'Ativo' : 'Desativado';
            var cor = row.ativacao === 'S' ? 'bg-green-soft' : 'bg-red-soft';
            var btnConsultar = '<button type="button" ' +
                    'class="btn ' + cor + ' btn-xs" ' +
                    'style="color:#FFF;">' +
                    descricao +
                    '</button>';
            return '<center>' + btnConsultar + '</center>';
        });
        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
        this.contarClientes();
    },
    contarClientes: function () {
        $.ajax({
            method: 'GET',
            url: location.pathname,
            data: {contarClientes: true},
            beforeSend: function () {
                load.mostra('.dashboard-stat2');
            }
        }).done(function (resultado) {
            $('#ativos').text(resultado['*ativos']);
            $('#inativos').text(resultado['*inativos']);
            $('#todos').text(resultado['*todos']);
        }).fail(function (resultado) {
            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: 'Erro ao carregar os totais',
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
        }).always(function () {
            load.esconde('.dashboard-stat2');
        });
    }

});
$(document).ready(function () {
    gridControlShopV2.iniciar();
});