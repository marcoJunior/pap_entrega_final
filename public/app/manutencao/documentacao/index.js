var boxText;
var gridDocumentacao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
//        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'tipo': 'text', 'data': 'sistema'},
        {'descricao': 'Recurso', 'tipo': 'text', 'data': 'recurso', 'width': '20%'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'descricao'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

//        this.criarBotaoEspecial(0, true, function (data, type, row) {
//            var nome = "'" + row.nome + "'";
//            var checked = '';
//            if (parseInt(row.ativo) === 1) {
//                checked = 'checked="checked"';
//            }
//            var checkbox = '<input class="hidden-box" onchange="formularioDocumentacao.clickAtivacao(' +
//                    +row.id + ',' + nome +
//                    ');" id="' + row.id +
//                    '" style="width: 25px!important;height: 15px!important;"' +
//                    ' type="checkbox" ' + checked + ' >';
//            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;' + checkbox + '</span>';
//        });
        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioDocumentacao.linhaSelecionada[(row.id + '-' + row.nome)] = row;
            var nome = "'" + row.nome + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioDocumentacao.clickBotaoConsultar('
                    + row.id + ',' + nome +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';
            return '<center>' + btnConsultar + '</center>';
        });
        this.criarBotaoEspecial(5, false, function (data, type, row) {
            formularioDocumentacao.linhaSelecionada[(row.id + '-' + row.nome)] = row;
            var nome = "'" + row.nome + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioDocumentacao.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';
            return '<center>' + btnExcluir + '</center>';
        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

var formularioDocumentacao = $.extend(new Formulario(), {
    iniciar: function () {
        $('#TabDocumentacao').on('click', function () {
            $('#tab1').show();
            $('#tab1').addClass('active');
            $('#tab2').hide();
            $('#tab2').removeClass('active');
        });

        $('#TabDescricaoDocumentacao').on('click', function () {
            $('#tab1').hide();
            $('#tab1').removeClass('active');
            $('#tab2').show();
            $('#tab2').addClass('active');
        });

        boxText = CKEDITOR.replace('editor1', {
            height: 300
        });

    },
    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);
        document.forms.documentacao.reset();
        $('#idRecurso').val(0);
        $('#TabDocumentacao').click();
        gridDocumentacaoDescricao.getDatarTable().ajax.reload();
    },
    clickBotaoConsultar: function (codigo, nome) {
        $('.modal').modal('hide');
        $('#TabDocumentacao').click();
        this.setLabelStatus(1);
        this.transicaoLayout(true);
        $.each(formularioDocumentacao.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('#documentacao').find('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            } else if (index.trim() === 'idSistema') {
                $('#documentacao').find('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioDocumentacao.linhaSelecionada[codigo + "-" + nome]['sistema']
                        );
            } else if (index.trim() === 'idRecurso') {
                $('#documentacao').find('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioDocumentacao.linhaSelecionada[codigo + "-" + nome]['recurso']
                        );
            }
        });

        if (typeof gridDocumentacaoDescricao.getDatarTable() === "undefined") {
            gridDocumentacaoDescricao.iniciar();
        } else {
            gridDocumentacaoDescricao.getDatarTable().ajax.reload();
        }

    },
    clickAtivacao: function (codigo, nome) {

        $.each(formularioDocumentacao.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('#documentacao').find('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });
        setTimeout(function () {
            var form = $('#documentacao');
            var disabled = form.find(':input:disabled').removeAttr('disabled');
            var valFormulario = form.serializeObject();
            disabled.attr('disabled', 'disabled');
            valFormulario.ativo = $('#' + codigo).prop('checked');
            formularioDocumentacao.alterarRegistro(valFormulario);
        }, 150);
    },
    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#id').val();
        var form = $('#documentacao');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('#documentacao').find('[name=ativo]').prop('checked');
        var continuar = true;

        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos, com asteristico !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';
        var descricao = formularioDocumentacao.status[formularioDocumentacao.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });
        $(toast).find('.btn-success').on('click', function () {
            formularioDocumentacao.alterarRegistro(valFormulario);
        });
    },
    alterarRegistro: function (formulario) {
        var method = formularioDocumentacao.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridDocumentacao.getDatarTable().ajax.reload();
                    formularioDocumentacao.transicaoLayout(false);
                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !";
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },
    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';
        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });
        $(toast).find('.btn-danger').on('click', function () {
            formularioDocumentacao.excluirRegistro(id);
        });
    },
    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridDocumentacao.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },
});
$(document).ready(function () {
    gridDocumentacao.iniciar();
    formularioDocumentacao.iniciar();
});
