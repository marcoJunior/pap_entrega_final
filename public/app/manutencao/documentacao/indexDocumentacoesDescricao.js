var gridDocumentacaoDescricao = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Funcionalidade', 'tipo': 'text', 'data': 'funcionalidade'},
        {'tipo': 'text', 'data': 'tipo'},
        {'data': 'Excluir'}
    ],
    pathname: '/documentacao_descricao',

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            formularioRecursoDescricao.linhaSelecionada[(row.id + '-' + row.nome)] = row;
            var nome = "'" + row.nome + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioRecursoDescricao.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';
            return '<center>' + btnExcluir + '</center>';
        });

        this.carregarGrid(
                '#datatableDocumentacao', gridDocumentacaoDescricao.pathname,
                this.colunas, this.colunasEspeciais, null, null, function () {
                    return {
                        'codigo': $('#idDocumentacao').val(),
                    };
                });

        $('#datatableDocumentacao tbody').on('click', 'tr', function () {
            $.each(gridDocumentacaoDescricao.getDatarTable().row(this).data(), function (index, value) {
                $('#especificacao_documentacao').find('[name=' + index.trim() + ']').val(value.trim());
                if (index.trim() === 'descricao') {
                    boxText.setData(value.trim());
                }
            });
        });


    }

});

var formularioRecursoDescricao = $.extend(new Formulario(), {
    confirmarAlteracaoDeRegistro: function () {
        $('#idDocumentacao').val($('#id').val());
        var codigo = $('#especificacao_documentacao').find('#id').val();
        var form = $('#especificacao_documentacao');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        valFormulario.descricao = boxText.getData();
        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos, com asteristico !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';
        var descricao = formularioRecursoDescricao.status[formularioRecursoDescricao.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });
        $(toast).find('.btn-success').on('click', function () {
            formularioRecursoDescricao.alterarRegistro(valFormulario);
        });
    },
    alterarRegistro: function (formulario) {
        var method = formularioRecursoDescricao.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = gridDocumentacaoDescricao.pathname
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridDocumentacaoDescricao.getDatarTable().ajax.reload();

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !";
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },
    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';
        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });
        $(toast).find('.btn-danger').on('click', function () {
            formularioRecursoDescricao.excluirRegistro(id);
        });
    },
    excluirRegistro: function (id) {

        this.requisicao(
                gridDocumentacaoDescricao.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridDocumentacaoDescricao.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },
});

$(document).ready(function () {
    gridDocumentacaoDescricao.iniciar();
});
