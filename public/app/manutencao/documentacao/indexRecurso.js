var gridRecursoSelecionar = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'descricao'},
        {'descricao': 'Chave', 'tipo': 'text', 'data': 'chave'},
        {'descricao': 'Valor', 'tipo': 'text', 'data': 'valor'},
        {'data': 'Detalhes'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridRecursoSelecionar.selecionarRecurso('
                    + row.id + ",'" + row.descricao +
                    '\');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatableRecursoSelecionar', '/recursos',
                this.colunas, this.colunasEspeciais);
    },

    selecionarRecurso: function (id, nome) {
        $('[name=idRecurso]').val(id + " - " + nome);
        $('.modal').modal('hide');
    }

});

$(document).ready(function () {
    gridRecursoSelecionar.iniciar();
});