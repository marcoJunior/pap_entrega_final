var Formulario = function () {
    var self = this;
    self.linhaSelecionada = {};
    self.statusSelecionado = 1;

    self.status = {
        1: 'Consulta/Edição',
        2: 'Adição'
    };

    this.transicaoLayout = function (formVisivel, complemento) {
        complemento = typeof complemento !== "undefined" ? complemento : '';
        if (formVisivel) {
            $('.Grid' + complemento).hide();
            $('.Formulario' + complemento).show();
        } else {
            $('.Formulario' + complemento).hide();
            $('.Grid' + complemento).show();
        }
    };

    this.setLabelStatus = function (codigo) {
        this.statusSelecionado = codigo;
        $('#statusForm').text(this.status[codigo]);
    };

    this.initDropZoneLogo = function (elemento, url, tipo, nomeArquivo) {
        url = typeof url === "undefined" ? "/dropzone/upload/logo" : url;
        tipo = typeof tipo === "undefined" ? 'image/*' : tipo;
        nomeArquivo = typeof nomeArquivo === "undefined" ? $('#id').val() + "" : nomeArquivo;
        $(elemento).dropzone({
            url: url,
            maxFilesize: 1,
            paramName: "file",
            maxThumbnailFilesize: 10,
            acceptedFiles: tipo,
            renameFilename: nomeArquivo,
            init: function () {
                this.on('success', function (file, json) {
                    if (url === "/dropzone/upload/logo") {
                        $('#logo').val(file.name);
                        $('#logoEmpresa').attr('src', "/imagens/admin/logo/cliente/" + file.name);
                    } else if (url === "/dropzone/upload/produtos") {
                        $('#imagem').val(file.name);
                        $('#imagemProduto').attr('src', "/imagens/admin/logo/produtos/" + file.name);
                    }
                });
            }
        }).addClass('dropzone');
    };

    this.initCep = function (input, btn) {
        $(input).mask("00000-000");
        if (typeof btn !== "undefined") {
            $(btn).on('click', function () {

                var cep = $('#cep').val().replace(/\D/g, "");

                if (!cep)
                    return;

                $.ajax({
                    method: 'GET',
                    dataType: "jsonp",
                    crossDomain: true,
                    timeout: 1000,
                    url: "https://viacep.com.br/ws/" + cep + "/json/",
                    beforeSend: function () {
                        load.mostra('html');
                    },
                }).done(function (resultado) {

                    if (resultado.erro == true) {
                        VanillaToasts.create({
                            title: 'Alerta de erro !',
                            text: "CEP invalido",
                            type: 'error', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                        return false;
                    }

                    $.each(resultado, function (i, val) {
                        var traducaoColunas = {
                            'uf': 'estado',
                            'cep': 'cep',
                            'bairro': 'bairro',
                            'localidade': 'cidade',
                            'logradouro': 'rua',
                            'complemento': 'complemento',
                        };

                        if (typeof traducaoColunas[i.trim()] !== 'undefined') {
                            $('[name=' + traducaoColunas[i.trim()] + ']').val(val.trim());
                        }

                    });

                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Endereço encontrado!<br>Confira as informações!',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }).fail(function (resultado) {
                    if (resultado.status == 404) {
                        VanillaToasts.create({
                            title: 'Alerta de erro !',
                            text: "CEP invalido",
                            type: 'error', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                        return false;
                    }
                }).always(function () {
                    load.esconde('html');
                });


            });
        }
    };

    this.initTelefone = function (elemento) {
        var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, '').length === 11
                    ? '(00) 00000-0000' : '(00) 0000-00009';
        };
        var spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            }
        };
        $(elemento).mask(SPMaskBehavior, spOptions);
    };

    this.initCpfCnpj = function (input, btn) {
        var MaskCpfCnpj = function (val) {
            return val.replace(/\D/g, '').length === 14
                    ? '00.000.000/0000-00' : '000.000.000-00999';
        };
        var MaskCpfCnpjOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(MaskCpfCnpj.apply({}, arguments), options);
            }
        };
        $(input).mask(MaskCpfCnpj, MaskCpfCnpjOptions);
        if (typeof btn !== "undefined") {
            $(btn).on('click', function () {
                var cnpj = $(input).val().replace(/\D/g, "");

                $.ajax({
                    method: 'GET',
                    dataType: "jsonp",
                    crossDomain: true,
                    timeout: 1000,
                    url: "https://www.receitaws.com.br/v1/cnpj/" + cnpj,
                    beforeSend: function () {
                        load.mostra('html');
                    },
                }).done(function (resultado) {
                    if (resultado.status == "ERROR") {
                        VanillaToasts.create({
                            title: 'Alerta de erro !',
                            text: resultado.message,
                            type: 'error', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () { } // executed when toast is clicked / optional parameter
                        });

                    } else {
                        $.each(resultado, function (i, val) {
                            if (typeof val == "object") {
                                if (i.trim() == 'qsa') {
                                    if (typeof val[0] !== "undefined") {
                                        $('[name=contato_nomeContato]').val(val[0]['nome'].trim());
                                    }
                                }
                            } else {
                                var traducaoColunas = {
                                    'nome': 'razaoSocial',
                                    'cep': 'cep',
                                    'logradouro': 'rua',
                                    'complemento': 'complemento',
                                    'bairro': 'bairro',
                                    'municipio': 'cidade',
                                    'uf': 'estado',
                                    'numero': 'numero',
                                };
                                if (i.trim() == 'abertura') {
                                    $('[name=' + traducaoColunas[i.trim()] + ']').val(
                                            moment(val.trim(), 'DD/MM/YYYY').format('YYYY-MM-DD'));
                                } else if (i.trim() == 'telefone') {
                                    if (typeof val.split("/")[0] !== "undefined") {
                                        $('[name=' + i.trim() + ']').val(val.split("/")[0].trim());
                                    }
                                } else if (typeof traducaoColunas[i.trim()] !== 'undefined') {
                                    $('[name=' + traducaoColunas[i.trim()] + ']').val(val.trim());
                                } else {
                                    $('[name=' + i.trim() + ']').val(val.trim());
                                }
                            }
                        });

                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Empresa encontrada!<br>Confira as informações!',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () { } // executed when toast is clicked / optional parameter
                        });
                    }
                }).fail(function (resultado) {
                    console.log(resultado);
                    console.log("error");
                }).always(function () {
                    load.esconde();
                });

            });
        }
    };

    this.initPreco = function (input) {
        $(input).mask("####0.00", {reverse: true});
    };

    this.requisicao = function (url, method, data, msgErro, success) {
        msgErro = typeof msgErro !== 'undefined' ? msgErro : 'Houve um erro!';
        method = typeof method !== 'undefined' ? method : 'POST';
        $.ajax({
            method: method,
            url: url,
            data: data,
            beforeSend: function () {
                load.mostra('html');
            }
        }).done(function (resultado) {
            if (typeof success === 'function') {
                success(resultado);
            }
        }).fail(function (resultado) {
            if (resultado.status === 403) {
                var resp = JSON.parse(resultado.responseText);
                msgErro = typeof resp.messagem !== "undefined" ? resp.messagem : msgErro;
            }

            VanillaToasts.create({
                title: 'Alerta de erro !',
                text: msgErro,
                type: 'error', // success, info, warning, error   / optional parameter​
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback: function () {} // executed when toast is clicked / optional parameter
            });
        }).always(function () {
            load.esconde('html');
            $('#loadUpdateBanco').hide();
        });
    }
};

function maxLengthCheck(object)
{
    if (object.value.length > object.maxLength)
        object.value = object.value.slice(0, object.maxLength)
}

function calculaDiaVencimento() {
    var dataVencimento = moment().subtract(-$('#diasValidade').val(), 'days').format('DD/MM/YYYY');
    $('#dataValidade').val(dataVencimento)
            .text(dataVencimento);
}
function formataReal(valor) {
    return "R$" + (parseFloat(valor).toFixed(2) + "").replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}
