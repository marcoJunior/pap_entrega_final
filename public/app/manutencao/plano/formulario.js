var formularioPlano = $.extend(new Formulario(), {
    iniciar: function () {
        this.initPreco('#preco');

        $('#TabPlano').on('click', function () {
            $('#tab1').show();
            $('#tab1').addClass('active');
            $('#tab2').hide();
            $('#tab2').removeClass('active');
            $('.datatable_Filtros').find('.btnAdicionar').show();
        });

        $('#TabRecursosContratado').on('click', function () {
            $('#tab1').hide();
            $('#tab1').removeClass('active');
            $('#tab2').show();
            $('#tab2').addClass('active');
            $('.datatable_Filtros').find('.btnAdicionar').hide();
        });

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $('#TabPlano').click();
        document.forms.plano.reset();

        $('#TabRecursosContratado').hide();
        $('.panelsTotais').hide();

        $('[name=descricao]').val('Novo');
        gridRecursosContratados.getDatarTable().ajax.reload();

        setTimeout(function () {
            formularioRecursosContratados.carregarTotais();
        }, 100);

        formularioPlano.statusSelecionado = 2;
    },
    transicaoLayout2: function (param) {
        this.transicaoLayout(param);
        if (!param) {
            $('#TabRecursosContratado').show();
            $('.panelsTotais').show();
        }
    },
    clickBotaoConsultar: function (codigo, nome) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout2(true);

        $.each(formularioPlano.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            } else if (index.trim() === 'idSistema') {
                $('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioPlano.linhaSelecionada[codigo + "-" + nome]['sistema']
                        );

            }
        });

        if (typeof gridRecursosContratados.getDatarTable() === "undefined") {
            gridRecursosContratados.iniciar();
        } else {
            gridRecursosContratados.getDatarTable().ajax.reload();
        }

        setTimeout(function () {
            formularioRecursosContratados.carregarTotais();
        }, 100);


    },

    clickAtivacao: function (codigo, nome) {

        $.each(formularioPlano.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });

        setTimeout(function () {
            var form = $('#plano');
            var disabled = form.find(':input:disabled').removeAttr('disabled');
            var valFormulario = form.serializeObject();

            disabled.attr('disabled', 'disabled');
            valFormulario.ativo = $('#' + codigo).prop('checked');

            formularioPlano.alterarRegistro(valFormulario);
        }, 150);
        formularioPlano.statusSelecionado = 2;
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#idSistema').val();

        var form = $('#plano');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('#plano').find('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioPlano.status[formularioPlano.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioPlano.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioPlano.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridPlano.getDatarTable().ajax.reload();
                    formularioPlano.transicaoLayout(false);

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !";
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioPlano.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + '/' + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridPlano.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    gridPlano.iniciar();
    formularioPlano.iniciar();
});