var gridPlano = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
//        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'tipo': 'text', 'data': 'sistema'},
        {'descricao': 'Descricao', 'tipo': 'text', 'data': 'descricao'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            formularioPlano.linhaSelecionada[(row.id + '-' + row.descricao)] = row;

            var nome = "'" + row.descricao + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioPlano.clickBotaoConsultar('
                    + row.id + ',' + nome +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioPlano.linhaSelecionada[(row.id + '-' + row.descricao)] = row;

            var nome = "'" + row.descricao + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioPlano.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});