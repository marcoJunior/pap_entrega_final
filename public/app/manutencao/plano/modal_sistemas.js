var gridSistema = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'nome'},
        {'data': 'Selecionar'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(2, false, function (data, type, row) {

            var nome = "'" + row.nome + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridSistema.selecionarSistema('
                    + row.id + ',' + nome +
                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatableSistema', '/sistemas',
                this.colunas, this.colunasEspeciais);
    },

    selecionarSistema: function (id, nome) {
        $('[name=idSistema]').val(id + " - " + nome);
        $('.modal').modal('hide');
    }

});

$(document).ready(function () {
    gridSistema.iniciar();
});