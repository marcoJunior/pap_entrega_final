var gridRecursosContratados = $.extend(new serviceGrid(), {

    colunasEspeciais: [],
    colunas: [
//        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'tipo': 'text', 'data': 'recurso'}, //'descricao': 'Plano',
        {'tipo': 'text', 'data': 'submodulo'}, //'descricao': 'submodulo',
        {'descricao': 'Qtd. Liberada', 'tipo': 'number', 'data': 'quantidade'},
        {'tipo': 'number', 'data': 'preco'}, //UNITARIO
        {'tipo': 'number', 'data': 'preco'}, //SUBTOTAL
        {'descricao': 'Data da Liberação', 'tipo': 'date', 'data': 'data'},
        {'data': 'Excluir'}
    ],
    iniciar: function () {

        this.criarBotaoEspecial(1, false, function (data, type, row) {
            return row.submodulo == '1' ? 'SIM' : 'NÃO';
        });

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            return "R$ " + (parseFloat(row.preco).toFixed(2) + "").replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        });
        this.criarBotaoEspecial(4, false, function (data, type, row) {
            var valor = parseFloat(row.quantidade).toFixed(2) * parseFloat(row.preco).toFixed(2);
            return "R$ " + (parseFloat(valor).toFixed(2) + "").replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        });

        this.criarBotaoEspecial(6, false, function (data, type, row) {

            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioRecursosContratados.confirmarExclusaoRegistro('
                    + row.id +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';
            return '<center>' + btnExcluir + '</center>';
        });

        this.carregarGrid(
                '#datatableRecursoContratado', '/recursos-contratados?filtroCliente=true',
                this.colunas, this.colunasEspeciais, null, null, function () {
                    return {
                        'codigo': $('#id').val(),
                    };
                });
    },
});
var formularioRecursosContratados = $.extend(new Formulario(), {

    carregarTotais: function () {

        var form = $('#plano');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();
        valFormulario['idPlano'] = typeof gridPlano.linhaSelecionada.id !== "undefined" ? gridPlano.linhaSelecionada.id : null;

        $.ajax({method: 'POST', timeout: 1000,
            url: "/recursos-contratados/" + gridPlano.linhaSelecionada.id + "/calcular-totais",
            data: {'formulario': valFormulario},
            beforeSend: function () {
                load.mostra('html');
            },
        }).done(function (resultado) {
            $('#qtdRecursos').text(resultado.qtdTotalRecursos);
            if (resultado.valorTotalPlano !== null) {
                $('#valorTotal').text("R$ " + (resultado.valorTotalPlano).replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
            } else {
                $('#valorTotal').text("R$ 0,00");
            }
        }).fail(function (resultado) {
            if (resultado.status == 404) {
                VanillaToasts.create({
                    title: 'Alerta de erro !',
                    text: "Não foi possivel carregar os planos contratados!",
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 5000, // hide after 5000ms, // optional parameter
                    callback: function () {} // executed when toast is clicked / optional parameter
                });
                return false;
            }
        }).always(function () {
            load.esconde('html');
        });
    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';
        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });
        $(toast).find('.btn-danger').on('click', function () {
            formularioRecursosContratados.excluirRegistro(id);
        });
    },
    excluirRegistro: function (id) {
        this.requisicao(
                '/recursos-contratados/' + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridRecursosContratados.getDatarTable().ajax.reload();
                    formularioRecursosContratados.carregarTotais();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },
});
