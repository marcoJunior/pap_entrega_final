var idCliente = 0;
var formularioProdutos = $.extend(new Formulario(), {
    iniciar: function () {
        this.initDropZoneLogo('#file-dropzone', '/dropzone/upload/produtos');
        this.initPreco('#produto #preco');

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        $('#TabEmpresas').click();
        $('#imagemProduto').attr('src', "/img/notfound.jpg");

        $('#TabPlanosContratado').hide();
        $('#TabContratarPlanos').hide();
        $('.panelsTotais').hide();
        $('#produtoSelecionadoGrid').hide();

        document.forms.produto.reset();

    },
    transicaoLayout2: function (param) {
        $('.panelsTotais').hide();
        if (!param) {
            $('#TabPlanosContratado').click();
        }
    },

    clickBotaoConsultar: function (codigo, fantasia) {
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        var logo = "/img/notfoundRow.png";
        var produtoImg = $('#produto #imagem').val();
        if (produtoImg !== "") {
            logo = "/imagens/admin/logo/produtos/" + produtoImg;
        }

        $('#imagemProduto').attr('src', logo);
    },

    confirmarAlteracaoDeRegistro: function () {
        var id = $('#idProduto').val();
        var nome = $('#nome').val();

        var form = $('#produto');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioProdutos.status[formularioProdutos.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + id + ' - ' + nome + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioProdutos.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario, url) {
        var method = formularioProdutos.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        url = typeof url !== "undefined" ? url : location.pathname;
        var id = $('#idProduto').val();
        if (id) {
            url = url + '/' + id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridProdutos.getDatarTable().ajax.reload();
                    formularioProdutos.transicaoLayout(false);
                    $('.panelsTotais').hide();

                    $('#TabPlanosContratado').show();
                    $('#TabContratarPlanos').show();
                    $('.panelsTotais').show();

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id, fantasia) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioProdutos.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + '/' + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridProdutos.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

    uploadLogo: function () {
        $('#file-dropzone').html('');
        $('.UploadLogo').modal();
    }

});

$(document).ready(function () {
    gridProdutos.iniciar();
    formularioProdutos.iniciar();
});
