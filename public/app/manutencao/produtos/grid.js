var gridProdutos = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'ID', 'tipo': 'number', 'data': 'id'},
        {'tipo': 'text', 'data': 'imagem', 'width': '10%'},
        {'descricao': 'Nome', 'tipo': 'text', 'data': 'nome'},
        {'descricao': 'Descricao', 'tipo': 'text', 'data': 'descricao'},
        {'descricao': 'Preco', 'tipo': 'text', 'data': 'preco'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(1, true, function (data, type, row) {
            var logo = "/img/notfoundRow.png";
            if (row.imagem.trim() !== "") {
                logo = "/imagens/admin/logo/produtos/" + row.imagem.trim();
            }
            return '<img style="max-width:100px;max-height:25px;" src="' + logo + '"/>';
        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            formularioProdutos.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioProdutos.clickBotaoConsultar('
                    + row.id + ',' + fantasia +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(6, false, function (data, type, row) {
            formularioProdutos.linhaSelecionada[(row.id + '-' + row.fantasia)] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioProdutos.confirmarExclusaoRegistro('
                    + row.id + ',' + fantasia +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);

        $('#datatable tbody').on('click', 'tr', function () {
            var row = gridProdutos.getDatarTable().data()[gridProdutos.linhaSelecionadaIndex];
            var codigo = row.id;
            var fantasia = row.fantasia;

            $('#clienteSelecionadoGrid').text("(" + fantasia + ")");
            $.each(row, function (index, value) {
                if (typeof value === "object") {
                    $.each(value, function (i, val) {
                        $('[name=' + i.trim() + ']').val(val.trim());
                    });
                } else {

                    $('[name=' + index.trim() + ']').val(value.trim());
                    if (index.trim() === "logo") {
                        var logo = "/img/notfound.jpg";
                        if (value.trim() !== "") {
                            logo = "/imagens/admin/logo/cliente/" + value.trim();
                        }
                        $('#logoEmpresa').attr('src', logo);
                    } else if (index.trim() === 'ativo') {
                        var check = $('[name=' + index.trim() + ']');
                        check.prop('checked', false);
                        if (!!value) {
                            check.prop('checked', true);
                        }
                    } else if (index.trim() === 'id') {
                        idCliente = value.trim();
                        $('[name=' + index.trim() + ']').val(value.trim());
                        setTimeout(function () {
                            $('[name=idCliente]').val(
                                    value.trim() + " - " + fantasia
                                    );
                        }, 150);

                    }
                }
            });

        });

    }

});
