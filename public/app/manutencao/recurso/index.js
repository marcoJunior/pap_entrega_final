var gridRecurso = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'descricao'},
        {'descricao': 'Chave', 'tipo': 'text', 'data': 'chave'},
        {'descricao': 'Valor', 'tipo': 'text', 'data': 'valor'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            var valor = parseFloat(row.valor).toFixed(2);
            return "R$ " + (parseFloat(valor).toFixed(2) + "").replace('.', ',').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
        });


        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioRecurso.linhaSelecionada[(row.id + '-' + row.descricao)] = row;

            var nome = "'" + row.descricao + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioRecurso.clickBotaoConsultar('
                    + row.id + ',' + nome +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(5, false, function (data, type, row) {
            formularioRecurso.linhaSelecionada[(row.id + '-' + row.descricao)] = row;

            var nome = "'" + row.descricao + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioRecurso.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

var formularioRecurso = $.extend(new Formulario(), {
    iniciar: function () {
        this.initPreco('#valor');
    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.sistema.reset();
    },

    clickBotaoConsultar: function (codigo, nome) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioRecurso.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            } else if (index.trim() === 'valor') {
                var valor = parseFloat(value.trim()).toFixed(2).toString().replace(',', '.').replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
                $('[name=' + index.trim() + ']').val(valor);
            }

        });
    },
    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#descricao').val();

        var form = $('#sistema');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('#sistema').find('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioRecurso.status[formularioRecurso.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioRecurso.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioRecurso.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    formularioRecurso.transicaoLayout(false);
                    gridRecurso.getDatarTable().ajax.reload();

                    if (typeof resultado.mensagem == "undefined") {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: resultado.mensagem,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioRecurso.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridRecurso.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    gridRecurso.iniciar();
    formularioRecurso.iniciar();
});
