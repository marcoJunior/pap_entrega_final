var gridRecursosContratados = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Data', 'tipo': 'date', 'data': 'data'},
        {'descricao': 'Quantidade', 'tipo': 'number', 'data': 'quantidade'},
        {'descricao': 'Plano', 'tipo': 'text', 'data': 'plano'},
//        {'descricao': 'Cliente', 'tipo': 'text', 'data': 'cliente'},
//        {'data': 'Selecionar'}
    ],

    iniciar: function () {

//        this.criarBotaoEspecial(2, false, function (data, type, row) {
//
//            var nome = "'" + row.nome + "'";
//            var btnConsultar = '<button type="button" ' +
//                    ' onclick="gridSistema.selecionarSistema('
//                    + row.id + ',' + nome +
//                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
//                    '<i class="fa fa-arrow-right"></i></button>';
//
//            return '<center>' + btnConsultar + '</center>';
//
//        });
//
        this.carregarGrid(
                '#datatableRecursoContratado', '/recursos-contratados',
                this.colunas, this.colunasEspeciais, null, null,
                {
                    'codigo': $('#id').val()
                });
    },

    selecionarSistema: function (id, nome) {
        $('[name=idSistema]').val(id + " - " + nome);
        $('.modal').modal('hide');
    }

});

$(document).ready(function () {
    gridRecursosContratados.iniciar();
});