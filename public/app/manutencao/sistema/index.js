var gridSistema = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'data': 'ativo', 'tipo': 'sim/nao'},
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'nome'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(0, true, function (data, type, row) {
            var nome = "'" + row.nome + "'";
            var checked = '';

            if (parseInt(row.ativo) === 1) {
                checked = 'checked="checked"';
            }
            var checkbox = '<input class="hidden-box" onchange="formularioSistema.clickAtivacao(' +
                    +row.id + ',' + nome +
                    ');" id="' + row.id +
                    '" style="width: 25px!important;height: 15px!important;"' +
                    ' type="checkbox" ' + checked + ' >';

            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;' + checkbox + '</span>';
        });

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            formularioSistema.linhaSelecionada[(row.id + '-' + row.nome)] = row;

            var nome = "'" + row.nome + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioSistema.clickBotaoConsultar('
                    + row.id + ',' + nome +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioSistema.linhaSelecionada[(row.id + '-' + row.nome)] = row;

            var nome = "'" + row.nome + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioSistema.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

var formularioSistema = $.extend(new Formulario(), {
    iniciar: function () {

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.sistema.reset();
    },

    clickBotaoConsultar: function (codigo, nome) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioSistema.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });
    },

    clickAtivacao: function (codigo, nome) {

        $.each(formularioSistema.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });

        setTimeout(function () {
            var form = $('#sistema');
            var disabled = form.find(':input:disabled').removeAttr('disabled');
            var valFormulario = form.serializeObject();

            disabled.attr('disabled', 'disabled');
            valFormulario.ativo = $('#' + codigo).prop('checked');

            formularioSistema.alterarRegistro(valFormulario);
        }, 150);
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#nome').val();

        var form = $('#sistema');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('#sistema').find('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioSistema.status[formularioSistema.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioSistema.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioSistema.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    formularioSistema.transicaoLayout(false);
                    gridSistema.getDatarTable().ajax.reload();

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !";
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioSistema.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridSistema.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    gridSistema.iniciar();
    formularioSistema.iniciar();
});
