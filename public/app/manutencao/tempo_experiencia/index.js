var gridTempoExperiencia = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Empresa', 'tipo': 'text', 'data': 'cliente', 'width': '25%'},
        {'descricao': 'Plano', 'tipo': 'text', 'data': 'plano'},
        {'descricao': 'Sistame/Modulo', 'tipo': 'number', 'data': 'sistema'},
        {'descricao': 'Data de cadastro', 'tipo': 'date', 'data': 'data'},
        {'descricao': 'Data de validade', 'tipo': 'date', 'data': 'dataValidade'},
        {'tipo': 'number', 'data': 'diasValidade'},
        {'tipo': 'number', 'data': 'diasVencimento'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'},
    ],

    iniciar: function () {

        this.criarBotaoEspecial(7, false, function (data, type, row) {
            try {
                var btnAviso = "btn-info";
                if (data < 10) {
                    btnAviso = "btn-danger";
                } else if (data < 20) {
                    btnAviso = "btn-warning";
                } else if (data > 30) {
                    btnAviso = "btn-success";
                }
                return '<center><a class="btn ' + btnAviso + ' btn-xs">' + data + ' dias</a></center>';
            } catch (ex) {
                return '<center><a class="btn btn-xs">' +
                        'Não definido</a></center>';
            }

        });

        this.criarBotaoEspecial(8, false, function (data, type, row) {
            formularioTempoExperiencia.linhaSelecionada[row.id] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioTempoExperiencia.clickBotaoConsultar('
                    + row.id + ',' + fantasia +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(9, false, function (data, type, row) {
            formularioTempoExperiencia.linhaSelecionada[row.id] = row;

            var fantasia = "'" + row.fantasia + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioTempoExperiencia.confirmarExclusaoRegistro('
                    + row.id + ',' + fantasia +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

var formularioTempoExperiencia = $.extend(new Formulario(), {
    iniciar: function () {
        if ($('#data').val() === "undefined" || $('#data').val() === "") {
            $('#data').val(moment().format('DD/MM/YYYY'));
        }
        $('#diasValidade').on('change', function () {
            calculaDiaVencimento();
        });
        $('#diasValidade').keyup(function () {
            calculaDiaVencimento();
        });
        $('#diasValidade').keydown(function () {
            calculaDiaVencimento();
        });
        $('#diasValidade').on('click', function () {
            if ($('#vanillatoasts-container').html() === '') {
                VanillaToasts.create({
                    title: 'Atenção !',
                    text: 'Dia de vencimento sera calculado há partir do dia atual para gerar uma nova validade !',
                    type: 'info', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
            }
            $('#data').val(moment().format('DD/MM/YYYY'));
        });

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.tempoExperiencia.reset();
        $('#data').val(moment().format('DD/MM/YYYY'));
    },

    clickBotaoConsultar: function (codigo, fantasia) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioTempoExperiencia.linhaSelecionada[codigo], function (index, value) {
            $('[name=' + index.trim() + ']').val(value.trim());

            if (index.trim() === 'idCliente') {
                $('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioTempoExperiencia.linhaSelecionada[codigo]['cliente']
                        );

            } else if (index.trim() === 'idContrato') {
                $('[name=' + index.trim() + ']').val(
                        value.trim() + " - " + formularioTempoExperiencia.linhaSelecionada[codigo]['sistema']
                        );
            } else if (index.trim() === 'dataValidade') {
                $('[name=' + index.trim() + ']').text(value.trim());
            }

        });
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#idCliente').val();
        var fantasia = $('#idSistema').val();

        var form = $('#tempoExperiencia');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');
        valFormulario.ativo = $('[name=ativo]').prop('checked');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioTempoExperiencia.status[formularioTempoExperiencia.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + ' / ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioTempoExperiencia.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioTempoExperiencia.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    gridTempoExperiencia.getDatarTable().ajax.reload();
                    formularioTempoExperiencia.transicaoLayout(false);

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !"
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id, fantasia) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + ' - ' + fantasia + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioTempoExperiencia.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridTempoExperiencia.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    gridTempoExperiencia.iniciar();
    formularioTempoExperiencia.iniciar();
});
