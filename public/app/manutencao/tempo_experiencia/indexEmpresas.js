var gridEmpresas = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Fantasia', 'tipo': 'text', 'data': 'fantasia'},
        {'descricao': 'Documento', 'tipo': 'text', 'data': 'documento'},
        {'data': 'Detalhes'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(3, false, function (data, type, row) {

            var nome = "'" + row.fantasia + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridEmpresas.selecionarCliente('
                    + row.id + ',' + nome +
                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });

        this.carregarGrid(
                '#datatableEmpresas', '/clientes',
                this.colunas, this.colunasEspeciais);
    },

    selecionarCliente: function (id, nome) {
        $('[name=idCliente]').val(id + " - " + nome);
        $('[name=idContrato]').val('');
        $('.modal').modal('hide');
        gridSistema.getDatarTable().ajax.reload();
    }

});

$(document).ready(function () {
    gridEmpresas.iniciar();
});