var gridSistema = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Cliente', 'tipo': 'text', 'data': 'cliente'},
        {'descricao': 'Apelido', 'tipo': 'text', 'data': 'apelido'},
        {'descricao': 'Plano', 'tipo': 'text', 'data': 'plano'},
        {'descricao': 'Descrição', 'tipo': 'text', 'data': 'sistema'},
        {'data': 'Selecionar'}
    ],

    iniciar: function () {

        this.criarBotaoEspecial(5, false, function (data, type, row) {

            var nome = "'" + row.sistema + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="gridSistema.selecionarSistema('
                    + row.id + ',' + nome +
                    ');" class="btn btn-info btn-xs BtnDetalhes">' +
                    '<i class="fa fa-arrow-right"></i></button>';

            return '<center>' + btnConsultar + '</center>';

        });


        var url = '/contrato';
        this.carregarGrid(
                '#datatableSistema', url,
                this.colunas, this.colunasEspeciais, null, null, function () {
                    return {
                        'cliente': parseInt($('#idCliente').val())
                    };
                });
    },

    selecionarSistema: function (id, nome) {
        $('[name=idContrato]').val(id + " - " + nome);
        $('.modal').modal('hide');
    }

});

$(document).ready(function () {
    gridSistema.iniciar();
});