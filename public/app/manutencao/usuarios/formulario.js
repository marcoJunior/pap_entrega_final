var idCliente = 0;
var formularioUsuario = $.extend(new Formulario(), {
    iniciar: function () {

    },

    adicionarRegistro: function () {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.usuarios.reset();
    },

    clickBotaoConsultar: function (codigo, nome) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioUsuario.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });
    },

    clickAtivacao: function (codigo, nome) {

        $.each(formularioUsuario.linhaSelecionada[codigo + "-" + nome], function (index, value) {

            $('[name=' + index.trim() + ']').val(value.trim());
            if (index.trim() === 'ativo') {
                var check = $('[name=' + index.trim() + ']');
                check.prop('checked', false);
                if (!!value) {
                    check.prop('checked', true);
                }
            }

        });

        setTimeout(function () {
            var form = $('#usuarios');
            var disabled = form.find(':input:disabled').removeAttr('disabled');
            var valFormulario = form.serializeObject();

            disabled.attr('disabled', 'disabled');
            valFormulario.ativo = $('#' + codigo).prop('checked');

            formularioUsuario.alterarRegistro(valFormulario);
        }, 150);
    },

    confirmarAlteracaoDeRegistro: function () {
        var codigo = $('#nome').val();

        var form = $('#usuarios');
        var disabled = form.find(':input:disabled').removeAttr('disabled');
        var valFormulario = form.serializeObject();

        disabled.attr('disabled', 'disabled');

        var continuar = true;
        $.each(valFormulario, function (index, value) {
            if (typeof $('[name=' + index + ']').attr('required') !== "undefined"
                    && value == "") {
                VanillaToasts.create({
                    title: 'Alerta de erro!',
                    text: 'Preencha todos os campos obrigatórios !',
                    type: 'error', // success, info, warning, error   / optional parameter​
                    timeout: 10000, // hide after 5000ms, // optional parameter
                });
                continuar = false;
                return continuar;
            }
        });
        if (!continuar) {
            return false;
        }

        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        var descricao = formularioUsuario.status[formularioUsuario.statusSelecionado];
        var toast = VanillaToasts.create({
            title: 'Alerta de ' + descricao + ' !',
            text: 'Deseja mesmo ' + descricao + ' cadastro (<b>' + codigo + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'info', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', function () {
            formularioUsuario.alterarRegistro(valFormulario);
        });

    },

    alterarRegistro: function (formulario) {
        var method = formularioUsuario.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        var url = location.pathname;
        if (formulario.id) {
            url = url + '/' + formulario.id;
        }

        this.requisicao(
                url,
                method,
                {'formulario': formulario},
                "Não foi possivel alterar o registro ! <br>Tente novamente mais tarde !",
                function (resultado) {
                    formularioUsuario.transicaoLayout(false);
                    gridUsuarios.getDatarTable().ajax.reload();

                    if (resultado[0] !== "Cadastro já existente !" && resultado[0] !== 0) {
                        VanillaToasts.create({
                            title: 'Sucesso !',
                            text: 'Registro alterado com sucesso !',
                            type: 'success', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    } else {
                        var erro = resultado[0];
                        if (resultado[0] === 0) {
                            erro = "Não foi possivel alterar o registro, tente novamente mais tarde !";
                        }
                        VanillaToasts.create({
                            title: 'Alerta !',
                            text: erro,
                            type: 'warning', // success, info, warning, error   / optional parameter​
                            timeout: 5000, // hide after 5000ms, // optional parameter
                            callback: function () {} // executed when toast is clicked / optional parameter
                        });
                    }

                }
        );
    },

    confirmarExclusaoRegistro: function (id) {

        $('.modal').modal('hide');
        var btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        var toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: 'Deseja mesmo excluir cadastro (<b>' + id + '</b>) ? <p>' +
                    btnConfirmar,
            type: 'error', // success, info, warning, error   / optional parameter​
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', function () {
            formularioUsuario.excluirRegistro(id);
        });

    },

    excluirRegistro: function (id) {
        this.requisicao(
                location.pathname + "/" + id,
                'DELETE',
                {'id': id},
                'Não foi possivel excluir o registro!',
                function (resultado) {
                    gridUsuarios.getDatarTable().ajax.reload();
                    VanillaToasts.create({
                        title: 'Sucesso !',
                        text: 'Registro excluido !',
                        type: 'success', // success, info, warning, error   / optional parameter​
                        timeout: 5000, // hide after 5000ms, // optional parameter
                        callback: function () {} // executed when toast is clicked / optional parameter
                    });
                }
        );
    },

});

$(document).ready(function () {
    formularioUsuario.iniciar();
});
