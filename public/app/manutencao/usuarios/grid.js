var gridUsuarios = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {'descricao': 'Id', 'tipo': 'number', 'data': 'id'},
        {'descricao': 'Usuario', 'tipo': 'text', 'data': 'usuario'},
        {'descricao': 'Nome completo', 'tipo': 'text', 'data': 'nomeCompleto'},
        {'data': 'Detalhes'},
        {'data': 'Excluir'}
    ],

    iniciar: function () {

//        this.criarBotaoEspecial(0, true, function (data, type, row) {
//            var nome = "'" + row.nome + "'";
//            var checked = '';
//
//            if (parseInt(row.ativo) === 1) {
//                checked = 'checked="checked"';
//            }
//            var checkbox = '<input class="hidden-box" onchange="formularioUsuario.clickAtivacao(' +
//                    +row.id + ',' + nome +
//                    ');" id="' + row.id +
//                    '" style="width: 25px!important;height: 15px!important;"' +
//                    ' type="checkbox" ' + checked + ' >';
//
//            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;' + checkbox + '</span>';
//        });

        this.criarBotaoEspecial(3, false, function (data, type, row) {
            formularioUsuario.linhaSelecionada[(row.id + '-' + row.nome)] = row;

            var nome = "'" + row.nome + "'";
            var btnConsultar = '<button type="button" ' +
                    ' onclick="formularioUsuario.clickBotaoConsultar('
                    + row.id + ',' + nome +
                    ');" class="btn bg-blue-soft btn-xs BtnDetalhes">' +
                    '<i class="font-white fa fa-search"></i></button>';

            return '<center>' + btnConsultar + '</center>';
        });

        this.criarBotaoEspecial(4, false, function (data, type, row) {
            formularioUsuario.linhaSelecionada[(row.id + '-' + row.nome)] = row;

            var nome = "'" + row.nome + "'";
            var btnExcluir = '<button type="button" ' +
                    ' onclick="formularioUsuario.confirmarExclusaoRegistro('
                    + row.id + ',' + nome +
                    ');" class="btn btn-danger btn-xs BtnExcluir">' +
                    '<i class="fa fa-trash"></i></button>';

            return '<center>' + btnExcluir + '</center>';

        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    }

});

$(document).ready(function () {
    gridUsuarios.iniciar();
});
