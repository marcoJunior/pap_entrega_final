/* global $, VanillaToasts, Formulario, gridTiposUsuarios, */

$(document).ready(() => {
    setTimeout(function () {
        $('.permissoes [type=checkbox], .permissoes .checker').hide();
    }, 150);

    $('.permissoes [name=btnLimparPermissoes]').on('click', function () {

        const input = $(this).parent().parent().find('.selecionaTipoPermissao');
        const label = input.parent().parent().parent();

        label.removeClass('label-success');
        label.addClass('label-danger');

        input.prop('checked', false);
        input.attr('value', 0);
    });

    $('.selecionaTipoPermissao').on('click', function () {
        const label = $(this).parent().parent().parent();
        if ($(this).is(':checked')) {
            label.addClass('label-success');
            label.removeClass('label-danger');
            $(this).attr('value', 1);
        } else {
            label.addClass('label-danger');
            label.removeClass('label-success');
            $(this).attr('value', 0);
        }

    });

});

const formularioTiposUsuario = $.extend(new Formulario(), {
    iniciar() {

    },

    adicionarRegistro() {
        this.setLabelStatus(2);
        this.transicaoLayout(true);

        document.forms.tiposPermissoes.reset();
    },

    clickBotaoConsultar(codigo) {
        $('.modal').modal('hide');
        this.setLabelStatus(1);
        this.transicaoLayout(true);

        $.each(formularioTiposUsuario.linhaSelecionada[codigo], (index, value) => {
            $(`[name=${index.trim()}]`).val(value.trim());
//            if (index.trim() === 'ativo') {
//                const check = $(`[name=${index.trim()}]`);
//                check.prop('checked', false);
//                if (value) {
//                    check.prop('checked', true);
//                }
//            }
        });

        $('.permissoesIdTipo').val(codigo);

        const method = 'GET';
        let url = `${location.pathname}/${codigo}/permissoes`;

        this.requisicao(url, method, {},
                'Não foi possivel consultar os registros! <br>Tente novamente mais tarde !',
                (resultado) => {

            $.each(resultado, (posicao, valor) => {
                $.each(valor, (posi, val) => {
                    const name = (valor.url).replace(/\//g, '');
                    const input = $(`#${name}${posi}`);

                    if (posi === 'id') {
                        input.val(val);
                    } else if (posi === 'get' || posi === 'post' || posi === 'put' || posi === 'delete') {

                        const label = input.parent().parent().parent();

                        input.prop('checked', 0);

                        label.addClass('label-danger');
                        label.removeClass('label-success');

                        $(this).attr('value', 0);
                        if (val === "1") {
                            input.click();
                        }

                    }
                });

            });
//            console.log(resultado);

        });



    },
    confirmarAlteracaoDeRegistro() {
        const data = $('#tiposPermissoes').serializeObject();

        if ($('#tiposPermissoes [name=descricao]').val().trim() === '') {
            VanillaToasts.create({
                title: 'Alerta !',
                text: 'Defina um tipo de usuario!',
                type: 'warning', // success, info, warning, error   / optional parameter
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback() { }, // executed when toast is clicked / optional parameter
            });

            return false;
        }

        const btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-danger">Cancelar</button>' +
                '<button type="button" class="btn btn-success">Confirmar</button>' +
                '</div>';

        const descricao = $('#tiposPermissoes [name=descricao]').val();
        const toast = VanillaToasts.create({
            title: `Confirmação !`,
            text: `Deseja mesmo editar permissões do tipo de usuario (<b>${descricao}</b>) ? <p>${btnConfirmar}`,
            type: 'info', // success, info, warning, error   / optional parameter
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-success').on('click', () => {
            formularioTiposUsuario.alterarRegistro(data);
        });
    },

    alterarRegistro(formulario) {
        const method = formularioTiposUsuario.statusSelecionado == 1 ? 'PUT' : 'POST';
        this.setLabelStatus(1);

        let url = location.pathname;
        if (formulario.id) {
            url = `${url}/${formulario.id}/permissoes`;
        }

        this.requisicao(
                url,
                method,
                {formulario},
                'Não foi possivel alterar os registros ! <br>Tente novamente mais tarde !',
                (resultado) => {
            formularioTiposUsuario.transicaoLayout(false);
            gridTiposUsuarios.getDatarTable().ajax.reload();
//
//            if (resultado[0] !== 'Cadastro já existente !' && resultado[0] !== 0) {
//                VanillaToasts.create({
//                    title: 'Sucesso !',
//                    text: 'Registro alterado com sucesso !',
//                    type: 'success', // success, info, warning, error   / optional parameter
//                    timeout: 5000, // hide after 5000ms, // optional parameter
//                    callback() { }, // executed when toast is clicked / optional parameter
//                });
//            } else {
//                let erro = resultado[0];
//                if (resultado[0] === 0) {
//                    erro = 'Não foi possivel alterar o registro, tente novamente mais tarde !';
//                }
//                VanillaToasts.create({
//                    title: 'Alerta !',
//                    text: erro,
//                    type: 'warning', // success, info, warning, error   / optional parameter
//                    timeout: 5000, // hide after 5000ms, // optional parameter
//                    callback() { }, // executed when toast is clicked / optional parameter
//                });
//            }
        });
    },

    confirmarExclusaoRegistro(id) {
        $('.modal').modal('hide');
        const btnConfirmar = '<div style="margin-top:20px;" class="brn-group text-center">' +
                '<button type="button" class="btn btn-default">Cancelar</button>' +
                '<button type="button" class="btn btn-danger">Confirmar</button>' +
                '</div>';

        const toast = VanillaToasts.create({
            title: 'Alerta de exclusão !',
            text: `Deseja mesmo excluir cadastro (<b>${id}</b>) ? <p>${
                    btnConfirmar}`,
            type: 'error', // success, info, warning, error   / optional parameter
            timeout: 10000, // hide after 5000ms, // optional parameter
        });

        $(toast).find('.btn-danger').on('click', () => {
            formularioTiposUsuario.excluirRegistro(id);
        });
    },

    excluirRegistro(id) {
        this.requisicao(
                `${location.pathname}/${id}`,
                'DELETE',
                {id},
                'Não foi possivel excluir o registro!',
                () => {
            gridTiposUsuarios.getDatarTable().ajax.reload();
            VanillaToasts.create({
                title: 'Sucesso !',
                text: 'Registro excluido !',
                type: 'success', // success, info, warning, error   / optional parameter
                timeout: 5000, // hide after 5000ms, // optional parameter
                callback() { }, // executed when toast is clicked / optional parameter
            });
        }
        );
    },

});

$(document).ready(() => {
    formularioTiposUsuario.iniciar();
});
