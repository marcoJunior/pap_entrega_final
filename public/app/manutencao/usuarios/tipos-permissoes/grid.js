/* global $, formularioTiposUsuario */
const gridTiposUsuarios = $.extend(new serviceGrid(), {

    colunasEspeciais: [],

    colunas: [
        {descricao: 'Id', tipo: 'number', data: 'id'},
        {descricao: 'Usuario', tipo: 'text', data: 'descricao'},
        {data: 'Detalhes'},
        {data: 'Excluir'},
    ],

    //        this.criarBotaoEspecial(0, true, function (data, type, row) {
    //            var nome = "'" + row.nome + "'";
    //            var checked = '';
    //
    //            if (parseInt(row.ativo) === 1) {
    //                checked = 'checked="checked"';
    //            }
    //            var checkbox = '<input class="hidden-box" onchange="
    // formularioTiposUsuario.clickAtivacao(' +
    //                    +row.id + ',' + nome +
    //                    ');" id="' + row.id +
    //                    '" style="width: 25px!important;height: 15px!important;"' +
    //                    ' type="checkbox" ' + checked + ' >';
    //
    //            return '<span value="' + row.ativo + '">&nbsp;&nbsp;&nbsp;'
    // + checkbox + '</span>';
    //        });
    //
    iniciar() {
        this.criarBotaoEspecial(2, false, (data, type, row) => {
            formularioTiposUsuario.linhaSelecionada[row.id] = row;

            const btnConsultar = `<button type="button" 
                onclick="formularioTiposUsuario.clickBotaoConsultar(${row.id});"  
                class="btn bg-blue-soft btn-xs BtnDetalhes">
                    <i class="font-white fa fa-search"></i>
                </button>`;

            return `<center>${btnConsultar}</center>`;
        });

        this.criarBotaoEspecial(3, false, (data, type, row) => {
            formularioTiposUsuario.linhaSelecionada[row.id] = row;

            const btnExcluir = `<button type="button" 
                    onclick="formularioTiposUsuario.confirmarExclusaoRegistro('${row.id});"
                    class="btn btn-danger btn-xs BtnExcluir">
                        <i class="fa fa-trash"></i>
                    </button>`;

            return `<center>${btnExcluir}</center>`;
        });

        this.carregarGrid(
                '#datatable', location.pathname,
                this.colunas, this.colunasEspeciais);
    },

});

$(document).ready(() => {
    gridTiposUsuarios.iniciar();
});
