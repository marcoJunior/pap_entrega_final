/* @flow */

import Grid from './../grid';
import { deletarPorId, editarPorId, buscarPorId } from './requests';
import { modalConfirmacao } from './../utils';

const textoExclusaoUsuario = usuario => `Deseja excluir o usuário ${usuario} ?`;

export default class GridUsuarios extends Grid {
    colunas = ['id', 'usuario', 'nome_completo'];

    executarPorLinha(tr: HTMLTableRowElement, usuario: Object) {
        const [editar, excluir] = tr.getElementsByTagName('button');

        editar.addEventListener('click', async () => this.mostrarEditar(usuario.id));

        excluir.addEventListener('click', async () => {
            await modalConfirmacao({
                titulo: 'Exclusão',
                texto: textoExclusaoUsuario(usuario.nomeCompleto),
            });
            await deletarPorId(usuario.id);
            await this.pesquisar();
        });
    }

    async mostrarEditar(id: number) {
        const resp = await buscarPorId(id);
        this.formulario.mostrar();
        this.formulario.preencher(resp.data);
        this.formulario.form.onsubmit = (e: Event) => {
            e.preventDefault();
            this.editar(id, this.formulario.getDadosForm());
            return false;
        };
    }

    async editar(id: number, dados: Object) {
        await editarPorId(id, dados);
        await this.pesquisar();
        this.formulario.esconder();
    }
}
