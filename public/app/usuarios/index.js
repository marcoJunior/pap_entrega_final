/* @flow */

import GridUsuarios from './GridUsuarios';

const tabela = document.getElementById('grid-usuarios');

if (tabela) {
    const grid = new GridUsuarios('/usuarios', tabela);
    grid.montarTabelaCompleta(window.Vars);
}
