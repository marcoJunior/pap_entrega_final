/* flow */
import { xhr } from './../grid/utils';

export const buscarPorId = id => xhr.get(`/usuarios/${id}`);
export const editarPorId = (id, usuario) => xhr.put(`/usuarios/${id}`, usuario);
export const deletarPorId = id => xhr.delete(`/usuarios/${id}`);
