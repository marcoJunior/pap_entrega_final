/* @flow */

import jQuery from 'jquery';

type Param = {
    titulo?: string,
    texto?: string,
    textoBotao?: string;
}

export const CANCELADO_PELO_USUARIO = 0;
export const USUARIO_IGNOROU_MODAL = 1;

const modal = document.getElementById('modal-confirmacao');
if (!modal) throw new TypeError('#modal-confirmacao não definida');
const $modal = jQuery(modal);

/**
 * Diálogo com o usuário
 *  - modalConfirmacao.then() é execultado se o usuário clica em confirmar
 *  - modalConfirmacao.catch() é execultado quando o usuário ignora a modal ou fecha
 *  - modalConfirmacao.catch((err) => {
 *      if (err === USUARIO_IGNOROU_MODAL) // usuário ignorou a modal
 *      if (err === CANCELADO_PELO_USUARIO) // clicou em cancelar ou fechou a modal
 *  });
 *
 * @param  {Object} param
 * @return {Promise}       [description]
 */
export default function modalConfirmacao(param: Param) {
    return new Promise((resolve, reject) => {
        const botaoConfirmar = modal.getElementsByClassName('confirmar')[0];
        const timeout = setTimeout(() => {
            reject(USUARIO_IGNOROU_MODAL);
            $modal.modal('hide');
        }, 10000);

        botaoConfirmar.onclick = () => {
            clearTimeout(timeout);
            resolve();
            $modal.modal('hide');
        };


        $modal.on('hide.bs.modal', () => reject(CANCELADO_PELO_USUARIO));

        if (param.titulo) {
            modal.getElementsByClassName('modal-title')[0].innerHTML = param.titulo;
        }

        if (param.texto) {
            modal.getElementsByClassName('modal-body')[0].innerHTML = param.texto;
        }

        if (param.textoBotao) {
            botaoConfirmar.innerHTML = param.textoBotao;
        }

        jQuery(modal).modal('show');
    });
}
