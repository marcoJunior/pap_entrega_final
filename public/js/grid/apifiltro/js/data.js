function initDatePicker() {
    $('.parametroData').datepicker({
        rtl: Metronic.isRTL(),
        orientation: "right",
        autoclose: true,
        format: "dd/mm/yyyy",
        language: 'pt-BR'
    }).on('changeDate', function () {
        filtrar();
    });
}

$(function () {
    initDatePicker();
    $('#paramDate').remove();
});