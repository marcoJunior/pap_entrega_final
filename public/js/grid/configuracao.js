var SelectGrid;
var SelectProdNew = {};
var numModal = {
    mvd: 0,
    fab: 1,
};
$(document).ready(function () {
    $('.media-list').height($('.TabelaSetLoad').height());

    // SelectGrid = new ServiceGrid();
    setJs();
    if (typeof ComponentsPickers !== 'undefined') {
        ComponentsPickers.init(); // Funcionamento do TimePickers    
    }
    Layout.init(); // Funcionamento do menu
    Metronic.init(); // Funcionamento do menu

    $(".BtnVoltarGrid").click(function () {
        var form = new FormularioGrid();
        //SelectGrid.limpaPesquisar();
        form.mudarTab(false, false);
    });

});

/**
 * Gerencia montagem e movimentação dos grids do sistema
 * @author Leandro Machado && Edição por Leandro Machado
 */
var ServiceGrid = function () {

    this.url = typeof window.location.search.split('?')[1] !== 'undefined' ? window.location.search.split('?')[1].split('&') : '';
    this.urlGrid = '';

    var self = this;
    this.cRet = [];
    this.msgErro = "Operação não pode ser realizada !";

    $("body").data("offset", 0);
    $("body").data("limit", (typeof $('#selectOptionGrid').val() !== "undefined") ? $('#selectOptionGrid').val() : 10);

    this.tipoMascaraPesquisa = [];

    this.dataGridPesquisa;
    this.htmlSelection = {};

    this.urlsResult = {
        empresas: '/empresas/selecionar',
        clientes: '/clientes/selecionar',
        usuarios: '/usuarios/selecionar',
        logs: '/logs/selecionar',
        pagamentos: '/pagamentos/selecionar'
    };

    $("body").data("dataAjax", {
        gridAjax: {
            parametro: {
                select: {
                    order: {
                        coluna: '',
                        valor: ''
                    },
                    offset: parseInt($("body").data("offset")),
                    limit: parseInt($("body").data("limit"))
                },
                where: {
                    diverso: {
                        coluna: [],
                        valor: '',
                        tipo: ''
                    },
                    between: setarData(),
                    selecionaLinha: {
                        coluna: '',
                        valor: ''
                    },
                    parametrisar: {
                        tipoItens: [],
                        itens: {},
                    },
                }
            },
            tela: 'produto',
        }
    });

    /**
     * Retorna url de acesso especifico ao parametro GET
     * @author Leandro Machado
     * @return {string} this.urlGrid 
     */

    this.getUrlGrid = function (numeroModal) {
        if (typeof numeroModal === "undefined") {
            numeroModal = 0;
        }
        $("body").data("dataAjax").gridAjax.tela = this.getParametroUrl('tipo').split('|')[numeroModal];
        this.urlGrid = this.urlsResult[this.getParametroUrl('tipo').split('|')[numeroModal]];

        if (location.pathname === '/dashboard' || location.pathname === '/controlshop') {
            return location.pathname + '/selecionar';
        }

        return this.urlGrid;
    };

    /**
     * Retorna um parametro GET da url
     * @author Leandro Machado
     * @return {string}
     */
    this.getParametroUrl = function (name) {
        var param = "";
        $.each(this.url, function (index, value) {
            if (value.indexOf(name) > -1) {
                param = value.split('=')[1];
            }
        });
        return param;
    };

    /**
     * Inclui parametros na tela
     * @param {objeto} Objeto de configurações de quais parametros devem ser printados em tela
     * @author Leandro Machado
     */
    this.setParametros = function (parametros) {
        $('.media-list').html('');
        $.each(parametros.configParam, function (i, config) {
            var inputParam = $('.param' + config.nome).clone();
            inputParam.find('.input' + config.nome).removeClass('input' + config.nome).addClass('input' + config.nome + "-" + i);
            inputParam.find('.input' + config.nome + "-" + i).attr('name', config.id + "-" + i);
            inputParam.find('.input' + config.nome + "-" + i).attr('placeholder', config.text);
            if (config.nome !== 'Data' && config.tipo !== "string") {
                inputParam.find('.input' + config.nome + "-" + i).mask(config.mask);
            }
            inputParam.find('.input' + config.nome + "-" + i).attr('maxlength', config.mask.length);
            $('.media-list').append(inputParam.html());

            if (config.nome === 'Data') {
                ComponentsPickers.init();
            }
        });
        $('[name=btnConfirmParametrizar]').attr('onclick', 'SelectGrid.confirmarParametros();');

    };

    /**
     * Limpeza geral dos campos de pesquisa
     * @author Leandro Machado
     */
    this.limpaPesquisar = function () {
        $("#input_pesquisar").val('');

        $("body").data("dataAjax").gridAjax.parametro.where.diverso.coluna = [];
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.valor = '';
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.tipo = '';
        $("body").data("dataAjax").gridAjax.parametro.where.between = setarData(false);

        this.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            self.carregar(data.gridPhp);
            $("#selectPesquisar").val();
        });
    };

    /**
     * Inicia paginação através do click
     * @param {string} acao se é proximo, anterior
     * @author Leandro Machado
     */
    this.paginar = function (acao) {
        Download = [];
        $('#TabelaVariacao').appendTo($('#retTabela'));

        $("body").data("limit", (typeof $("body").data("limit") !== 'undefined') ? parseInt($("body").data("limit")) : 10)
        $("body").data("offset", (acao === "voltar") ? parseInt(parseInt($("body").data("offset")) - $("body").data("limit")) : parseInt(parseInt($("body").data("offset")) + $("body").data("limit")));

        this.verificarPagina();

        $("body").data("dataAjax").gridAjax.parametro.select.offset = parseInt($("body").data("offset"));
        $("body").data("dataAjax").gridAjax.parametro.select.limit = parseInt($("body").data("limit"));

        this.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            self.carregar(data.gridPhp);
        });

    };

    /**
     * 
     * Verifica qual será a ação tomada após click identificado
     * @param {string} acao se é proximo, anterior
     * @param {string} resultado array da query
     * @author Leandro Machado
     */
    this.verificarPagina = function () {
        $("body").data("pagina", parseInt(Math.round($("body").data("offset") / $("body").data("limit"))) + 1);
        $("[name=numeroPagina]").text($("body").data("pagina"));

        if (!$("#titulo-coluna-grid").data("data").gridPhp.data) {
            this.movimentosPaginacao('bloqueiadois');
            return false;
        }

        var linhasAtuais = $("#titulo-coluna-grid").data("data").gridPhp.data.length;
//        var linhasTotais = self.dataGridPesquisa["contData"]["count"];
        //      var paginaFinal = parseInt(linhasTotais) / $("body").data("limit");

        if (linhasAtuais < parseInt($("body").data("limit")) && $("body").data("pagina") <= 1) {
            this.movimentosPaginacao('bloqueiadois');
            return false;
        } else {
            this.movimentosPaginacao('voltar');
        }

        if ($("body").data("pagina") <= 1) {
            this.movimentosPaginacao('voltar');
            return false;
        }

        if ($("#titulo-coluna-grid").data("data").gridPhp.data.length < $("body").data().limit) {
            //if ($("body").data("pagina") >= paginaFinal) {
            this.movimentosPaginacao('proxima');
            return false;
        }

        this.movimentosPaginacao('desbloqueiaosdois');
    };

    /**
     * Aplica movimentos dos botões de paginação
     * @param {string} desbloquear proxima || voltar
     * @author Leandro Machado
     */
    this.movimentosPaginacao = function (desbloquear) {
        var auxiliar = (desbloquear === "proxima") ? "voltar" : "proxima";
        var acoes = ["proxima", "voltar"];

        if (desbloquear === 'bloqueiadois') {
            $.each(acoes, function (index, val) {
                $("[name=" + val + "Pagina]").attr('class', 'disabled');
                $("[name=" + val + "Pagina]").removeAttr('onclick');
            });

            $("body").data("offset", 0);
            return false;
        }
        if (desbloquear === 'desbloqueiaosdois') {
            $.each(acoes, function (index, val) {
                $("[name=" + val + "Pagina]").attr('class', '');
                $("[name=" + val + "Pagina]").attr('onclick', 'SelectGrid.paginar("' + val + '")');
            });

            return false;
        }

        $("[name=" + desbloquear + "Pagina]").attr('class', 'disabled');
        $("[name=" + desbloquear + "Pagina]").removeAttr('onclick');
        $("[name=" + auxiliar + "Pagina]").attr('class', '');
        $("[name=" + auxiliar + "Pagina]").attr('onclick', 'SelectGrid.paginar("' + auxiliar + '")');
    };

    /**
     * Carrega o grid em questão com a quantidade de linhas selecionadas
     * @author Leandro Machado
     */
    this.selecionaQuantidade = function () {

        $("body").data("limit", $('#selectOptionGrid').val())
        $("body").data("offset", $("body").data("offset"));

        $("body").data("dataAjax").gridAjax.parametro.select.limit = parseInt($("body").data("limit"));
        $("body").data("dataAjax").gridAjax.parametro.select.offset = parseInt($("body").data("offset"));

        this.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            self.carregar(data.gridPhp);
            self.verificarPagina('inicio');
        });
    };

    /**
     * Efetua pesquisa geral na tabela pertencente ou grid
     * @author Leandro Machado
     * @return {grid}
     */
    this.pesquisar = function () {

        $("body").data("dataAjax").gridAjax.parametro.where.diverso.coluna = $("#selectPesquisar").val();
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.valor = $("#input_pesquisar").val().toUpperCase();
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.tipo = $("#selectOption").val();

        var chave = $("#selectPesquisar").find(":selected").attr('tipo');

        if (chave === 'int-data') {
            var array = $("#input_pesquisar").val().split("/");
            var pesquisarData = array[2] + '-' + array[1] + '-' + array[0];
            $("body").data("dataAjax").gridAjax.parametro.where.diverso.valor = pesquisarData;
        }

        $("body").data("dataAjax").gridAjax.parametro.select.offset = 0;
        $("body").data("offset", 0);
        $("body").data("pagina", $("body").data("offset"));
        this.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            self.carregar(data.gridPhp);
            $("#selectPesquisar").val($("body").data("dataAjax").gridAjax.parametro.where.diverso.coluna);
            $("#selectOption").val($("body").data("dataAjax").gridAjax.parametro.where.diverso.tipo);

            if (data.gridPhp.data === null) {
                toastr.warning('Não foram encontrados valor para sua pesquisa!', '');
            }
        });

    };

    /**
     * Retorna o grid ordenado pela coluna selecionada
     * @author Leandro Machado
     * @return {grid}
     */
    this.ordenar = function (campo) {
        if (campo === 'btn-detalhes-primeiro') {
            return false;
        }
        $('#selectPesquisar').val(campo);

        $('#titulo-coluna-grid th').each(function (coluna, value) {
            $('#' + value.id).attr('class', 'sorting');
        });

        var order = 'asc';
        var tipo = $('#titulo-clone-' + campo).attr('value');

        if (tipo === 'asc') {
            order = 'desc';
        } else {
            order = 'asc';
        }

        $('#titulo-clone-' + campo).attr('class', 'sorting_' + order);
        $('#titulo-clone-' + campo).attr('value', order);

        $("body").data("dataAjax").gridAjax.parametro.select.order.coluna = campo;
        $("body").data("dataAjax").gridAjax.parametro.select.order.valor = order;

        this.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            data.gridPhp.order = true;
            self.carregar(data.gridPhp);
        });
    };

    /**
     * Retorna formulario modificado para identificação da ação a ser executada
     * @author Leandro Machado
     * @return {html}
     */
    this.setFormulario = function () {
        var btnAdicao = '<button name="btn-adicionar" onclick="novo();" class="btn blue pull-right">' +
                '<i class="fa fa-plus"></i> ' +
                'Adicionar' +
                '</button>';

        $('[name=btn-adicionar]').remove();
        $(".portlet-title").eq(0).append(btnAdicao);

        $('#panelParamForm').show();
    };

    /**
     * Retorna o grid especificado por parametro GET
     * @author Leandro Machado
     * @return {grid}
     */
    this.carregar = function (grid) {

        if (this.url.length > 1) {
            $.each(this.url, function (indexParam, parametro) {
                var auxParametro = parametro.split('=');
                if (auxParametro[0] !== "tipo") {
                    $("body").data("dataAjax").gridAjax.parametro.where.selecionaLinha.coluna = auxParametro[0];
                    $("body").data("dataAjax").gridAjax.parametro.where.selecionaLinha.valor = auxParametro[1];
                }
            });
        }
        if (typeof grid === "undefined") {

            /**
             * Ajax de carregamento!
             */
            self.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
                self.setRetornoDoBanco(data.gridPhp);
            });

        } else {
            this.dataGridPesquisa = grid;
            this.setGrid(grid);
        }


    };

    /**
     * O retorno do banco começa neste metodo.
     * 
     * @param {type} grid
     * @returns {ediçãodiretadehtml}
     */
    this.setRetornoDoBanco = function (grid) {
        this.setGrid(grid);
        //$("title").text(grid.titulo);
        this.setLinhaTitulos(self.dataGridPesquisa);
        this.setParametros(self.dataGridPesquisa);
    };

    this.setGrid = function (grid) {
        this.dataGridPesquisa = grid;

        this.setTituloPrincipal(grid);
        this.setPesquisaAvancada(grid);
        this.setLinhasCompleta(grid);

        var selecionaLinha = $("body").data("dataAjax").gridAjax.parametro.where.selecionaLinha;
        if (selecionaLinha.coluna !== "" && selecionaLinha.valor !== "") {
            verDetalhesItem(selecionaLinha.valor);
        }
        if (self.url.length > 1) {
            selecionarTabForm(false);
        }

        //$('.totalEncontrado').html('Total encontrado: ' + parseInt(grid.contData['count']));

        this.carregaSelectTipoPesquisa();

        if (SelectGrid.offset === 0) {
            this.verificarPagina('');
        }
    };

    /**
     * Define titulo principal dinamicamente
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.setTituloPrincipal = function (grid) {
        if (typeof grid !== "undefined" && typeof grid.titulo !== "undefined") {
            $("#title-grid").text((grid.titulo).toUpperCase());
        }
        if (typeof $('[name=btn-adicionar]').val() === "undefined" && this.getParametroUrl('tipo').split('|')[numModal[SelectProdNew.table]] === 'produto' || this.getParametroUrl('tipo').split('|')[numModal[SelectProdNew.table]] === 'fabricante') {
            this.setFormulario();
        }
    };

    /**
     * Define o tipo de filtro de pesquisa com relação ao tipo de coluna a ser pesquisada
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.setPesquisaAvancada = function (grid) {
        if (!$('#selectPesquisar option').length) {
            var selectOptionTipo = '';
            if (typeof grid.colunas !== "undefined" && !grid.order) {
                $('#selectPesquisar').html('');
                $('#titulo-coluna-grid').html('');

                $.each(grid.colunas, function (coluna, value) {
                    if (value.exibir === 'true') {

                        if (coluna !== 'btn-detalhes') {
                            selectOptionTipo += '<option tipo="' + value.tipo + '" value="' + coluna + '">' + value.titulo + '</option>';
                        }
                    }
                });
                if (typeof $('#selectPesquisar').html() === "undefined" || $('#selectPesquisar').html() === "") {
                    $('#selectPesquisar').html(selectOptionTipo);
                }
            }
        }

    };

    /**
     * Retorna linha de titulos do grid 
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.setLinhaTitulos = function (grid) {
        var html = '';
        if (typeof grid.colunas !== "undefined" && !grid.order) {
            $('#titulo-coluna-grid').html('');
            this.tipoMascaraPesquisa = grid.colunas;
            $.each(grid.colunas, function (coluna, value) {

                if (value.exibir === 'true') {
                    html = $('#tabela-clone').clone().find('#col-titulo-clone');
                    html.find('#titulo-clone').text(value.titulo);

                    if (coluna === 'opcao-menu' || coluna === 'btn-detalhes') {
                        html.find('#titulo-clone').removeClass('sorting');
                        html.find('#titulo-clone').attr("width", "1%");

                    } else {
                        html.find('#titulo-clone').attr('id', 'titulo-clone-' + coluna);
                        html.find('#titulo-clone-' + coluna).attr('onclick', "SelectGrid.ordenar('" + coluna + "');");
                        if (coluna === 'btn-detalhes-primeiro') {
                            html.find('#titulo-clone' + coluna).attr("width", "1%");
                        }
                    }
                    if (coluna === 'btn-detalhes-primeiro') {
                        html.find('#titulo-clone').html('<input type="checkbox" >');
                    }
                    $('#titulo-coluna-grid').append(html.html());
                }
            });
            $("#titulo-clone-btn-detalhes-primeiro").removeClass("sorting");
        }
        //$('input[type=checkbox]').iCheck({checkboxClass: 'icheckbox_minimal-blue', radioClass: 'iradio_minimal', increaseArea: '20%'});
        // $.fn.bootstrapSwitch.defaults.size = 'small';
        // $("input[type=checkbox]").bootstrapSwitch('state', true, true);
        //  $("input[type=checkbox]").bootstrapSwitch('size', 'mini');
    };

    /**
     * Retorna o conjunto de linhas para montagem do grid
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.setLinhasCompleta = function (grid) {
        var retHtmlRow = '';
        $('#TabelaGrid > tbody').html('');
        ResultAjaxColunas = grid.colunas;
        if (grid.data && typeof grid.data !== "undefined") {
            ResultAjax = grid.data;
            $('.LinhasGrid').remove();
            $.each(grid.data, function (indexRow, row) {
                var auxRow = '<tr class="LinhasGrid" id="row-' + indexRow + '">';
                var nColuns = 0;
                $.each(grid.colunas, function (indexColuna, colunaValor) {
                    if (colunaValor.exibir === 'true') {
                        nColuns++;
                        if (indexColuna.search('btn-') > -1) {
                            auxRow += self.getTdColunaOpcao(colunaValor, row);
                            return true;
                        }

                        var mask = '';
                        if (colunaValor.mask) {
                            mask = colunaValor.mask;
                        }

                        retHtmlRow = $('#tabela-clone').clone().find('#rows-clone');
                        var clonetd = retHtmlRow.find('#cell-clone');
                        clonetd.attr('id', 'cell-clone-' + indexColuna);
                        $('#inputMaskGrid').val(row[indexColuna]);

                        if (colunaValor.tipo !== 'string') {
                            $('#inputMaskGrid').mask(mask);
                        }
                        $('#inputMaskGrid').attr('maxlength', mask.length);
                        clonetd.text($('#inputMaskGrid').val());
                        auxRow += retHtmlRow.html();
                    }
                });
                auxRow += '</tr>';
                auxRow += '<tr style="display:none;"></tr>';
                auxRow += '<tr style="display:none; background-color:#fff;" class="TabelaVariacao-' + row.codigo + ' tabelaEsconde" ><td style="display:none;" colspan="' + nColuns + '" class="TabelaVariacao-' + row.codigo + ' tabelaEsconde"></td></tr>';
                $('#TabelaGrid').append(auxRow);
                //$('input').iCheck({checkboxClass: 'icheckbox_minimal-blue', radioClass: 'iradio_minimal', increaseArea: '20%'});
                // $("input[type=checkbox]").bootstrapSwitch('state', true, true);
            });

        }

        if (self.getParametroUrl('tipo') !== "fabricante" && typeof SelectGrid.htmlSelection['fabricante'] === 'undefined') {
//            self.ajaxGerenciar($("body").data("dataAjax"), '', false, this.urlsResult['fabricante'], numModal[SelectProdNew.table], function (data) {
//                SelectGrid.htmlSelection['fabricante'] = '<option value="0">Fabricantes</option>';
//                $.each(data.gridPhp.data, function (index, valor) {
//                    SelectGrid.htmlSelection['fabricante'] += '<option value="' + valor.codigo + '">' + valor.nome + '</option>';
//                });
//            });
        }
    };

    this.getTdColunaOpcao = function (menu, dataRow) {
        var html = '<td><center>';
        $.each(menu, function (row, opcaoMenu) {
            if (opcaoMenu && typeof opcaoMenu.icon !== 'undefined') {
                html += '<' + opcaoMenu.tipo + ' value="' + dataRow[opcaoMenu.value] + '" id="linha-' + dataRow[opcaoMenu.value] + '" title="'
                        + opcaoMenu.title + '" onclick="'
                        + opcaoMenu.onclick + '(\''
                        + dataRow[opcaoMenu.value] + '\')" class="'
                        + opcaoMenu.class + '"><i style="'
                        + opcaoMenu.style + '" class= "fa '
                        + opcaoMenu.icon + ' "></i></'
                        + opcaoMenu.tipo + '>';
                if (menu.titulo === 'Opções') {
                    html += '&nbsp;&nbsp;';
                }

            }

        });
        html += "</center></td>";
        return html;
    };

    /**
     * Execução de todos os ajax do modulo de grid
     * @author Leandro Machado
     * @return {objeto}
     */
    this.ajaxGerenciar = function (data, msg, async, url, numeroModal, success) {
        var cRet = [];
        data.gridAjax.parametro.where.between = setarData(false);

        $.ajax({
            type: "POST",
            url: (url ? url : this.getUrlGrid(numeroModal)),
            data: data,
            async: async,
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                if ($.isFunction(success)) {
                    success(data);
                }
                cRet = data;
                $("#titulo-coluna-grid").data("data", data);
                if (msg) {
                    if (typeof data[0] !== "undefined") {
                        self.modalMsg("Alerta !", msg);
                    } else {
                        self.modalMsg("Alerta !", this.msgErro);
                    }
                }
            },
            error: function () {
                self.modalMsg("Alerta !", this.msgErro);
            },
            complete: function () {
                setTimeout(function () {
                    carregando(false);
                }, 0);
                self.verificarPagina();

                //ServicosGrid.eventos();
            }
        });
        return cRet;
    };

    /**
     * Retorna uma modal de mensagem
     * @param {strig} titulo Titulo em destaque da modal
     * @param {strig} msg Mensagem a ser definida como conteudo principal
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.modalMsg = function (titulo, msg) {
        var modal = new Modal();
        modal.show('#selectIcons', msg);
        $("#modalTitel").text(titulo);
    };

    /**
     * Retorna html referente aos campos de pesquisa para resolução mobile
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.getHtmlMobile = function () {

//        var BoxPesquisaResponsiva = $('#BoxPesquisaResponsiva');
//
//        if (BoxPesquisaResponsiva.find('#passo2GridOrder').html().trim() === "") {
//
//            BoxPesquisaResponsiva.show();
//
//            BoxPesquisaResponsiva.find('#passo2GridOrder').html(this.cloneInputsPesquisa.find('#auxSelectPesquisar').html());
//            BoxPesquisaResponsiva.find('#insertSelectOption').html(this.cloneInputsPesquisa.find('#auxInsertSelectOption').html());
//            BoxPesquisaResponsiva.find('#passo7GridBtnSearch').html(this.cloneInputsPesquisa.find('#auxInput_pesquisar').html());
//            BoxPesquisaResponsiva.find('#passo5GridBtnSearch').html(this.cloneInputsPesquisa.find('#auxBtn-search').html());
//            BoxPesquisaResponsiva.find('#passo6GridLimpar').html(this.cloneInputsPesquisa.find('#auxBtn-refresh').html());
//
//            this.limpaHtmlDesktop();
//        }
    };

    /**
     * Limpa capos de pesquisa 
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.limpaHtmlDesktop = function () {
        $('#BoxPesquisa .form-inline').html('');
        this.setFormulario();
    };

    /**
     * Retorna html referente aos campos de pesquisa
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.getHtmlDesktop = function () {
        var BoxPesquisa = $('#BoxPesquisa');
        if (typeof BoxPesquisa.find('.form-inline').html() !== 'undefined' && BoxPesquisa.find('.form-inline').html().trim() === "") {
            BoxPesquisa.show();
            BoxPesquisa.find('.form-inline').html(this.cloneInputsPesquisa.html());
            this.limpaHtmlMobile();
        }
    };

    /**
     * Limpa capos de pesquisa mobile
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.limpaHtmlMobile = function () {
        $('#BoxPesquisaResponsiva').hide();

        var BoxPesquisaResponsiva = $('#BoxPesquisaResponsiva .form-inline');

        BoxPesquisaResponsiva.find('#passo2GridOrder').html('');
        BoxPesquisaResponsiva.find('#insertSelectOption').html('');
        BoxPesquisaResponsiva.find('#passo5GridBtnSearch').html('');
        BoxPesquisaResponsiva.find('#passo7GridBtnSearch').html('');
        BoxPesquisaResponsiva.find('#passo6GridLimpar').html('');

    };

    /**
     * Define dinamicamente conforme resolução do aparelho qwual tipo de pesquisa deve ser definida
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.setHtmlPesquisa = function () {
        if (this.isMobile()) {
            this.getHtmlMobile();
        } else {
            this.getHtmlDesktop();
        }
    };

    /**
     * Define dinamicamente conforme resolução do aparelho qwual tipo de pesquisa deve ser definida
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.moverHtmlPesquisa = function () {
        if (this.isMobile()) {
            this.moverHtmlMobile();
        } else {
            this.moverHtmlDesktop();
        }
    };

    /**
     * Retorna html referente aos campos de pesquisa
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.moverHtmlDesktop = function () {

        var BoxPesquisa = $('#BoxPesquisa');
        var BoxPesquisaResponsiva = $('#BoxPesquisaResponsiva');

        if (BoxPesquisa.find('#insertSelectOption').html() === "" || typeof $('#BoxPesquisa').find('#insertSelectOption').html() !== 'undefined' && $('#BoxPesquisa').find('#insertSelectOption').html().trim() === "") {

            BoxPesquisa.show();
            BoxPesquisaResponsiva.find('#selectPesquisar').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.find('#selectOption').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.find('#input_pesquisar').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.find('#btn-search').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.find('#btn-refresh').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.find('[name=btn-adicionar]').appendTo(BoxPesquisa.find('.form-inline'));
            BoxPesquisaResponsiva.hide();

        }

    };

    /**
     * Retorna html referente aos campos de pesquisa
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.moverHtmlMobile = function () {

        var BoxPesquisa = $('#BoxPesquisa');
        var BoxPesquisaResponsiva = $('#BoxPesquisaResponsiva');

        if (BoxPesquisaResponsiva.find('#passo2GridOrder').html() === "" || BoxPesquisaResponsiva.find('#passo2GridOrder').html().trim() === "") {

            BoxPesquisaResponsiva.show();
            BoxPesquisa.find('#selectPesquisar').appendTo(BoxPesquisaResponsiva.find('#passo2GridOrder'));
            BoxPesquisa.find('#selectOption').appendTo(BoxPesquisaResponsiva.find('#insertSelectOption'));
            BoxPesquisa.find('#input_pesquisar').appendTo(BoxPesquisaResponsiva.find('#passo7GridBtnSearch'));
            BoxPesquisa.find('#btn-search').appendTo(BoxPesquisaResponsiva.find('#passo5GridBtnSearch'));
            BoxPesquisa.find('#btn-refresh').appendTo(BoxPesquisaResponsiva.find('#passo6GridLimpar'));
            BoxPesquisa.find('[name=btn-adicionar]').appendTo(BoxPesquisaResponsiva);
            BoxPesquisa.hide();
        }

    };

    this.cloneInputsPesquisa = $('#clonePesquisa').clone();

    /**
     * Verifica resolução compativel com aparelhos mobile
     * @author Leandro Machado
     * @return {edição direta de html}
     */
    this.isMobile = function () {
        var resolucao = window.innerWidth;
        return resolucao < 1220;
    };

    this.carregaSelectTipoPesquisa = function () {
        var chave = $("#selectPesquisar").find(":selected").attr('tipo');
        var chaveSelecionada = $("#selectOption").find(":selected").attr('tipo');

        if (chave !== chaveSelecionada) {
            $('#selectOption option').each(function (index, value) {
                if (chave.indexOf('data') > 0) {
                    chave = chave.split("-")[0];
                }
                $(value).attr('tipo') === chave ? $(value).show() : $(value).hide();
            });

            $("#selectOption option:not([style$='display: none;'])").eq(0).prop('selected', true);
        }


        var colun = $("#selectPesquisar").val();
        var auxMask = '';
        var tipo = '';
        if (typeof ResultAjaxColunas !== "undefined" && typeof ResultAjaxColunas[colun] !== "undefined") {
            auxMask = ResultAjaxColunas[colun].mask;//.match(/#/g).toString().replace(/,/g, '');
            tipo = ResultAjaxColunas[colun].tipo;
        }
        if (tipo !== 'string') {
            $('#input_pesquisar').mask(auxMask);
        } else {
            $('#input_pesquisar').unmask();
        }
        $('#input_pesquisar').attr('maxlength', auxMask.length);
    };

    if (location.pathname === "/grid" || location.pathname === "/dashboard" && typeof $("#titulo-coluna-grid").data("data") === 'undefined') {
        //$("body").data("dataAjax").gridAjax.parametro.where.between = this.getOrdenacaoInicial();
        //this.carregar();

        this.setHtmlPesquisa();
        $(window).resize(function () {
            setTimeout(function () {
                self.moverHtmlPesquisa();
            }, 150);
        });
    }
};

var FormularioGrid = function () {
    this.editar = function () {
        $('#tab-detalhes form').find('input, select, a').not('#LinhaCompleta-0 > td > a').removeClass('disabled').prop('disabled', false);
        //$('#tab-detalhes form').find('#codigo').addClass('disabled').prop('disabled', true);
        $('#btnSalvarProduto').show();
        $('#btnSalvarFabricante').show();
    };

    this.desabilitar = function (form) {
        $("form").find('input, select, a').addClass('disabled').prop('disabled', false);
        //$('#tab-detalhes form').find('#codigo').addClass('disabled').prop('disabled', true);
    };

    this.mudarTab = function (isDefault, acao) {
        //$('#tab-detalhes form').find('#codigo').addClass('disabled').prop('disabled', true);
        var tipoGrid = SelectGrid.getParametroUrl('tipo').split('|')[0];
        $("form").find('input, select, a').addClass('disabled').prop('disabled', true);

        if (isDefault) {
            $(".portlet-body .form > form").trigger("reset");
            $('#portlet-body').hide('slow');
            $('#tab-detalhes').show('slow');
            $('#tabDetalhamentoForm').show('slow');
            $('.BtnVoltarGrid').show('slow');
            $('[name=btn-parametrizar]').hide('slow');

            if (location.pathname === '/grid') {
                $('#formCadastro' + capitalize(location.search.split("=")[1])).show();
                $("#title-grid").text(tipoGrid.toUpperCase() + ' - ' + acao.toUpperCase());
            }
        } else {
            $("#title-grid").text(tipoGrid.toUpperCase());
            $('[name=btn-adicionar]').html('Adicionar <i class="fa fa-plus"></i>').show('slow');
            $('#btnSalvarProduto').show('slow');
            $('#btnSalvarFabricante').show('slow');
            $('#portlet-body').show('slow');
            $('[name=btn-parametrizar]').show('slow');
            $('#tab-detalhes').hide('slow');
            $('.BtnVoltarGrid').hide('slow');
        }
    };
};

function setJs() {
    SelectGrid = new ServiceGrid();
    var tipo = SelectGrid.getParametroUrl('tipo').split('|');

    $.each(tipo, function (index, value) {
        if (value) {
            $('[name=tipGrid]').html('<script src="js/modulos/grids/' + value + '.js"></script>');
            showTab('tab-detalhes-' + value);
            return false;
        } else {
            $('[name=tipGrid]').html('<script src="js/modulos/' + location.pathname.split("/")[1] + '.js"></script>');
            return false;
        }
    });
}

function showTab(id) {
    var xTab = $('#' + id).clone();
    $('#tab-detalhes').html('');
    $('#tab-detalhes').append(xTab);
    xTab.show();
}

function trim(str) {
    return str.replace(/^\s+|\s+$/g, "");
}

function formatarTelefone(s) {

    s = parseInt(s);
    s = String(s);
    if (s.length > 8) {
        var s2 = ("" + s).replace(/\D/g, '');
        var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
        m[1] = s2.match(/^(\d{3})(\d{3})(\d{4})$/)[1][0] + s2.match(/^(\d{3})(\d{3})(\d{4})$/)[1][1];
        return (!m) ? null : "(" + m[1] + ") " + s2.match(/^(\d{3})(\d{3})(\d{4})$/)[1][2] + m[2] + " - " + m[3];
    } else {
        s = 'Não Cadastrado';
        return s;
    }
}

//var Teste = (function () {
//
//    function printar() {
//        console.log("Testes");
//    }
//
//    return {
//        print: printar
//    };
//
//})();

function capitalize(string)
{
    try {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    } catch (e) {
        console.log(e);
    }
}

function setarData(pesquisar) {
    if (typeof pesquisar !== 'undefined' && pesquisar === true) {
        var service = new ServiceGrid;
        $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
        service.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            //service.carregar(data.gridPhp);
            service.setRetornoDoBanco(data.gridPhp);
            $("#selectPesquisar").val();
        });

        return false;
    }
    return $("body").data("ordenacaoinicial");
}