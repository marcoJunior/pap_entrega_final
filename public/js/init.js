var Modal = function ()
{

    this.id;
    this.cRet = "";
    this.show = function (status, msg, xhtml, titulo) {

        $("#modalTitel").text(titulo);

        if (status === 'sucesso') {
            this.id = '#successModal';
        }
        else if (status === 'erro') {
            this.id = '#catchError';
        }
        else {
            this.id = status;
        }

        if (typeof msg === 'undefined') {
            this.cRet = "Mostrando mensagem padrão, escreva a mensagem no segundo parametro";
        }
        else if (typeof msg !== 'string') {
            this.cRet = "a msg deve ser string";
        }

        if (typeof xhtml !== "undefined") {
            $("#xHtmlModal").html(xhtml);
            $(this.id).modal('toggle');
        }
        else {
            if (this.cRet !== "") {
                return this.cRet;
            }

            $(this.id).modal().find('.modalMsg').text(msg);
        }

    };
    /**
     * 
     * @param {function} objModal
     * @objModal{function} sim 
     * @objModal{function} nao
     * @objModal{String} msg
     * @returns {undefined}
     */

    this.modalConfirmacao = function (objModal) {
        $('#modalConfirmacao').modal();
        if (!$.isFunction(objModal.sim)) {
            console.log('O primeiro parametro deve ser uma função');
            return false;
        }

        $('#modalConfirmacao .sim').off().on('click', function () {
            objModal.sim();
        });
        if ($.isFunction(objModal.nao)) {
            $('#modalConfirmacao .nao').off().on('click', function () {
                objModal.nao();
            });
        }

        if (typeof objModal.msg === 'string') {
            $('#modalConfirmacao .msg').text(objModal.msg);
        }
    };
    this.showId = function (id) {
        $('#' + id).modal();
    };
    this.getError = function () {
        return this.cRet;
    };
};

function carregando(mostrar) {
    if (mostrar) {
        $("#loading-mask").show();
        return false;
    }

    $("#loading-mask").hide();
}

function clearForm() {
    jQuery('#formEditePerfil').each(function () {
        this.reset();
    });
}