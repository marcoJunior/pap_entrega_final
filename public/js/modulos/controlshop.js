$(document).ready(function () {
    var objeto = {
        coluna: 'sr_recno',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    $("body").data("dataAjax").gridAjax.parametro.where.filtros = getFiltros();
    SelectGrid.carregar();
});

$(document).ajaxStop(function () {
    $.each($("#titulo-coluna-grid").data("data").gridPhp.contData.clientes, function (chave, valor) {
        $("#" + chave).text(valor);
    });
});

var Servicos = function ()
{

    this.buscar = function (buscar) {
        $("body").data("pagina", 0);
        $("body").data("pagina", parseInt($("#selectOptionGrid").val()));

        SelectGrid.carregaSelectTipoPesquisa('serie');
        var service = new ServiceGrid();
        this.setarBusca(buscar);
        $("body").data("dataAjax").gridAjax.parametro.select.offset = 0;

        service.ajaxGerenciar($("body").data("dataAjax"), '', true, false, numModal[SelectProdNew.table], function (data) {
            service.carregar(data.gridPhp, false);
            $("#selectPesquisar").val('serie');
            if (data.gridPhp.data === null) {
                toastr.warning('Não foram encontrados valor para sua pesquisa!', '');
            }
        });
    };

    this.setarBusca = function (buscar) {
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.coluna = 'ativacao';
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.valor = buscar.toUpperCase();
        $("body").data("dataAjax").gridAjax.parametro.where.diverso.tipo = $("#selectOption").val();
    };
};

var ServicosService = new Servicos();