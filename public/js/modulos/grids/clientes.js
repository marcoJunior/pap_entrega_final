$(document).ready(function () {
    $(".BuscarCliente").attr('disabled', 'false');

    Servicos.init();

    var objeto = {
        coluna: 'cficodusu',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});

function novo() {
    Servicos.novo();
}

$(document).ajaxStop(function () {
    Servicos.ativados();

    $('input[type=checkbox]').on('ifClicked', function (event) {
        var ativacao = '';
               
        if ($("#" + this.id + ":checked").length === 1) {
            ativacao = 0;
        } else {
            ativacao = 1;
        }

        Servicos.ativarOuDesativar(this.value, ativacao);
    });

});

var Servicos = (function () {
    var eventos = function () {
        // valida se os campos não 
        $("form.buscarcliente input").change(function () {
            Servicos.validar(".buscarcliente", 'buscar');
        });
    };

    var ativados = function () {
        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valor) {
            if (valor && valor.ativo === '1') {
                $('#linha-' + valor.codigo).iCheck('check');
            }
            $('input').iCheck({checkboxClass: 'icheckbox_minimal-blue linha-check-' + valor.codigo, radioClass: 'iradio_minimal', increaseArea: '20%'});
        });
    };

    var ativarOuDesativar = function (codigo, ativacao) {
        $.ajax({
            type: "POST",
            url: '/cliente/servicos',
            data: {
                dados: {codigo: codigo, ativacao: ativacao},
                acao: 'atualizarUsuario'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {

                $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (index, value) {
                    if (value && parseInt(value.codigo) === parseInt(codigo)) {
                        $("#titulo-coluna-grid").data("data").gridPhp.data[index].ativo = ativacao;
                    }
                });
                carregando(false);
                toastr.success('Atualizado com sucesso.', 'Informação!');
            }
        });
    };

    var editar = function (codigo) {
        $('[name=email').attr('readonly', true);
        $(".BtnVoltarGrid").remove();
        $("form.cliente .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>').show("slow");
        $(".horizontal-form.buscarcliente").hide();

        setTimeout(function () {
            $("form.cliente").show();
            $("#cliente-encontrado").show('slow');
        }, 200);

        $('#cliente').removeClass('requer-erro');
        $("#cliente").off();
        $(".SalvarCliente").off();
        $(".SalvarCliente").click(function () {
            set('editar');
        });
        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
        form.mudarTab(true, 'Editar');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));
        carregar(codigo);

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });
    };

    var novo = function () {
        $('[name=email').attr('readonly', true);
        $(".BtnVoltarGrid").remove();
        $("form.buscarcliente .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $("form.cliente").hide();
        $("#cliente-encontrado").hide();
        $(".horizontal-form.buscarcliente").show("slow");
        $("form.cliente").hide("slow");
        $("[name=btn-adicionar]").hide("slow");
        $('#cliente').removeClass('requer-erro');
        $(".SalvarCliente").off();

        $(".SalvarCliente").click(function () {
            set('adicionar');
        });

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
        form.mudarTab(true, 'Incluir registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));
    };

    /** 
     * 
     * @param {string} acao Nome da função no php
     */
    var set = function (acao) {
        if (validar("form.cliente", 'salvar')) {
            $.ajax({
                type: "POST",
                url: '/cliente/servicos',
                data: {
                    dados: $("form.cliente").serializeArray(),
                    acao: acao
                },
                beforeSend: function () {
                    carregando(true);
                },
                success: function (data) {
                    if (data) {
                        SelectGrid.limpaPesquisar();
                        var form = new FormularioGrid();
                        form.mudarTab(false, false);
                        $("[name=btn-adicionar]").show("slow");
                        toastr.success('Salvo com sucesso.', 'Informação!');
                    } else {
                        toastr.error('Registro já existe.', 'Informação!');
                    }
                },
                complete: function () {
                    carregando(false);
                }
            });
        } else {
            toastr.warning('Preencher todos os campos corretamente.', 'Informação!');
        }
    };

    /** 
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    var carregar = function (codigo) {
        $.each(ResultAjax, function (indexRow, row) {
            if (row.codigo === codigo) {
                $.each(row, function (index, value) {
                    $('[name=' + index + ']').val(row[index]);

                    if (index !== 'sistema' && index !== 'ativo') {
                        $('[name=' + index + ']').text(row[index]);
                    }

                });
                return false;
            }
        });
    };

    /** 
     * Verifica se existem valores para liberação de botão salvar
     */
    var validar = function (elemento, tipo) {
        var continuar = true;

        if (tipo === 'salvar') {
            $.each($(elemento).serializeArray(), function (chave, valor) {
                if ((valor.name === 'email' || valor.name === 'sistema' || valor.name === 'ativo') && valor.value === '') {
                    continuar = false;
                }
            });
            return continuar;
        }

        $.each($(elemento).serializeArray(), function (chave, valor) {
            if (valor.value === '') {
                continuar = false;
            }
        });
        if (continuar) {
            $(".buscarcliente").removeAttr('disabled');
            return false;
        }
        $(".buscarcliente").attr('disabled', 'true');

    };

    var getUsuario = function () {
        var url = 'app.csgestor.com.br';

        if (location.origin.search("dev") > 0) {
            url = location.origin.split("http://")[1].split(".")[0] + '.csgestor.com.br';
        }

        $.ajax({
            type: "POST",
            url: 'http://' + url + '/login/carregamento-admin',
            data: {emailProcurar: $("#emailprocurar").val(), senhaProcurar: $("#senhaprocurar").val(), empresaProcurar: $("#empresaprocurar").val()},
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                if (data[0] == false) {
                    toastr.error("Cliente não encontrado");
                    return false;
                }

                $.each(data, function (chave, valor) {
                    $('[name=' + chave + ']').val(valor);
                });

                if ($('[name=email').val() === '') {
                    $('[name=email').attr('readonly', false);
                }

                $("[name=empresa]").val($("#empresaprocurar").val());
            },
            complete: function () {
                carregando(false);
                $("form.cliente").show();
                $("#cliente-encontrado").show('slow');
            }
        });
    };

    /** 
     * @param {string} codigo Código do usuário
     */
    var remover = function (codigo) {
        $.ajax({
            type: "POST",
            url: '/cliente/servicos',
            data: {
                codigo: codigo,
                acao: 'remover'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                SelectGrid.limpaPesquisar();
                var form = new FormularioGrid();
                form.mudarTab(false, false);
                $("[name=btn-adicionar]").show("slow");
                toastr.success('Removido com sucesso.', 'Informação!');
            },
            complete: function () {
                carregando(false);
            }
        });
    };

    return {
        init: function () {
            eventos();
        },
        setar: set,
        carregar: carregar,
        validar: validar,
        editar: editar,
        novo: novo,
        remover: remover,
        get: getUsuario,
        ativados: ativados,
        ativarOuDesativar: ativarOuDesativar,
    };
})();
