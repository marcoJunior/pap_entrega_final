$(document).ready(function () {

    $(".SalvarEmpresa").attr('disabled', 'false');
    $("#pagamento_online").multiSelect();
    $(".form-control").focusout(function () {
        var servicos = new Servicos();
        servicos.validar();
    });

    $("#empresa").focusout(function () {
        var servicos = new Servicos();
        servicos.verificarRegistro(this.value);
    });


    $(".GerarTabelas").click(function () {
        var servicos = new Servicos();
        servicos.gerarTabelas();
    });

    $(".TestarConexao").click(function () {
        var servicos = new Servicos();
        servicos.set('testarConexao');
    });

    var objeto = {
        coluna: 'sr_recno',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ct", null);
    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});

function novo() {
    $('#empresa').removeClass('requer-erro');
    $(".SalvarEmpresa").off();
    $("#empresa").focusout(function () {
        var servicos = new Servicos();
        servicos.verificarRegistro(this.value);
    });
    $(".SalvarEmpresa").click(function () {
        var servicos = new Servicos();
        servicos.set('adicionar');
    });
    var form = new FormularioGrid();
    var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
    form.mudarTab(true, 'Incluir registro');
    form.desabilitar($('#formCadastro' + capitalize(tipo)));

}

/**
 * @param {string} cod sr_recno da empresa
 */
function editar(cod) {
    $('#empresa').removeClass('requer-erro');
    $("#empresa").off();
    $(".SalvarEmpresa").off();
    $(".SalvarEmpresa").click(function () {
        var servicos = new Servicos();
        servicos.set('editar');
    });
    var form = new FormularioGrid();
    var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
    var novoForm = new Servicos(tipo);
    form.mudarTab(true, 'Editar');
    form.desabilitar($('#formCadastro' + capitalize(tipo)));
    novoForm.carregar(cod);
}

/**
 * @param {string} codigo sr_recno da empresa
 */
function remover(codigo) {
    var servicos = new Servicos();
    var empresa = [];

    $.each(ResultAjax, function (chave, valores) {
        if (valores.codigo === codigo) {
            empresa = valores;
            servicos.remover(empresa, 'remover');
            return false;
        }
    });
}

var Servicos = function () {
    var self = this;

    this.gerarTabelas = function () {
        $.ajax({
            type: "POST",
            url: '/gerar/tabelas',
            data: {
                dados: self.getDados()
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if (data[0] === 'erro') {
                    toastr.error('Conexão falhou.');
                    return false;
                }

                var html = '';
                var contador = 1;
                $.each(data, function (tabela, desctabela) {
                    html += '<tr><th scope="row">' + contador + '</th><td>' + tabela + '</td><td>' + desctabela + '</td><td>Tabela gerada com sucesso!</td></tr>';
                    contador++;
                });

                $("#tabelasCriadas tbody").html(html);
                $("#tabelasCriadas").modal("toggle");
            }
        });
    };

    /**
     * @param {string} acao Nome da função no php
     */
    this.set = function (acao) {
        var qtdCli = $("[name=qtdClientes]").val();
        if (typeof qtdCli === 'undefined' || qtdCli === '' || qtdCli < 0) {
            toastr.error('Inserir quantidade de cliente.', 'Informação!');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: self.getDados(),
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                if ('teste' in data && data.teste) {
                    $("body").data("ct", true);
                    toastr.success('Conexão verificada com sucesso.', 'Informação!');
                    return false;
                } else if ('teste' in data && !data.teste) {
                    $("body").data("ct", false);
                    toastr.error('Conexão falhou.');
                    return false;
                }

                if (data) {
                    SelectGrid.limpaPesquisar();
                    var form = new FormularioGrid();
                    form.mudarTab(false, false);
                    toastr.success('Salvo com sucesso.', 'Informação!');
                } else {
                    toastr.error('Registro já existe.', 'Informação!');
                }
            },
            error: function (retorno) {
                if (retorno.responseText.search("pg_connect") > 0) {
                    toastr.error('Conexão falhou.');
                }
            },
            complete: function () {
                carregando(false);
            }
        });
    };

    this.getDados = function () {
        var serialize = $(".empresas").serializeArray();
        var json = 'csb2b: ' + $(".csb2b").val() + ', csgestor: ' + $(".csgestor").val() + '';
        serialize[serialize.length] = {name: 'projetos', value: json};
        return serialize;
    };

    /**
     * @param {string} acao Nome da função no php
     */
    this.remover = function (valores, acao) {
        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: valores,
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);
                if (data) {
                    SelectGrid.limpaPesquisar();
                    toastr.success('Registro removido com sucesso.', 'Informação!');
                }
            }
        });
    };
    /**
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    this.carregar = function (codigo) {
        $("[name=cielo]").val(0);
        $("[name=pagseguro]").val(0);
        $("[name=moip]").val(0);

        $.each(ResultAjax, function (indexRow, row) {
            if (row.codigo === codigo) {
                $.each(row, function (index, value) {
                    if (index === 'projetos') {
                        var splitprojeto = value.split(',');
                        $('.' + $.trim(splitprojeto[0].split(":")[0])).val($.trim(splitprojeto[0].split(":")[1]));
                        $('.' + $.trim(splitprojeto[1].split(":")[0])).val($.trim(splitprojeto[1].split(":")[1]));
                    } else {
                        console.log(index);
                        $('[name=' + index + ']').val(value);
                    }

                });
                return false;
            }
        });
    };
    /**
     * @param {string} registro registro a ser cadastrado
     */
    this.verificarRegistro = function (registro) {
        if (registro === '' || typeof registro === 'undefined') {
            $("#empresa").attr('class', 'form-control requer-erro');
            $(".SalvarEmpresa").attr('disabled', 'true');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: {coluna: 'cfiapevld', valor: registro},
                acao: 'verificarRegistro'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);
                if (data[0] === false) {
                    $("#empresa").attr('class', 'form-control requer-erro');
                    //toastr.error('Registro já existe.', 'Informação!');
                    $(".SalvarEmpresa").attr('disabled', 'true');
                    $(".help-block-empresa").text('Já existe registro com mesmo apelido');
                } else {
                    $("#empresa").attr('class', 'form-control');
                    self.validar();
                    $(".help-block-empresa").text('');
                }
            }
        });
    };

    /**
     * Busca e verifica se existem valores para liberação de botão salvar
     */
    this.validar = function () {
        var continuar = true;
        $.each($(".empresas").serializeArray(), function (chave, valor) {
            if (
                    valor.name !== 'iptintometrico' &&
                    valor.name !== 'portatintometrico' &&
                    valor.name !== 'bancotintometrico' &&
                    valor.name !== 'codigo' &&
                    valor.name !== 'dominio' &&
                    valor.name !== 'subdominio'

                    ) {
                if (valor.value === '') {
                    continuar = false;
                }
            }
        });

        if (continuar && $("#empresa").attr('class').indexOf('requer-erro') < 0) {
            $(".SalvarEmpresa").removeAttr('disabled');
        } else {
            $(".SalvarEmpresa").attr('disabled', 'true');
        }
    };
};
