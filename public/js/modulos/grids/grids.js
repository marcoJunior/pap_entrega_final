$(document).ready(function () {
    $(".BuscarCliente").attr('disabled', 'false');

    Servicos.init();

    var objeto = {
        coluna: 'cficodusu',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});

function novo() {
    Servicos.novo();
}

$(document).ajaxStop(function () {
    Servicos.ativados();

    $('input[type=checkbox]').on('ifClicked', function (event) {
        var ativacao = '';
               
        if ($("#" + this.id + ":checked").length === 1) {
            ativacao = 0;
        } else {
            ativacao = 1;
        }

        Servicos.ativarOuDesativar(this.value, ativacao);
    });

});

var Servicos = (function () {
    var eventos = function () {
        // valida se os campos não 
        $("form.buscarcliente input").change(function () {
            Servicos.validar(".buscarcliente", 'buscar');
        });
    };

    var ativados = function () {
        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valor) {
            if (valor && valor.ativo === '1') {
                $('#linha-' + valor.codigo).iCheck('check');
            }
            $('input').iCheck({checkboxClass: 'icheckbox_minimal-blue linha-check-' + valor.codigo, radioClass: 'iradio_minimal', increaseArea: '20%'});
        });
    };

    var ativarOuDesativar = function (codigo, ativacao) {
        $.ajax({
            type: "POST",
            url: '/cliente/servicos',
            data: {
                dados: {codigo: codigo, ativacao: ativacao},
                acao: 'atualizarUsuario'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {

                $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (index, value) {
                    if (value && parseInt(value.codigo) === parseInt(codigo)) {
                        $("#titulo-coluna-grid").data("data").gridPhp.data[index].ativo = ativacao;
                    }
                });
                carregando(false);
                toastr.success('Atualizado com sucesso.', 'Informação!');
            }
        });
    };

    var editar = function (codigo) {
        $('[name=email').attr('readonly', true);
        $(".BtnVoltarGrid").remove();
        $("form.cliente .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>').show("slow");
        $(".horizontal-form.buscarcliente").hide();

        setTimeout(function () {
            $("form.cliente").show();
            $("#cliente-encontrado").show('slow');
        }, 200);

        $('#cliente').removeClass('requer-erro');
        $("#cliente").off();
        $(".SalvarCliente").off();
        $(".SalvarCliente").click(function () {
            set('editar');
        });
        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
        form.mudarTab(true, 'Editar');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));
        carregar(codigo);

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });
    };

    var novo = function () {
        $('[name=email').attr('readonly', true);
        $(".BtnVoltarGrid").remove();
        $("form.buscarcliente .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $("form.cliente").hide();
        $("#cliente-encontrado").hide();
        $(".horizontal-form.buscarcliente").show("slow");
        $("form.cliente").hide("slow");
        $("[name=btn-adicionar]").hide("slow");
        $('#cliente').removeClass('requer-erro');
        $(".SalvarCliente").off();

        $(".SalvarCliente").click(function () {
            set('adicionar');
        });

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
        form.mudarTab(true, 'Incluir registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));
    };

    /** 
     * 
     * @param {string} acao Nome da função no php
     */
    var set = function (acao) {
        if (validar("form.cliente", 'salvar')) {
            $.ajax({
                type: "POST",
                url: '/cliente/servicos',
                data: {
                    dados: $("form.cliente").serializeArray(),
                    acao: acao
                },
                beforeSend: function () {
                    carregando(true);
                },
                success: function (data) {
                    if (data) {
                        SelectGrid.limpaPesquisar();
                        var form = new FormularioGrid();
                        form.mudarTab(false, false);
                        $("[name=btn-adicionar]").show("slow");
                        toastr.success('Salvo com sucesso.', 'Informação!');
                    } else {
                        toastr.error('Registro já existe.', 'Informação!');
                    }
                },
                complete: function () {
                    carregando(false);
                }
            });
        } else {
            toastr.warning('Preencher todos os campos corretamente.', 'Informação!');
        }
    };

    /** 
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    var carregar = function (codigo) {
        $.each(ResultAjax, function (indexRow, row) {
            if (row.codigo === codigo) {
                $.each(row, function (index, value) {
                    $('[name=' + index + ']').val(row[index]);

                    if (index !== 'sistema' && index !== 'ativo') {
                        $('[name=' + index + ']').text(row[index]);
                    }

                });
                return false;
            }
        });
    };

    /** 
     * Verifica se existem valores para liberação de botão salvar
     */
    var validar = function (elemento, tipo) {
        var continuar = true;

        if (tipo === 'salvar') {
            $.each($(elemento).serializeArray(), function (chave, valor) {
                if ((valor.name === 'email' || valor.name === 'sistema' || valor.name === 'ativo') && valor.value === '') {
                    continuar = false;
                }
            });
            return continuar;
        }

        $.each($(elemento).serializeArray(), function (chave, valor) {
            if (valor.value === '') {
                continuar = false;
            }
        });
        if (continuar) {
            $(".buscarcliente").removeAttr('disabled');
            return false;
        }
        $(".buscarcliente").attr('disabled', 'true');

    };

    var getUsuario = function () {
        var url = 'app.csgestor.com.br';

        if (location.origin.search("dev") > 0) {
            url = location.origin.split("http://")[1].split(".")[0] + '.csgestor.com.br';
        }

        $.ajax({
            type: "POST",
            url: 'http://' + url + '/login/carregamento-admin',
            data: {emailProcurar: $("#emailprocurar").val(), senhaProcurar: $("#senhaprocurar").val(), empresaProcurar: $("#empresaprocurar").val()},
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                if (data[0] == false) {
                    toastr.error("Cliente não encontrado");
                    return false;
                }

                $.each(data, function (chave, valor) {
                    $('[name=' + chave + ']').val(valor);
                });

                if ($('[name=email').val() === '') {
                    $('[name=email').attr('readonly', false);
                }

                $("[name=empresa]").val($("#empresaprocurar").val());
            },
            complete: function () {
                carregando(false);
                $("form.cliente").show();
                $("#cliente-encontrado").show('slow');
            }
        });
    };

    /** 
     * @param {string} codigo Código do usuário
     */
    var remover = function (codigo) {
        $.ajax({
            type: "POST",
            url: '/cliente/servicos',
            data: {
                codigo: codigo,
                acao: 'remover'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                SelectGrid.limpaPesquisar();
                var form = new FormularioGrid();
                form.mudarTab(false, false);
                $("[name=btn-adicionar]").show("slow");
                toastr.success('Removido com sucesso.', 'Informação!');
            },
            complete: function () {
                carregando(false);
            }
        });
    };

    return {
        init: function () {
            eventos();
        },
        setar: set,
        carregar: carregar,
        validar: validar,
        editar: editar,
        novo: novo,
        remover: remover,
        get: getUsuario,
        ativados: ativados,
        ativarOuDesativar: ativarOuDesativar,
    };
})();

$(document).ready(function () {

    $(".SalvarEmpresa").attr('disabled', 'false');
    $("#pagamento_online").multiSelect();
    $(".form-control").focusout(function () {
        var servicos = new Servicos();
        servicos.validar();
    });

    $("#empresa").focusout(function () {
        var servicos = new Servicos();
        servicos.verificarRegistro(this.value);
    });


    $(".GerarTabelas").click(function () {
        var servicos = new Servicos();
        servicos.gerarTabelas();
    });

    $(".TestarConexao").click(function () {
        var servicos = new Servicos();
        servicos.set('testarConexao');
    });

    var objeto = {
        coluna: 'sr_recno',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ct", null);
    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});

function novo() {
    $('#empresa').removeClass('requer-erro');
    $(".SalvarEmpresa").off();
    $("#empresa").focusout(function () {
        var servicos = new Servicos();
        servicos.verificarRegistro(this.value);
    });
    $(".SalvarEmpresa").click(function () {
        var servicos = new Servicos();
        servicos.set('adicionar');
    });
    var form = new FormularioGrid();
    var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
    form.mudarTab(true, 'Incluir registro');
    form.desabilitar($('#formCadastro' + capitalize(tipo)));

}

/**
 * @param {string} cod sr_recno da empresa
 */
function editar(cod) {
    $('#empresa').removeClass('requer-erro');
    $("#empresa").off();
    $(".SalvarEmpresa").off();
    $(".SalvarEmpresa").click(function () {
        var servicos = new Servicos();
        servicos.set('editar');
    });
    var form = new FormularioGrid();
    var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];
    var novoForm = new Servicos(tipo);
    form.mudarTab(true, 'Editar');
    form.desabilitar($('#formCadastro' + capitalize(tipo)));
    novoForm.carregar(cod);
}

/**
 * @param {string} codigo sr_recno da empresa
 */
function remover(codigo) {
    var servicos = new Servicos();
    var empresa = [];

    $.each(ResultAjax, function (chave, valores) {
        if (valores.codigo === codigo) {
            empresa = valores;
            servicos.remover(empresa, 'remover');
            return false;
        }
    });
}

var Servicos = function () {
    var self = this;

    this.gerarTabelas = function () {
        $.ajax({
            type: "POST",
            url: '/gerar/tabelas',
            data: {
                dados: self.getDados()
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if (data[0] === 'erro') {
                    toastr.error('Conexão falhou.');
                    return false;
                }

                var html = '';
                var contador = 1;
                $.each(data, function (tabela, desctabela) {
                    html += '<tr><th scope="row">' + contador + '</th><td>' + tabela + '</td><td>' + desctabela + '</td><td>Tabela gerada com sucesso!</td></tr>';
                    contador++;
                });

                $("#tabelasCriadas tbody").html(html);
                $("#tabelasCriadas").modal("toggle");
            }
        });
    };

    /**
     * @param {string} acao Nome da função no php
     */
    this.set = function (acao) {
        var qtdCli = $("[name=qtdClientes]").val();
        if (typeof qtdCli === 'undefined' || qtdCli === '' || qtdCli < 0) {
            toastr.error('Inserir quantidade de cliente.', 'Informação!');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: self.getDados(),
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                if ('teste' in data && data.teste) {
                    $("body").data("ct", true);
                    toastr.success('Conexão verificada com sucesso.', 'Informação!');
                    return false;
                } else if ('teste' in data && !data.teste) {
                    $("body").data("ct", false);
                    toastr.error('Conexão falhou.');
                    return false;
                }

                if (data) {
                    SelectGrid.limpaPesquisar();
                    var form = new FormularioGrid();
                    form.mudarTab(false, false);
                    toastr.success('Salvo com sucesso.', 'Informação!');
                } else {
                    toastr.error('Registro já existe.', 'Informação!');
                }
            },
            error: function (retorno) {
                if (retorno.responseText.search("pg_connect") > 0) {
                    toastr.error('Conexão falhou.');
                }
            },
            complete: function () {
                carregando(false);
            }
        });
    };

    this.getDados = function () {
        var serialize = $(".empresas").serializeArray();
        var json = 'csb2b: ' + $(".csb2b").val() + ', csgestor: ' + $(".csgestor").val() + '';
        serialize[serialize.length] = {name: 'projetos', value: json};
        return serialize;
    };

    /**
     * @param {string} acao Nome da função no php
     */
    this.remover = function (valores, acao) {
        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: valores,
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);
                if (data) {
                    SelectGrid.limpaPesquisar();
                    toastr.success('Registro removido com sucesso.', 'Informação!');
                }
            }
        });
    };
    /**
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    this.carregar = function (codigo) {
        $("[name=cielo]").val(0);
        $("[name=pagseguro]").val(0);
        $("[name=moip]").val(0);

        $.each(ResultAjax, function (indexRow, row) {
            if (row.codigo === codigo) {
                $.each(row, function (index, value) {
                    if (index === 'projetos') {
                        var splitprojeto = value.split(',');
                        $('.' + $.trim(splitprojeto[0].split(":")[0])).val($.trim(splitprojeto[0].split(":")[1]));
                        $('.' + $.trim(splitprojeto[1].split(":")[0])).val($.trim(splitprojeto[1].split(":")[1]));
                    } else {
                        console.log(index);
                        $('[name=' + index + ']').val(value);
                    }

                });
                return false;
            }
        });
    };
    /**
     * @param {string} registro registro a ser cadastrado
     */
    this.verificarRegistro = function (registro) {
        if (registro === '' || typeof registro === 'undefined') {
            $("#empresa").attr('class', 'form-control requer-erro');
            $(".SalvarEmpresa").attr('disabled', 'true');
            return false;
        }

        $.ajax({
            type: "POST",
            url: '/empresas/servicos',
            data: {
                dados: {coluna: 'cfiapevld', valor: registro},
                acao: 'verificarRegistro'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);
                if (data[0] === false) {
                    $("#empresa").attr('class', 'form-control requer-erro');
                    //toastr.error('Registro já existe.', 'Informação!');
                    $(".SalvarEmpresa").attr('disabled', 'true');
                    $(".help-block-empresa").text('Já existe registro com mesmo apelido');
                } else {
                    $("#empresa").attr('class', 'form-control');
                    self.validar();
                    $(".help-block-empresa").text('');
                }
            }
        });
    };

    /**
     * Busca e verifica se existem valores para liberação de botão salvar
     */
    this.validar = function () {
        var continuar = true;
        $.each($(".empresas").serializeArray(), function (chave, valor) {
            if (
                    valor.name !== 'iptintometrico' &&
                    valor.name !== 'portatintometrico' &&
                    valor.name !== 'bancotintometrico' &&
                    valor.name !== 'codigo' &&
                    valor.name !== 'dominio' &&
                    valor.name !== 'subdominio'

                    ) {
                if (valor.value === '') {
                    continuar = false;
                }
            }
        });

        if (continuar && $("#empresa").attr('class').indexOf('requer-erro') < 0) {
            $(".SalvarEmpresa").removeAttr('disabled');
        } else {
            $(".SalvarEmpresa").attr('disabled', 'true');
        }
    };
};

$(document).ready(function () {
    $(".BuscarCliente").attr('disabled', 'false');

    Servicos.init();

    var objeto = {
        coluna: 'cfidtalog DESC',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});


var Servicos = (function () {
    var eventos = function () {
        $("[name=btn-adicionar]").remove();
    };

    return {
        init: function () {
            eventos();
        },
        eventos: eventos,
    };
})();

$(document).ready(function () {
    $(".SalvarUsuario").attr('disabled', true);
    //Servicos.init();

    var objeto = {
        coluna: 'cficodusu',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();

    $("[name=btn-adicionar]").removeAttr('onclick');
    $("[name=btn-adicionar]").click(function () {
        ServicosGrid.novo();
    });

    $('#senha').password();
});

$(document).ajaxStop(function () {
    ServicosGrid.ativados();

    $('input[type=checkbox]').on('ifClicked', function (event) {
        var ativacao = '';
        if ($("#" + this.id + ":checked").length === 1) {
            ativacao = 0;
        } else {
            ativacao = 1;
        }

        ServicosGrid.ativarOuDesativar(this.value, ativacao);
    });

});

var ServicosGrid = (function () {
    var eventos = function () {
        $("form.usuarios").focusout(function () {
            validar();
        });
        ServicosGrid.ativados();
        
    };

    var ativados = function () {
        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valor) {
            if (valor && valor.ativacao === '1') {
                $('#linha-' + valor.codigo).iCheck('check');
            }
            $('input').iCheck({checkboxClass: 'icheckbox_minimal-blue linha-check-' + valor.codigo, radioClass: 'iradio_minimal', increaseArea: '20%'});
        });
    };

    var set = function (acao) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                dados: $("form.usuarios").serializeArray(),
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if ('erro' in data) {
                    toastr.error('Registro já existe.', 'Informação!');
                    return false;
                }

                SelectGrid.limpaPesquisar();
                var form = new FormularioGrid();
                form.mudarTab(false, false);
                $("[name=btn-adicionar]").show("slow");

                toastr.success('Salvo com sucesso.', 'Informação!');
            }
        });
    };

    var novo = function () {
        $(".SalvarUsuario").attr('onclick', "ServicosGrid.set('adicionar')");

        $(".BtnVoltarGrid").remove();
        $("form.usuarios .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $(".horizontal-form.usuarios").show("slow");

        $("[name=btn-adicionar]").hide("slow");
        $('#usuarios').removeClass('requer-erro');
        $(".SalvarUsuario").off();

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];

        form.mudarTab(true, 'Incluir registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));

        ServicosGrid.init();
    };

    /** 
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    var carregar = function (codigo) {
        $(".SalvarUsuario").attr('onclick', "ServicosGrid.set('editar')");

        $(".BtnVoltarGrid").remove();
        $("form.usuarios .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $(".horizontal-form.usuarios").show("slow");

        $("[name=btn-adicionar]").hide("slow");
        $('#usuarios').removeClass('requer-erro');


        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];

        form.mudarTab(true, 'Editar registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));

        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valores) {
            if (parseInt(valores.codigo) === parseInt(codigo)) {
                $.each(valores, function (name, valor) {
                    if (name !== 'tipo' && name !== 'ativacao') {
                        $('[name=' + name + ']').text(valor);
                        $('[name=' + name + ']').val(valor);
                    } else {
                        $('[name=' + name + ']').val(valor);
                    }

                });
            }
        });

        ServicosGrid.init();
    };

    /** 
     * Verifica se existem valores para liberação de botão salvar
     */
    var validar = function () {
        var camposvazios = false;
        $.each($("form.usuarios input").serializeArray(), function (chave, valor) {
            if (valor.value === '' && valor.name !== 'codigo') {
                camposvazios = true;
            }
        });
        $(".SalvarUsuario").attr('disabled', true);

        if (!camposvazios) {
            $(".SalvarUsuario").attr('disabled', false);
        }
    };


    /** 
     * @param {string} codigo Código do usuário
     */
    var remover = function (codigo) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                codigo: codigo,
                acao: 'remover'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if ('erro' in data) {
                    toastr.error('Erro, tente novamente mais tarde.', 'Informação!');
                    return false;
                }

                SelectGrid.limpaPesquisar();
                toastr.success('Usuário ' + codigo + ' removido com sucesso.', 'Informação!');
            }
        });
    };

    var ativarOuDesativar = function (codigo, ativacao) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                dados: {codigo: codigo, ativacao: ativacao},
                acao: 'atualizarUsuario'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {

                $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (index, value) {
                    if (value && parseInt(value.codigo) === parseInt(codigo)) {
                        $("#titulo-coluna-grid").data("data").gridPhp.data[index].ativacao = ativacao;
                    }
                });
                carregando(false);
                toastr.success('Atualizado com sucesso.', 'Informação!');
            }
        });
    };

//    var verificarRegistro = function (coluna, valor) {
//        $.ajax({
//            type: "POST",
//            url: '/usuarios/servicos',
//            data: {
//                dados: {coluna: coluna, valor: valor},
//                acao: 'verificar'
//            },
//            beforeSend: function () {
//                carregando(true);
//            },
//            success: function (data) {
//                carregando(false);
//
//                if (data[0]) {
//                    $(".SalvarUsuario").attr('disabled', true);
//                    toastr.error('Registro já existe.', 'Informação!');
//                }
//            }
//        });
//    };
    return {
        init: function () {
            eventos();
        },
        novo: novo,
        set: set,
        carregar: carregar,
        remover: remover,
        ativados: ativados,
        ativarOuDesativar: ativarOuDesativar,
//        verificar: verificarRegistro
    };

})();
