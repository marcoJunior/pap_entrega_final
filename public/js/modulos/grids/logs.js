$(document).ready(function () {
    $(".BuscarCliente").attr('disabled', 'false');

    Servicos.init();

    var objeto = {
        coluna: 'cfidtalog DESC',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();
});


var Servicos = (function () {
    var eventos = function () {
        $("[name=btn-adicionar]").remove();
    };

    return {
        init: function () {
            eventos();
        },
        eventos: eventos,
    };
})();
