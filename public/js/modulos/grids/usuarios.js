$(document).ready(function () {
    $(".SalvarUsuario").attr('disabled', true);
    //Servicos.init();

    var objeto = {
        coluna: 'cficodusu',
        inicial: $("#cfidatini").val(),
        final: $("#cfidatfim").val()
    };

    $("body").data("ordenacaoinicial", objeto);
    $("body").data("dataAjax").gridAjax.parametro.where.between = $("body").data("ordenacaoinicial");
    SelectGrid.carregar();

    $("[name=btn-adicionar]").removeAttr('onclick');
    $("[name=btn-adicionar]").click(function () {
        ServicosGrid.novo();
    });

    $('#senha').password();
});

$(document).ajaxStop(function () {
    ServicosGrid.ativados();

    $('input[type=checkbox]').on('ifClicked', function (event) {
        var ativacao = '';
        if ($("#" + this.id + ":checked").length === 1) {
            ativacao = 0;
        } else {
            ativacao = 1;
        }

        ServicosGrid.ativarOuDesativar(this.value, ativacao);
    });

});

var ServicosGrid = (function () {
    var eventos = function () {
        $("form.usuarios").focusout(function () {
            validar();
        });
        ServicosGrid.ativados();
        
    };

    var ativados = function () {
        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valor) {
            if (valor && valor.ativacao === '1') {
                $('#linha-' + valor.codigo).iCheck('check');
            }
            $('input').iCheck({checkboxClass: 'icheckbox_minimal-blue linha-check-' + valor.codigo, radioClass: 'iradio_minimal', increaseArea: '20%'});
        });
    };

    var set = function (acao) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                dados: $("form.usuarios").serializeArray(),
                acao: acao
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if ('erro' in data) {
                    toastr.error('Registro já existe.', 'Informação!');
                    return false;
                }

                SelectGrid.limpaPesquisar();
                var form = new FormularioGrid();
                form.mudarTab(false, false);
                $("[name=btn-adicionar]").show("slow");

                toastr.success('Salvo com sucesso.', 'Informação!');
            }
        });
    };

    var novo = function () {
        $(".SalvarUsuario").attr('onclick', "ServicosGrid.set('adicionar')");

        $(".BtnVoltarGrid").remove();
        $("form.usuarios .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $(".horizontal-form.usuarios").show("slow");

        $("[name=btn-adicionar]").hide("slow");
        $('#usuarios').removeClass('requer-erro');
        $(".SalvarUsuario").off();

        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];

        form.mudarTab(true, 'Incluir registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));

        ServicosGrid.init();
    };

    /** 
     * Carrega valores do formulário para edição
     * @param {string} codigo Código da empresa
     */
    var carregar = function (codigo) {
        $(".SalvarUsuario").attr('onclick', "ServicosGrid.set('editar')");

        $(".BtnVoltarGrid").remove();
        $("form.usuarios .pull-right button").before('<button type="button" class="btn red BtnVoltarGrid"><i class="fa fa-arrow-left"></i> Voltar</button>');
        $(".horizontal-form.usuarios").show("slow");

        $("[name=btn-adicionar]").hide("slow");
        $('#usuarios').removeClass('requer-erro');


        $(".BtnVoltarGrid").click(function () {
            var form = new FormularioGrid();
            form.mudarTab(false, false);
        });

        var form = new FormularioGrid();
        var tipo = SelectGrid.getParametroUrl('tipo').split('|')[0];

        form.mudarTab(true, 'Editar registro');
        form.desabilitar($('#formCadastro' + capitalize(tipo)));

        $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (chave, valores) {
            if (parseInt(valores.codigo) === parseInt(codigo)) {
                $.each(valores, function (name, valor) {
                    if (name !== 'tipo' && name !== 'ativacao') {
                        $('[name=' + name + ']').text(valor);
                        $('[name=' + name + ']').val(valor);
                    } else {
                        $('[name=' + name + ']').val(valor);
                    }

                });
            }
        });

        ServicosGrid.init();
    };

    /** 
     * Verifica se existem valores para liberação de botão salvar
     */
    var validar = function () {
        var camposvazios = false;
        $.each($("form.usuarios input").serializeArray(), function (chave, valor) {
            if (valor.value === '' && valor.name !== 'codigo') {
                camposvazios = true;
            }
        });
        $(".SalvarUsuario").attr('disabled', true);

        if (!camposvazios) {
            $(".SalvarUsuario").attr('disabled', false);
        }
    };


    /** 
     * @param {string} codigo Código do usuário
     */
    var remover = function (codigo) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                codigo: codigo,
                acao: 'remover'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {
                carregando(false);

                if ('erro' in data) {
                    toastr.error('Erro, tente novamente mais tarde.', 'Informação!');
                    return false;
                }

                SelectGrid.limpaPesquisar();
                toastr.success('Usuário ' + codigo + ' removido com sucesso.', 'Informação!');
            }
        });
    };

    var ativarOuDesativar = function (codigo, ativacao) {
        $.ajax({
            type: "POST",
            url: '/usuarios/servicos',
            data: {
                dados: {codigo: codigo, ativacao: ativacao},
                acao: 'atualizarUsuario'
            },
            beforeSend: function () {
                carregando(true);
            },
            success: function (data) {

                $.each($("#titulo-coluna-grid").data("data").gridPhp.data, function (index, value) {
                    if (value && parseInt(value.codigo) === parseInt(codigo)) {
                        $("#titulo-coluna-grid").data("data").gridPhp.data[index].ativacao = ativacao;
                    }
                });
                carregando(false);
                toastr.success('Atualizado com sucesso.', 'Informação!');
            }
        });
    };

//    var verificarRegistro = function (coluna, valor) {
//        $.ajax({
//            type: "POST",
//            url: '/usuarios/servicos',
//            data: {
//                dados: {coluna: coluna, valor: valor},
//                acao: 'verificar'
//            },
//            beforeSend: function () {
//                carregando(true);
//            },
//            success: function (data) {
//                carregando(false);
//
//                if (data[0]) {
//                    $(".SalvarUsuario").attr('disabled', true);
//                    toastr.error('Registro já existe.', 'Informação!');
//                }
//            }
//        });
//    };
    return {
        init: function () {
            eventos();
        },
        novo: novo,
        set: set,
        carregar: carregar,
        remover: remover,
        ativados: ativados,
        ativarOuDesativar: ativarOuDesativar,
//        verificar: verificarRegistro
    };

})();
