if (window.location.search === "") {
    var urlPath = window.location.pathname;
} else {
    var urlPath = window.location.pathname + window.location.search;
}
var Menu = $('ul.page-sidebar-menu');

//Marca os menus de acordo com a URL
var link = Menu.find('li a[href="' + urlPath + '"]');
link.parents('li').addClass('active open');
if (link.parents('li').hasClass('active open') && urlPath !== link.attr('href')) {
    link.parents('li').removeClass('active open');
}

//TECLA DE ATALHO
Mousetrap.bind('n', function () {
    setOrcWithClienteConsumidor();
}, 'keyup');



function styleValidaInput(id) {
    var input = jQuery(id);
    input.wrap('<span class="valid"></span>');
}

function showModal(status, msg) {
    var status, id, msg;

    if (status === 'sucesso') {
        id = '#successModal';
    }
    else if (status === 'erro') {
        id = '#catchError';
    }
    else {
        console.log("o primeiro parametro deve ser = 'sucesso' ou 'erro'");

        return false;
    }
    
    if (typeof msg === 'undefined') {
        console.log("Mostrando mensagem padrão, escreva a mensagem no segundo parametro");
    }
    else if (typeof msg !== 'string'){
        console.log("a msg deve ser string");
        return false;
    }

    $(id).modal()
            .find('.modalMsg').text(msg);
}