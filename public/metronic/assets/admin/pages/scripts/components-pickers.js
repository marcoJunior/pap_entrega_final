var ComponentsPickers = function () {

    var handleDatePickerPtBr = function () {
        $.fn.datepicker.dates = {};
        $.fn.datepicker.dates['pt-br'] = {
            days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
            daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            daysMin: ["Do", "Se", "Te", "Qa", "Qi", "Se", "Sa"],
            months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            today: "Hoje",
            clear: "Clear",
            format: "dd/mm/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };
    };

    var handleDatePickers = function () {
        if (jQuery().datepicker) {
            $('.date-picker').datepicker({
                rtl: Metronic.isRTL(),
                orientation: "right",
                autoclose: true,
                locale: {
                    applyLabel: 'Ok',
                    fromLabel: 'De',
                    toLabel: 'até',
                    customRangeLabel: 'Selecione um periodo',
                    daysOfWeek: ['Do', 'Se', 'Te', 'Qua', 'Qui', 'Se', 'Sa'],
                    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                    firstDay: 1
                }
            });
            //$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
        }

        /* Workaround to restrict daterange past date select: http://stackoverflow.com/questions/11933173/how-to-restrict-the-selectable-date-ranges-in-bootstrap-datepicker */
    }

    var handleTimePickers = function () {

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });
        }
    }

    var handleDateRangePickers = function (datini, datfim) {
        if (!jQuery().daterangepicker) {
            return;
        }
        if (datini === "not") {
            datini = moment().startOf('month').format('DD/MM/YYYY');
        }
        if (datfim === "not") {
            datfim = moment().endOf('month').format('DD/MM/YYYY');
        }


        $('#defaultrange').daterangepicker({
            opens: (Metronic.isRTL() ? 'left' : 'right'),
            format: 'DD/MM/YYYY',
            separator: ' até ',
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            minDate: '01/01/2010',
            maxDate: '31/12/2015',
        },
                function (start, end) {
                    $('#defaultrange input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        $('#defaultrange_modal').daterangepicker({
            opens: (Metronic.isRTL() ? 'left' : 'right'),
            format: 'DD/MM/YYYY',
            separator: ' até ',
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            minDate: '01/01/2010',
            maxDate: '31/12/2015',
        },
                function (start, end) {
                    $('#defaultrange_modal input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                }
        );

        // this is very important fix when daterangepicker is used in modal. in modal when daterange picker is opened and mouse clicked anywhere bootstrap modal removes the modal-open class from the body element.
        // so the below code will fix this issue.
        $('#defaultrange_modal').on('click', function () {
            if ($('#daterangepicker_modal').is(":visible") && $('body').hasClass("modal-open") == false) {
                $('body').addClass("modal-open");
            }
        });

        $('#reportrange').daterangepicker({
            opens: (Metronic.isRTL() ? 'left' : 'right'),
            startDate: datini, //moment().startOf('month'),
            endDate: datfim, //moment().endOf('month'),
            minDate: '01/01/2010',
            maxDate: '31/12/2050',
            dateLimit: {
                days: 365
            },
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Hoje': [moment(), moment()],
                'Ontem': [moment().subtract(2, 'days'), moment().subtract(1, 'days')],
                'Mes atual': [moment().startOf('month'), moment().endOf('month')],
                'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
                'Ultimos 3 meses': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
                'Ultimos 6 meses': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
                'Ultimos 12 meses': [moment().subtract(11, 'month').startOf('month'), moment().endOf('month')]
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Ok',
                fromLabel: 'De',
                toLabel: 'até',
                customRangeLabel: 'Selecione um periodo',
                daysOfWeek: ['Do', 'Se', 'Te', 'Qua', 'Qui', 'Se', 'Sa'],
                monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
                firstDay: 1
            }
        }, function (start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
        }
        );
        //Set the initial state of the picker label

        jQuery("#cfidatini").val(datini);
        jQuery("#cfidatfim").val(datfim);

        //$('#reportrange span').html(moment().startOf('month').format('DD/MM/YYYY') + ' - ' + moment().endOf('month').format('DD/MM/YYYY'));

        $('#reportrange span').html(datini + ' - ' + datfim);
    }

    var handleDatetimePicker = function () {

        if (!jQuery().datetimepicker) {
            return;
        }

        $(".form_datetime").datetimepicker({
            autoclose: true,
            isRTL: Metronic.isRTL(),
            format: "dd MM yyyy - hh:ii",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left")
        });

        $(".form_advance_datetime").datetimepicker({
            isRTL: Metronic.isRTL(),
            format: "dd MM yyyy - hh:ii",
            autoclose: true,
            todayBtn: true,
            startDate: "2013-02-14 10:00",
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            minuteStep: 10
        });

        $(".form_meridian_datetime").datetimepicker({
            isRTL: Metronic.isRTL(),
            format: "dd MM yyyy - HH:ii P",
            showMeridian: true,
            autoclose: true,
            pickerPosition: (Metronic.isRTL() ? "bottom-right" : "bottom-left"),
            todayBtn: true
        });

        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
    }

    var handleClockfaceTimePickers = function () {

        if (!jQuery().clockface) {
            return;
        }

        $('.clockface_1').clockface();

        $('#clockface_2').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2').clockface('toggle');
        });

        $('#clockface_2_modal').clockface({
            format: 'HH:mm',
            trigger: 'manual'
        });

        $('#clockface_2_modal_toggle').click(function (e) {
            e.stopPropagation();
            $('#clockface_2_modal').clockface('toggle');
        });

        $('.clockface_3').clockface({
            format: 'H:mm'
        }).clockface('show', '14:30');
    }

    var handleColorPicker = function () {
        if (!jQuery().colorpicker) {
            return;
        }
        $('.colorpicker-default').colorpicker({
            format: 'hex'
        });
        $('.colorpicker-rgba').colorpicker();
    }


    return {
        //main function to initiate the module
        init: function () {
            handleDatePickerPtBr();
            handleDatePickers();
            handleTimePickers();
            handleDatetimePicker();
            handleDateRangePickers('not', 'not');
            handleClockfaceTimePickers();
            handleColorPicker();
        },
        initNew: function (datini, datfim) {
            handleDatePickers();
            handleTimePickers();
            handleDatetimePicker();
            handleDateRangePickers(datini, datfim);
            handleClockfaceTimePickers();
            handleColorPicker();
        }
    };
}();
    