/**
 * Serialize object para names com níveis
 * @example chave[0][subChave];
 *
 * jQuery serializeObject
 * @copyright 2014, macek <paulmacek@gmail.com>
 * @link https://github.com/macek/jquery-serialize-object
 * @license BSD
 * @version 2.5.0
 */
(function (root, factory) {

    // AMD
    if (typeof define === "function" && define.amd) {
        define(["exports", "jquery"], function (exports, $) {
            return factory(exports, $);
        });
    }

    // CommonJS
    else if (typeof exports !== "undefined") {
        var $ = require("jquery");
        factory(exports, $);
    }

    // Browser
    else {
        factory(root, (root.jQuery || root.Zepto || root.ender || root.$));
    }

}(this, function (exports, $) {

    var patterns = {
        validate: /^[a-z_][a-z0-9_]*(?:\[(?:\d*|[a-z0-9_]+)\])*$/i,
        key: /[a-z0-9_]+|(?=\[\])/gi,
        push: /^$/,
        fixed: /^\d+$/,
        named: /^[a-z0-9_]+$/i
    };

    function FormSerializer(helper, $form) {

        // private variables
        var data = {},
                pushes = {};

        // private API
        function build(base, key, value) {
            base[key] = value;
            return base;
        }

        function makeObject(root, value) {

            var keys = root.match(patterns.key), k;

            // nest, nest, ..., nest
            while ((k = keys.pop()) !== undefined) {
                // foo[]
                if (patterns.push.test(k)) {
                    var idx = incrementPush(root.replace(/\[\]$/, ''));
                    value = build([], idx, value);
                }

                // foo[n]
                else if (patterns.fixed.test(k)) {
                    value = build([], k, value);
                }

                // foo; foo[bar]
                else if (patterns.named.test(k)) {
                    value = build({}, k, value);
                }
            }

            return value;
        }

        function incrementPush(key) {
            if (pushes[key] === undefined) {
                pushes[key] = 0;
            }
            return pushes[key]++;
        }

        function encode(pair) {
            switch ($('[name="' + pair.name + '"]', $form).attr("type")) {
                case "checkbox":
                    return pair.value === "on" ? true : pair.value;
                default:
                    return pair.value;
            }
        }

        function addPair(pair) {
            if (!patterns.validate.test(pair.name))
                return this;
            var obj = makeObject(pair.name, encode(pair));
            data = helper.extend(true, data, obj);
            return this;
        }

        function addPairs(pairs) {
            if (!helper.isArray(pairs)) {
                throw new Error("formSerializer.addPairs expects an Array");
            }
            for (var i = 0, len = pairs.length; i < len; i++) {
                this.addPair(pairs[i]);
            }
            return this;
        }

        function serialize() {
            return data;
        }

        function serializeJSON() {
            return JSON.stringify(serialize());
        }

        // public API
        this.addPair = addPair;
        this.addPairs = addPairs;
        this.serialize = serialize;
        this.serializeJSON = serializeJSON;
    }

    FormSerializer.patterns = patterns;

    FormSerializer.serializeObject = function serializeObject() {
        return new FormSerializer($, this).
                addPairs(this.serializeArray()).
                serialize();
    };

    FormSerializer.serializeJSON = function serializeJSON() {
        return new FormSerializer($, this).
                addPairs(this.serializeArray()).
                serializeJSON();
    };

    if (typeof $.fn !== "undefined") {
        $.fn.serializeObject = FormSerializer.serializeObject;
        $.fn.serializeJSON = FormSerializer.serializeJSON;
    }

    exports.FormSerializer = FormSerializer;

    return FormSerializer;
}));


function intervaloData(title, value) {
    if (value) {
        var val = JSON.parse(value);
        datini = val['inicial'];
        datfim = val['final'];
    }

    var elemento = new $('#reportrange');

    if (!jQuery().daterangepicker)
        return;

    if (title)
        elemento.attr('title', title);

    elemento.daterangepicker({
        startDate: moment().startOf('month'),
        endDate: moment().endOf('month'),
        minDate: '01/01/2010',
        maxDate: '31/12/2050',
        dateLimit: {
            days: 365
        },
        showDropdowns: true,
        ranges: {
            'Hoje': [moment(), moment()],
            'Ontem': [moment().subtract(2, 'days'), moment().subtract(1, 'days')],
            'Mes atual': [moment().startOf('month'), moment().endOf('month')],
            'Mes anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Ultimos 3 meses': [moment().subtract(2, 'month').startOf('month'), moment().endOf('month')],
            'Ultimos 6 meses': [moment().subtract(5, 'month').startOf('month'), moment().endOf('month')],
            'Ultimos 12 meses': [moment().subtract(11, 'month').startOf('month'), moment().endOf('month')],
            'Ultimos 24 meses': [moment().subtract(22, 'month').startOf('month'), moment().endOf('month')]
        },
        buttonClasses: ['btn'],
        applyClass: 'green',
        cancelClass: 'default',
        separator: ' to ',
        locale: {
            format: 'DD/MM/YYYY',
            applyLabel: 'Ok',
            fromLabel: 'De',
            toLabel: 'até',
            customRangeLabel: 'Selecione um periodo',
            daysOfWeek: ['Do', 'Se', 'Te', 'Qua', 'Qui', 'Se', 'Sa'],
            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            firstDay: 1
        }
    }, function (start, end) {
        $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));

        /**
         * Filtro novo
         * Utiliza valur no getFiltro()
         */
        $('[name=intervaloData]').val(JSON.stringify({
            inicial: start.format('DD/MM/YYYY'),
            final: end.format('DD/MM/YYYY')
        }));

        if (typeof selectDate() !== "undefined") {
            selectDate();
        }

    });

    $('#reportrange span').html(moment().startOf('month').format('DD/MM/YYYY') + ' - ' + moment().endOf('month').format('DD/MM/YYYY'));
    jQuery("#configFiltro, #paramDate").show();

}


/**
 * Retorna um objeto jQuery que podemos usar serialize como se fosse formulário
 *
 * @uso prepararSerialize('div#contato').serializeObject();
 * @param {string} elem
 * @returns {jQuery}
 */
function prepararSerialize(elem) {
    return jQuery(elem).prop('elements', jQuery('*', elem).addBack().get());
}

function getFiltros(local) {

    var retorno;
    if (typeof local !== "undefined") {
        retorno = prepararSerialize(local)
                .eq(0)
                .serializeObject();
    } else {
        retorno = prepararSerialize('.PainelAcoes .FiltroGrid')
                .eq(0)
                .serializeObject();
    }

    if (typeof QueryString.datini !== "undefined" && typeof QueryString.datfim !== "undefined") {
        intervaloData(
                JSON.parse(retorno.intervaloData).inicial,
                JSON.parse(retorno.intervaloData).final,
                'Selecionar intervalo de data'
                );
    }

    if (typeof retorno['funcionarios'] !== "undefined") {
        var newFuncionarios = [];
        $.each($('[name=funcionarios]').serializeArray(), function (index, funcionario) {
            newFuncionarios.push(funcionario['value']);
        });
        retorno['funcionarios'] = newFuncionarios;
    }

    if (typeof retorno['status'] !== "undefined") {
        var newStatus = [];
        $.each($('.FiltroGrid [name=status]').serializeArray(), function (index, status) {
            newStatus.push(status['value']);
        });
        retorno['status'] = newStatus;
    }

    return retorno;
}
/**
 * Variavel global que pega os parametros da URL como objeto
 * @type Function|@exp;objetoStringParaObjeto@pro;query_string
 * @return {serialize|array} Parametros da url
 */
var QueryString = function () {
    var query = window.location.search.substring(1);
    return objetoStringParaObjeto(query);
}();

/**
 * tranforma variavel com formato de url em objeto
 * @param {string} string Objeto em formato de string
 * @returns {Object}
 */
function objetoStringParaObjeto(string) {
    var query_string = {};
    var vars = string.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [query_string[pair[0]], decodeURIComponent(pair[1])];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}

var Loanding = function () {

    this.mostra = function (elemento, mensagem) {
        elemento = (typeof elemento !== "undefined") ? elemento : 'html';
        mensagem = (typeof mensagem !== "undefined") ? mensagem : 'Aguarde ...';

        if (elemento == "html") {
            $('.navbar-fixed-top').css('z-index', '999');
        } else {
            $('.navbar-fixed-top').css('z-index', '1040');
        }

        Metronic.blockUI({
            target: elemento,
            message: mensagem,
            zIndex: 9995,
            boxed: true
        });
    };
    this.esconde = function (elemento) {
        elemento = (typeof elemento !== "undefined") ? elemento : 'html';
        setTimeout(function () {
            Metronic.unblockUI(elemento);
        }, 250);
    };

    /**
     * Verifica resolução compativel com aparelhos mobile
     * @author Marco Junior
     * @return {booleam}
     */
    this.isMobile = function (tip) {

        var tamanho = {
            pequeno: 376,
            medio: 769,
            grande: 1025,
        };

        var resolucao = window.innerWidth;
        return resolucao < tamanho[tip];
    };

    /**
     * @param {Object} elemento
     */
    this.loadbyDiv = function (elemento) {
        if (typeof elemento !== 'object') {
            return;
        }

        this.hideLoadbyDiv(elemento);

        elemento.each(function (index, elmt) {

            var $load = $('<div>', {
                class: 'load',
                css: {
                    "position": "absolute",
                    "left": "0",
                    "right": "0",
                    "width": "100%",
                    "height": "100%",
                    "border-radius": $(elmt).find('.dashboard-stat2').css('border-radius'),
                    "max-width": $(elmt).width() + "px",
                    "max-height": $(elmt).height() + "px",
                    "margin-left": "auto",
                    "margin-right": "auto",
                    "display": "block",
                    "background-color": "#FFFFFF",
                    "opacity": "0.9",
                    "z-index": "9999999",
                },
//                    "width": $(elmt).width() + "px",
//                    "height": $(elmt).height() + "px",
                html: $('<img>', {
                    'src': 'http://imagens.csplay.com.br/spin.gif',
                    css: {
                        "position": 'absolute',
                        "margin-left": 'auto',
                        "margin-right": 'auto',
                        "margin-top": 'auto',
                        "margin-bottom": 'auto',
                        "left": '0',
                        "right": '0',
                        "bottom": '0',
                        "top": '0',
                        "max-width": '150px',
                    }
                })
            });

            $(elmt).prepend($load);
        });
    };

    this.hideLoadbyDiv = function (elemento) {
        if (typeof elemento !== 'object') {
            return;
        }

        elemento.children('.load').remove();
    };


};

var load = new Loanding();

function sair() {
    document.cookie = 'login=; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;';
    location.reload();
}
