function initDatePicker() {
    $('.portlet-title .actions [name=data]').datepicker({
        format: "dd/mm/yyyy",
        language: "pt-BR",
        orientation: "top right"
    }).on('changeDate', function () {
        filtrar();
    });
}
//
$(function () {
    initDatePicker();
});