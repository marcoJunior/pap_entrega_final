/**
 * @author Janley Santos Soares <janley@maxscalla.com.br>
 */

$(function () {
    $('#selectLoja').multiselect({
        includeSelectAllOption: true,
        nonSelectedText: 'Lojas',
        checkboxName: 'selectedLojas',
    });
});