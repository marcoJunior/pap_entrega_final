/**
 * @author Marco Junior <junior@maxscalla.com.br>
 */

var chart;

var Grafico = new function () {
    this.BarChart = function (config) {
        chart = new google.charts.Bar(document.getElementById(config.local));
        chart.draw(
            google.visualization.arrayToDataTable(config.data),
            google.charts.Bar.convertOptions(config.options)
        );
        google.visualization.events.addListener(chart, 'select', selectBarChart);
    };
    this.PieChart = function (config) {
        chart = new google.visualization.PieChart(document.getElementById(config.local));
        chart.draw(
            new google.visualization.DataView(google.visualization.arrayToDataTable(config.data)),
            config.options
        );
        google.visualization.events.addListener(chart, 'select', selectPiechart);
    };
    this.LineChart = function (config) {

        data = new google.visualization.DataTable();

        data.addColumn('string', 'Descrição');

        $.each(config.colunas, function (i, row) {
            data.addColumn('number', i);
        });

        data.addRows(config.data);

        /*
        $.each(config.colunas, function (n, valor) {
            data.addColumn('number', valor);
        });


        var linha = [];

        $.each(config.data, function (n, valor) {
            data.addColumn('number', n);
            $.each(valor, function (i, linha) {
                if(typeof linha[linha.descricao] == "undefined"){
                    linha[linha.descricao] = {};
                }
                linha[linha.descricao].push([linha.descricao,linha.valor]);
            });
        });

        data.addRows(linha);

        data.addRows([
            ['01/01', 37.8],
            ['02/01', 30.9],
            ['03/01', 25.4],
            ['04/01', 11.7],
            ['05/01', 11.9],
            ['06/01', 8.8],
            ['07/01', 7.6],
            ['08/01', 12.3],
            ['09/01', 16.9],
            ['10/01', 12.8],
            ['11/01', 5.3],
            ['12/01', 6.6],
            ['13/01', 4.8],
            ['14/01', 4.2],
            ['15/01', 15.2],
        ]);
*/

        chart = new google.charts.Line(document.getElementById(config.local));
        chart.draw(data, google.charts.Line.convertOptions(config.options));

    };
};

function selectPiechart() {
    console.log('Grafico de pizza');
}

function selectBarChart() {
    console.log('Grafico de colunas');
}