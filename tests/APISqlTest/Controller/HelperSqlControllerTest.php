<?php

namespace APISqlTest\Controller;

use Zend\ServiceManager\ServiceManager;
use APISql\Service\HelperSql;
use Zend\Session\Container;
use InvalidArgumentException;

class HelperSqlControllerTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var helperSql
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    private $helperSql;

    /**
     * {@inheritDoc}
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    protected function setUp() {
        $this->helperSql = new HelperSql();
    }

    /**
     * @test
     * 
     * Teste para garantir o retorno 0 se por acaso o parametro 
     * for coisa que não seja um objeto, passado um inteiro retornar o mesmo.
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    public function converteObjectSqlToArrayReturnZero() {
        $this->assertEquals(0, $this->helperSql->converteObjectSqlToArray(array()));
        $this->assertEquals(0, $this->helperSql->converteObjectSqlToArray(null));
        $this->assertEquals(0, $this->helperSql->converteObjectSqlToArray(''));
        $this->assertEquals(0, $this->helperSql->converteObjectSqlToArray(false));
        $this->assertEquals(0, $this->helperSql->converteObjectSqlToArray(0));

        $this->assertEquals(10, $this->helperSql->converteObjectSqlToArray(10));
    }

    /**
     * @test
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    public function createWhereInReturnString() {
        $this->assertEquals('2,1', $this->helperSql->createWhereIn(array(2, 1)));
    }

    /**
     * @test
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestNotColumn() {
        $session = new Container();
        $session->typeLogin = 'admin';

        $this->setExpectedException(\InvalidArgumentException::class);


        $this->helperSql->clausesByRepOrNot(array(2, 1), '', '');
    }

    /**
     * @test
     * @author  Janley Santos <janley@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestTypeLoginNull() {
        $session = new Container();

        $this->setExpectedException(\InvalidArgumentException::class);

        $session->typeLogin = '';
        $this->helperSql->clausesByRepOrNot(array(2, 1), '', '');
    }

    /**
     * @test
     * @return string Regra para aplicar campo do representante na query
     * fazendo um in com os valores do array (reps selecionados)
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestAdminAndReps() {
        $session = new Container();
        $session->typeLogin = 'admin';

        $this->assertEquals('cfirepped IN(18,36)', $this->helperSql->clausesByRepOrNot(array(18, 36), 'cfirepped', false));
    }

    /**
     * @test
     * @return string Regra para não aplicar condição na query
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestAdminAndNotReps() {
        $session = new Container();
        $session->typeLogin = 'admin';

        $this->assertEquals('', $this->helperSql->clausesByRepOrNot('', 'cfirepped', false));
    }

    /**
     * @test
     * Para quando login for de representante.
     * @return string Regra para aplicar campo do representante 
     * com valor do $session-> cus na sessão
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestIsRepresentante() {
        $session = new Container();
        $session->typeLogin = 'representante';
        $session->cus = '36';

        $this->assertEquals('cfirepped = 36', $this->helperSql->clausesByRepOrNot('', 'cfirepped', false));
    }

    /**
     * @test
     * @return string Regra para aplicar campo do representante 
     * com valor do $session-> cus na sessão
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestIsFuncionario() {
        $session = new Container();
        $session->typeLogin = 'funcionario';
        $session->cus = '1';

        $this->assertEquals('cfirepped = 1', $this->helperSql->clausesByRepOrNot('', 'cfirepped', false));
    }

    /**
     * @test
     * @return string Regra para aplicar campo do representante 
     * com valor do $session-> cus na sessão
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function clausesByRepOrNotTestNotLastParameter() {
        $session = new Container();
        $session->typeLogin = 'admin';
        $session->cus = '1';

        $this->assertEquals('cfirepped IN(18,36)', $this->helperSql->clausesByRepOrNot(array(18, 36), 'cfirepped'));
    }

}
