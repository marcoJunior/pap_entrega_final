<?php

namespace CadastroTest\Controller;

//use Zend\ServiceManager\ServiceManager;
use Cadastro\Controller\IndexController as CadastroController;

//use Zend\Session\Container;
//use InvalidArgumentException;

class CadastroTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var helperSql
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    private $cadastro;

    /**
     * {@inheritDoc}
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    protected function setUp()
    {
        $this->cadastro = new CadastroController();
    }

    /**
     * 
     * Teste para verificar chave de acao
     * @author  Leandro Machado <leandro@maxscalla.com.br>
     */
    public function testeSeChaveAcaoExisteParaAcessarMetodo()
    {
        $this->assertEquals(false, $this->cadastro->ServicosEmpresaAction(['post' => [0 => 0]]));
    }

}
