/* eslint-disable import/no-extraneous-dependencies */

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require('path');

const TARGET = process.env.npm_lifecycle_event;
process.env.BABEL_ENV = TARGET;
const debug = process.env.NODE_ENV !== 'production';
const extractText = new ExtractTextPlugin('dist/css/[name].css', {allChunks: true});

module.exports = {
    context: path.resolve(__dirname, 'public'),
    devtool: debug ? 'eval' : 'source-map',
    entry: {
        admin: [
            './metronic/sass/global/components/_customizacao-tema.scss',
        ],
        login: ['./app/login'],
        usuarios: ['./app/usuarios'],
    },
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'dist/[name].min.js',
    },
    module: {
        loaders: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
            },
            {
                test: /\.(css|scss|sass)$/,
                loader: extractText.extract('css?sourceMap!sass?sourceMap&includePaths[]=./node_modules'),
            },
        ],
    },
    plugins: debug ? [
        extractText,
    ] : [
        extractText,
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({
            mangle: false,
            compress: {warnings: false},
        }),
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'public'),
        stats: 'error-only',
    },
    resolve: {
        extensions: ['', '.js', 'index.js', 'index.jsx', '.json', '.jsx', '.scss'],
    },
    externals: {
        jquery: 'jQuery',
    },
};
